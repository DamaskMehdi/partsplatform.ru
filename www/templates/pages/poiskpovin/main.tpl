<h1>{:$h1_title:}</h1>

<br><br><br>

{:if !empty($message):}
<div class="l-form">
	{:if $message == 'no_result':}
	<span style="color: red;font-size: 16px;">Ничего не найдено по Вашему запросу, попробуйте ввести данные еще раз.</span>
	{:/if:}
</div>
<br><br><br>
{:/if:}

<div class="l-form">
	<form action="/poiskpovin" method="get">
		<input type="hidden" name="ft" value="findByVIN">
		<span class="l-formtitle">Поиск автомобиля по VIN:</span> 
		<br>
		<input type="text" name="vin" value="" style="width: 215px;">
		<input type="submit" value="Искать">
		<br>
		<small>Введите VIN кузова автомобиля, например WAUBH54B11N111054<br> <a href="#" class="open-example-vin">Что такое VIN номер кузова?</a></small>
		<br>
		<div class="example-vin">
			<img src="/images/engine_code_example2.jpg" width="400" />
		</div>
	</form>
	<br><br>
	<form action="/poiskpovin" method="get">
		<input type="hidden" name="ft" value="findByFrame">
		<span class="l-formtitle">Поиск автомобиля по кузову:</span>
		<br>
		<input id="frame" name="frame" size="17" value="" width="90" type="text" style="width: 100px;"> 
		- 
		<input id="frameNo" name="frameNo" size="17" value="" width="120" type="text" style="width: 100px;">
		<input type="submit" value="Искать">
		<br>
		<small>Введите код и номер кузова автомобиля, например XZU423-0001026<br> <a href="#" class="open-example-engine-code">Что такое Frame код и номер кузова?</a></small>
		<br>
		<div class="example-code-engine">
			<img src="/images/engine_code_example.jpg" width="400" />
		</div>
	</form>
	<br><br>
	<form action="/poiskpovin" method="get">
		<input type="hidden" name="ft" value="findByArticle">
		<span class="l-formtitle">Поиск запчасти по артикулу:</span> 
		<br>
		<input type="text" name="oem" value="" style="width: 215px;">
		<input type="submit" value="Искать">
	</form>
	<br><br>
	
	{:if !empty($brands):}
	<span class="l-formtitle">Поиск автомобиля по марке:</span>
	
	<table class="l-brand-table">
		<tr>
		{:foreach from=$brands item=column:}
			<td valign="top">
				{:foreach from=$column key=letter item=letter_brands:}
				<table style="margin-bottom: 15px;">
					<tr>
						<td style="vertical-align: top;width: 30px;color: #b60000;font-weight: bold;">{:$letter:}</td>
						<td style="vertical-align: top;width: 275px;">
						{:foreach from=$letter_brands item=brand:}
						<div><a href="{:$brand.link:}">{:$brand.name:}</a></div>
						{:/foreach:}
						</td>
					</tr>
				</table>
				{:/foreach:}
			</td>
		{:/foreach:}
		</tr>
	</table>
	{:/if:}
	
</div>


<style>
.l-form {
	margin: 0 30px;
}

.l-form input[type=text] {
	border: 1px solid #ccc;
	font-size: 20px;
}

.l-formtitle {
	font-size: 18px;
}

.l-brand-table {
	font-size: 22px;
	margin: 20px 0 40px 0;
}

.l-brand-table div {
	margin-bottom: 15px;
}

.example-code-engine, .example-vin {
	margin: 20px 10px;
	display: none;
}

</style>

<script>
$(function() {

	$('.open-example-engine-code').click(function() {
		$('.example-code-engine').toggle();
		return false;
	});
	
	$('.open-example-vin').click(function() {
		
		//$('<div id="fancybox-loading"><div><img src="/images/engine_code_example2.jpg" width="400" /></div></div>').appendTo("body");
	
		$('.example-vin').toggle();
		return false;
	});

});
</script>
