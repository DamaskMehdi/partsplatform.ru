<div class="l-form" style="margin: 40px 40px 300px 0px;">
{:if !empty($vehicles):}

<h1>Список модификаций</h1>

<link type="text/css" rel="stylesheet" href="/css/blue/style.css">
<script type="text/javascript" src="/js/jquery.metadata.js"></script>
<script type="text/javascript" src="/js/jquery.tablesorter.min.js"></script>
<div class="tab_content">
	<table align="center" style="width: auto;" cellpadding="0" cellspacing="0"  class="tablesorter parts">
		<thead> 
			<tr>
				<th class="{sorter: false}">Модификация</th>
				<th class="{sorter: false}">КП</th>
				<th class="{sorter: false}">Двигатель</th>
				<th class="{sorter: false}">Модель</th>
				<th class="{sorter: false}">Выпущено</th>
				<th class="{sorter: false}">Рынок</th>
			</tr>
		</thead>
		<tbody>
			{:foreach $vehicles as $vehicle:}
			<tr>
				<td><a class="link" href="{:$vehicle.link:}">{:$vehicle.name:}</a></td>
				<td><a class="link" href="{:$vehicle.link:}">{:if !empty($vehicle.transmission):}{:$vehicle.transmission:}{:else:}нет данных{:/if:}</a></td>
				<td><a class="link" href="{:$vehicle.link:}">{:if !empty($vehicle.engine):}{:$vehicle.engine:}{:else:}нет данных{:/if:}</a></td>
				<td><a class="link" href="{:$vehicle.link:}">{:if !empty($vehicle.model):}{:$vehicle.model:}{:else:}нет данных{:/if:}</a></td>
				<td><a class="link" href="{:$vehicle.link:}">{:if !empty($vehicle.manufactured):}{:$vehicle.manufactured:}{:else:}нет данных{:/if:}</a></td>
				<td><a class="link" href="{:$vehicle.link:}">{:if !empty($vehicle.market):}{:$vehicle.market:}{:else:}нет данных{:/if:}</a></td>
			</tr>
			{:/foreach:}
		</tbody>
	</table>
</div>
{:/if:}
</div>

<script> 
$(function() {
	//$$(".chzn-select").chosen(); 
});
</script>

<style>
	.parts td { font-size: 17px;; }
</style>

