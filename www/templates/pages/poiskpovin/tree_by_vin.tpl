
{:if !empty($car_info):}
<h1>{:$h1_title:}</h1>
<script type="text/javascript" src="{:$SITE_URL:}/js/jquery.plugin.menuTree.js" charset="utf-8"></script>
<br><br><br>

<div class="mtable">
	<div class="car-specification">
		<div>Найден автомобиль {:$car_info['attributes']->row['brand']:}, {:$car_info['attributes']->row['name']:}</div>
		<a href="#" id="show_car_specification">Смотреть спецификацию автомобиля</a>
		<table class="car-specification-table" style="display: none;">
		{:foreach from=$car_info['attributes']->row->attribute item=value:}
			<tr>
				<td><b>{:$value['name']:}</b></td>
				<td>{:$value['value']:}</td>
			</tr>
		{:/foreach:}
		</table>
	</div>
	<br>
	<br>
	
	<table>
		<tr>
			<td valign="top">
				<div style="display: none;">
					<span>Вид:</span>
					<select style="margin-left: 30xp;">
						<option selected>дерево</option>
						<option>оригинальный каталог</option>
					</select>
				</div>
				<!--h2>Дерево запчастей</h2-->
				<div class="tree_groups">
					{:$tree_html:}
				</div>
			</td>
			<td valign="top">
				<div class="group_articles">
				</div>
			</td>
		</tr>
	</table>
	
	<table>
{:*foreach from=$car_info item=value key=name:}
	{:if $name != 'attributes':}
		<tr>
			<td><b>{:$name:}</b></td>
			<td>{:$value:}</td>
		</tr>
	{:else:}
		{:foreach from=$value item=item:}
			<tr>
				<td><b>{:$item['name']:}</b></td>
				<td>{:$item['value']:}</td>
			</tr>
		{:/foreach:}
	{:/if:}
{:/foreach*:}
</table>

</div>


<style>

.car-specification {
	font-size: 14px;
	padding-bottom: 15px;
	border-bottom: 1px solid #ccc;
}

.car-specification div {
	font-size: 20px;
}

.car-specification-table {
	margin-top: 20px;
	color: #555;
	font-size: 12px;
	//border-bottom: 1px solid #aaa;
}

.current-leaf {
	color: #e85000;	
}

.car-specification-table td {
	padding: 0 15px 10px 0;
}

.tree_groups {
	width: 450px;
}

.group_articles {
	//float: left;
}
</style>

{:else:}

<div class="mtable">
	<br><br>
	<h4>Ничего не найдено по введенному VIN-коду, попробуйте еще раз</h4>
	
	<form action="/poiskpovin" method="get">
		<input type="hidden" name="ft" value="findByVIN">
		Искать по VIN: <input type="text" name="vin" value="">
		<input type="submit" value="Искать">
	</form>
</div>


{:/if:}

<script>
$(function(){
	$('#show_car_specification').click(function() {
		if ($('.car-specification-table').css('display') !== 'none') {
			$('.car-specification-table').hide();
		} else {
			$('.car-specification-table').show();
		}
		
		return false;
	});
	
	
	$('.tree_groups').on('click', 'a', function() {
		return getTree($(this));
	});
	
	$('#qgTree a').click(function() {
		$('#qgTree .current-leaf').removeClass('current-leaf');
		$(this).addClass('current-leaf');
		
		var parent = $(this).parent().parent();
		if (!parent.hasClass('qgExpandLeaf')) {
			if (parent.hasClass('qgExpandOpen')) {
				parent.removeClass('qgExpandOpen').addClass('qgExpandClosed');
			} else {
				parent.removeClass('qgExpandClosed').addClass('qgExpandOpen');
			}
		}
	
		return getTree($(this));
	});
	
	function getTree(obj) {
		$('.group_articles').empty();
		$('<div id="fancybox-loading"><div></div></div>').appendTo("body");
		
		$.ajax({
		  url: obj.prop('href')
		}).done(function(data) {
			$("#fancybox-loading").remove();
			$('.group_articles').html(data);
		  
			$('.guayaquil_zoom').colorbox({
				href: function () {
						var url = $(this).attr('full');
						return url;
					},
				photo:true,
				rel: "img_group",
				opacity: 0.3,
				title : function () {
					var title = $(this).attr('title');
					var url = $(this).attr('link');
					return '<a href="' + url + '">' + title + '</a>';
				},
				current: 'Рис. {current} из {total}',
				maxWidth : '98%',
				maxHeight : '98%'
				}
			);
		});
	
		return false;
	};
	

});
</script>