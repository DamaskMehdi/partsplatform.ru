{:if !empty($breadcrumbs):}
	<div class="path">
		<a href="/">Главная</a>
		&rarr; 
		<a href="/poiskpovin">Поиск запчастей</a>
		{:foreach $breadcrumbs as $i:}
			{:if $i@last:} &rarr; <span> {:$i.title:}</span>
				{:else:} &rarr; <a href="{:$i.url:}"> {:$i.title:}</a>
			{:/if:}
		{:/foreach:} 
	</div>
	<div class="clear"></div>
{:/if:}