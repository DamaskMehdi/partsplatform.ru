<script src="/lib/ppv/guayaquillib/render/jquery.tooltip.js"></script>
<script src="/lib/ppv/guayaquillib/render/details/detailslist.js"></script>

<div class="mtable">
{:if !empty($parts):}
	
	{:foreach from=$parts.categories item=category:}
	<div class="gdCategory">
		<h4>Категория: <a href="{:$category.link:}">{:$category.name:}</a></h4>
		{:foreach from=$category.units item=unit:}
			{:*$unit.img*:}
			<table class="gdUnit">
				<tr>
					<td class="gdImageCol">
						<div class="guayaquil_unit_icons">
							<!--div class="guayaquil_zoom" link="{:$unit.link:}" full="{:$unit.imageurl:}" title="{:$unit.code:}: {:$unit.name:}"><img src="{:$unit.zoom_image:}"></div-->
						</div>
						<a href="{:$unit.link:}"><div class="gdImage{:$unit.gdImage:}" style="width:{:$unit.size:}px; height:{:$unit.size:}px;">{:$unit.img:}</div></a>
						<a href="{:$unit.link:}"><b>{:$unit.code:}:</b> {:$unit.name:}</a>
					</td>
					<td class="gdDetailCol" valign=top>
						<table class="guayaquil_table" id="g_DetailTable0">
						{:foreach from=$unit.details item=detail:}
							<tr>
								<td style="width: 120px;">
								<a href="{:$detail.link:}">{:$detail.oem:}</a>
								</td>
								<td style="font-size: 12px;">
								{:$detail.name:}
								</td>
								<td style="width: 110px;">
									<a href="{:$detail.link:}">показать цены</a>
									<div style="display: none;">
									{:foreach from=$detail.attributes item=attr:}
										<b>{:$attr.name:}:</b> {:$attr.value:}<br>
									{:/foreach:}
									</div>
								</td>
							</tr>
						{:/foreach:}
						</table>
					</td>
				</tr>
			</table>
		{:/foreach:}
	</div>
	{:/foreach:}

<style>

.gdUnit {
	border-left: 1px solid #aaa;
}

.gdImageCol {
	padding: 25px;
	width: 180px;
	text-align: center;
	vertical-align: top;
}

.guayaquil_table {
	font-size: 12px;
	width: 100%;
}

.guayaquil_table td {
	vertical-align: top;
	padding: 10px 20px 5px 10px !important;
	border-bottom: 1px solid #aaa;
}
</style>

{:else:}

<h4>Ничего не найдено, воспользуйтесь <a href="{:$unfound_link:}">иллюстрированным каталогом</a> для поиска требуемой детали </h4>

{:/if:}
</div>
	
<script type=\"text/javascript\">
	var opennedimage = '/lib/ppv/guayaquillib/render/details/images/openned.gif';
	var closedimage = '/lib/ppv/guayaquillib/render/details/images/closed.gif';
	$(document).ready(function($){
		$('td.g_rowdatahint').tooltip({track: true, delay: 0, showURL: false, fade: 250, positionLeft: true, bodyHandler: g_getHint});
		$('img.g_addtocart').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return 'Добавить деталь в корзину'; } });
		$('td[name=c_toggle] img').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return 'Показать/скрыть информацию о дубликатах'; } });
		$('img.c_rfull').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '<h3>Тип заменяемости</h3>Точный дубликат'; } });
		$('img.c_rforw').tooltip({track: true, delay: 0, showURL: false, fade: 250,	bodyHandler: function() { return '<h3>Тип заменяемости</h3>Замена возможна указанным дубликатом возможна, но обратная замена не гарантируется'; } });
		$('img.c_rbackw').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '<h3>Тип заменяемости</h3>Замена НЕ возможна'; } });
	});
	</script>
	
{:*$parts_html*:}