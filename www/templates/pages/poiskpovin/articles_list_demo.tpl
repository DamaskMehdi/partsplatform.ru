<h1>{:$h1_title:}</h1>

<br><br><br>

<link type="text/css" rel="stylesheet" href="css/blue/style.css">
<script type="text/javascript" src="js/jquery.metadata.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<div class="tab_content" style="margin-bottom: 100px;">
	<table align="center" width="100%" cellpadding="0" cellspacing="0"  class="tablesorter parts">
		<thead> 
			<tr>
				<th width="20%" class="{sorter: false}">Бренд</th>
				<th width="50" class="{sorter: false}">Фото</th>
				<th class="{sorter: false}" width="100">Артикул</th>
				<th class="{sorter: false}">Описание</th>
				<th width="200">Ценa, руб.</th>
				<th width="200">Продавец</th>
			</tr>
		</thead>
		<tbody>
			{:foreach $details_with_prices as $item:}
			<tr>
				<td rowspan="{:$item.prices|count:}">{:$item.manufacturer:}</td>
				<td align="center" rowspan="{:$item.prices|count:}">
					{:if !empty($item.image):}
						<a class="fancybox-print" href="https://atr.by{:$item.image_full:}" title="{:$item.oem:}"><img src="https://atr.by{:$item.image:}" alt="" height="50" align="center"></a>
					{:else:}
						<img src="images/fotopic.png" alt="" data-article="{:$item.oem:}" data-brand="{:$item.manufacturer:}" align="center" class="not-image">
						<a class="fancybox-print" href="#" title="PBP419"></a>
					{:/if:}
				</td>
				<td rowspan="{:$item.prices|count:}">
					<a class="link" href="{:$item.link:}">{:$item.oem:}</span>
				</td>
				<td rowspan="{:$item.prices|count:}">
					{:if !empty($item.name):}{:$item.name:}{:/if:}
				</td>
				{:if !empty($item.prices):}
					{:foreach from=$item.prices item=price name=nprice:}
					{:if !$smarty.foreach.nprice.first:}
					<tr>
					{:/if:}
						<td>
						<div>
							<span style="color: red;font-size: 24px;">{:$price.price:}</span>
							<br>
						</div>
						</td>
						<td>
							<span>магазин: {:$price.shop_name:}</span>
							<br>
							<a href="#" class="show_contacts" data-shop_id="{:$price.shop_id:}">показать контакты</a>
						</td>
					{:if !$smarty.foreach.nprice.last:}
					</tr>
					{:/if:}
					{:/foreach:}
				{:else:}
				<td>
					Нет предложений
				</td>
				<td>
					
				</td>
				{:/if:}
			</tr>
			{:/foreach:}
		</tbody>
	</table>
</div>

<script>
$(function() {

	$('.show_contacts').click(function() {
		var element = $(this);
		
		$.ajax({
			url: '/',
			type: 'get',
			dataType: 'json',
			data: {'action' : 'getContacts', 'shop_id' : $(this).data('shop_id') },
			success: function(data) {
				if (data.success) {
					element.after('<div>' + data.data.contacts + '</div>').remove(); 
					
				} else {
					alert('Нет контактов');
				}
			}
		});
		
		return false;
	});
});
</script>
