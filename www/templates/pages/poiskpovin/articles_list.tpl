<h1>{:$h1_title:}</h1>

<br><br><br>

{:if !empty($details_with_prices):}
<link type="text/css" rel="stylesheet" href="css/blue/style.css">
<script type="text/javascript" src="js/jquery.metadata.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<div class="tab_content">
	<table align="center" width="100%" cellpadding="0" cellspacing="0"  class="tablesorter parts">
		<thead> 
			<tr>
				<th width="20%" class="{sorter: false}">Бренд</th>
				<th width="50" class="{sorter: false}">Фото</th>
				<th class="{sorter: false}" width="100">Артикул</th>
				<th class="{sorter: false}">Описание</th>
				<th width="200">Цена, руб.</th>
			</tr>
		</thead>
		<tbody>
			{:foreach $details_with_prices as $item:}
			<tr>
				<td colspan="7" style="background:#636363;font-weight:bold;color:#fff;padding:10px;">Полное или частичное соответствие</td>
			</tr>
			<tr>
				<td>{:$item.manufacturer:}</td>
				<td align="center">
					{:if !empty($item.image):}
						<a class="fancybox-print" href="https://atr.by{:$item.image_full:}" title="{:$item.oem:}"><img src="https://atr.by{:$item.image:}" alt="" height="50" align="center"></a>
					{:else:}
						<img src="images/fotopic.png" alt="" data-article="{:$item.oem:}" data-brand="{:$item.manufacturer:}" align="center" class="not-image">
						<a class="fancybox-print" href="#" title="PBP419"></a>
					{:/if:}
				</td>
				<td>
					<a class="link" href="{:$item.link:}">{:$item.oem:}</span>
				</td>
				<td>
					{:if $item.name:}{:$item.name:}{:/if:}
					<br><em class="small"><a href="{:$item.link:}" target="_blank">Подробная информация и отзывы</a></em>
				</td>
				<td>
					{:if empty($item.price_min) || $item.price_max == 999999999:}<a href="" class="tipped_ajax link" onclick="return false" data-tipped="/" data-querystring="part={:$item.oem:}&brand={:$item.manufacturer:}">Показать цену</a>{:else:}
						<b class="red">{:$item.price_min:} - {:$item.price_max:}</b><br>
						<a class="label label-warning nobr" style="font-size: 12px;" href="{:$item.link:}">
						{:if $item.sellers > 1:}
							Сравнить предложения
						{:else:}
							Показать предложение
						{:/if:}
						</a>
						<br>
						<small>продавцов: {:$item.sellers:}</small>
					{:/if:}
				</td>
			</tr>
				{:if !empty($item.replacement):}
				<tr>
					<td colspan="8" style="background:#636363;font-weight:bold;color:#fff;padding:10px;">Аналоги искомой запчасти</td>
				</tr>
				{:foreach $item.replacement as $repl:}
				<tr>
					<td>{:$repl.manufacturer:}</td>
					<td align="center">
						{:if !empty($repl.image):}
							<a class="fancybox-print" href="https://atr.by{:$repl.image_full:}" title="{:$repl.oem:}"><img src="https://atr.by{:$repl.image:}" alt="" height="50" align="center"></a>
						{:else:}
							<img src="images/fotopic.png" alt="" data-article="{:$repl.oem:}" data-brand="{:$repl.manufacturer:}" align="center" class="not-image">
							<a class="fancybox-print" href="#" title="PBP419"></a>
						{:/if:}
					</td>
					<td>
						<a class="link" href="{:$repl.link:}">{:$repl.oem:}</span>
					</td>
					<td>
						{:if $repl.name:}{:$repl.name:}{:/if:}
						<br><em class="small"><a href="{:$repl.link:}" target="_blank">Подробная информация и отзывы</a></em>
					</td>
					<td>
						{:if empty($repl.price_min) || $repl.price_max == 999999999:}<a href="" class="tipped_ajax link" onclick="return false" data-tipped="/" data-querystring="part={:$repl.oem:}&brand={:$repl.manufacturer:}">Показать цену</a>{:else:}
							<b class="red">{:$repl.price_min:} - {:$repl.price_max:}</b><br>
							<a class="label label-warning nobr" style="font-size: 12px;" href="{:$repl.link:}">
							{:if $repl.sellers > 1:}
								Сравнить предложения
							{:else:}
								Показать предложение
							{:/if:}
							</a>
							<br>
							<small>продавцов: {:$repl.sellers:}</small>
						{:/if:}
					</td>
				</tr>
				{:/foreach:}
				{:/if:}
			{:/foreach:}
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function($){
		/*$("table.tablesorter.parts").tablesorter({widthFixed:true,widgets:['zebra']});
		$("table.tablesorter.analogs").tablesorter({widthFixed:true,widgets:['zebra']});
		$('.tipped_ajax').each(function() {Tipped.create(this, {skin: "light",ajax: { data: $(this).data('querystring')}});});*/
		
		$('.not-image').click(function() {
			var img = $(this);

			$.ajax({
				url: '/', //'/test22.php',
				type:'POST',
				data:{ gifg: $(this).data('article'), brand: $(this).data('brand') },
				dataType: 'json',
				success: function(data) {
					if (data.success){
						img.prop('src', data.image);
					}
				},
				error: function(data) {
					img.parent().html('нет изображения');
				}
			});
		});
	});
</script>
{:else:}
	<br><br>
	<div class="table border mtable messages">
		<p>Не найдено совпадение по артикулу '{:$oem:}'</p>
	</div>
{:/if:}

<style>
.l-form {
	margin: 0 30px;
}

.article-list-table {
	border-collapse: collapse;
	font-size: 16px;
	margin: 20px 0 60px 40px ;
}


.article-list-table td, th {
	border-bottom: 1px solid #ddd;
	padding: 7px 175px 7px 15px;
}

.basic-detail {
	background: #eee;
}

.replacement-name {
	padding: 3px 5px 3px 30px !important;
}

.price {
	color: red;
}

</style>