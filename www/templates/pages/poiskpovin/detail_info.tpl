<div class="description">
{:if !empty($detail):}
	<table width="100%">
	<tr>
		<td valign="top" width="220">
			<span class="detail-img-big"  style="height: auto;">
			{:if !empty($detail->FindDetails->detail->images):}
				{:foreach from=$detail->FindDetails->detail->images item=img:}
				<a href="#" class="fancybox"><img width="200" src="{:$img:}"></a>
				{:/foreach:}
			{:elseif !empty($detail->FindDetails->detail.tecdoc_image):}
				<a href="{:$detail->FindDetails->detail.tecdoc_image_big:}" class="fancybox">
					<img width="200" src="{:$detail->FindDetails->detail.tecdoc_image:}">
				</a>
			{:else:}
				<img src="/images/no-image.png" alt="Нет фото" title="Нет фото">
			{:/if:}
			</span>
		</td>
		<td valign="top">
			<table width="90%" cellspacing="0" border="0" class="tbody__table">
				<tr><td>Производитель:</td><td class="b">{:$detail->FindDetails->detail.manufacturer:}</td></tr>
				<tr><td>Артикул:</td><td class="b">{:$detail->FindDetails->detail.oem:}</td></tr>
				<!--tr><td>Страна:</td><td class="b"><img src="http://pix.atr-catalog.by/countries/DE.png" align="left" alt="Германия" class="coupic"> Германия</td></tr-->
				<tr><td>Описание:</td><td>{:$detail->FindDetails->detail.name:}</td></tr>
				{:if $detail->FindDetails->detail.weight:}<tr><td>Вес:</td><td>{:$detail->FindDetails->detail.weight:} кг</td></tr>{:/if:}
			</table>
		</td>
		<td valign="top" width="200">
			{:if !empty($detail->FindDetails->detail.prices_range):}
				<b class="red priceformat font-30">{:$detail->FindDetails->detail.prices_range:} р.</b>
			{:else:}
				Нет предложений
			{:/if:}
		</td>
	</tr>
	</table>
	
	<dl class="tabs2" style="margin-top: 20px;">
        <dt class="selected"><i class="fa fa-tags"></i> &nbsp; <span>Сравнить цены</span></dt>
        <dd class="selected">
            <div class="tab-content table price_table" style="margin:0;">
                {:if !empty($shops):}
                <table cellpadding="6" cellspacing="0">
                    <tr>
                        <th class="th" width="15%">Цена, руб</th>
                        <th width="10%"></th>
						<th class="th" width="35%">Продавец</th>
                        <th class="th" width="25%">Контакты</th>
                        <th class="th" width="15%">&nbsp;</th>
                    </tr>
                    {:foreach $shops as $shop:}
                    <tr data-article_id="{:if !empty($detail->FindDetails->detail.tecdoc_art_id):}{:$detail->FindDetails->detail.tecdoc_art_id:}{:/if:}" data-company_id="{:$shop.shop_id:}">
                        <td>
                            <span class="shop-price">{:$shop.PRICE_RUB|priceformat:} <small>руб.</small></span>
                            <p><small>{:'d.m.Y H:i:s'|date:($shop.IMPORT_DATE|strtotime):}</small></p>
                        </td>
						<td align="center"><img class="show_company_zakaz cursor_hand" data-price_id="{:$shop.price_id:}" src="/images/cart_2.png"></td>
                        <td class="show_company_info">
                            <div class="">
                                <span class="label label-success">В наличии</span> &nbsp;&nbsp;
                                <strong>
									{:$shop.shop_name:}
								</strong>
                                <p>Бесплатная доставка по городу. Скидки для постоянных покупателей.</p>
                            </div>
                        </td>
                        <td valign="top" style="font-size: 18px; font-weight: bold;">
							<button class="open-contacts btn btn-primary" data-shop-id="{:$shop.shop_id:}">Контакты</button>
						    {:*if !empty($shop.email):}<p class="blue"><i class="fa fa-envelope"></i> {:$shop.email:}</p>{:/if:}
		                    {:if !empty($shop.PHONES):}{:$shop.PHONES|replace:',':'<br/>':}{:/if*:}
                        </td>
                        <td>
                            <div class="shop_rating">
								{:if !empty($shop.shop_comments_num):}
									<a class="show_company_review stars" href="/?{:$shop.shop_name:}" title="Оценка: {:$shop.shop_avg_mark:}">
										{:for $i=1 to $shop.shop_avg_mark|round:}
											<img src="/images/star_blue_20.png" />
										{:/for:}
										{:for $j=1 to 5-$i+1:}
											<img src="/images/star_gray_20.png" />
										{:/for:}
									</a>
								{:/if:}
                                <p><a class="show_company_review" href="/?{:$shop.shop_name:}"><small class="label label-default">{:if !empty($shop.shop_comments_num):}Отзывы: {:$shop.shop_comments_num:}{:else:}Пока отзывов нет{:/if:}</small></a></p>
                            </div>
                        </td>
                    </tr>
                    {:/foreach:}
                </table>
                <div id="company_info" class="hidden">
                    <h3 id="company_name">Карточка продавца</h3>
                    <dl class="tabs2">
                        <dt class="selected"><span>Информация</span></dt>
                        <dd class="selected" id="comp_info">
                            <table width="100%">
                                <tr>
                                    <td width="45%" valign="top">
                                        <div class="firm-info">
                                        </div>
                                    </td>
                                    <td width="55%" valign="top">
                                        <div class="firm-map">
                                            <div class="firm-address"><em class="fa fa-map-marker"></em> <strong></strong></div>
                                            <div id="map" style="width:100%; height:350px;"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </dd>
                        <dt><span>Заказ</span></dt>
                        <dd id="comp_order"></dd>
                        <dt><span>Отзывы</span></dt>
                        <dd id="comp_review"></dd>
                        <dt><span>Проверки</span></dt>
                        <dd id="comp_check"></dd>
                        <dt><span>Реквизиты</span></dt>
                        <dd id="comp_requisits"></dd>
                    </dl>
                    <div id="map_popup" class="width:100%;height:240px;border:2px solid #ddd;"></div>
                </div>
                {:else:} Нет предложений продавцов {:/if:}
            </div>
        </dd>
		
		<dt><span>Характеристики</span></dt>
		<dd>
			<div class="tab-content table" style="margin:0;">
			{:if !empty($tecdoc_criterias):}
				<table width="30%" cellpadding="6" cellspacing="0">
					<tr>
						<th class="th" width="20%">Параметр</th>
						<th class="th" width="15%">Значение</th>
					</tr>
					{:foreach $tecdoc_criterias as $item:}
					<tr>
						<td>{:$item.title:}</td>
						<td style="text-align:right;">{:$item.value:}</td>
					</tr>
					{:/foreach:}
				</table>
			{:else:}
				<p class="font-18">Нет информации</p>
			{:/if:}
			</div>
		</dd>
		<dt><span>Отзывы о запчасти</span></dt>
		<dd>
			<div class="tab-content">
				<table width="100%" class="comments" cellspacing="0">
					{:if !empty($comments):}
						{:foreach $comments as $comm:}
							<tr>
								<td width="80" valign="top"><img src="{:$comm.email|gravatar:}" alt=""></td>
								<td valign="top">
									<span>{:if !empty($comm.user_name):}{:$comm.user_name:}{:else:}Noname{:/if:} <b style="width:{:math equation='(x * y )' x=15 y=$comm.mark:}px;"></b> <em>{:$comm.add_date|date_format:'%d.%m.%Y в %H:%M':}</em></span>
									<div>{:$comm.comment_text:}</div>
								</td>
							</tr>
						{:/foreach:}
					{:else:}
						<tr class="no-comment"><td colspan="2" class="font-18">На данную запчаcть еще нет отзывов</td></tr>
					{:/if:}
				</table>
				<script type="text/javascript">$(function(){$('.container').rating();});</script>
				<div class="comment-form">
					<form action="/" method="post">
						<input type="hidden" id="article_id" name="article_id" value="">
						<div>
							<b>Текст отзыва:</b>
							<textarea id="comment_text" name="comment_text"></textarea>
						</div>
						<div>
							<b>Ваша оценка:</b>
							<section class="container">
								<input type="radio" name="mark" class="rating" value="1" />
								<input type="radio" name="mark" class="rating" value="2" />
								<input type="radio" name="mark" class="rating" value="3" />
								<input type="radio" name="mark" class="rating" value="4" />
								<input type="radio" name="mark" class="rating" value="5" />
							</section><br><br>
							<button name="add-comment" id="add-comment" class="__button save">Добавить отзыв</button>
						</div>
					</form>
				</div>
			</div>
		</dd>
		<dt><span>Аналоги</span></dt>
		<dd>
			<link type="text/css" rel="stylesheet" href="css/blue/style.css">
			<script type="text/javascript" src="js/jquery.metadata.js"></script>
			<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
			<table align="center" width="100%" cellpadding="0" cellspacing="0"  class="tablesorter analogs">
			<thead> 
				<tr>
					<th width="20%">Бренд</th>
					<th width="50" class="{sorter: false}">Фото</th>
					<th class="{sorter: false}" width="100">Артикул</th>
					<th class="{sorter: false}">Описание</th>
					<th class="{sorter: false}" width="60">Наличие</th>
					<th class="{sorter: false}" width="70">Срок</th>
					<th width="100">Цена, руб.</th>
					<th width="150" class="{sorter: false}"></th>
				</tr>
			</thead>
			<tbody>
			{:foreach $detail->FindDetails->detail->replacements->replacement as $item:}
				<tr>
					<td>{:$item->detail.manufacturer:}</td>
						<td>
							
						</td>
						<td>
							<a class="link" href="/poiskpovin?ft=detailInfo&detailid={:$item->detail.detailid:}">{:$item->detail.oem:}</span>
						</td>
						<td>
							{:if $item->detail.name:}{:$item->detail.name:}{:/if:}
						</td>
						<td>
							{:if !empty($item.quantity):}<p class="green">{:$item.quantity:} шт.</p>
								{:else:} {:if !empty($item.quantity) && $item.quantity>0:}{:$item.quantity:}{:else:}<span class="icon-ok green">&nbsp;</span>{:/if:} <span class="diagram diagram_{:$item.stats_success:}"></span>
							{:/if:}
						</td>
						<td>{:if !empty($item.delivery_time):}{:$item.delivery_time:} ({:$item.delivery_time_guar:}) дн.{:/if:}</td>
						<td>
							{:if empty($item.price) || $item.price == 999999999:}<a href="" class="tipped_ajax link" onclick="return false" data-tipped="/" data-querystring="part={:$item.number:}&brand={:$item.brand:}">Узнать цену под заказ</a>{:else:}<b class="red">{:$item.price:} бел. руб.</b>{:/if:}
						</td>
						<td>
							
						</td>
				</tr>
			{:/foreach:}
			</tbody>
		</table>
		</dd>
	</dl>
	
<script type="text/javascript">
$(function () {
	$('dl.tabs2 dt').click(function () {
		$(this).siblings().removeClass('selected').end().next('dd').andSelf().addClass('selected');
	});
	
	
});
</script>
{:else:}
Запчасть не найдена
{:/if:}
</div>