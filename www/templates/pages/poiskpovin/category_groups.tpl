
<div class="categories-table" style="margin-top: 50px;">
{:if !empty($groups.groups):}

	<div class="car-specification">
		<div>Автомобиль {:$groups.car_info->row['brand']:}, {:$groups.car_info->row['name']:}</div>
		<a href="#" id="show_car_specification">Смотреть спецификацию автомобиля</a>
		<table class="car-specification-table" style="display: none;">
		{:foreach from=$groups.car_info->row->attribute item=value:}
			<tr>
				<td><b>{:$value['name']:}</b></td>
				<td>{:$value['value']:}</td>
			</tr>
		{:/foreach:}
		</table>
	</div>

	<div class="category-list">
	{:foreach from=$groups.categories->row item=category:}
		{:if $category['active'] == 1:}
		<span class="active">{:$category.name:}</span>
		{:else:}
		<span><a href="{:$category.link:}">{:$category.name:}</a></span>
		{:/if:}
	{:/foreach:}
		<div style="clear: both;"></div>
	</div>

	<div class="groups">
	{:foreach from=$groups.groups->row item=group:}
		<div>
			<a href="{:$group.link:}">
				<center class="img-block"><img src="{:$group.imageurl:}" ></center>
				<center>
					{:$group.name:}
					<br>
					<span class="code">{:$group.code:}</span>
				</center>
			</a>
		</div>
	{:/foreach:}
	</div>
	<div style="clear: both;"></div>


<style>

.car-specification {
	font-size: 14px;
	padding-bottom: 15px;
	border-bottom: 1px solid #ccc;
}

.car-specification div {
	font-size: 20px;
}

.car-specification-table {
	margin-top: 20px;
	color: #555;
	font-size: 12px;
	//border-bottom: 1px solid #aaa;
}

.car-specification-table td {
	padding: 0 15px 10px 0;
}


.categories-table {
	margin: 170px 30px;
}

.category-list {
	color: #555;
	margin: 0px 0 30px 0;
	padding: 20px 240px 0px 100px;
	//border-top: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}

.category-list span {
	display: block;
	margin-right: 30px;
	padding: 5px 0;
	font-size: 18px;
	float: left;
}

.category-list .active{
	color: red;
}


.groups a {
	text-decoration: none;
}

.groups div {
	float: left;
	width: 160px;
	height: 260px;
	margin: 15px;
	border: 1px solid #fff;
}

.groups div:hover {
	border: 1px solid #ccc;
}

.groups .code {
	color: #000;
}

.groups .img-block {
	height: 175px;
	vertical-align: center;
}

</style>

{:else:}

Нет данных

{:/if:}


<script>
$(function(){
	$('#show_car_specification').click(function() {
		if ($('.car-specification-table').css('display') !== 'none') {
			$('.car-specification-table').hide();
		} else {
			$('.car-specification-table').show();
		}
		
		return false;
	});
	
});
</script>