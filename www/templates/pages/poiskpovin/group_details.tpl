
<div style="margin: 40px;">
	<div class="unit-image">
	</div>
	{:if !empty($details.car_info):}
	<div>
		<div class="car-specification">
			<div>Автомобиль {:$details.car_info->row['brand']:}, {:$details.car_info->row['name']:}</div>
			<a href="#" id="show_car_specification">Смотреть спецификацию автомобиля</a>
			<table class="car-specification-table" style="display: none;">
			{:foreach from=$details.car_info->row->attribute item=value:}
				<tr>
					<td><b>{:$value['name']:}</b></td>
					<td>{:$value['value']:}</td>
				</tr>
			{:/foreach:}
			</table>
		</div>
	
	
		<h3>{:$details.unit_info->row.name:}</h3>
		<p class="guayaquil_unit_note">
		{:foreach from=$details.unit_info->row->attribute item=attr:}
		<b>{:$attr.name:}:</b> {:$attr.value:}<br/>
		</p>

		{:/foreach:}
		<div id="g_container" style="vertical-align:top;height:602px;width:100%;margin-top: 30px; overflow:hidden;">
			<div style="height:100%;" class="inline_block">
				<div class="guayaquil_unit_icons">
					<div class="guayaquil_zoom" full="{:$details.unit_info->row.largeimageurl:}" title="{:$details.unit_info->row.code:}: {:$details.unit_info->row.name:}"><img src="/css/images/zoom.png"></div>
				</div>
				<div id="viewport" class="inline_block" style="position:absolute; border: 1px solid #ddd; background: white; width:644px; height:600px; overflow: auto;">
				{:foreach from=$details.image_map->row item=area:}
					<div name="{:$area.code:}" class="dragger g_highlight" style="position:absolute; width:{:$area.x2-$area.x1:}px; height:{:$area.y2-$area.y1:}px; margin-top:{:$area.y1:}px; margin-left:{:$area.x1:}px; overflow:hidden;">
						<img src="/css/images/spacer.gif" width="200" height="200"/>
					</div>
				{:/foreach:}
				
					<img class="dragger" onLoad="rescaleImage(-100);" src="{:$details.unit_info->row.largeimageurl:}"/>
				</div>
			</div>
			
			<div id="viewtable" style="overflow:auto; height:100%;" class="inline_block">
				<script type="text/javascript">
				var opennedimage = '/css/images/openned.gif'; 
				var closedimage = '/css/images/closed.gif'; 
				jQuery(document).ready(function($){ 
				jQuery('td.g_rowdatahint').tooltip({track: true, delay: 0, showURL: false, fade: 250, positionLeft: true, bodyHandler: g_getHint}); 
				jQuery('img.g_addtocart').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return 'Добавить деталь в корзину'; } }); 
				jQuery('td[name=c_toggle] img').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return 'Показать/скрыть информацию о дубликатах'; } }); 
				jQuery('img.c_rfull').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '<h3>Тип заменяемости</h3>Точный дубликат'; } }); 
				jQuery('img.c_rforw').tooltip({track: true, delay: 0, showURL: false, fade: 250,	bodyHandler: function() { return '<h3>Тип заменяемости</h3>Замена возможна указанным дубликатом возможна, но обратная замена не гарантируется'; } }); 
				jQuery('img.c_rbackw').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '<h3>Тип заменяемости</h3>Замена НЕ возможна'; } }); 
				});
				</script>
				<table class="guayaquil_table" width="100%" id="g_DetailTable1">
					<tr>
						<th id="c_pnc">№</th>
						<th id="c_oem">Артикул</th>
						<th id="c_name">Наименование детали</th>
						<th id="c_tooltip">&nbsp;</th>
					</tr>
					{:foreach from=$details.details->row item=detail name=dt:}
					<tr class="g_collapsed g_highlight" name="{:$detail['codeonimage']:}" id="d_{:$smarty.foreach.dt.iteration:}" onmouseout="hl(this, 'out');" onmouseover="hl(this, 'in');">
						<td name="c_pnc" class="g_ttd">{:$detail['codeonimage']:}</td>
						<td name="c_oem" class="g_ttd"><a href="{:$detail['link']:}">{:$detail['oem']:}</a></td>
						<td name="c_name" class="g_ttd">{:$detail['name']:}</td>
						<td>
							<div style="display: none;">
							{:foreach from=$detail->attribute item=attr:}
								<div><b>{:$attr.name:}:</b> {:$attr.value:}</div>
							{:/foreach:}
							</div>
						</td>
					</tr>
					{:/foreach:}
				</table>
			</div>
		</div>
		
		<table style="margin-top:5px;width: 30%;" border="0">
			<tbody>
				<tr>
					<th colspan="2" align="left">Управление</th>
				</tr>
				<tr>
					<td align="left"><img src="/css/images/mouse_wheel.png"></td>
					<td> - масштабирование изображения</td>
					<!--td align="left"><img src="/libs/ppv/guayaquillib/render/images/lmb.png"> <img src="/libs/ppv/guayaquillib/render/images/move.png"></td>
					<td> - прокрутка картинки</td-->
				</tr>
				<tr>
					<td align="left"><img src="/css/images/mouse.png"> <img src="/css/images/pointer.png"></td>
					<td> - подсветка всех деталей на картинке и в таблице</td>
					<!--td align="left"><img src="/libs/ppv/guayaquillib/render/images/lmb.png">   <img src="/libs/ppv/guayaquillib/render/details/images/info.gif"></td>
					<td> - показ подробной информации о запчасти</td-->
				</tr>
			</tbody>
		</table>
	</div>
	{:else:}
	<h1>Информация не найдена.</h1>
	{:/if:}


<style>

.car-specification {
	font-size: 14px;
	padding-bottom: 15px;
	border-bottom: 1px solid #ccc;
}

.car-specification div {
	font-size: 20px;
}

.car-specification-table {
	margin-top: 20px;
	color: #555;
	font-size: 12px;
	//border-bottom: 1px solid #aaa;
}

.car-specification-table td {
	padding: 0 15px 10px 0;
}

.unit-image {
	float: left;
	width: 45%;
}

.unit-parts {
	float: left;
}
</style>

<script>
$(function(){
	$('#show_car_specification').click(function() {
		if ($('.car-specification-table').css('display') !== 'none') {
			$('.car-specification-table').hide();
		} else {
			$('.car-specification-table').show();
		}
		
		return false;
	});
	
});
</script>