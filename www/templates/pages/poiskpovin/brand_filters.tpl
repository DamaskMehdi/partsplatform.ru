
<div style="margin: 40px 40px 300px 40px;">
{:if !empty($html_filters):}

	{:*<link href="/css/chosen.css" media="screen" rel="stylesheet" type="text/css">*:}
	<script src="/js/mootools-core-1.3.2.js" type="text/javascript"></script>
	<script src="/js/mootools-yui-compressed.js" type="text/javascript"></script>
	<script src="/js/mootools-more-1.3.2.1.js" type="text/javascript"></script>
	<script src="/js/chosen.min.js" type="text/javascript"></script>

	<h1>{:$h1_title:}</h1>
	
	{:$html_filters:}
	
{:else:}

<div class="l-form">
	<form action="/poiskpovin" method="get">
		<input type="hidden" name="ft" value="findByVIN">
		<span class="l-formtitle">Поиск автомобиля по VIN:</span> 
		<br>
		<input type="text" name="vin" value="" style="width: 215px;">
		<input type="submit" value="Искать">
		<br>
		<small>Введите VIN кузова автомобиля, например WAUBH54B11N111054<br> <a href="#" class="open-example-vin">Что такое VIN номер кузова?</a></small>
		<br>
		<div class="example-vin">
			<img src="/images/engine_code_example2.jpg" width="400" />
		</div>
	</form>
</div>

{:/if:}
	
</div>

<script> 
$(function() {
	//$$(".chzn-select").chosen(); 
	
	$('.open-example-vin').click(function() {
		
		//$('<div id="fancybox-loading"><div><img src="/images/engine_code_example2.jpg" width="400" /></div></div>').appendTo("body");
	
		$('.example-vin').toggle();
		return false;
	});
});
</script>

<style>
.gWizardVehicleLink {
	font-size: 20px;
	color: #b60000;
	border: 1px solid #ccc;
	border-radius: 3px;
	padding: 5px;
	margin-left: 28%;
}

.gWizardVehicleLink:hover {
	text-decoration: none;
}

#findByParameterIdentifocation select {
	display: inline;
	margin-right: 10px;
}

.example-code-engine, .example-vin {
	margin: 20px 10px;
	display: none;
}
</style>

