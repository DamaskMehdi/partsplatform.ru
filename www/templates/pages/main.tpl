<div class="clear" style="background-color: #222;">
	<div class="header__search header__search2" style="padding: 200px 0px 100px 20px; width: 100%;">
		<div style="margin: auto;width: 50%">
			<form action="/poiskpovin" method="get" id="header_search_form" style="float: none;width: 97%;">
				<!--input type="text" placeholder="Введите текст для поиска или номер запчасти"/-->
				<input id="article" class="field ui-autocomplete-input" type="text" name="search_text" placeholder="VIN / артикул" autocomplete="off" value="{:if !empty($header_search_text):}{:$header_search_text:}{:/if:}" style="font-size: 30px;height: 70px;" />
				<div id="search-by-parts" class="ui-search-results"></div>
				<input type="submit" value="Поиск" style="font-size: 30px;height: 68px;width: 120px;" />
			</form>
		</div>
		
		{:if !$page->user->isAuthorized():}
		<div style="margin: 50px 0px 0px 0px; text-align: center;">
			<div style="margin: auto;">
				<a href="/registershop" class="btn btn-primary" style="display: block-inline;margin-right: 60px;">Зарегистрировать магазин</a>
				<a href="/registerplatform" class="btn btn-primary" style="display: block-inline;">Зарегистрировать площадку</a>
			</div>
		</div>
		{:/if:}
	</div>
	
	
</div>



<style>
.btn-primary:active:hover, html .page .btn-primary:active:focus, html .page .btn-primary:active.focus, html .page .btn-primary.active:hover, html .page .btn-primary.active:focus, html .page .btn-primary.active.focus, .open > html .page .btn-primary.dropdown-toggle:hover, .open > html .page .btn-primary.dropdown-toggle:focus, .open > html .page .btn-primary.dropdown-toggle.focus {
    color: #fff;
    background-color: #FF0000;
    border-color: #229692;
}

html .page .btn-primary:active, html .page .btn-primary.active, .open > html .page .btn-primary.dropdown-toggle {
    -webkit-box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.15);
    box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.15);
}

html .page .btn-primary:focus, html .page .btn-primary.focus, html .page .btn-primary:hover, html .page .btn-primary:active, html .page .btn-primary.active, .open > html .page .btn-primary.dropdown-toggle {
    color: #fff;
    background-color: #FF0000;
    border-color: #229692;
}

.btn-primary:active:hover, .btn-primary:active:focus, .btn-primary:active.focus, .btn-primary.active:hover, .btn-primary.active:focus, .btn-primary.active.focus, .open > .btn-primary.dropdown-toggle:hover, .open > .btn-primary.dropdown-toggle:focus, .open > .btn-primary.dropdown-toggle.focus {
    color: #fff;
    background-color: #FF0000;
    border-color: #1e6765;
}

html .page .btn-primary {
    color: #fff;
    background-color: #3ec7c2;
    border-color: #3ec7c2;
    border-bottom: 4px solid #229692;
}

.bg-gray-base a:hover, .bg-gray-darker a:hover, .bg-gray-dark a:hover {
    color: #fff;
}

.bg-gray-base a, .bg-gray-darker a, .bg-gray-dark a, .bg-gray-base a:active, .bg-gray-darker a:active, .bg-gray-dark a:active, .bg-gray-base a:focus, .bg-gray-darker a:focus, .bg-gray-dark a:focus {
    color: #3ec7c2;
}

.btn:active, .btn.active {
    -webkit-box-shadow: none;
    box-shadow: none;
}

.btn:focus, .btn:active, .btn:active:focus {
    outline: none;
}

.btn-primary:active, .btn-primary.active, .open > .btn-primary.dropdown-toggle {
    background-image: none;
}

.btn-primary:active, .btn-primary.active, .open > .btn-primary.dropdown-toggle {
    color: #fff;
    background-color: #2fa39f;
    border-color: #2d9b97;
}

.btn-primary:hover {
    color: #fff;
    background-color: #2fa39f;
    border-color: #2d9b97;
}

.btn:active, .btn.active {
    outline: 0;
    background-image: none;
    -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
    box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
}

.btn:hover, .btn:focus, .btn.focus {
    color: #fff;
    text-decoration: none;
}

a:hover, a:focus {
    color: #3ec7c2;
    text-decoration: none;
}

a, a:active, a:focus {
    color: #27292b;
}

a:hover, a:focus {
    color: #3ec7c2;
    text-decoration: none;
}

a:active, a:hover {
    outline: 0;
}

.btn {
    padding: 15px 39px 11px 39px;
    font-size: 18px;
    font-weight: 400;
    text-transform: uppercase;
    transition: .3s ease-out;
}

.btn-primary {
    color: #fff;
    background-color: #FF0000;
    border-color: #3ec7c2;
}

.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 13px 39px;
    font-size: 14px;
    line-height: 1.71429;
    border-radius: 0;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

a {
    display: inline-block;
    text-decoration: none;
    transition: .33s all ease-out;
}

a {
    color: #27292b;
    text-decoration: none;
}
</style>