<h1>Авторизация</h1>
<div class="content">
	<div class="login_form __left">
		<form action="/" method="GET">
			<input type="hidden" name="af" value="a">
			<input type="hidden" name="action" value="login_account">
			<table class="t_register">
				<tr>
					<td>Email:</td>
					<td><input type="text" name="email" id="email" value="" placeholder="Ваш Email"></td>
				</tr>
				<tr>
					<td>Пароль:</td>
					<td><input type="password" name="password" id="password" value="" placeholder="Ваш пароль"></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="button" id="login_account" class="battery__form-button" value="Войти">
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div class="login_information __left">
		
	</div>
</div>

<script>
$(function() {	
	$('#login_account').click(function() {
		if ($('#email').val() == '') {
			alert('Заполните email');
		} else if ($('#password').val() == '') {
			alert('Заполните поле пароля');
		} else {
			var params = {
				af: 'a',
				action: 'login_account',
				email: $('#email').val(),
				password: $('#password').val()
			};
		
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: params,
				success: function(data) {
					if (data.success) {
						window.location.href = "/account/";
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Неверный логин или пароль!');
						}
					}
				}
			});
		}
		return false;
	});	
});
</script>