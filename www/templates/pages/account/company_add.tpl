<h1>Добавить компанию</h1>
{:if $page->user->getField('seller'):}
{:if !$company->isExist():}
<div class="tab-content">
	<div class="cabinet_form __left">
		<form id="company_form">
			<input type="hidden" name="af" value="a">
			<input type="hidden" name="action" value="{:if $company->isExist():}edit_company{:else:}add_company{:/if:}">
			<input type="hidden" name="account_id" value="{:$page->user->getId():}">
			{:if $company->isExist():}<input type="hidden" name="id" value="{:$company->getId():}">{:/if:}
			{:if $company->getAddress()->isExist():}<input type="hidden" name="address_id" value="{:$company->getAddress()->getId():}">{:/if:}
			<table class="t_register">
				<tr>
					<td width="30%">Компания *:</td>
					<td><input type="text" name="name" id="name" value="{:$company->getField('name'):}"></td>
				</tr>
				<tr>
					<td>Чем торгуем:</td>
					<td>
						<input type="checkbox" name="sale_types[]" value="1" {:if $company->isSale(1):}checked{:/if:}>
						<label for="oficial_dealer">Запчасти</label><br>
						<input type="checkbox" name="sale_types[]" value="2" {:if $company->isSale(2):}checked{:/if:}>
						<label for="oficial_dealer">Шины</label><br>
						<input type="checkbox" name="sale_types[]" value="3" {:if $company->isSale(3):}checked{:/if:}>
						<label for="oficial_dealer">Аккумуляторы</label><br>
						<input type="checkbox" name="sale_types[]" value="4" {:if $company->isSale(4):}checked{:/if:}>
						<label for="oficial_dealer">Масла</label><br>
						<input type="checkbox" name="sale_types[]" value="5" {:if $company->isSale(5):}checked{:/if:}>
						<label for="oficial_dealer">Стартеры и генераторы</label>
					</td>
				</tr>
				<tr>
					<td>Стоимость клика по контактам, руб</td>
					<td>
						<input type="text" name="click_price" id="click_price" value="{:if $company->getField('click_price'):}{:$company->getField('click_price'):}{:/if:}">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="oficial_dealer" id="oficial_dealer" value="1" {:if $company->getField('oficial_dealer'):}checked{:/if:}>
						<label for="oficial_dealer">Официальный авто-дилер</label>
					</td>
				</tr>
				<tr>
					<td>Марка авто:</td>
					<td>
						<select id="dealer_mark_id" name="dealer_mark_id[]">
							<option value="0"> выбрать </option>
							{:foreach from=$category_values.2.values item=value:}
							<option value="{:$value.value:}" {:if in_array($value.value, $company->getField('dealer_marks_id')):}selected{:/if:}>{:$value.name:}</option>
							{:/foreach:}
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="has_internet_shop" id="has_internet_shop" value="1" {:if $company->getField('has_internet_shop'):}checked{:/if:}>
						<label for="has_internet_shop">Есть интернет магазин</label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="has_auto_repair" id="has_auto_repair" value="1" {:if $company->getField('has_auto_repair'):}checked{:/if:}>
						<label for="has_auto_repair">Есть автосервис</label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="retail" id="retail" value="1" {:if $company->getField('retail'):}checked{:/if:}>
						<label for="retail">Торговля в розницу</label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="wholesale" id="wholesale" value="1" {:if $company->getField('wholesale'):}checked{:/if:}>
						<label for="wholesale">Торговля оптом</label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="delivery" id="delivery" value="1" {:if $company->getField('delivery'):}checked{:/if:}>
						<label for="delivery">Есть доставка курьером</label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="delivery_to_region" id="delivery_to_region" value="1" {:if $company->getField('delivery_to_region'):}checked{:/if:}>
						<label for="delivery_to_region">Есть доставка в регионы</label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="accept_vin_request" id="accept_vin_request" value="1" {:if $company->getField('accept_vin_request'):}checked{:/if:}>
						<label for="accept_vin_request">Принимаем запросы по VIN-коду</label>
					</td>
				</tr>
				<tr>
					<td>Запросы по маркам:</td>
					<td>
						<select name="vin_request_mark_id[]">
							<option value="0"> выбрать </option>
							{:foreach from=$category_values.2.values item=value:}
							<option value="{:$value.value:}" {:if in_array($value.value, $company->getField('vin_request_marks_id')):}selected{:/if:}>{:$value.name:}</option>
							{:/foreach:}
						</select>
					</td>
				</tr>
				<tr>
					<td>Запросы из регионов:</td>
					<td>
						<select name="vin_request_region_id[]">
							<option value="0"> выбрать </option>
							{:foreach from=$category_values.1.values item=value:}
							<option value="{:$value.value:}" {:if in_array($value.value, $company->getField('vin_request_regions_id')):}selected{:/if:}>{:$value.name:}</option>
							{:/foreach:}
						</select>
					</td>
				</tr>
				<tr>
					<td>Еmail для VIN-запросов:</td>
					<td>
						<input type="text" name="vin_request_email" id="vin_request_email" value="{:$company->getField('vin_request_email'):}">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="request_by_email" id="request_by_email" value="1" {:if $company->getField('request_by_email'):}checked{:/if:}>
						<label for="request_by_email">Принимаем заказы по e-mail</label>
					</td>
				</tr>
				<tr>
					<td>Еmail для заказов:</td>
					<td>
						<input type="text" name="request_email" id="request_email" value="{:$company->getField('request_email'):}">
					</td>
				</tr>
				<tr>
					<td>Адрес сайта:</td>
					<td>
						<input type="text" name="web" id="web" value="{:$company->getField('web'):}">
					</td>
				</tr>
				<tr>
					<td>Телефон 1:</td>
					<td>
						<input type="text" name="cc[]" value="{:$company->getField('cc0'):}" maxlength="4" size="4" style="width:40px"> ( <input type="text" name="oc[]" value="{:$company->getField('oc0'):}" maxlength="4" size="4" style="width:40px"> ) <input type="text" name="number[]" value="{:$company->getField('number0'):}" maxlength="9" size="9" style="width:100px"> доб. <input type="text" name="addnumber[]" value="{:$company->getField('addnumber0'):}" maxlength="3" size="3" style="width:40px">
					</td>
				</tr>
				<tr>
					<td>Телефон 2:</td>
					<td>
						<input type="text" name="cc[]" value="{:$company->getField('cc1'):}" maxlength="4" size="4" style="width:40px"> ( <input type="text" name="oc[]" value="{:$company->getField('oc1'):}" maxlength="4" size="4" style="width:40px"> ) <input type="text" name="number[]" value="{:$company->getField('number1'):}" maxlength="9" size="9" style="width:100px"> доб. <input type="text" name="addnumber[]" value="{:$company->getField('addnumber1'):}" maxlength="3" size="3" style="width:40px">
					</td>
				</tr>
				</table>
				
				<div id="accordion">
					<h3>Реквизиты компании</h3>
					<div>
						<table class="t_register">
							<tr>
								<td width="30%">Наименование полное:</td>
								<td><input type="text" name="full_name" id="full_name" value="{:$company->getField('full_name'):}"></td>
							</tr>
							<tr>
								<td>ИНН *:</td>
								<td><input type="text" name="inn" id="inn" value="{:$company->getField('inn'):}"></td>
							</tr>
							<tr>
								<td>КПП:</td>
								<td>
									<input type="text" name="kpp" id="kpp" value="{:$company->getField('kpp'):}">
								</td>
							</tr>
							<tr>
								<td>Банк *:</td>
								<td><input type="text" name="bank" id="bank" value="{:$company->getField('bank'):}"></td>
							</tr>
							<tr>
								<td>Счет *:</td>
								<td><input type="text" name="bill" id="bill" value="{:$company->getField('bill'):}"></td>
							</tr>
							<tr>
								<td>Корр. счет:</td>
								<td><input type="text" name="correspondent_bill" id="correspondent_bill" value="{:$company->getField('correspondent_bill'):}"></td>
							</tr>
							<tr>
								<td>БИК *:</td>
								<td><input type="text" name="bik" id="bik" value="{:$company->getField('bik'):}"></td>
							</tr>
							<tr>
								<td>ФИО Директора *:</td>
								<td><input type="text" name="fio_director" id="fio_director" value="{:$company->getField('fio_director'):}"></td>
							</tr>
						</table>
					</div>
					
					<h3>Юридический адрес</h3>
					<div>
						<table class="t_register">
							<tr>
								<td width="30%">Страна:</td>
								<td>
									<input type="text" name="country" id="country" value="{:$company->getAddress()->getField('country'):}">
								</td>
							</tr>
							<tr>
								<td>Область/край:</td>
								<td><input type="text" name="area" id="area" value="{:$company->getAddress()->getField('area'):}"></td>
							</tr>
							<tr>
								<td>Город/нас.пункт:</td>
								<td><input type="text" name="city" id="city" value="{:$company->getAddress()->getField('city'):}"></td>
							</tr>
							<tr>
								<td>Улица:</td>
								<td>
									<select name="street_type" id="street_type" style="width:20%;">
										{:foreach from=$category_values.4.values item=value:}
										<option value="{:$value.name:}" {:if $company->getAddress()->getField('street_type') == $value.name:}selected{:/if:}>{:$value.name:}</option>
										{:/foreach:}
									</select>
									&nbsp;<input type="text" name="street" id="street" value="{:$company->getAddress()->getField('street'):}" style="width:70%">
								</td>
							</tr>
							<tr>
								<td>Дом/корп.(стр.):</td>
								<td><input type="text" name="house" id="house" value="{:$company->getAddress()->getField('house'):}" size="5" maxlength="5" style="width:40px"> / <input type="text" name="building" id="building" value="{:$company->getAddress()->getField('building'):}" size="5" maxlength="5" style="width:40px"></td>
							</tr>
							<tr>
								<td>Квартира, офис:</td>
								<td><input type="text" name="flat" id="flat" value="{:$company->getAddress()->getField('flat'):}" size="5" maxlength="5" style="width:40px"> / <input type="text" name="office" id="office" value="{:$company->getAddress()->getField('office'):}" size="5" maxlength="5" style="width:40px"></td>
							</tr>
							<tr>
								<td>Отметить на карте</td>
								<td>
									<div class="the-map hidden">
										<input type="hidden" id="location" name="location" value="{:implode(',',$company->getField('location')):}">
										<input type="hidden" id="lat" name="c[lat]" value="{:if !empty($location[0]):}{:$location[0]:}{:else:}55.75399400{:/if:}">
										<input type="hidden" id="lng" name="c[lng]" value="{:if !empty($location[1]):}{:$location[1]:}{:else:}37.62209300{:/if:}">
										<input type="hidden" id="zoom" name="c[zoom]" value="88">
										<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
										<div>
											<p>Проверьте местоположение на карте. Если определено не верно, вы можете указать его точнее кликнув на карту.</p>
											<p class="descr hidden" id="undomapdesc"><small>Вы определили местоположение вручную. <a href="#" class="undo">Вернуть автоматическое определение местоположения</a></small></p>
										</div>
										<div id="map" style="width:100%; height:240px;margin-top:10px;border:2px solid #fff;"></div>
										<script src="/js/map2.js" type="text/javascript"></script>
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="button" id="company_change_btn" class="battery__form-button" value="{:if $company->isExist():}Сохранить{:else:}Добавить{:/if:}"></td>
							</tr>
						</table>
					</div>
				</div>
		</form>
	</div>
	<div class="cabinet_information __left">
		<p><strong>Информация о компании</strong></p>
		<p>Поля отмеченные "*" обязательные для заполнения</p>
	</div>
	<div class="clear"></div>
</div>
{:else:}
У вас уже существует компания
{:/if:}
{:else:}
Вы не можете добавить компанию
{:/if:}

<script>
$(function() {

	$('#company_change_btn').click(function() {
		if ($('#company_form input[name=name]').val() == '') {
			alert('Введите название компании');
		} else if ($('#company_form input[name=inn]').val() == '') {
			alert('Введите ИНН');
		} else if ($('#company_form input[name=bank]').val() == '') {
			alert('Введите Банк');
		} else if ($('#company_form input[name=bill]').val() == '') {
			alert('Введите счет');
		} else if ($('#company_form input[name=bik]').val() == '') {
			alert('Введите БИК');
		} else if ($('#company_form input[name=fio_director]').val() == '') {
			alert('Введите ФИО Директора');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('form#company_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});

});
</script>