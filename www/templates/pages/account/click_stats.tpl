<h1>Статистика кликов</h1>
<div class="content">
	<dl class="tabs2">
		<dt><i class="fa fa-info-circle"></i> &nbsp; <span><a href="/account/main">Общая информация</a></span></dt>
		<dt id="company"><i class="fa fa-users"></i> &nbsp; <span><a href="/account/company">Компания</a></span></dt>
		<dt class="selected" id="company"><i class="fa fa-users"></i> &nbsp; <span>Статистика кликов</span></dt>
		{:if $company->isSale(1):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_parts">Импорт запчастей</a></span></dt>
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_cross">Импорт кроссов</a></span></dt>
		{:/if:}
		{:if $company->isSale(2):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_tires">Импорт шин</a></span></dt>
		{:/if:}
		{:if $company->isSale(3):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_accumulators">Импорт аккумуляторов</a></span></dt>
		{:/if:}
		{:if $company->isSale(4):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_oils">Импорт масел</a></span></dt>
		{:/if:}
		{:if $company->isSale(5):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_starters">Импорт стартеров</a></span></dt>
		{:/if:}
		
	
		<dd class="selected">
			<br>
			<h3>Ваш баланс составляет <span style="color: red;">{:$company->getField('balance'):}</span> <small>руб</small></h3>
			<h4>Стоимость одного клика составляет {:$company->getField('click_price'):} руб <a href="/account/company_edit">редактировать</a></h4> 
			<br>
			<table class="tr_unf">
				<tr>
					<th>Дата</th>
					<th>IP пользователя</th>
					<th>Стоимость клика, р</th>
				</tr>
				{:foreach from=$clicks_stat item=click:}
				<tr>
					<td>
						{:$click.date_create|date_format:'%H:%M %d.%m.%y':}
					</td>
					<td>
						{:$click.client_ip:}
					</td>
					<td>
						{:$click.click_price:}
						{:if $click.click_price == 0:}(повторный клик){:/if:}
						
					</td>
				</tr>
				{:/foreach:}
			</table>
			<div style="margin: 10px 0 20px 0;">
				{:if !empty($click_stats_pages):}
					<div class="pages _left">
					{:foreach from=$click_stats_pages item=page:}
						{:if empty($page.url):}
							<span>{:$page.label:}</span>
						{:else:}
							<a href="{:$page.url:}">{:$page.label:}</a>
						{:/if:}
					{:/foreach:}
					</div>
				{:/if:}
			</div>
		</dd>
	</dl>
</div>

<style>
.tr_unf { }
.tr_unf tr { border-bottom: 1px solid #eee; }
.tr_unf td { padding: 3px 0 3px 0; font-size: 10pt; }

#loader {
	text-align: center;
	opacity: 0.8;
	height: 100%;
    width: 100%;
    background: #000;
    border: 0;
    padding-top: 205px;
    z-index: 999 !important;
    cursor: default;
    top: 0% !important;
    left: 0% !important;
    position: fixed !important;
}
</style>
