<h1>Личный кабинет</h1>
<div class="content">
	<dl class="tabs2">
		<dt><i class="fa fa-info-circle"></i> &nbsp; <span><a href="/account/main">Общая информация</a></span></dt>
		<dt class="selected" id="company"><i class="fa fa-users"></i> &nbsp; <span>Компания</span></dt>
		<dt><i class="fa fa-info-circle"></i> &nbsp; <span><a href="/account/click_stats">Статистика кликов</a></span></dt>
		{:if $company->isSale(1):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_parts">Импорт запчастей</a></span></dt>
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_cross">Импорт кроссов</a></span></dt>
		{:/if:}
		{:if $company->isSale(2):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_tires">Импорт шин</a></span></dt>
		{:/if:}
		{:if $company->isSale(3):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_accumulators">Импорт аккумуляторов</a></span></dt>
		{:/if:}
		{:if $company->isSale(4):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_oils">Импорт масел</a></span></dt>
		{:/if:}
		{:if $company->isSale(5):}
			<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_starters">Импорт стартеров</a></span></dt>
		{:/if:}
		
	
		<dd class="selected">
		{:if $page->user->getField('seller'):}
			{:if $company->isExist():}
			<div class="tab-content">
				<div class="cabinet_form __left" style="background-color: #fff;">
					<p style="font-size: 12pt;"><strong>Данные компании</strong></p>
					
					<div class="__right"><a href="/account/company_edit" style="font-size: 10pt;">редактировать</a></div><div class="clear"></div>
				
					<div><span class="title-left">Название:</span> {:$company->getField('name'):}</div>
					{:if $company->getField('sale_types'):}
					<div><span class="title-left">Торгует:</span> 
					{:if $company->isSale(1):}Запчастями<br>{:/if:}
					{:if $company->isSale(2):}Шинами<br>{:/if:}
					{:if $company->isSale(3):}Аккумуляторами<br>{:/if:}
					{:if $company->isSale(4):}Маслами<br>{:/if:}
					{:if $company->isSale(5):}Стартерами и генераторами<br>{:/if:}
					</div>
					{:/if:}
					<div><span class="title-left">Стоимость клика по контактам:</span> {:$company->getField('click_price'):} руб</div>
					<div><span class="title-left">Email для заказов:</span> {:$company->getField('request_email'):}</div>
					<div><span class="title-left">Email для VIN-запросов:</span> {:$company->getField('vin_request_email'):}</div>
					<div><span class="title-left">Адрес сайта:</span> {:$company->getField('web'):}</div>
					<div><span class="title-left">Телефоны:</span> {:$company->getField('phones'):}</div>
					
					<div>
						<span class="title-left">Расположение на карте:</span><br>
						<div class="the-map hidden">
							<input type="hidden" id="location" name="location" value="{:implode(',',$company->getField('location')):}">
							<input type="hidden" id="lat" name="c[lat]" value="{:if !empty($location[0]):}{:$location[0]:}{:else:}55.75399400{:/if:}">
							<input type="hidden" id="lng" name="c[lng]" value="{:if !empty($location[1]):}{:$location[1]:}{:else:}37.62209300{:/if:}">
							<input type="hidden" id="zoom" name="c[zoom]" value="88">
							<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
							<div id="map" style="width:100%; height:240px;margin-top:10px;border:2px solid #fff;"></div>
							<script src="/js/map2.js" type="text/javascript"></script>
						</div>
					</div>
				
					<div style="margin-top: 30px;"><a href="/account/documents">Загрузить документы компании</a></div>
				</div>
				<div class="cabinet_information __left">
					<p><strong>Адреса магазинов</strong></p>
					{:if !empty($shops):}
						{:foreach $shops as $shop:}
						<div class="shop-mini">
							<div class="__right"><a href="/account/shop_edit?shop_id={:$shop->getField('id'):}" style="font-size: 9pt;">редактировать</a></div><div class="clear"></div>
							<p><span class="title-left">Название:</span> {:$shop->getField('name'):}</p>
							<p><span class="title-left">Телефоны:</span> {:$shop->getField('phones'):}</p>
						</div>
						{:/foreach:}
					{:else:}
					<p>Нет магазинов</p>
					{:/if:}
					<p><a href="/account/shop_add">Добавить магазин</a></p>
				</div>
				<div class="clear"></div>
			</div>
			{:else:}
			У вас еще не создана компания. <a href="/account/company_add">добавить компанию</a>
			{:/if:}
		{:else:}
		Ошибка
		{:/if:}
		</dd>
	</dl>
</div>

<script>
$(function() {

	$('.shop_links a').click(function() {
		var link_number = $('.shop_links a').index(this);
		$('.shop_content div.shop_item').hide().eq(link_number).show();
		$('.shop_links a').removeClass('active');$(this).addClass('active');
		return false;
	});	
	
	$('.delete_import_settings').click(function() {
		if (confirm('Вы действительно хотите удалить схему импорта?')) {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: { af: 'a', action: 'delete_import_settings', is_id: $(this).data('id') },
				success: function(data) {
					if (data.success) {
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});	
	
	$('.upload_user_pricelist_btn').click(function() {
		var form = $(this).parent();
		var price_file = form.find('[name=user_pricelist]');
	
		/*if ($('#pricelist_to_shop').val() == 0) {
			alert('Выберите магазин');
		} else*/ if ($('.loader_block').is(':visible')) {
			alert('Ожидайте окончания импорта файла');
		} else if (price_file.val() == '') {
			alert('Выберите файл');
		} else {
			$('.loader_block').show();		
			form.ajaxSubmit({
				dataType: 'json',
				success: function(data) {
					$('.loader_block').hide();
					if (data.success) {
						alert('Прайс-лист загружен на сервер, в течении нескольких минут он будет загружен в систему');
						window.location.reload();
					} else {
						alert('Произошла ошибка при загрузке прайс-листа');
					}
				},
				error: function() {
					$('.loader_block').hide();
					alert('Произошла ошибка при загрузке прайс-листа');
				}
			});
		}
	});
	
	$('#company_change_btn').click(function() {
		if ($('#company_form input[name=name]').val() == '') {
			alert('Введите название компании');
		} else if ($('#company_form input[name=inn]').val() == '') {
			alert('Введите ИНН');
		} else if ($('#company_form input[name=bank]').val() == '') {
			alert('Введите Банк');
		} else if ($('#company_form input[name=bill]').val() == '') {
			alert('Введите счет');
		} else if ($('#company_form input[name=bik]').val() == '') {
			alert('Введите БИК');
		} else if ($('#company_form input[name=fio_director]').val() == '') {
			alert('Введите ФИО Директора');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('form#company_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	$('.shop_change_btn').click(function() {
		var parent_form = $(this).parents('form.shop_form');
	
		if (parent_form.find('input[name=company_id]').val() == '') {
			alert('Для добавления магазина добавьте компанию');
		} else if (parent_form.find('input[name=city]').val() == '') {
			alert('Введите город');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: parent_form.serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	$('#add_document').click(function() {
		if ($('#document_form input[name=company_id]').val() == '') {
			alert('Для добавления документов добавьте компанию');
		} else if ($('#document_form #document_type_id').val() == '') {
			alert('Выберите тип документа');
		} else if ($('#document_form #document_file').val() == '') {
			alert('Выберите файл');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('#document_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	//$("select.dealer_mark_id").multiselect(); 
	//$("#accordion").accordion();
});
</script>