<table class="left">
	<tr>
		<td>
			<input type="hidden" id="ctl00_BodyPlace_UserPageControlATI" name="ctl00_BodyPlace_UserPageControlATI" value="0" />
			<div class="dxtcLite_ZzapAqua dxtcSys dxtc-init dxtc-top" id="ctl00_BodyPlace_UserPageControl" style="width:700px;">
				<ul class="dxtc-strip dxtc-stripContainer">
					<li class="dxtc-leftIndent">&nbsp;</li>
					<li class="dxtc-tab dxtc-lead" onclick="return aspxTCTClick(event, &#39;ctl00_BodyPlace_UserPageControl&#39;, 0)" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Основные поля</span></a></li>
					<li class="dxtc-activeTab dxtc-lead"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Основные поля</span></a></li>
					<li class="dxtc-spacer" style="width:3px;">&nbsp;</li>
					<li class="dxtc-tab" onclick="return aspxTCTClick(event, &#39;ctl00_BodyPlace_UserPageControl&#39;, 1)" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Информация</span></a></li>
					<li class="dxtc-activeTab" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Информация</span></a></li>
					<li class="dxtc-spacer" style="width:3px;">&nbsp;</li>
					<li class="dxtc-tab" onclick="return aspxTCTClick(event, &#39;ctl00_BodyPlace_UserPageControl&#39;, 2)" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Адрес</span></a></li>
					<li class="dxtc-activeTab" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Адрес</span></a></li>
					<li class="dxtc-spacer" style="width:3px;">&nbsp;</li>
					<li class="dxtc-tab" onclick="return aspxTCTClick(event, &#39;ctl00_BodyPlace_UserPageControl&#39;, 3)" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Реквизиты</span></a></li>
					<li class="dxtc-activeTab" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Реквизиты</span></a></li>
					<li class="dxtc-spacer" style="width:3px;">&nbsp;</li>
					<li class="dxtc-tab" onclick="return aspxTCTClick(event, &#39;ctl00_BodyPlace_UserPageControl&#39;, 4)" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Документы</span></a></li>
					<li class="dxtc-activeTab" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Документы</span></a></li>
					<li class="dxtc-spacer" style="width:3px;">&nbsp;</li>
					<li class="dxtc-tab" onclick="return aspxTCTClick(event, &#39;ctl00_BodyPlace_UserPageControl&#39;, 5)" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Оповещения</span></a></li>
					<li class="dxtc-activeTab" style="display:none;"><a class="dxtc-link" style="font-size:18px;text-decoration:none;"><span class="dx-vam">Оповещения</span></a></li>
					<li class="dxtc-rightIndent">&nbsp;</li>
				</ul>
				<div class="dxtc-content">
					<div>
						<div>
							<table cellpadding="4">
								<tr>
									<td class="f18 right" width="30%">Кто вы?*</td>
									<td>
										<span class="dxichCellSys dxeBase_ZzapAqua f18 dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserBCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserBCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserBCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Покупатель</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua f18 dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserACheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserACheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserACheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Продавец</label></span>
										<br />
										<span id="ctl00_BodyPlace_UserPageControl_CustModeValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;</td>
									<td class="f18">
										<table class="dxeRadioButtonList_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_TypeUserButtonList" border="0" style="border-style:None;font-size:18px;border-collapse:collapse;border-collapse:separate;">
											<tr>
												<td class="dxe">
													<input id="ctl00_BodyPlace_UserPageControl_TypeUserButtonList_VI" type="hidden" name="ctl00$BodyPlace$UserPageControl$TypeUserButtonList" />
													<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserButtonList_RB0" style="font-size:18px;">
														<span class="dxEditors_edtRadioButtonUnchecked_ZzapAqua dxeIRadioButton_ZzapAqua dxichSys">
															<input id="ctl00_BodyPlace_UserPageControl_TypeUserButtonList_RB0_I" name="ctl00$BodyPlace$UserPageControl$TypeUserButtonList$RB0" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" />
														</span>
														<label>Частное лицо</label>
													</span>
													<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserButtonList_RB1" style="font-size:18px;">
														<span class="dxEditors_edtRadioButtonChecked_ZzapAqua dxeIRadioButton_ZzapAqua dxichSys">
															<input id="ctl00_BodyPlace_UserPageControl_TypeUserButtonList_RB1_I" name="ctl00$BodyPlace$UserPageControl$TypeUserButtonList$RB1" value="C" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" />
														</span>
														<label>Компания</label>
													</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="f18 right">
										<span id="ctl00_BodyPlace_UserPageControl_NameLastLabel" class="f18">ФИО*</span>
									</td>
									<td>
										<table>
											<tr>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_NameLastEdit_Raw" name="ctl00_BodyPlace_UserPageControl_NameLastEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_NameLastEdit" border="0" style="font-size:18px;height:33px;width:180px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;">
																<input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_NameLastEdit_I" name="ctl00$BodyPlace$UserPageControl$NameLastEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_NameLastEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_NameLastEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_NameLastEdit&#39;)" value="Фамилия" type="text" />
															</td>
														</tr>
													</table>
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_NameFirstEdit_Raw" name="ctl00_BodyPlace_UserPageControl_NameFirstEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_NameFirstEdit" border="0" style="font-size:18px;height:33px;width:110px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;">
																<input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_NameFirstEdit_I" name="ctl00$BodyPlace$UserPageControl$NameFirstEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_NameFirstEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_NameFirstEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_NameFirstEdit&#39;)" value="Имя" type="text" />
															</td>
														</tr>
													</table>
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_NameSecondEdit_Raw" name="ctl00_BodyPlace_UserPageControl_NameSecondEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_NameSecondEdit" border="0" style="font-size:18px;height:33px;width:130px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_NameSecondEdit_I" name="ctl00$BodyPlace$UserPageControl$NameSecondEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_NameSecondEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_NameSecondEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_NameSecondEdit&#39;)" value="Отчество" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_NameLastValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_NameFirstValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_NameSecondValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr id="CompanyNameTr">
									<td class="f18 right">Компания*</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyNickEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyNickEdit_I" name="ctl00$BodyPlace$UserPageControl$CompanyNickEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyNickEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyNickEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyNickValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;</td>
									<td class="f18">&nbsp;</td>
								</tr>
								<tr>
									<td class="f18 right">Регион*</td>
									<td>
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
											<table class="dxeLoadingPanel_ZzapAqua dxlpLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="font-size:18px;height:100%;border-collapse:collapse;">
												<tr>
													<td class="dx" style="font-size:18px;">
														<img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" />
														<br />
														<span id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_TL">Загрузка&hellip;</span>
													</td>
												</tr>
											</table>
										</div>
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_LD" class="dxeLoadingDiv_ZzapAqua dxlpLoadingDiv_ZzapAqua dx-ft" style="left:0px;top:0px;z-index:29999;display:none;position:absolute;">
										</div>
										<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua f18" cellspacing="1" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo" border="0" style="font-size:18px;height:33px;width:350px;">
											<tr>
												<td style="display:none;"><input id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_VI" name="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_VI" type="hidden" value="30100" /></td><td class="dxic" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_I" name="ctl00$BodyPlace$UserPageControl$AddrCodeRegionCombo" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo&#39;)" value="Беларусь, Минская область" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
											</tr>
										</table>
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDDWS" name="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
											<div class="dxpc-mainDiv dxpc-shadow" style="font-size:18px;">
												<div class="dxpc-contentWrapper">
													<div class="dxpc-content">
														<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_LDeletedItems" name="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_LDeletedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_LInsertedItems" name="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_LInsertedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_LCustomCallback" name="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_LCustomCallback" value="" /><table class="dxeListBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_L" border="0" style="font-size:18px;border-collapse:collapse;border-collapse:separate;">
															<tr>
																<td valign="top">
																	<div id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_L_D" class="dxlbd" style="width:100%;overflow:auto;">
																		<input id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_L_VI" type="hidden" name="ctl00$BodyPlace$UserPageControl$AddrCodeRegionCombo$DDD$L" />
																		<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;border-collapse:separate;visibility:hidden!important;display:none!important;">
																			<tr id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_L_LBI-1" class="dxeListBoxItemRow_ZzapAqua">
																				<td id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_L_LBII" class="dxeListBoxItem_ZzapAqua">&nbsp;</td>
																			</tr>
																		</table>
																		<table id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionCombo_DDD_L_LBT" cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;border-collapse:separate;">
																			<tr class="dxeListBoxItemRow_ZzapAqua">
																				<td class="dxeListBoxItem_ZzapAqua">(неизвестный)</td>
																			</tr>
																		</table>
																	</div>
																</td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
										<span id="ctl00_BodyPlace_UserPageControl_AddrCodeRegionValidator" style="display:inline-block;color:Red;font-weight:bold;width:290px;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">Телефон для связи
									</td>
									<td>
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont3Edit" border="0" style="font-size:18px;height:33px;width:40px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont3Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelCodeCont3Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCont3Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCont3Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">(</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit" border="0" style="font-size:18px;height:33px;width:45px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelCodeCity3Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Edit&#39;)" value="код" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">)</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrTel3Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrTel3Edit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTel3Edit" border="0" style="font-size:18px;height:33px;width:100px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTel3Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTel3Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTel3Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTel3Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrTel3Edit&#39;)" value="номер" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">доб.
												</td>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelAdd3Edit" border="0" style="font-size:18px;height:33px;width:50px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelAdd3Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelAdd3Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelAdd3Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelAdd3Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont3Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity3Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTel3Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelAdd3Validator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;
										<input name="email" id="email" style="visibility: hidden; width: 1px; height: 1px;" autocomplete="off" />
										
									</td>
									<td class="f12">&nbsp;
										<input type="password" name="password" id="password_fake" style="visibility: hidden; width: 1px; height: 1px;" autocomplete="off" />
										
									</td>
								</tr>
								<tr>
									<td class="f18 right">Е-мейл*
									</td>
									<td>
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrEmail1Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrEmail1Edit_Raw" value="" />
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrEmail1Edit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrEmail1Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrEmail1Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmail1Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmail1Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrEmail1Edit&#39;)" value="addr@mail.com" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrEmail1Validator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">
										<span id="ctl00_BodyPlace_UserPageControl_PasswordLabel" class="f18">Пароль*:</span>
									</td>
									<td>
										<input name="ctl00$BodyPlace$UserPageControl$PasswordEdit" type="password" id="ctl00_BodyPlace_UserPageControl_PasswordEdit" autocomplete="off" style="font-size:18px;height:33px;width:200px;" /><br />
										<span id="ctl00_BodyPlace_UserPageControl_PasswordValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">
										<span id="ctl00_BodyPlace_UserPageControl_PasswordComfirmLabel" class="f18 nowrap">Пароль, еще раз*:</span>
									</td>
									<td>
										<input name="ctl00$BodyPlace$UserPageControl$PasswordComfirmEdit" type="password" id="ctl00_BodyPlace_UserPageControl_PasswordComfirmEdit" autocomplete="off" style="font-size:18px;height:33px;width:200px;" /><br />
										<span id="ctl00_BodyPlace_UserPageControl_PasswordComfirmValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;
									</td>
									<td class="f12">
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua f14 dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserNCheckBox" style="font-size:18px;">
											<span class="dxWeb_edtCheckBoxChecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserNCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserNCheckBox" value="C" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span>
											<label>Разрешить просмотр контактов продавцам</label>
										</span>
										<br />
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;
									</td>
									<td class="f18">&nbsp;
									</td>
								</tr>
							</table>
									
						</div>
					</div>
					<div style="display:none;">
						<div>
							<table cellpadding="4">
								<tr>
									<td width="30%">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td class="f18 right top">&nbsp;</td>
									<td>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserDCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserDCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserDCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Официальный авто-дилер</label></span>
										<br />
										<table>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;</td>
												<td><span class="f12">Авто-дилер по маркам</span> </td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;</td>
												<td>
													<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua" cellspacing="1" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup" border="0" style="width:350px;">
														<tr>
															<td class="dxic" onmousedown="return aspxDDMC_MD(&#39;ctl00_BodyPlace_UserPageControl_ManufactGridLookup&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_I" name="ctl00$BodyPlace$UserPageControl$ManufactGridLookup" readonly="readonly" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_ManufactGridLookup&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_ManufactGridLookup&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_ManufactGridLookup&#39;)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_ManufactGridLookup&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
														</tr>
													</table>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDDWS" name="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
													<div id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
														<div class="dxpc-mainDiv dxpc-shadow">
															<div class="dxpc-contentWrapper">
																<div class="dxeDropDownWindow_ZzapAqua dxpc-content">
																	<table class="dxgvControl_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv" border="0" style="border-width:0px;width:350px;border-collapse:collapse;border-collapse:separate;">
																		<tr>
																			<td>
																				<div class="dxgvCSD" style="height:200px;width:350px;overflow-x:hidden;overflow-y:auto;">
																					<table id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_DXMainTable" class="dxgvTable_ZzapAqua" cellspacing="0" cellpadding="0" onclick="aspxGVTableClick(&#39;ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv&#39;, event);" border="0" style="width:100%;border-collapse:collapse;empty-cells:show;table-layout:fixed;overflow:hidden;">
																						<tr>
																							<td style="width:50px;"></td><td style="width:300px;"></td>
																						</tr>
																						<tr id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_DXDataRow0" class="dxgvDataRow_ZzapAqua">
																							<td class="dxgvCommandColumn_ZzapAqua dxgv" align="center"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_DXSelBtn0" value="U" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxgv">ALFA ROMEO</td>
																						</tr>
																					</table>
																				</div>
																				<img id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_IADD" class="dxGridView_gvDragAndDropArrowDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="|" style="position:absolute;visibility:hidden;top:-100px;" />
																				<img id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_IADU" class="dxGridView_gvDragAndDropArrowUp_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="|" style="position:absolute;visibility:hidden;top:-100px;" />
																				<img id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_IDHF" class="dxGridView_gvDragAndDropHideColumn_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="Скрыть" style="position:absolute;visibility:hidden;top:-100px;" />
																				<table id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_DXStatus" class="dxgvStatusBar_ZzapAqua" border="0" style="width:100%;">
																					<tr class="dxgv">
																						<td id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_tcStatusBar" style="width:100%;">
																							<table width="100%">
																								<tr>
																									<td class="float-right">
																										<div class="dxbButton_ZzapAqua dxbButtonSys dxbTSys" id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_StatusBar_CloseButton_0" style="-moz-user-select:none;">
																											<div class="dxb">
																												<input class="dxb-hb" value="Закрыть" type="submit" name="ctl00$BodyPlace$UserPageControl$ManufactGridLookup$DDD$gv$StatusBar$TC$CloseButton" /><span class="dx-vam">Закрыть</span>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																				<div id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
																					<table class="dxgvLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="height:100%;border-collapse:collapse;">
																						<tr>
																							<td class="dx"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_TL">Загрузка&hellip;</span></td>
																						</tr>
																					</table>
																				</div>
																				<div id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_LD" class="dxgvLoadingDiv_ZzapAqua" style="display:none;z-index:29999;position:absolute;"></div>
																				<table id="ctl00_BodyPlace_UserPageControl_ManufactGridLookup_DDD_gv_DXStyleTable" border="0" style="display:none;">
																					<tr>
																						<td></td>
																					</tr><tr class="dxgvEditingErrorRow_ZzapAqua">
																						<td class="dxgv" data-colSpan="2"></td>
																					</tr><tr class="dxgvSelectedRow_ZzapAqua">
																						<td></td>
																					</tr><tr class="dxgvFocusedRow_ZzapAqua">
																						<td></td>
																					</tr><tr class="dxgvFocusedGroupRow_ZzapAqua">
																						<td></td>
																					</tr><tr>
																						<td class="dxgvBatchEditCell_ZzapAqua dxgv"></td><td class="dxgvBatchEditModifiedCell_ZzapAqua dxgv"></td><td class="dxgvBatchEditCell_ZzapAqua dxgvBatchEditModifiedCell_ZzapAqua dxgv"></td>
																					</tr><tr class="dxgvDataRow_ZzapAqua">
																						<td></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_ManufactValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserICheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserICheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserICheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Есть интернет магазин</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserSCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserSCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserSCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Есть автосервис</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserRCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserRCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserRCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Торговля в розницу</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserWCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserWCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserWCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Торговля оптом</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserPCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserPCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserPCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Есть доставка курьером</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserYCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserYCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserYCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Есть доставка в регионы</label></span>
										<br />
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserXCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserXCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserXCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>X</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserQCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserQCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserQCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Q</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserTCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserTCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserTCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>T</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserLCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserLCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserLCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>L</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserOCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserOCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserOCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>O</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserCCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserCCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserCCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>C</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserJCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserJCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserJCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>J</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserMCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserMCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserMCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>M</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserKCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserKCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserKCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>K</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserZCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserZCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserZCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Z</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUser1CheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUser1CheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUser1CheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>1</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUser2CheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUser2CheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUser2CheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>2</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUser3CheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUser3CheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUser3CheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>3</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUser2BCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUser2BCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUser2BCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>B</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUser2YCheckBox" style="font-size:18px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUser2YCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUser2YCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Y</label></span>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserECheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserECheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserECheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Принимаем запросы по VIN-коду</label></span>
										<br />
										<table id="ctl00_BodyPlace_UserPageControl_VinTable">
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td><span class="f12">Запросы по следующим маркам:</span></td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td>
													<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua" cellspacing="1" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup" border="0" style="width:350px;">
														<tr>
															<td class="dxic" onmousedown="return aspxDDMC_MD(&#39;ctl00_BodyPlace_UserPageControl_VinManufactGridLookup&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_I" name="ctl00$BodyPlace$UserPageControl$VinManufactGridLookup" readonly="readonly" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_VinManufactGridLookup&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_VinManufactGridLookup&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_VinManufactGridLookup&#39;)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_VinManufactGridLookup&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
														</tr>
													</table>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDDWS" name="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
													<div id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
														<div class="dxpc-mainDiv dxpc-shadow">
															<div class="dxpc-contentWrapper">
																<div class="dxeDropDownWindow_ZzapAqua dxpc-content">
																	<table class="dxgvControl_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv" border="0" style="border-width:0px;width:350px;border-collapse:collapse;border-collapse:separate;">
																		<tr>
																				<td><div class="dxgvCSD" style="height:200px;width:350px;overflow-x:hidden;overflow-y:auto;">
																					<table id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_DXMainTable" class="dxgvTable_ZzapAqua" cellspacing="0" cellpadding="0" onclick="aspxGVTableClick(&#39;ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv&#39;, event);" border="0" style="width:100%;border-collapse:collapse;empty-cells:show;table-layout:fixed;overflow:hidden;">
																						<tr>
																							<td style="width:50px;"></td><td style="width:300px;"></td>
																						</tr><tr id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_DXDataRow0" class="dxgvDataRow_ZzapAqua">
																							<td class="dxgvCommandColumn_ZzapAqua dxgv" align="center"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_DXSelBtn0" value="U" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxgv">ALFA ROMEO</td>
																						</tr>
																					</table>
																				</div>
																				<img id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_IADD" class="dxGridView_gvDragAndDropArrowDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="|" style="position:absolute;visibility:hidden;top:-100px;" />
																				<img id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_IADU" class="dxGridView_gvDragAndDropArrowUp_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="|" style="position:absolute;visibility:hidden;top:-100px;" />
																				<img id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_IDHF" class="dxGridView_gvDragAndDropHideColumn_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="Скрыть" style="position:absolute;visibility:hidden;top:-100px;" />
																				<table id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_DXStatus" class="dxgvStatusBar_ZzapAqua" border="0" style="width:100%;">
																					<tr class="dxgv">
																						<td id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_tcStatusBar" style="width:100%;">
																							<table width="100%">
																								<tr>
																									<td>
																										<table class="dxeBase_ZzapAqua dxeTAR" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_StatusBar_SelectAllCheckBox_0" border="0" style="border-collapse:collapse;">
																											<tr>
																												<td class="dxichCellSys"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_StatusBar_SelectAllCheckBox_0_S" name="ctl00$BodyPlace$UserPageControl$VinManufactGridLookup$DDD$gv$StatusBar$TC$SelectAllCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxichTextCellSys"><label>Выбрать все</label></td>
																											</tr>
																										</table>
																									</td>
																									<td class="float-right">
																										<div class="dxbButton_ZzapAqua dxbButtonSys dxbTSys" id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_StatusBar_CloseButton_0" style="-moz-user-select:none;">
																											<div class="dxb">
																												<input class="dxb-hb" value="Закрыть" type="submit" name="ctl00$BodyPlace$UserPageControl$VinManufactGridLookup$DDD$gv$StatusBar$TC$CloseButton" /><span class="dx-vam">Закрыть</span>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																				<div id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
																					<table class="dxgvLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="height:100%;border-collapse:collapse;">
																						<tr>
																							<td class="dx"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_TL">Загрузка&hellip;</span></td>
																						</tr>
																					</table>
																				</div>
																				<div id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_LD" class="dxgvLoadingDiv_ZzapAqua" style="display:none;z-index:29999;position:absolute;">

																				</div>
																				<table id="ctl00_BodyPlace_UserPageControl_VinManufactGridLookup_DDD_gv_DXStyleTable" border="0" style="display:none;">
																					<tr>
																						<td></td>
																					</tr><tr class="dxgvEditingErrorRow_ZzapAqua">
																						<td class="dxgv" data-colSpan="2"></td>
																					</tr><tr class="dxgvSelectedRow_ZzapAqua">
																						<td></td>
																					</tr><tr class="dxgvFocusedRow_ZzapAqua">
																						<td></td>
																					</tr><tr class="dxgvFocusedGroupRow_ZzapAqua">
																						<td></td>
																					</tr><tr>
																						<td class="dxgvBatchEditCell_ZzapAqua dxgv"></td><td class="dxgvBatchEditModifiedCell_ZzapAqua dxgv"></td><td class="dxgvBatchEditCell_ZzapAqua dxgvBatchEditModifiedCell_ZzapAqua dxgv"></td>
																					</tr><tr class="dxgvDataRow_ZzapAqua">
																						<td></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
													<span id="ctl00_BodyPlace_UserPageControl_VinManufactValidator" style="color:Red;font-weight:bold;display:none;"></span>
												</td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td><span class="f12">Запросы из следующих регионов:</span></td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td>
													<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua" cellspacing="1" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup" border="0" style="width:350px;">
														<tr>
															<td class="dxic" onmousedown="return aspxDDMC_MD(&#39;ctl00_BodyPlace_UserPageControl_VinRegionGridLookup&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_I" name="ctl00$BodyPlace$UserPageControl$VinRegionGridLookup" readonly="readonly" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_VinRegionGridLookup&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_VinRegionGridLookup&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_VinRegionGridLookup&#39;)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_VinRegionGridLookup&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
														</tr>
													</table>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDDWS" name="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
													<div id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
														<div class="dxpc-mainDiv dxpc-shadow">
															<div class="dxpc-contentWrapper">
																<div class="dxeDropDownWindow_ZzapAqua dxpc-content">
																	<table class="dxgvControl_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv" border="0" style="border-width:0px;width:350px;border-collapse:collapse;border-collapse:separate;">
																		<tr>
																			<td>
																			<div class="dxgvCSD" style="height:200px;width:350px;overflow-x:hidden;overflow-y:auto;">
																				<table id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_DXMainTable" class="dxgvTable_ZzapAqua" cellspacing="0" cellpadding="0" onclick="aspxGVTableClick(&#39;ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv&#39;, event);" border="0" style="width:100%;border-collapse:collapse;empty-cells:show;table-layout:fixed;overflow:hidden;">
																					<tr>
																						<td style="width:50px;"></td><td style="width:300px;"></td>
																					</tr><tr id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_DXDataRow113" class="dxgvDataRow_ZzapAqua">
																						<td class="dxgvCommandColumn_ZzapAqua dxgv" align="center"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_DXSelBtn113" value="U" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxgv">(неизвестный)</td>
																					</tr>
																				</table>
																			</div>
																			<img id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_IADD" class="dxGridView_gvDragAndDropArrowDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="|" style="position:absolute;visibility:hidden;top:-100px;" /><img id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_IADU" class="dxGridView_gvDragAndDropArrowUp_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="|" style="position:absolute;visibility:hidden;top:-100px;" /><img id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_IDHF" class="dxGridView_gvDragAndDropHideColumn_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="Скрыть" style="position:absolute;visibility:hidden;top:-100px;" />
																			<table id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_DXStatus" class="dxgvStatusBar_ZzapAqua" border="0" style="width:100%;">
																				<tr class="dxgv">
																					<td id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_tcStatusBar" style="width:100%;">
																						<table width="100%">
																							<tr>
																								<td>
																									<table class="dxeBase_ZzapAqua dxeTAR" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_StatusBar_SelectAllCheckBox_0" border="0" style="border-collapse:collapse;">
																										<tr>
																											<td class="dxichCellSys"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_StatusBar_SelectAllCheckBox_0_S" name="ctl00$BodyPlace$UserPageControl$VinRegionGridLookup$DDD$gv$StatusBar$TC$SelectAllCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxichTextCellSys"><label>Выбрать все</label></td>
																										</tr>
																									</table>
																								</td>
																								<td class="float-right">
																									<div class="dxbButton_ZzapAqua dxbButtonSys dxbTSys" id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_StatusBar_CloseButton_0" style="-moz-user-select:none;">
																										<div class="dxb">
																											<input class="dxb-hb" value="Закрыть" type="submit" name="ctl00$BodyPlace$UserPageControl$VinRegionGridLookup$DDD$gv$StatusBar$TC$CloseButton" /><span class="dx-vam">Закрыть</span>
																										</div>
																									</div>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																			<div id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
																				<table class="dxgvLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="height:100%;border-collapse:collapse;">
																					<tr>
																						<td class="dx"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_TL">Загрузка&hellip;</span></td>
																					</tr>
																				</table>
																			</div>
																			<div id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_LD" class="dxgvLoadingDiv_ZzapAqua" style="display:none;z-index:29999;position:absolute;">

																			</div>
																			<table id="ctl00_BodyPlace_UserPageControl_VinRegionGridLookup_DDD_gv_DXStyleTable" border="0" style="display:none;">
																				<tr>
																					<td></td>
																				</tr><tr class="dxgvEditingErrorRow_ZzapAqua">
																					<td class="dxgv" data-colSpan="2"></td>
																				</tr><tr class="dxgvSelectedRow_ZzapAqua">
																					<td></td>
																				</tr><tr class="dxgvFocusedRow_ZzapAqua">
																					<td></td>
																				</tr><tr class="dxgvFocusedGroupRow_ZzapAqua">
																					<td></td>
																				</tr><tr>
																					<td class="dxgvBatchEditCell_ZzapAqua dxgv"></td><td class="dxgvBatchEditModifiedCell_ZzapAqua dxgv"></td><td class="dxgvBatchEditCell_ZzapAqua dxgvBatchEditModifiedCell_ZzapAqua dxgv"></td>
																				</tr>
																			</table></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
													<span id="ctl00_BodyPlace_UserPageControl_VinRegionValidator" style="color:Red;font-weight:bold;display:none;"></span>
												</td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td><span class="f12">Е-мейл для VIN-запросов:</span></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td class="f-grey">
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrEmailVinEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailVinEdit&#39;)" value="addr@mail.com" type="text" /></td>
														</tr>
													</table>
													Можно несколько через точку-запятую, например: mail1@mail.com; mail2@mail.com<br />
													<span id="ctl00_BodyPlace_UserPageControl_AddrEmailVinValidator" style="color:Red;font-weight:bold;display:none;"></span>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<span class="dxichCellSys dxeBase_ZzapAqua f-darkgray dxeDisabled_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserVCheckBox"><span class="dxWeb_edtCheckBoxUncheckedDisabled_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserVCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserVCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Услуга активирована. (Для пробного периода, свяжитесь с нами!)</label></span>
												</td>
											</tr>
										</table>
										<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserGCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserGCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserGCheckBox" value="I" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Принимаем заказы по e-mail</label></span>
										<br />
										<table id="ctl00_BodyPlace_UserPageControl_MailTable">
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td><span class="f12">Е-мейл для заказов:</span></td>
											</tr>
											<tr id="ctl00_BodyPlace_UserPageControl_OrderEmailTr">
												<td>&nbsp;</td>
												<td class="f-grey">
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrEmailOrderEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailOrderEdit&#39;)" value="addr@mail.com" type="text" /></td>
														</tr>
													</table>
													Можно несколько через точку-запятую, например: mail1@mail.com; mail2@mail.com<br />
													<span id="ctl00_BodyPlace_UserPageControl_AddrEmailOrderValidator" style="color:Red;font-weight:bold;display:none;"></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="f18 right">Адрес сайта
									</td>
									<td class="f18">
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrWebEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrWebEdit_Raw" value="" />
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrWebEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrWebEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrWebEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrWebEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrWebEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrWebEdit&#39;)" value="http://www.ваш-сайт.com" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrWebValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18" colspan="2">
										<br />
										Местоположение и телефоны магазина
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;&nbsp;&nbsp;&nbsp;Город*
									</td>
									<td>
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
											<table class="dxeLoadingPanel_ZzapAqua dxlpLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="font-size:18px;height:100%;border-collapse:collapse;">
												<tr>
													<td class="dx" style="font-size:18px;"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_TL">Загрузка&hellip;</span></td>
												</tr>
											</table>
										</div>
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_LD" class="dxeLoadingDiv_ZzapAqua dxlpLoadingDiv_ZzapAqua dx-ft" style="left:0px;top:0px;z-index:29999;display:none;position:absolute;">
										</div>
										<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua f18" cellspacing="1" cellpadding="0" SelectedValue="0" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo" border="0" style="font-size:18px;height:33px;width:290px;">
											<tr>
												<td style="display:none;"><input id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_VI" name="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_VI" type="hidden" value="0" /></td><td class="dxic" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_I" name="ctl00$BodyPlace$UserPageControl$AddrCodeCityCombo" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo&#39;)" value="(нет)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
											</tr>
										</table>
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDDWS" name="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
											<div class="dxpc-mainDiv dxpc-shadow" style="font-size:18px;">
												<div class="dxpc-contentWrapper">
													<div class="dxpc-content">
														<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_LDeletedItems" name="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_LDeletedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_LInsertedItems" name="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_LInsertedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_LCustomCallback" name="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_LCustomCallback" value="" /><table class="dxeListBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_L" border="0" style="font-size:18px;border-collapse:collapse;border-collapse:separate;">
															<tr>
																<td valign="top"><div id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_L_D" class="dxlbd" style="width:100%;overflow:auto;">
																	<input id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_L_VI" type="hidden" name="ctl00$BodyPlace$UserPageControl$AddrCodeCityCombo$DDD$L" /><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;border-collapse:separate;visibility:hidden!important;display:none!important;">
																		<tr id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_L_LBI-1" class="dxeListBoxItemRow_ZzapAqua">
																			<td id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_L_LBII" class="dxeListBoxItem_ZzapAqua">&nbsp;</td>
																		</tr>
																	</table><table id="ctl00_BodyPlace_UserPageControl_AddrCodeCityCombo_DDD_L_LBT" cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;border-collapse:separate;">
																		<tr class="dxeListBoxItemRow_ZzapAqua">
																			<td class="dxeListBoxItem_ZzapAqua">(нет)</td>
																		</tr>
																	</table>
																</div></td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
										<span id="ctl00_BodyPlace_UserPageControl_AddrCodeCityValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">Метро
									</td>
									<td class="f18">
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
											<table class="dxeLoadingPanel_ZzapAqua dxlpLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="font-size:18px;height:100%;border-collapse:collapse;">
												<tr>
													<td class="dx" style="font-size:18px;"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_TL">Загрузка&hellip;</span></td>
												</tr>
											</table>
										</div>
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_LD" class="dxeLoadingDiv_ZzapAqua dxlpLoadingDiv_ZzapAqua dx-ft" style="left:0px;top:0px;z-index:29999;display:none;position:absolute;">

										</div>
										<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua f18" cellspacing="1" cellpadding="0" SelectedValue="0" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo" border="0" style="font-size:18px;height:33px;width:290px;">
											<tr>
												<td style="display:none;"><input id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_VI" name="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_VI" type="hidden" value="0" /></td><td class="dxic" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_I" name="ctl00$BodyPlace$UserPageControl$AddrCodeMetroCombo" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo&#39;)" value="(нет)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
											</tr>
										</table>
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDDWS" name="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
										<div id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
											<div class="dxpc-mainDiv dxpc-shadow" style="font-size:18px;">
												<div class="dxpc-contentWrapper">
													<div class="dxpc-content">
														<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_LDeletedItems" name="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_LDeletedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_LInsertedItems" name="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_LInsertedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_LCustomCallback" name="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_LCustomCallback" value="" /><table class="dxeListBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_L" border="0" style="font-size:18px;border-collapse:collapse;border-collapse:separate;">
															<tr>
																<td valign="top"><div id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_L_D" class="dxlbd" style="width:100%;overflow:auto;">
																	<input id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_L_VI" type="hidden" name="ctl00$BodyPlace$UserPageControl$AddrCodeMetroCombo$DDD$L" /><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;border-collapse:separate;visibility:hidden!important;display:none!important;">
																		<tr id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_L_LBI-1" class="dxeListBoxItemRow_ZzapAqua">
																			<td id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_L_LBII" class="dxeListBoxItem_ZzapAqua">&nbsp;</td>
																		</tr>
																	</table><table id="ctl00_BodyPlace_UserPageControl_AddrCodeMetroCombo_DDD_L_LBT" cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;border-collapse:separate;">
																		<tr class="dxeListBoxItemRow_ZzapAqua">
																			<td class="dxeListBoxItem_ZzapAqua">(нет)</td>
																		</tr>
																	</table>
																</div></td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="f18 right">Телефон&nbsp;1*
									</td>
									<td>
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont1Edit" border="0" style="font-size:18px;height:33px;width:30px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont1Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelCodeCont1Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCont1Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCont1Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">(
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit" border="0" style="font-size:18px;height:33px;width:45px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelCodeCity1Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Edit&#39;)" value="код" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">)
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrTel1Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrTel1Edit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTel1Edit" border="0" style="font-size:18px;height:33px;width:100px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTel1Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTel1Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTel1Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTel1Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrTel1Edit&#39;)" value="номер" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">доб.
												</td>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelAdd1Edit" border="0" style="font-size:18px;height:33px;width:50px;border-collapse:collapse;">
														<tr>
														<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelAdd1Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelAdd1Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelAdd1Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelAdd1Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont1Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity1Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTel1Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelAdd1Validator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;&nbsp;&nbsp;&nbsp;Телефон&nbsp;2&nbsp;
									</td>
									<td>
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont2Edit" border="0" style="font-size:18px;height:33px;width:30px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont2Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelCodeCont2Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCont2Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCont2Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">(
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit" border="0" style="font-size:18px;height:33px;width:45px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelCodeCity2Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Edit&#39;)" value="код" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">)
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrTel2Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrTel2Edit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTel2Edit" border="0" style="font-size:18px;height:33px;width:100px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTel2Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTel2Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTel2Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTel2Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrTel2Edit&#39;)" value="номер" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">доб.
												</td>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrTelAdd2Edit" border="0" style="font-size:18px;height:33px;width:50px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrTelAdd2Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrTelAdd2Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelAdd2Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrTelAdd2Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCont2Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelCodeCity2Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTel2Validator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrTelAdd2Validator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18" colspan="2">Дополнительные е-мейл адреса &nbsp; &nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18 right top">&nbsp;</td>
									<td class="f-grey">
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrEmail2Edit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrEmail2Edit_Raw" value="" /><table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrEmail2Edit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrEmail2Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrEmail2Edit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmail2Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmail2Edit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrEmail2Edit&#39;)" value="addr@mail.com" type="text" /></td>
											</tr>
										</table>
										Можно несколько через точку-запятую, например: mail1@mail.com; mail2@mail.com
										<br />
										<span id="ctl00_BodyPlace_UserPageControl_AddrEmail2Validator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">ICQ
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrIcqEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrIcqEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrIcqEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrIcqEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrIcqEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrIcqValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">Skype
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrSkypeEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrSkypeEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrSkypeEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrSkypeEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrSkypeEdit&#39;)" type="text" /></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="f18">&nbsp;
									</td>
									<td class="f18">&nbsp;
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div style="display:none;">
						<div>
							<table cellpadding="4">
								<tr>
									<td width="30%">&nbsp;
									</td>
									<td>&nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18 right">Адрес магазина
									</td>
									<td>&nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;&nbsp;Индекс
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrIndexEdit" border="0" style="font-size:18px;height:33px;width:162px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrIndexEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrIndexEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrIndexEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrIndexEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrIndexEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrIndexValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Страна
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrCountryEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrCountryEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrCountryEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCountryEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCountryEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrCountryEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrCountryValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Область/край
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrAreaEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrAreaEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrAreaEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrAreaEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrAreaEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrAreaEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrAreaValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;Город/нас.пункт
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrCityEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrCityEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrCityEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCityEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrCityEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrCityEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrCityValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">
										<div id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
											<table class="dxeLoadingPanel_ZzapAqua dxlpLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="font-size:18px;height:100%;border-collapse:collapse;">
												<tr>
													<td class="dx" style="font-size:18px;"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_TL">Загрузка&hellip;</span></td>
												</tr>
											</table>
										</div>
										<div id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_LD" class="dxeLoadingDiv_ZzapAqua dxlpLoadingDiv_ZzapAqua dx-ft" style="left:0px;top:0px;z-index:29999;display:none;position:absolute;">

										</div>
										<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua f18" cellspacing="1" cellpadding="0" DataTextField="class_street_type" DataValueField="code_street_type" SelectedValue="0" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo" border="0" style="font-size:18px;height:33px;">
											<tr>
												<td style="display:none;"><input id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_VI" name="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_VI" type="hidden" value="0" /></td><td class="dxic" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_I" name="ctl00$BodyPlace$UserPageControl$AddrStreetTypeCombo" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo&#39;)" value="(выбрать)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
											</tr>
										</table>
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDDWS" name="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
										<div id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
											<div class="dxpc-mainDiv dxpc-shadow" style="font-size:18px;">
												<div class="dxpc-contentWrapper">
													<div class="dxpc-content">
														<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_LDeletedItems" name="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_LDeletedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_LInsertedItems" name="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_LInsertedItems" value="" /><input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_LCustomCallback" name="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_LCustomCallback" value="" /><table class="dxeListBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_L" border="0" style="font-size:18px;border-collapse:collapse;border-collapse:separate;">
															<tr>
																<td valign="top"><div id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_L_D" class="dxlbd" style="width:100%;overflow:auto;">
																	<input id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_L_VI" type="hidden" name="ctl00$BodyPlace$UserPageControl$AddrStreetTypeCombo$DDD$L" /><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;border-collapse:separate;visibility:hidden!important;display:none!important;">
																		<tr id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_L_LBI-1" class="dxeListBoxItemRow_ZzapAqua">
																			<td id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_L_LBII" class="dxeListBoxItem_ZzapAqua">&nbsp;</td>
																		</tr>
																	</table><table id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeCombo_DDD_L_LBT" cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;border-collapse:separate;">
																		<tr class="dxeListBoxItemRow_ZzapAqua">
																			<td class="dxeListBoxItem_ZzapAqua">(выбрать)</td>
																		</tr>
																	</table>
																</div></td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
									</td>
									<td valign="middle">
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrStreetEdit" border="0" style="font-size:18px;height:33px;width:360px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrStreetEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrStreetEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrStreetEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrStreetTypeValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrStreetValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Дом/корп.(стр.)
									</td>
									<td>
										<table class="m0 p0">
											<tr>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrHouseEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrHouseEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrHouseEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrHouseEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrHouseEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrHouseEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrHouseEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrHouseEdit&#39;)" value="дом" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">/</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit_Raw" value="" /><table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit" border="0" style="font-size:18px;height:33px;width:100px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrSubHouseEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrSubHouseEdit&#39;)" value="корп.(стр.)" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrHouseValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrSubHouseValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Квартира, офис
									</td>
									<td>
										<table>
											<tr>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrFlatEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrFlatEdit_Raw" value="" /><table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrFlatEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrFlatEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrFlatEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrFlatEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrFlatEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrFlatEdit&#39;)" value="кв." type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">,
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrSubFlatEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrSubFlatEdit&#39;)" value="офис" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrFlatValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_AddrSubFlatValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="right">&nbsp; &nbsp;Координаты на карте
									</td>
									<td>
										<table>
											<tr>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua dxeReadOnly_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrMapGeo1Edit" border="0" style="width:180px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrMapGeo1Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrMapGeo1Edit" readonly="readonly" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrMapGeo1Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrMapGeo1Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
												<td>
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua dxeReadOnly_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrMapGeo2Edit" border="0" style="width:180px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrMapGeo2Edit_I" name="ctl00$BodyPlace$UserPageControl$AddrMapGeo2Edit" readonly="readonly" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrMapGeo2Edit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrMapGeo2Edit&#39;)" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<span class="dxichCellSys dxeBase_ZzapAqua dxeReadOnly_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUser2GCheckBox" style="font-size:12px;display:none;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUser2GCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUser2GCheckBox" value="U" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Маркер был установлен клиентом</label></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="center" colspan="2">
										<img id="ctl00_BodyPlace_UserPageControl_ctl94" src="i/information.png" alt="Информация" style="display:none;" />
										<span class="dxeBase_ZzapAqua" id="ctl00_BodyPlace_UserPageControl_AddressVerificationInfoLabel" style="color:Red;display:none;"></span>
										<div id="map_canvas" style="height: 300px; width: 100%;">
										</div>
										<br />
									</td>
								</tr>
								<tr>
									<td class="f18 right"><a class="f18" href="javascript:void(0);" onclick="OnAddressesClick(this)">Дополнительные<br />
										адреса...</a>
									</td>
									<td>
										<div class="dxpnlControl_ZzapAqua" id="ctl00_BodyPlace_UserPageControl_AddressesCallbackPanel">
											<span class="dxeBase_ZzapAqua f14b" id="ctl00_BodyPlace_UserPageControl_AddressesCallbackPanel_AddressesLabel"></span>
										</div>
										<div id="ctl00_BodyPlace_UserPageControl_AddressesCallbackPanel_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
											<table class="dxpnlLoadingPanelWithContent_ZzapAqua dxlpLoadingPanelWithContent_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="height:100%;border-collapse:collapse;">
												<tr>
													<td class="dx"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_AddressesCallbackPanel_TL">Загрузка&hellip;</span></td>
												</tr>
											</table>
										</div>
										<div id="ctl00_BodyPlace_UserPageControl_AddressesCallbackPanel_LD" class="dxpnlLoadingDivWithContent_ZzapAqua dxlpLoadingDivWithContent_ZzapAqua dx-ft" style="left:0px;top:0px;z-index:29999;display:none;position:absolute;">
										</div>
									</td>
								</tr>
								<tr>
									<td class="f18 right">
										<span id="ctl00_BodyPlace_UserPageControl_AddrWorkTimeLabel0">Время работы</span>
									</td>
									<td>
										<table class="dxeMemoSys dxeMemo_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrWorkTimeEdit" border="0" style="font-size:14pt;height:80px;width:290px;border-collapse:collapse;border-collapse:separate;">
											<tr>
												<td style="width:100%;"><textarea class="dxeMemoEditArea_ZzapAqua dxeMemoEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrWorkTimeEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrWorkTimeEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrWorkTimeEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrWorkTimeEdit&#39;)" cols="" rows="" style="height:80px;width:100%;"></textarea></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_AddrWorkTimeLabel1">Например: &quot;С 9:00 до 20:00, вск. - выходной &quot;</span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">Дополнительная информация
									</td>
									<td>
										<table class="dxeMemoSys dxeMemo_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CommentEdit" border="0" style="font-size:14pt;height:80px;width:290px;border-collapse:collapse;border-collapse:separate;">
											<tr>
												<td style="width:100%;"><textarea class="dxeMemoEditArea_ZzapAqua dxeMemoEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CommentEdit_I" name="ctl00$BodyPlace$UserPageControl$CommentEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CommentEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CommentEdit&#39;)" cols="" rows="" style="height:80px;width:100%;"></textarea></td>
											</tr>
										</table>
										Здесь можно указать дополнительную контактную информацию
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;
									</td>
									<td class="f18">&nbsp;
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div style="display:none;">
						<div>
							<table cellpadding="4">
								<tr>
									<td width="30%">&nbsp;
									</td>
									<td>&nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18" colspan="2">Реквизиты юридического лица &nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Наименование полное
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyNameEdit" border="0" style="font-size:18px;height:33px;width:360px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyNameEdit_I" name="ctl00$BodyPlace$UserPageControl$CompanyNameEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyNameEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyNameEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyNameValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; ИНН*
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyInnEdit" border="0" style="font-size:18px;height:33px;width:162px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyInnEdit_I" name="ctl00$BodyPlace$UserPageControl$CompanyInnEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyInnEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyInnEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyInnValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; 
										<span class="dxeBase_ZzapAqua right" id="ctl00_BodyPlace_UserPageControl_CompanyKppEgrpouLabel" style="font-size:18px;"></span>
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyKppEgrpouTextBox" border="0" style="font-size:18px;height:33px;width:162px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyKppEgrpouTextBox_I" name="ctl00$BodyPlace$UserPageControl$CompanyKppEgrpouTextBox" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyKppEgrpouTextBox&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyKppEgrpouTextBox&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyKppEgrpouValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Банк*
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyBankEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyBankEdit_I" name="ctl00$BodyPlace$UserPageControl$CompanyBankEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyBankEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyBankEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyBankValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Счет*
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyAccEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyAccEdit_I" name="ctl00$BodyPlace$UserPageControl$CompanyAccEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyAccEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyAccEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyAccValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; 
										<span class="dxeBase_ZzapAqua right" id="ctl00_BodyPlace_UserPageControl_CompanyKorrAccCertNoLabel" style="font-size:18px;"></span>
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyKorrAccCertNoTextBox" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyKorrAccCertNoTextBox_I" name="ctl00$BodyPlace$UserPageControl$CompanyKorrAccCertNoTextBox" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyKorrAccCertNoTextBox&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyKorrAccCertNoTextBox&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyKorrAccCertNoValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; 
										<span class="dxeBase_ZzapAqua right" id="ctl00_BodyPlace_UserPageControl_CompanyBikMfoLabel" style="font-size:18px;"></span>
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_CompanyBikMfoTextBox" border="0" style="font-size:18px;height:33px;width:162px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_CompanyBikMfoTextBox_I" name="ctl00$BodyPlace$UserPageControl$CompanyBikMfoTextBox" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyBikMfoTextBox&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_CompanyBikMfoTextBox&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_CompanyBikMfoValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; ФИО Директора*
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegNameBossEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegNameBossEdit_I" name="ctl00$BodyPlace$UserPageControl$LegNameBossEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegNameBossEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegNameBossEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegNameBossValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;
									</td>
									<td>&nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18" colspan="2">&nbsp;Юридический адрес &nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp;&nbsp;Индекс
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrIndexEdit" border="0" style="font-size:18px;height:33px;width:162px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrIndexEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrIndexEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrIndexEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrIndexEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrIndexValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Страна
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrCountryEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrCountryEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrCountryEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrCountryEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrCountryEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrCountryValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Область/край
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrAreaEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrAreaEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrAreaEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrAreaEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrAreaEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrAreaValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Город/насел. пункт
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrCityEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrCityEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrCityEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrCityEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrCityEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrCityValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">
										<table class="dxeButtonEditSys dxeButtonEdit_ZzapAqua f18" cellspacing="1" cellpadding="0" DataTextField="class_street_type" DataValueField="code_street_type" SelectedValue="0" id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo" border="0" style="font-size:18px;height:33px;">
											<tr>
												<td style="display:none;"><input id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_VI" name="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_VI" type="hidden" value="0" /></td><td class="dxic" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo&#39;, event)" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_I" name="ctl00$BodyPlace$UserPageControl$LegAddrStreetTypeCombo" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo&#39;)" onchange="aspxETextChanged(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo&#39;)" value="(выбрать)" type="text" /></td><td id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_B-1" class="dxeButton dxeButtonEditButton_ZzapAqua" onmousedown="return aspxDDDropDown(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo&#39;, event)" style="-moz-user-select:none;"><img id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_B-1Img" class="dxEditors_edtDropDown_ZzapAqua" src="/DXR.axd?r=1_19-TxfV9" alt="v" /></td>
											</tr>
										</table>
										<input type="hidden" id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDDWS" name="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDDWS" value="0:0:-1:-10000:-10000:0:0:0:1:0:0:0" />
										<div id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDD_PW-1" class="dxpcDropDown_ZzapAqua dxpclW dxpc-ddSys" style="width:0px;cursor:default;z-index:10000;display:none;">
											<div class="dxpc-mainDiv dxpc-shadow" style="font-size:18px;">
												<div class="dxpc-contentWrapper">
													<div class="dxpc-content">
														<table class="dxeListBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDD_L" border="0" style="font-size:18px;border-collapse:collapse;border-collapse:separate;">
															<tr>
																<td valign="top">
																	<div id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDD_L_D" class="dxlbd" style="width:100%;overflow:auto;">
																		<input id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDD_L_VI" type="hidden" name="ctl00$BodyPlace$UserPageControl$LegAddrStreetTypeCombo$DDD$L" />
																		<table id="ctl00_BodyPlace_UserPageControl_LegAddrStreetTypeCombo_DDD_L_LBT" cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;border-collapse:separate;">
																			<tr class="dxeListBoxItemRow_ZzapAqua">
																				<td class="dxeListBoxItem_ZzapAqua">(выбрать)</td>
																			</tr>
																		</table>
																	</div>
																</td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
									</td>
									<td>
										<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrStreetEdit" border="0" style="font-size:18px;height:33px;width:360px;border-collapse:collapse;">
											<tr>
												<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrStreetEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrStreetEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrStreetEdit&#39;)" type="text" /></td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrStreetValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Дом/корп.(стр.)
									</td>
									<td>
										<table class="m0 p0">
											<tr>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit_Raw" name="ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrHouseEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_LegAddrHouseEdit&#39;)" value="дом" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">/
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit_Raw" name="ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrSubHouseEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_LegAddrSubHouseEdit&#39;)" value="корп.(стр.)" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrHouseValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrSubHouseValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18 right">&nbsp; Комната, офис
									</td>
									<td>
										<table>
											<tr>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit_Raw" name="ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrFlatEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_LegAddrFlatEdit&#39;)" value="комната" type="text" /></td>
														</tr>
													</table>
												</td>
												<td class="f18">,
												</td>
												<td>
													<input type="hidden" id="ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit_Raw" name="ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit_Raw" value="" />
													<table class="dxeTextBoxSys dxeTextBox_ZzapAqua" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit" border="0" style="font-size:18px;height:33px;width:95px;border-collapse:collapse;">
														<tr>
															<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit_I" name="ctl00$BodyPlace$UserPageControl$LegAddrSubFlatEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_LegAddrSubFlatEdit&#39;)" value="офис" type="text" /></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrFlatValidator" style="color:Red;font-weight:bold;display:none;"></span>
										<span id="ctl00_BodyPlace_UserPageControl_LegAddrSubFlatValidator" style="color:Red;font-weight:bold;display:none;"></span>
									</td>
								</tr>
								<tr>
									<td class="f18">&nbsp;
									</td>
									<td>&nbsp;
									</td>
								</tr>
								<tr>
									<td class="f18">&nbsp;
									</td>
									<td>&nbsp;
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div style="display:none;">
						<div>
							<table width="100%">
								<tr>
									<td>
										<br />
										<iframe id="Documents" src="/user/userinfodocuments.aspx?code_user=2147478127&hash=31DA53FA71D7D6369A463954989E7020&add=1" scrolling="auto" frameborder="0" width="100%" height="500px"></iframe>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div style="display:none;">
						<div>
							<center>
								<table class="left">
									<tr>
										<td class="f18">
											<table class="dxeBase_ZzapAqua dxeTAR" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_NotifyPricePublishedCheckBox" border="0" style="font-size:18px;border-collapse:collapse;">
												<tr>
													<td class="dxichCellSys"><span class="dxWeb_edtCheckBoxChecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_NotifyPricePublishedCheckBox_S" name="ctl00$BodyPlace$UserPageControl$NotifyPricePublishedCheckBox" value="C" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxichTextCellSys"><label>Оповещать о публикации прайс-листа</label></td>
												</tr>
											</table>
											<br />
											<table class="dxeBase_ZzapAqua dxeTAR" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_NotifyPriceOutdatedCheckBox" border="0" style="font-size:18px;border-collapse:collapse;">
												<tr>
													<td class="dxichCellSys"><span class="dxWeb_edtCheckBoxChecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_NotifyPriceOutdatedCheckBox_S" name="ctl00$BodyPlace$UserPageControl$NotifyPriceOutdatedCheckBox" value="C" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxichTextCellSys"><label>Оповещать об устаревшем прайс-листе</label></td>
												</tr>
											</table>
											<br />
											<table class="dxeBase_ZzapAqua dxeTAR" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_NotifyStatCheckBox" border="0" style="font-size:18px;border-collapse:collapse;">
												<tr>
													<td class="dxichCellSys"><span class="dxWeb_edtCheckBoxChecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_NotifyStatCheckBox_S" name="ctl00$BodyPlace$UserPageControl$NotifyStatCheckBox" value="C" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span></td><td class="dxichTextCellSys"><label>Присылать статистику</label></td>
												</tr>
											</table>
											<br />
											<span class="dxichCellSys dxeBase_ZzapAqua dxeTAR" id="ctl00_BodyPlace_UserPageControl_TypeUserHCheckBox" style="font-size:18px;"><span class="dxWeb_edtCheckBoxUnchecked_ZzapAqua dxICheckBox_ZzapAqua dxichSys"><input id="ctl00_BodyPlace_UserPageControl_TypeUserHCheckBox_S" name="ctl00$BodyPlace$UserPageControl$TypeUserHCheckBox" value="U" type="text" readonly="readonly" style="border-width:0;width:0;height:0;padding:0;margin:0;position:relative;background-color:transparent;display:block;" /></span><label>Не присылать напоминания &#39;оставить отзыв о покупателе&#39;</label></span>
											<br />
											<br />
										</td>
									</tr>
								</table>
								<table class="left">
									<tr>
										<td class="f18" colspan="2">Е-мейл для оповещений
										</td>
									</tr>
									<tr>
										<td class="f18">&nbsp;</td>
										<td class="f-grey">
											<input type="hidden" id="ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit_Raw" name="ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit_Raw" value="" />
											<table class="dxeTextBoxSys dxeTextBox_ZzapAqua f18" cellspacing="0" cellpadding="0" id="ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit" border="0" style="font-size:18px;height:33px;width:290px;border-collapse:collapse;">
												<tr>
													<td class="dxic" style="width:100%;"><input class="dxeEditArea_ZzapAqua dxeEditAreaSys" id="ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit_I" name="ctl00$BodyPlace$UserPageControl$AddrEmailNotifyEdit" onfocus="aspxEGotFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit&#39;)" onblur="aspxELostFocus(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit&#39;)" onchange="aspxEValueChanged(&#39;ctl00_BodyPlace_UserPageControl_AddrEmailNotifyEdit&#39;)" value="addr@mail.com" type="text" /></td>
												</tr>
											</table>
											Можно несколько через точку-запятую, например: mail1@mail.com; mail2@mail.com
											<br />
											<span id="ctl00_BodyPlace_UserPageControl_AddrEmailNotifyValidator" style="color:Red;font-weight:bold;display:none;"></span>
										</td>
									</tr>
								</table>
							</center>
							<br />
						</div>
					</div>
				</div>
			</div>
			<b class="dx-clear"></b>
			<div id="ctl00_BodyPlace_UserPageControl_LP" align="center" style="left:0px;top:0px;z-index:30000;display:none;">
				<table class="dxtcLoadingPanel_ZzapAqua dxlpLoadingPanel_ZzapAqua" cellspacing="0" cellpadding="0" border="0" style="height:100%;border-collapse:collapse;">
					<tr>
						<td class="dx"><img class="dxlp-loadingImage dxlp-imgPosTop" src="/DXR.axd?r=1_19-TxfV9" alt="" align="middle" /><br /><span id="ctl00_BodyPlace_UserPageControl_TL">Загрузка&hellip;</span></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
