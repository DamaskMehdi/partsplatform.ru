<h1>Личный кабинет</h1>
<div class="content" style="margin: 0 0 50px 0px;">
	<dl class="tabs2">
	{:if $page->user->getField('type') == 2:}
		<dt>&nbsp; <span><a href="/account/">Настройки магазина</a></span></dt>
		<dt class="selected" id="company">&nbsp; <span>Статистика транзакций</span></dt>
	
		<dd class="selected">
			<div class="tab-content">
				<div class="cabinet_form __left">
				{:if !empty($transactions_list):}
					<table class="t_register">
						<tr>
							<th>Операция</th>
							<th>IP клиента</th>
							<th>Сумма списания, руб</th>
							<th>Время списания</th>
						</tr>
						{:foreach $transactions_list as $tr:}
						<tr>
							<td>{:$tr.description:}</td>
							<td>{:$tr.ip_client:}</td>
							<td>{:$tr.amount:}</td>
							<td>{:$tr.date_create:}</td>
						</tr>
						{:/foreach:}
					</table>
				{:/if:}
				</div>
			</div>
		</dd>
	{:elseif $page->user->getField('type') == 1:}
		<dt>&nbsp; <span><a href="/account/">Настройки площадки</a></span></dt>
		<dt class="selected" id="company">&nbsp; <span>Статистика транзакций</span></dt>
	
		<dd class="selected">
			<div class="tab-content">
				<div class="cabinet_form __left">
				{:if !empty($transactions_list):}
					<table class="t_register">
						<tr>
							<th>Операция</th>
							<th>ID магазина</th>
							<th>Сумма пополнения, руб</th>
							<th>Время пополнения</th>
						</tr>
						{:foreach $transactions_list as $tr:}
						<tr>
							<td>{:$tr.description:}</td>
							<td>{:$tr.shop_id_from:}</td>
							<td>{:$tr.amount_platform:}</td>
							<td>{:$tr.date_create:}</td>
						</tr>
						{:/foreach:}
					</table>
				{:else:}
				Нет проведенных транзакций
				{:/if:}
				</div>
			</div>
		</dd>
	{:/if:}
	</dl>
</div>