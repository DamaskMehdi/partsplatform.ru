<h1>Личный кабинет</h1>
<div class="content">
	<dl class="tabs2">
	{:if $page->user->getField('type') == 2:}
		<dt class="selected">&nbsp; <span>Настройки магазина</span></dt>
		<dt id="company">&nbsp; <span><a href="/account/transactions">Статистика транзакций</a></span></dt>
	
		<dd class="selected">
			<form action="">
			<div class="tab-content">
				<div class="cabinet_form __left">
					<form>
						<table class="t_register">
							<tr>
								<td>Название:</td>
								<td><input type="text" name="name" id="name" value="{:$account_data.name:}"></td>
							</tr>
							<tr>
								<td>Цена за клик:</td>
								<td><input type="text" name="click_price" id="click_price" value="{:$account_data.click_price:}"></td>
							</tr>
							<tr>
								<td>Контактные телефоны:</td>
								<td><input type="text" name="phones" id="phones" value="{:$account_data.phones:}"></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" id="save_account_common" class="battery__form-button" value="Сохранить"></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			</form>
		</dd>
	{:elseif $page->user->getField('type') == 1:}
		<dt class="selected">&nbsp; <span>Настройки площадки</span></dt>
		<dt id="company">&nbsp; <span><a href="/account/transactions">Статистика транзакций</a></span></dt>
	
		<dd class="selected">
			<form action="">
			<div class="tab-content">
				<div class="cabinet_form __left">
					<form>
						<table class="t_register">
							<tr>
								<td>Адрес:</td>
								<td><input type="text" name="platform_url" id="platform_url" value="{:$account_data.platform_url:}"></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" id="save_account_common" class="battery__form-button" value="Сохранить"></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			</form>
		</dd>
	{:/if:}
	</dl>
</div>

<script>
$(function() {

	$('.shop_links a').click(function() {
		var link_number = $('.shop_links a').index(this);
		$('.shop_content div.shop_item').hide().eq(link_number).show();
		$('.shop_links a').removeClass('active');$(this).addClass('active');
		return false;
	});	
	
	$('#company_change_btn').click(function() {
		if ($('#company_form input[name=name]').val() == '') {
			alert('Введите название компании');
		} else if ($('#company_form input[name=inn]').val() == '') {
			alert('Введите ИНН');
		} else if ($('#company_form input[name=bank]').val() == '') {
			alert('Введите Банк');
		} else if ($('#company_form input[name=bill]').val() == '') {
			alert('Введите счет');
		} else if ($('#company_form input[name=bik]').val() == '') {
			alert('Введите БИК');
		} else if ($('#company_form input[name=fio_director]').val() == '') {
			alert('Введите ФИО Директора');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('form#company_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	$('.shop_change_btn').click(function() {
		var parent_form = $(this).parents('form.shop_form');
	
		if (parent_form.find('input[name=company_id]').val() == '') {
			alert('Для добавления магазина добавьте компанию');
		} else if (parent_form.find('input[name=city]').val() == '') {
			alert('Введите город');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: parent_form.serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	$('#add_document').click(function() {
		if ($('#document_form input[name=company_id]').val() == '') {
			alert('Для добавления документов добавьте компанию');
		} else if ($('#document_form #document_type_id').val() == '') {
			alert('Выберите тип документа');
		} else if ($('#document_form #document_file').val() == '') {
			alert('Выберите файл');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('#document_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	//$("select.dealer_mark_id").multiselect(); 
	//$("#accordion").accordion();
});
</script>