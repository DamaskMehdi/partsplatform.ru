{:*<h1>Регистрация</h1>*:}
<div class="content">
	{:if $isSeller:}<img src="/images/account/seller-teaser.png?2" id="seller-teaser">{:/if:}
	<div class="register_form __left">
		<form action="/" method="GET" id="register_account_form">
			<input type="hidden" name="af" value="a">
			<input type="hidden" name="action" value="register_account">
			{:if $isSeller:}<input type="hidden" name="seller" id="seller" value="1">
			{:else:}<input type="hidden" name="buyer" id="buyer" value="1">{:/if:}
			<table class="t_register">
				<tr>
					<td>Тип *:</td>
					<td><input type="radio" name="type" id="type" value="1" checked> Частное лицо &nbsp;&nbsp;&nbsp; <input type="radio" name="type" id="type" value="2"> Компания</td>
				</tr>
				<tr>
					<td>Имя *:</td>
					<td><input type="text" name="first_name" id="first_name" value=""></td>
				</tr>
				<tr>
					<td>Регион *:</td>
					<td>
						<select name="region_id" id="region_id">
							<option value="0"> выбрать </option>
							{:foreach from=$regions_list item=region:}
							<option value="{:$region.id:}">{:$region.name:}</option>
							{:/foreach:}
						</select>
					</td>
				</tr>
				{:*
				<tr>
					<td>Страна:</td>
					<td>
						<input type="text" name="country" id="country" value="">
					</td>
				</tr>
				<tr>
					<td>Город/нас.пункт:</td>
					<td><input type="text" name="city" id="city" value=""></td>
				</tr>
				<tr>
					<td>Улица:</td>
					<td>
						<select name="street_type" id="street_type" style="width:20%;">
							{:foreach from=$category_values.4.values item=value:}
							<option value="{:$value.name:}" {:if $company->getAddress()->getField('street_type') == $value.name:}selected{:/if:}>{:$value.name:}</option>
							{:/foreach:}
						</select>
						&nbsp;<input type="text" name="street" id="street" value="" style="width:68%">
					</td>
				</tr>
				<tr>
					<td>Дом/корп.(стр.):</td>
					<td><input type="text" name="house" id="house" value="" size="5" maxlength="5" style="width:40px"> / <input type="text" name="building" id="building" value="" size="5" maxlength="5" style="width:40px"></td>
				</tr>
				<tr>
					<td>Квартира, офис:</td>
					<td><input type="text" name="flat" id="flat" value="" size="5" maxlength="5" style="width:40px"> / <input type="text" name="office" id="office" value="" size="5" maxlength="5" style="width:40px"></td>
				</tr>
				*:}
				<tr>
					<td>Телефон:</td>
					<td>
						<input type="text" name="cc" id="cc" value="" maxlength="4" size="4" style="width:40px;"> ( <input type="text" name="oc" id="oc" value="" maxlength="4" size="4" style="width:40px;"> ) <input type="text" name="number" id="number" value="" maxlength="9" size="9" style="width:100px;"> доб. <input type="text" name="addnumber" id="addnumber" value="" maxlength="3" size="3" style="width:40px;">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="checkbox" name="allow_view_contacts" id="allow_view_contacts" value="1">
						<label>Разрешить просмотр контактов продавцам</label>
					</td>
				</tr>
				<tr>
					<td>Email *:</td>
					<td><input type="text" name="email" id="email" value=""></td>
				</tr>
				<tr>
					<td>Пароль *:</td>
					<td><input type="password" name="password" id="password" value=""></td>
				</tr>
				<tr>
					<td>Повторно пароль *:</td>
					<td><input type="password" name="password2" id="password2" value=""></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="button" id="register_account" class="battery__form-button" value="Зарегистрироваться"></td>
				</tr>
			</table>
			{:*
				<div style="float:left;width:50%;">
					<input type="hidden" id="location" name="location" value="">
					<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
					<script src="/js/map.js" type="text/javascript"></script>
					<div>
						<button id="ya-search">Отметить на карте</button> &nbsp;
						<span style="font-size:10px;width:390px;line-height:1em;padding-top:4px;">Если адрес определился неверно, перетащите метку в нужное место при помощи мыши</span>
					</div>
					<div id="map" style="width:100%; height:430px;margin-top:15px;"></div>
				</div>
			*:}
		</form>
	</div>
	<div class="register_information __left">
		<p>Создание учетной записи поможет покупать быстрее. Вы сможете контролировать состояние заказа, а также просматривать заказы сделанные ранее. Вы сможете накапливать призовые баллы и получать скидочные купоны.</p>
		<p>А постоянным покупателям мы предлагаем гибкую систему скидок и персональное обслуживание.</p>
		<p>Если у Вас уже есть учетная запись, вы можете <a href="/account/enter" class="b">авторизоваться на сайте</a></p>
	</div>
</div>
<script>
$(function() {

	$('#register_account').click(function() {
		if ($('#type:checked').val() == undefined) {
			alert('Выберите тип');
		} else if ($('#first_name').val() == '') {
			alert('Заполните имя');
		} else if ($('#last_name').val() == '') {
			alert('Заполните фамилию');
		} else if ($('#patronymic').val() == '') {
			alert('Заполните отчество');
		} else if ($('#region_id').val() == 0) {
			alert('Выберите регион');
		} else if ($('#email').val() == '') {
			alert('Заполните email');
		} else if ($('#password').val() == '' || $('#password2').val() == '') {
			alert('Заполните поля пароля');
		} else if ($('#password').val() != $('#password2').val()) {
			alert('Пароли не совпадают');
		} else {

			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('form#register_account_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Вы успешно зарегистрировались! Теперь вы можете войти под своим email и паролем');
						window.location.href = "/";
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Ошибка при регистирации');
						}
					}
				}
			});
		}

		return false;
	});

});
</script>