<h1>Личный кабинет</h1>
<div class="content">
	<dl class="tabs2">
		<dt><i class="fa fa-info-circle"></i> &nbsp; <span><a href="/account/main">Общая информация</a></span></dt>
		<dt id="company"><i class="fa fa-users"></i> &nbsp; <span><a href="/account/company">Компания</a></span></dt>
		<dt class="selected"><i class="fa fa-file-text-o"></i> &nbsp; <span>Документы</span></dt>
		<dt><i class="fa fa-cloud-upload"></i> &nbsp; <span><a href="/account/import_parts">Импорт прайса</a></span></dt>
		<dt><i class="fa fa-upload"></i> &nbsp; <span><a href="/account/import_tires">Импорт шин</a></span></dt>
		<dt><i class="fa fa-upload"></i> &nbsp; <span><a href="/account/import_cross">Импорт кроссов</a></span></dt>

		<dd class="selected">
			<div class="tab-content">
				<div class="cabinet_form __left">	
					<form id="document_form">
						<input type="hidden" name="af" value="a">
						<input type="hidden" name="action" value="add_document">
						<input type="hidden" name="company_id" value="{:$company->getId():}">
						<table class="t_register">
							<tr>
								<td>Тип документа:</td>
								<td>
									<select name="document_type_id" id="document_type_id">
										<option value=""> выбрать </option>
										{:foreach from=$category_values.5.values item=value:}
										<option value="{:$value.value:}">{:$value.name:}</option>
										{:/foreach:}
									</select>
								</td>
							</tr>
							<tr>
								<td>Файл для загрузки :</td>
								<td><input type="file" name="document_file" id="document_file" value=""></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="checkbox" name="allow_view_document" id="allow_view_document" value="1" {:if $page->user->getField('allow_view_document'):}checked{:/if:}>
									<label for="allow_view_document">Разрешить просмотр документа покупателям</label>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="button" id="add_document" class="battery__form-button" value="Загрузить документ"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="cabinet_information __left">
					<p><strong>Документы компании</strong></p>
					<p>Поля отмеченные "*" обязательные для заполнения</p>
				</div>
				{:* Доработка *:}
				{:*if $page->user->getDocuments():}
					<table class="t_register">
						<tr>
							<th>№</th>
							<th>Тип</th>
							<th>Имя файла</th>
							<th>Документ доступен для покупателей</th>
						</tr>
						{:foreach from=$company->getDocuments() item=document :}
						<tr>
							<td>1</td>
							<td>{:$document.type:}</td>
							<td>{:$document.filename:}</td>
							<td>
								<input type="checkbox" name="allow_view_document" id="allow_view_document" value="1" {:if $document.allow_view_document:}checked{:/if:}>
							</td>
						</tr>
						{:/foreach:}
						<tr>
							<td></td>
							<td><input type="button" id="save_account_common" class="battery__form-button" value="Изменить"></td>
						</tr>
					</table>
				{:else:}
					Нет загруженных документов
				{:/if*:}
			</div>
		</dd>
	</dl>
</div>

<script>
$(function() {

	$('.shop_links a').click(function() {
		var link_number = $('.shop_links a').index(this);
		$('.shop_content div.shop_item').hide().eq(link_number).show();
		$('.shop_links a').removeClass('active');$(this).addClass('active');
		return false;
	});	
	
	$('.delete_import_settings').click(function() {
		if (confirm('Вы действительно хотите удалить схему импорта?')) {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: { af: 'a', action: 'delete_import_settings', is_id: $(this).data('id') },
				success: function(data) {
					if (data.success) {
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});	
	
	$('.upload_user_pricelist_btn').click(function() {
		var form = $(this).parent();
		var price_file = form.find('[name=user_pricelist]');
	
		/*if ($('#pricelist_to_shop').val() == 0) {
			alert('Выберите магазин');
		} else*/ if ($('.loader_block').is(':visible')) {
			alert('Ожидайте окончания импорта файла');
		} else if (price_file.val() == '') {
			alert('Выберите файл');
		} else {
			$('.loader_block').show();		
			form.ajaxSubmit({
				dataType: 'json',
				success: function(data) {
					$('.loader_block').hide();
					if (data.success) {
						alert('Прайс-лист загружен на сервер, в течении нескольких минут он будет загружен в систему');
						window.location.reload();
					} else {
						alert('Произошла ошибка при загрузке прайс-листа');
					}
				},
				error: function() {
					$('.loader_block').hide();
					alert('Произошла ошибка при загрузке прайс-листа');
				}
			});
		}
	});
	
	$('#company_change_btn').click(function() {
		if ($('#company_form input[name=name]').val() == '') {
			alert('Введите название компании');
		} else if ($('#company_form input[name=inn]').val() == '') {
			alert('Введите ИНН');
		} else if ($('#company_form input[name=bank]').val() == '') {
			alert('Введите Банк');
		} else if ($('#company_form input[name=bill]').val() == '') {
			alert('Введите счет');
		} else if ($('#company_form input[name=bik]').val() == '') {
			alert('Введите БИК');
		} else if ($('#company_form input[name=fio_director]').val() == '') {
			alert('Введите ФИО Директора');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('form#company_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	$('.shop_change_btn').click(function() {
		var parent_form = $(this).parents('form.shop_form');
	
		if (parent_form.find('input[name=company_id]').val() == '') {
			alert('Для добавления магазина добавьте компанию');
		} else if (parent_form.find('input[name=city]').val() == '') {
			alert('Введите город');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: parent_form.serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	$('#add_document').click(function() {
		if ($('#document_form input[name=company_id]').val() == '') {
			alert('Для добавления документов добавьте компанию');
		} else if ($('#document_form #document_type_id').val() == '') {
			alert('Выберите тип документа');
		} else if ($('#document_form #document_file').val() == '') {
			alert('Выберите файл');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('#document_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});
	
	//$("select.dealer_mark_id").multiselect(); 
	//$("#accordion").accordion();
});
</script>