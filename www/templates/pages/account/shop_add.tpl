<h1>Добавить магазин</h1>
{:if $page->user->getField('seller'):}
{:if $company->isExist():}
<div class="tab-content">
	<div class="cabinet_form __left">
		<div class="shop_content">
				<div class="shop_item">
					<form class="shop_form">
						<input type="hidden" name="af" value="a">
						<input type="hidden" name="action" value="add_shop">
						<input type="hidden" name="company_id" value="{:$company->getId():}">
					
						<table class="t_register">
							<tr>
								<td>Название магазина:</td>
								<td><input type="text" name="name" id="name" value="{:$shop->getField('name'):}"></td>
							</tr>
							<tr>
								<td>Почтовый индекс:</td>
								<td><input type="text" name="post_index" id="post_index" value="{:$shop->getAddress()->getField('post_index'):}"></td>
							</tr>
							<tr>
								<td>Страна:</td>
								<td>
									<input type="text" name="country" id="country" value="{:$shop->getAddress()->getField('country'):}">
								</td>
							</tr>
							<tr>
								<td>Область/край:</td>
								<td><input type="text" name="area" id="area" value="{:$shop->getAddress()->getField('area'):}"></td>
							</tr>
							<tr>
								<td>Город/нас.пункт:</td>
								<td><input type="text" name="city" id="city" value="{:$shop->getAddress()->getField('city'):}"></td>
							</tr>
							<tr>
								<td>Улица:</td>
								<td>
									<select name="street_type" id="street_type" style="width:20%">
										<option value=""> выбрать </option>
										{:foreach from=$category_values.4.values item=value:}
										<option value="{:$value.name:}" {:if $shop->getAddress()->getField('street_type') == $value.name:}selected{:/if:}>{:$value.name:}</option>
										{:/foreach:}
									</select>
									&nbsp;<input type="text" name="street" id="street" value="{:$shop->getAddress()->getField('street'):}" style="width:70%">
								</td>
							</tr>
							<tr>
								<td>Дом/корп.(стр.):</td>
								<td><input type="text" name="house" id="house" value="{:$shop->getAddress()->getField('house'):}" size="5" maxlength="5" style="width:40px"> / <input type="text" name="building" id="building" value="{:$shop->getAddress()->getField('building'):}" size="5" maxlength="5" style="width:40px"></td>
							</tr>
							<tr>
								<td>Квартира, офис:</td>
								<td><input type="text" name="flat" id="flat" value="{:$shop->getAddress()->getField('flat'):}" size="5" maxlength="5" style="width:40px"> / <input type="text" name="office" id="office" value="{:$shop->getAddress()->getField('office'):}" size="5" maxlength="5" style="width:40px"></td>
							</tr>
							<tr>
								<td>Телефон 1:</td>
								<td>
									<input type="text" name="cc[]" value="{:$shop->getField('cc0'):}" maxlength="4" size="4" style="width:40px"> ( <input type="text" name="oc[]" value="{:$shop->getField('oc0'):}" maxlength="4" size="4" style="width:40px"> ) <input type="text" name="number[]" value="{:$shop->getField('number0'):}" maxlength="9" size="9" style="width:100px"> доб. <input type="text" name="addnumber[]" value="{:$shop->getField('addnumber0'):}" maxlength="3" size="3" style="width:40px">
								</td>
							</tr>
							<tr>
								<td>Телефон 2:</td>
								<td>
									<input type="text" name="cc[]" value="{:$shop->getField('cc1'):}" maxlength="4" size="4" style="width:40px"> ( <input type="text" name="oc[]" value="{:$shop->getField('oc1'):}" maxlength="4" size="4" style="width:40px"> ) <input type="text" name="number[]" value="{:$shop->getField('number1'):}" maxlength="9" size="9" style="width:100px"> доб. <input type="text" name="addnumber[]" value="{:$shop->getField('addnumber1'):}" maxlength="3" size="3" style="width:40px">
								</td>
							</tr>
							<tr>
								<td>Email магазина:</td>
								<td><input type="text" name="email" id="email" value="{:$shop->getField('email'):}"></td>
							</tr>
							<tr>
								<td>ICQ:</td>
								<td><input type="text" name="icq" id="icq" value="{:$shop->getField('icq'):}"></td>
							</tr>
							<tr>
								<td>Skype:</td>
								<td><input type="text" name="skype" id="skype" value="{:$shop->getField('skype'):}"></td>
							</tr>
							<tr>
								<td>Время работы:</td>
								<td><input type="text" name="work_time" id="work_time" value="{:$shop->getField('work_time'):}"></td>
							</tr>
							<tr>
								<td>Дополнительная информация:</td>
								<td><textarea name="description" id="description" style="width:100%">{:$shop->getField('description'):}</textarea></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="button" class="shop_change_btn battery__form-button" value="{:if $shop->isExist():}Сохранить{:else:}Добавить{:/if:}"></td>
							</tr>
						</table>
					</form>
				</div>
		</div>
	</div>
	<div class="cabinet_information __left">
		<p><strong>Информация о магазинах</strong></p>
		<p>Поля отмеченные "*" обязательные для заполнения</p>
	</div>
	<div class="clear"></div>
</div>
{:else:}
Необходимо создать компанию для добавления магазина
{:/if:}
{:else:}
Вы не можете добавить магазин
{:/if:}

<script>
$(function() {

	$('#company_change_btn').click(function() {
		if ($('#company_form input[name=name]').val() == '') {
			alert('Введите название компании');
		} else if ($('#company_form input[name=inn]').val() == '') {
			alert('Введите ИНН');
		} else if ($('#company_form input[name=bank]').val() == '') {
			alert('Введите Банк');
		} else if ($('#company_form input[name=bill]').val() == '') {
			alert('Введите счет');
		} else if ($('#company_form input[name=bik]').val() == '') {
			alert('Введите БИК');
		} else if ($('#company_form input[name=fio_director]').val() == '') {
			alert('Введите ФИО Директора');
		} else {
			$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: $('form#company_form').serialize(),
				success: function(data) {
					if (data.success) {
						alert('Сохранено');
						window.location.reload();
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Не удалось сохранить');
						}
					}
				}
			});
		}
	});

});
</script>