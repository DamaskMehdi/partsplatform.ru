<!doctype html>
<!--[if IE 7 ]><html lang="ru-RU" class="ie78 ie7"><![endif]-->
<!--[if IE 8 ]><html lang="ru-RU" class="ie78 ie8"><![endif]-->
<!--[if IE 9 ]><html lang="ru-RU" class="ie9">
<![endif]--><!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ru-RU"><!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>{:$page->title|replace:'{REGION}':$page->getRegion()->getField('city_to'):}</title>
	{:if $page->meta_description != '':}<meta name="description" content="{:$page->meta_description|replace:'"':'':}" />{:/if:}
	{:if $page->meta_keywords != '':}<meta name="keywords" content="{:$page->meta_keywords|replace:'"':'':}" />{:/if:}

	{:*<base href='https://atr.ru/'>*:}
	{:if $page->getStyles():}
		{:foreach from=$page->getStyles() item=style:}<link rel="stylesheet" TYPE="text/css" href="{:$style:}">{:/foreach:}
	{:/if:}
	{:*<link rel="shortcut icon" href="favicon.ico" /> *:}
	<link rel="stylesheet" href="/css/font-awesome.min.css">
	<script src="https://yastatic.net/jquery/2.1.1/jquery.min.js"></script>
	{:*<script src="/js/jquery.js"></script>
	<script src="/js/jquery-migrate.js"></script>*:}
	{:if $page->getScripts():}
		{:foreach from=$page->getScripts() item=script:}<script type="text/javascript" src="{:$script:}"></script>{:/foreach:}
	{:/if:}
	<script type="text/javascript">
		site_url = '{:$CONFIG.SITEURL:}';
	</script>
</head>
<body ng-app="app">
    <main role="main" style="padding: 0px;">
        <div style="padding: 0px;">
			{:include file="blocks/header.tpl":}
		</div>

		<div>
		{:include file="pages/$page_template":}
		</div>
    </main>
    <footer>
        <div>{:include file="blocks/footer.tpl":}</div>
    </footer>
<!--script src="/js/libs/angular.min.js"></script>
<script src="/js/libs/angular-route.js"></script>
<script src="/js/atr.min.js"></script-->
<script>$.scrollUp({'location': 'right','image_src': '/images/arrow_up.png','wait': 100,'time': 300,});</script>
{:*Yandex*:}<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter33183913 = new Ya.Metrika({ id:33183913, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/33183913" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
{:*Google*:}<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-69176502-1', 'auto');ga('send', 'pageview');</script>
<script type='text/javascript'>(function(){ var widget_id = 'wxGnstmxnU';var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
</body>
</html>