{:if $page->user->isAuthorized():}
	<a href="/account/balance"><span>Баланс: {:$page->user->getField('balance'):}</span></a>
	<a href="/account/"><span>{:$page->user->getEmail():}</span></a>
	<a href="/account/logout" id="logout_btn"><span>Выход</span></a>
{:else:}
	<a href="/account/enter" class="key"><span>Вход</span></a>
	<!--a href="/account/register"><span>Регистрация</span></a-->
{:/if:}