<?php
//session_start();
if($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die('WRONG REQUEST!');
//include('../libs/includes.php');

$json = new Response_Json();
$user = new User();
$input = $_REQUEST;

if (!empty($input['action'])) {

	switch ($input['action']) {
		
		case 'show_shop_contacts':
			if (!empty($input['shop_id'])) {
				$shop = new Model_Shop();
				$contacts = $shop->getContacts($input['shop_id'], $_SERVER['HTTP_X_REAL_IP']);
				$json->setData($contacts);
			} else {
				$json->setMessage('Ошибка');
			}
			break;

	
		case 'upload_user_pricelist':
			//ini_set('memory_limit', '512M');
			set_time_limit(0);

			try {
				$shop_id = !empty($input['pricelist_to_shop_id']) ? $input['pricelist_to_shop_id'] : 0;
				if (!ImportMod_Transfer_Helper::uploadFileByUser($input['settings_id'], $shop_id)) {
					$json->setMessage('Ошибка при импорте файла!');
				} elseif (!empty($input['upload_type']) && $input['upload_type'] == 'tires') {
					$sheduler = new ImportMod_Sheduler_Sheduler(array('import_settings_id' => $input['settings_id']), Environment::getDB('local'));
					if ($sheduler->isExist()) {
						$manager = new ImportMod_Sheduler_Manager(Environment::getDB('local'));
						if (!$manager->importItem($sheduler, true)) {
							$json->setMessage('Ошибка при импорте файла!');
						}
						$manager->close();
					} else {
						$json->setMessage('Ошибка при импорте файла!');
					}
				}
			} catch (Exception $exc) {
				$json->setMessage('Ошибка при импорте файла!');
			}
			break;

		case 'delete_import_settings':
			$settings = new ImportMod_Settings_Settings($input['is_id'], Environment::getDB('local'));
			if (!$settings->delete()) {
				$json->setMessage('Ошибка при удалении настроек!');
			}
			break;

		case 'edit_account':
			if ($user->edit($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении!');
			}
			break;

		case 'get_company_info':
			$md_company = new Model_Company(Environment::getDB('local'));
			if (!empty($input['article_id'])) {
				$item_id = $input['article_id'];
				$type = 'part';
			} elseif (!empty($input['tire_id'])) {
				$item_id = $input['tire_id'];
				$type = 'tire';
			} else {
				$item_id = 0;
				$type = 'unknown';
			}
			$price_id = !empty($input['price_id']) ? $input['price_id'] : '';
			$company_info = $md_company->getCompanyInfoHtml($input['company_id'], $item_id, $price_id, $type);
			if (!empty($company_info)) {
				$json->setData($company_info);
			} else {
				$json->setMessage('Не удалось получить информацию по компании!');
			}
			break;

		case 'add_company':
			$company = new Item_Company(array(), Environment::getDB('local'));
			if ($company->add($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении!');
			}
			break;

		case 'edit_company':
			$company = new Item_Company(array('id' => $input['id']), Environment::getDB('local'));
			if ($company->edit($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении!');
			}
			break;

		case 'add_shop':
			$shop = new Item_Shop(array(), Environment::getDB('local'));
			if ($shop->add($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении!');
			}
			break;

		case 'edit_shop':
			$shop = new Item_Shop(array('id' => $input['id']), Environment::getDB('local'));
			if ($shop->edit($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении!');
			}
			break;


		case 'add_import_setting':
			$settings = new ImportMod_Settings_Settings(array(), Environment::getDB('local'));
			if ($settings->add($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении настроек!');
			}
			break;

		case 'edit_import_setting':
			$settings = new ImportMod_Settings_Settings($input['id'], Environment::getDB('local'));
			if ($settings->replace($input)) {
				//$json->setMessage('Ошибка при сохранении настроек!');
			} else {
				$json->setMessage('Ошибка при сохранении настроек!');
			}
			break;

		case 'add_tires_import_setting':
			$settings = new ImportMod_Settings_Settings(array(), Environment::getDB('local'));
			if ($settings->add($input)) {

			} else {
				$json->setMessage('Ошибка при сохранении настроек!');
			}
			break;

		case 'edit_tires_import_setting':
			$settings = new ImportMod_Settings_Settings($input['id'], Environment::getDB('local'));
			if ($settings->replace($input)) {
				//$json->setMessage('Ошибка при сохранении настроек!');
			} else {
				$json->setMessage('Ошибка при сохранении настроек!');
			}
			break;

		case 'search_part_sphinx':
			/*$url = 'http://atr-catalog.by/json/sphinx/' . urlencode($input['text']);
			$data = json_decode(file_get_contents($url), true);d($url);*/

			$text = !empty($input['text']) ? $input['text'] : '';
			/*$search_part = new Model_SearchPart();
			$search_part->setDB(Environment::getDB('local'));*/
			$search_part = new Model_GlobalSearch();
			$result = $search_part->findPart($text);

			if (!empty($result)) {
				$json->setData($result);
			} else {
				$json->setMessage('Ничего не найдено!');
			}
			break;

		case 'register_account':
			// @TODO add validation
			$account = new Item_Account(array('email' => $input['email']), Environment::getDB('local'));
			if ($account->isExist()) {
				$json->setMessage('Пользователь уже зарегистирован!');
			} else {
				//$account = new Item_Account(array(), Environment::getDB('local'));
				if (!$account->add($input)) {
					$json->setMessage('Пользователь не зарегистирован!');
				}
			}
			break;

		case 'login_account':
			if ($user->login($input['email'], $input['password'])) {

			} else {
				$json->setMessage('Неверный email или пароль!');
			}
			break;

		case 'logout':
			$user->logout();
			break;

		case 'update_import_tire_brand':
			$tires = new Model_TiresConnection();
			$tires->mappingBrand($input['supplier_id'], $input['price_id'], $input['brand_id']);

			break;

		case 'update_import_tire_model':
			$tires = new Model_TiresConnection();
			$tires->mappingModel($input['supplier_id'], $input['price_id'], $input['model_id']);

			break;
			
		case 'connect_company_autoparts_brand':
			$tecdoc = new Model_TecdocNew();
			$tecdoc->connectBrandToCompanyBrand($input['company_id'], $input['brand_type'], $input['brand_company'], $input['brand_system_id']);
			break;

		default:
			$json->setMessage('Проверьте, пожалуйста, правильность заполнения полей!');
			break;
	}
} else {
	$json->setMessage('Неверные параметры');
}

$json->send()->stop();