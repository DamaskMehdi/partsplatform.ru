<?php

	header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	ini_set('upload_max_filesize', '30M');
	ini_set('post_max_size', '29M');

	if (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'getContacts') {
		$contacts = Environment::getModel('Account')->getShopContacts($_REQUEST['shop_id']);
		
		$data = array(
			'success' => true,
			'data' => array(
				'contacts' => $contacts,
			),
		);
		
		echo json_encode($data);
		exit();
	} elseif (!empty($_REQUEST['af'])) {
		include('actions.php');
		
	} elseif (!empty($_REQUEST['ft'])) {
		$view = new View_Poiskpovin();
		$view->getGroupArticlesPage(true);
	
	} elseif (isset($_REQUEST['podbor_akb'])) {
		$goods = new Model_Goods();
		if (isset($_REQUEST['type'])) {
			$data = $goods->getPodborAkbMarks($_REQUEST['type']);
		} elseif (!empty($_REQUEST['mark'])) {
			$data = $goods->getPodborAkbModels($_REQUEST['mark']);
		} elseif (!empty($_REQUEST['model'])) {
			$data = $goods->getPodborAkbModifications($_REQUEST['model']);
		} else {
			$data = array();
		}
		
		echo json_encode($data);
		exit();
	
	} elseif (isset($_REQUEST['makes']) || isset($_REQUEST['category'])) {
		# Обработка запросов по каталогу запчастей №2
		$url = '';
		
		if(isset($_REQUEST['category']) && isset($_REQUEST['url'])) $url = $_REQUEST['url'].'/'.$_REQUEST['category'];
		else {
			if(isset($_REQUEST['makes'])) $url = $_REQUEST['makes'];
			if(isset($_REQUEST['series'])) $url .= '/'.$_REQUEST['series'];
			if(isset($_REQUEST['years'])) $url .= '/'.$_REQUEST['years'];
			if(isset($_REQUEST['bodies'])) $url .= '/'.$_REQUEST['bodies'];
			if(isset($_REQUEST['engines'])) $url .= '/'.$_REQUEST['engines'];
			if(isset($_REQUEST['powers'])) $url .= '/'.$_REQUEST['powers'];
			if(isset($_REQUEST['models'])) $url .= '/'.$_REQUEST['models']; 
		}

		$xml = new Model_Xml($url);
		$content = $xml->getDataXML();
		
		if(isset($_REQUEST['category']) && isset($_REQUEST['url'])) {
			$html = "";
			if (!empty($content['category']['img'])) {
				$html = "<div id='cont'>
					<a class='zoom' href='http://pix2.atr-catalog.by/big/".$content['category']['img']."'><img src='http://pix2.atr-catalog.by/".$content['category']['img']."'></a>
					<span class='magnifier'></span>
				</div>";
			}
			$html .= '<div class="table border">
						<table width="100%" cellpadding="6" cellspacing="0">
							<tr>
								<th width="10%">Номер</th>
								<th width="20%">Артикул</th>
								<th width="60%">Название</th>
							</tr>';
			if(empty($content['data']))	$html .= '<tr><td colspan="3" align="center">Нет данных</td></tr>';
			else
			{
				foreach ($content['data'] as $value) {	
					$html .= '<tr><td>'.(string)$value['number'].'</td><td><a href="/catalog2/details/'.(string)$value['id'].'" rel="nofollow" target="_blank">'.(string)$value['article'].'</a></td><td>'.(string)$value['name'].'</td></tr>';	
				}
			}
			$html .= "</table></div>";
			echo $html;
			?>
				<script type="text/javascript" src="../js/jquery.zoomy.js"></script>
				<script type="text/javascript">$(function(){$('.zoom').zoomy({zoomSize: 150});});</script>
			<?php
		}
		else echo json_encode($content);
	}
	# Информация о запчасти
	elseif(!empty($_REQUEST['info']))
	{
		$json = new Model_Json('info:'.$_REQUEST['info']);
		$content = $json->getDataJSON();

		if(count($content)>1)
		{
			$html = "<div class='table border'>";
			if(!empty($content['properties']) && count($content['properties'])>0)
			{
				$html .= "<table width='500' cellpadding='3' cellspacing='0'><tr><th colspan='2'>Характеристики</th></tr>";
				foreach($content['properties'] as $val)
				{
					$html .= "<tr><td>".$val['title']."</td><td>".$val['value']."</td></tr>";
				}
				$html .= "</table>";
			}
			
			if(!empty($content['analogs']) && count($content['analogs'])>0)
			{
				$html .= "<table style='margin-top:15px;' width='500' cellpadding='3' cellspacing='0'><tr><th colspan='2'>Оригинальные номера, аналогом которых является эта деталь</th></tr>";
				foreach($content['analogs'] as $val)
				{
					$html .= "<tr><td>".$val['brand']."</td><td><a href='search/".$val['article']."' target='_blank'>".$val['article']."</a></td></tr>";
				}
				$html .= "</table>";
			}
			
			$html .= "</div>";
			echo $html;
		}
		else echo "Нет данных!";
	}
	# Вывод информации о бренде
	elseif(!empty($_REQUEST['brandinfo']))
	{
		if($_REQUEST['brandinfo']!='undefined') {
			$json = new Model_Json('brandinfo:'.$_REQUEST['brandinfo']);
			$content = $json->getDataJSON();
			//d($content);
			if(!empty($content['brand_info'])) {
				$html = '<div style="max-width:600px;"><table width="100%">
						 <tr>'.((!empty($content['brand_info']['image']))?'<td valign="top" style="padding-top:15px;"><img src="'.$content['brand_info']['image'].'"></td>':'').'
						 <td style="padding-left:15px" vlaign="top" class="description">
							<p class="font-16 b">'.$content['brand_info']['name'].'</p>
							<p>'.htmlspecialchars_decode(stripslashes($content['brand_info']['info'])).'</p>
							'.((!empty($content['brand_info']['url']))?'<p>Сайт компании: <a href="'.$content['brand_info']['url'].'" rel="nofollow" target="_blank">'.$content['brand_info']['url'].'</a></p>':'').'
							<p>Страна производитель: <b>'.$content['brand_info']['country'].'</b></p>
						</td></tr></table></div>';
				echo $html;
			} else echo "Нет информации о производителе";
		} else echo "Нет информации о производителе";
	}
	# Вывод информации под заказ (Froza.ru)
	elseif(!empty($_REQUEST['part']))
	{
		$json = new Model_Json('zakaz:'.$_REQUEST['part'].':'.$_REQUEST['brand']);
		$content = $json->getDataJSON();
		if(!empty($content['part']['article'])) {
			$html = '<strong class="font-16">'.$content['part']['brand'].' - '.$content['part']['article'].'</strong>
				<p style="margin-bottom:10px;max-width:300px;">'.$content['part']['title'].'</p>
				<table width="100%" cellpadding="0" cellspacing="0" class="tablesorter">
					<thead>
						<tr>
							<th class="{sorter: false}">Наличие</th>
							<th class="{sorter: false}">Цена, руб</th>
							<th class="{sorter: false}" width="80">Срок</th>
							<th width="100" class="{sorter: false}"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding-top:10px;"><span class="icon-ok green">'.(($content['part']['quantity']>0)?$content['part']['quantity']:'').'</span></td>
							<td style="padding-top:10px;">'.$content['part']['price'].'</td>
							<td style="padding-top:10px;">'.$content['part']['delivery_time'].' ('.$content['part']['delivery_time_guar'].') дн.</td>
							<td valign="middle"><input type="number" name="count" data-article="'.$content['part']['article'].'" data-brand="'.$content['part']['brand'].'" data-shop-id="'.$content['part']['shop_id'].'" data-supplier-id="'.$content['part']['supplier_id'].'" data-price="'.$content['part']['price'].'" value="1" class="ct_count tip-white" size="2" maxlength="3" min="1" max="99" title="Количество"> <button class="icon-basket btn add-to-cart-zakaz"></button></td>
						</tr>
					</tbody>
				</table><script>addToCart();</script>';
			echo $html;
		} else echo "Позвоните в отдел продаж";
	}
	elseif(!empty($_REQUEST['print'])) {
		$json = new Model_Json('fullinfo:'.$_REQUEST['url']);
		$content = $json->getDataJSON();
		
		$html = "<div class='print'>
			<h2>".$content['part']['title']." ".$content['part']['brand']." ".$content['part']['article']."</h2>
			<table width='100%'>
				<tr>
					<td><img src='".$content['part']['images']['full']."'></td>
					<td>";
		
		if(!empty($content['properties'])) {
			foreach($content['properties'] as $val) {
				$html .= "<p>".$val['title'].": <strong>".$val['value']."</strong></p>";
			}
		}
		
		$html = "	</td>
				</tr>
			</table>
			<hr>
			<table width='100%'>
				<tr>
					<td>
						<h3>ул. Маяковского, 2</h3>
						<p>
							666-60-29 ( Velcom,МТС,Life:) )<br>
							223-30-36 ( Городской )
						</p>
					</td>
					<td>
						<h3>ул. Притыцкого, 62/10</h3>
						<p>
							655-00-00 ( Velcom,МТС,Life:) )<br>
							380-31-42 ( Городской )
						</p>
					</td>
					<td>
						<h3>ул. Есенина, 76</h3>
						<p>
							660-38-11 ( Velcom,МТС,Life:) )<br>
							224-46-54 ( Городской )
						</p>
					</td>
				</tr>
			</table>
		</div>";
		echo $html;
	}
	elseif(!empty($_REQUEST['mobile'])) {
		$_SESSION['mobile'] = 1;
	} elseif (isset($_REQUEST['podbor_shin'])) {
		$db = Environment::getDB();
		$podbor = new Model_PodborShin($db);
		
		if (!empty($_REQUEST['brand'])) {
			$data = $podbor->getModels($_REQUEST['brand']);
		} elseif (!empty($_REQUEST['model'])) {
			$data = $podbor->getYears($_REQUEST['model']);
		} elseif (!empty($_REQUEST['year'])) {
			$data = $podbor->getModifications($_REQUEST['year']);
		} else {
			$data = array();
		}
		
		$db->close();
		
		echo json_encode($data);
		exit();
		
	} elseif (isset($_REQUEST['starter_models'])) {
		$db = Environment::getDB();
		
		$select = "select distinct md.*
			from stg_model md, stg_model_compat pmc, stg_product p
			where pmc.model_id = md.id and pmc.product_id = p.id and p.category_id = " . $_REQUEST['category_id'] . " and md.mark_id = " . $_REQUEST['mark'] . "
			order by md.name";
		
		$data = $db->query($select)->fetchAll();
		
		$db->close();
		
		echo json_encode($data);
		exit();
	} 
	elseif (isset($_REQUEST['action'])) 
	{
		$account = new Model_Account();
		$account->params = $_REQUEST;
		echo $account->getDataAkk();
	} 
	# Вывод запчастей определенной категории 
	 else {
		$json = new Model_Json($_REQUEST['url']);
		$content = $json->getDataJSON();
		
		if(!empty($content['parts']) && count($content['parts'])>0)
		{
			$html='<div>
					<table width="100%" cellpadding="0" cellspacing="0" class="tablesorter">
						<thead><tr>
							<th width="150" class="{sorter: false}">Фото</th>
							<th class="{sorter: false}">Описание</th>
							<th width="200">Цена, руб.</th>
						</tr></thead><tbody>';
			foreach($content['parts'] as $val)
			{
				if (!empty($val['article_id'])) {
					$link = '/catalog1/details/' . $val['article_id'] . (!empty($val['cross_id']) ? '/' . $val['cross_id'] : '');
				} else {
					$link = '';
				}
			
				$html .= '<tr>
							<td valign="top">
								<div class="detail-img">
									<noindex>'.((!empty($val['images']))?'<a class="fancybox" href="'.$val['images']['full'].'" rel="nofollow" title="'.$val['brand'].' '.$val['article'].'"><img src="'.$val['images']['small'].'" alt="'.$val['brand'].' '.$val['article'].'"></a><br>':'<span class="detail-img"><img src="/images/no-image.png" alt="Нет фото" title="Нет фото"></span>').'</noindex>
								</div>
							</td>
							<td valign="top">
								<img align="left" src="'.$val['country_logo'].'" alt="'.$val['country'].'" title="'.((!empty($val['country']))?$val['country']:"Неизвестно").'" class="coupic tip-white">
								<a class="various fancybox.ajax b country" rel="nofollow" href="?brandinfo='.((!empty($val['article_id']))?$val['article_id']:'undefined').'">'.$val['brand'].'</a>
								<div>
									Артикул: '.((!empty($val['article_id']))?'<a class="various fancybox.ajax tip b" href="?info='.$val['article_id'].'" title="Характеристики детали" rel="nofollow">'.$val['article'].'</a>':'<span class="b">'.$val['article'].'</span>').'<br/>
									'.(($val['title']!='')?'<span>'.$val['title'].'</span>':'').'
								</div>
							</td>';
							if(!empty($val['shops']))
							{
								$min_price = $max_price = 0;
								foreach ($val['shops'] as $shop) {
									if (empty($min_price) || $min_price > $shop['price_rub']) {
										$min_price = $shop['price_rub'];
									}
									if (empty($max_price) || $max_price < $shop['price_rub']) {
										$max_price = $shop['price_rub'];
									}
								}
							
								$shops_count = count($val['shops']);
								$html .= '<td align="center"><b>' . $min_price . '</b> <small>р.</small><b> - ' . $max_price . '</b> <small>р.</small></b>';
								if (!empty($val['shops'][0]['price_id'])) {
									$html .= '<br><a href="' . $link .'" class="label label-warning nobr">Cравнить предложения</a><br>
										<small>от ' . $shops_count . ' ' . Common_Helper::pluralTuning($shops_count, array("продавца", "продавцов", "продавцов")) . '</small>';
								}
								$html .= '</td>';
								
							} else {
								$html .= '<td align="center"><span class="hidden">999999999999</span>
											<a href="" class="tipped_ajax link" onclick="return false" data-tipped="/" data-querystring="part='.$val['number'].'&brand='.$val['brand'].'">Нет предложений.<br> Сообщить вам о появлении</a>
										</td>';
							}
				$html .= '</tr>';
			}
			$html .= '</tbody></table>';
			echo $html;
		}
		else echo htmlspecialchars_decode(stripslashes($content['text']));
	}