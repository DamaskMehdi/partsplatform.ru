<?php

class ParseUrl
{
	const PAGE_TYPE_MAIN = 'main';
	const PAGE_TYPE_MODULE = 'module';
	const PAGE_TYPE_CATEGORY = 'category';
	const PAGE_TYPE_AUTO_ITEM = 'auto_item';
	const PAGE_TYPE_ITEM = 'item';

	private $_site_structure;
	
	private $_original_uri;
	private $_original_domain;
	private $_uri_parts = array(
		'basic' => '',
		'get_params' => '',
	);
	private $_uri_array = array();
	
	
	private $_page_params = array(
		'sort' => array('date_start' => Common_Helper::SORT_FIELD_DESC),
		'uri' => '',
		'search_string' => '',
		'items_per_page' => 10,
		'is_exist' => true,
		'is_new_url' => true,
		'page_number' => 1,
		'redirect_url' => '',
	);
	
	private $_unidentified_uri;

	
	
	public function __construct() 
	{
		$this->_original_uri = trim(Environment::getServerVar('REQUEST_URI'), '/');
		$this->_original_domain = trim(Environment::getServerVar('SERVER_NAME'));

		$this->parse();
	}
	
	public function parse($uri = '')
	{
		if (!empty($uri)) {
			$this->_original_uri = $uri;
		}
		
		$this->_checkIncorrectUrl()
			->_separateGetParameters()
			->_parseSortLabel()
			->_defineStructure();
	}

	private function _checkIncorrectUrl()
	{
		if (preg_match("/([^a-zA-Z0-9\+\.\/\-\_\#’Ц\?=&%:\$])/", $this->_original_uri)) {
			throw new Exception_IncorrectUrl($this->_original_uri);
		}
		
		return $this;
	}
	
	private function _separateGetParameters()
	{
		$this->_original_uri = trim(str_replace(Config::getParam('DOMAIN'), '', $this->_original_uri), '/');
	
		if (mb_strpos($this->_original_uri, '?') !== false) {
			$this->_uri_parts['basic'] = mb_substr($this->_original_uri, 0, mb_strpos($this->_original_uri, '?'));
			$this->_uri_parts['get_params'] = mb_substr($this->_original_uri, mb_strpos($this->_original_uri, '?') + 1);
			$this->_page_params['search_string'] = $this->_uri_parts['get_params'];
		} else {
			$this->_uri_parts['basic'] = $this->_original_uri;
		}
		
		if (preg_match('~-id([0-9]+)$~', $this->_uri_parts['basic'], $matches)) {
			if (!empty($this->_uri_parts['get_params'])) {
				$this->_uri_parts['get_params'] .= '&id=' . $matches[1];
			} else {
				$this->_uri_parts['get_params'] = 'id=' . $matches[1];
			}
			Environment::setGetVar('id', $matches[1]);
			$this->_uri_parts['basic'] = str_replace($matches[0], '', $this->_uri_parts['basic']);
		}
		
		
		$this->_unidentified_uri = $this->_uri_parts['basic'];
		
		return $this;
	}
	
	private function _parseSortLabel()
	{
		if (!empty ($this->_uri_parts['get_params'])) {
			$order_pattern = "/" . Common_Helper::SORT_LABEL . "=([a-zA-Z0-9\.\/\-\_\#Ц\?=%:]+)/";
			if (preg_match($order_pattern, $this->_uri_parts['get_params'], $order_value)) {
				$this->_page_params['sort'] = array();
				foreach (explode(Common_Helper::SORT_FIELD_DELIMITER, $order_value[1]) as $item) {
					$field = substr($item, 0, strrpos($item, Common_Helper::SORT_DELIMITER));
					$dir = substr($item, strrpos($item, Common_Helper::SORT_DELIMITER) + 1);
					$this->_page_params['sort'][$field] = $dir;
				}
			}
		}

		return $this;
	}

	
	private function _overrideGetParameters()
	{
		if (mb_strpos($this->_original_uri, '?') !== false) {
			$search_string = mb_substr($this->_original_uri, mb_strpos($this->_original_uri, '?') + 1);
			parse_str($search_string, $get_array);
		}
	
		if (!empty($get_array)) {
			Environment::setGet($get_array);
		} else {
			Environment::setGet(array());
		}
	
		return $this;
	}
	
	private function _defineStructure()
	{
	
		$this->_defineRegion()
			->_definePageTypeName()
			->_definePageValues();
			
		$this->_page_params['uri'] = $this->_unidentified_uri;
	}
	
	
	private function _defineRegion()
	{
		$region = new Structure_Region(array('domain' => $this->_original_domain));
		if (!$region->isExist()) {
			$region = new Structure_Region(array('domain' => Config::getParam('DOMAIN')));
		}
		
		Environment::setRegion($region);
		Config::setParam('SITEURL', 'https://' . $region->getField('domain') . '/');
		
		return $this;
	}

	
	private function _definePageTypeName()
	{
		$this->_uri_array = explode('/', $this->_uri_parts['basic']);
		/*$page = Structure_FactoryPage::create($this->_uri_parts['basic']);

		if ($page->isExist()) {
			Environment::setPage($page);
			$this->_addToPageUri($page->getField('uri'));
		} else {
			throw new Exception('Undefined page ' . $this->_uri_parts['basic']);
		}
*/
		return $this;
	}

	
	private function _definePageValues() 
	{
		$this->_definePageNumber()
			->_defineItemsPerPage();
		
		return $this;
	}
	
	private function _definePageNumber()
	{
		$found_page_number = false;
		if (preg_match('~page([0-9]+)~', $this->_unidentified_uri, $page)) {
			$found_page_number = true;
			
			$this->setPage($page[1]);
			$this->_removeFromIdentified($page[0]);
		}
			
		if (preg_match('~([&]?)page=([0-9]+)([&]?)~', $this->_uri_parts['get_params'], $page)) {
			if (!$found_page_number) {
				$this->setPage($page[2]);
			}
			
			if (empty($page[3]) || empty($page[1])) {
				$this->_page_params['search_string'] = str_replace($page[0], '', $this->_page_params['search_string']);
			} else {
				$this->_page_params['search_string'] = str_replace(substr($page[0], 1), '', $this->_page_params['search_string']);
			}
		}
		
		return $this;
	}
	
	private function _defineItemsPerPage()
	{
		if (preg_match('~[/|^]perpage([0-9]+)$~', $this->_unidentified_uri, $page)) {
			$this->setItemsPerPage($page[1]);
			
			$this->_addToPageUri($page[0]);
			$this->_removeFromIdentified($page[0]);
		} else {
			$this->setItemsPerPage(Constants::ITEMS_PER_PAGE_DEFAULT);
		}
		
		return $this;
	}
	
	
	private function _removeFromIdentified($remove_value)
	{
		$this->_unidentified_uri = trim(preg_replace('~[/]{2,}~', '/', Common_Helper::str_replace_once($remove_value, '', $this->_unidentified_uri)), '/');

		return $this;
	}
	
	private function _addToPageUri($add_value)
	{
		$this->_page_params['uri'] .= '/' . $add_value;
		
		return $this;
	}

	
	public function getModuleName() 
	{
		return isset($this->_uri_array[0]) ? $this->_uri_array[0] : null; 
	}
	
	public function getCategoryName() 
	{
		return isset($this->_uri_array[1]) ? $this->_uri_array[1] : null; 
	}
	
	public function getUriArray() 
	{
		return $this->_uri_array; 
	}
	
	public function getSearchString() 
	{
		return !empty($this->_uri_parts['get_params']) ? $this->_page_params['search_string'] : ''; 
	}

	public function getPageUri() 
	{
		return $this->_page_params['uri'];
	}
	
	public function getPageFullUri() 
	{
		return $this->_page_params['uri'] . (!empty($this->_page_params['search_string']) ? '?' . $this->_page_params['search_string'] : '');
	}
	
	public function getSort()
	{
		return $this->_page_params['sort'];
	}
	
	// rename to getPageNumber or getCurrentPage
	public function getPage() 
	{
		return $this->_page_params['page_number'];
	}
	
	public function setPage($page_number) 
	{
		if (is_numeric($page_number) && $page_number > 0) {
			if ($page_number > Paginator::PAGE_NUMBER_MAX) {
				$this->_page_params['page_number'] = Paginator::PAGE_NUMBER_MAX;
			} else {
				$this->_page_params['page_number'] = $page_number;
			}
		}
	}
	
	public function getPerPage() 
	{
		return $this->_page_params['items_per_page'];
	}
	
	public function setItemsPerPage($items_per_page) 
	{
		$this->_page_params['items_per_page'] = $items_per_page;
	}
	
	public function getPageType() 
	{
		return $this->_page_params['page_type'];
	}
	
	public function getRedirectUrl() 
	{
		return $this->_page_params['redirect_url'];
	}
	
	public function isNewUrl() 
	{
		return empty($this->_old_page);
	}
}
