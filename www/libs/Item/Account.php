<?php

class Item_Account extends Item_Abstract
{
	const TABLE = 'atrru.parts_accounts';
	
	const ROLE_ADMIN = 3;
	const ROLE_MANAGER = 2;
	const ROLE_USER = 1;
	const ROLE_UNKNOWN = 0;
	
	const PHONE_ADD_LABEL = ' доб. ';
	

	protected $_table = self::TABLE;

	protected $_fields = array(
		'id',
		'email',
		'pwd',
		'type',
		'wallet_address',
		'balance',
	);

	private $_password;
	
	protected function _afterAdd($success)
	{
		if ($success) {
			// @TODO set up Mail Sender
			//Mail_Sender::sendActivation($this->_data['email'], $this->_password);
		}

		return $success;
	}
	
	protected function _afterEdit($success)
	{
				
		return $success;
	}

	
	protected function _beforeAdd()
	{
		$this->_createPassword();

	}
	
	protected function _beforeEdit()
	{

	}

	
	private function _createPassword()
	{
		$this->_password = $this->_data['password'];
		
		$this->_data['password'] = md5($this->_data['password']);
		
		return $this;
	}
	
	
	public function getField($field)
	{
		$phone_fields = array('cc', 'oc', 'number', 'addnumber');

		if (in_array($field, $phone_fields)) {
			$value = '';
			if (!empty($this->_data['contact_phone'])) {
				if ($field == 'cc') {
					$value = substr($this->_data['contact_phone'], 0, strpos($this->_data['contact_phone'], '('));
				} elseif ($field == 'oc') {
					$value = substr($this->_data['contact_phone'], strpos($this->_data['contact_phone'], '(') + 1, strpos($this->_data['contact_phone'], ')') - strpos($this->_data['contact_phone'], '(') - 1);
				} elseif ($field == 'number') {
					if (strpos($this->_data['contact_phone'], self::PHONE_ADD_LABEL) !== false) {
						$value = substr($this->_data['contact_phone'], strpos($this->_data['contact_phone'], ')') + 1, strpos($this->_data['contact_phone'], self::PHONE_ADD_LABEL) - strpos($this->_data['contact_phone'], ')') - 1);
					} else {
						$value = substr($this->_data['contact_phone'], strpos($this->_data['contact_phone'], ')') + 1);
					}
				} elseif ($field == 'addnumber') {
					if (strpos($this->_data['contact_phone'], self::PHONE_ADD_LABEL) !== false) {
						$value = substr($this->_data['contact_phone'], strpos($this->_data['contact_phone'], self::PHONE_ADD_LABEL) + strlen(self::PHONE_ADD_LABEL));
					}
				}
			}
		} else {
			$value = parent::getField($field);
		}
		
		return $value;
	}
	
	protected function _convertFields()
	{
		
	}
}