<?php

abstract class Item_Abstract
{

	/**
	 * Connection to DB
	 * @var DBClassHelper 
	 */
	private $_db;

	/**
	 * Name of the basic table
	 * @var string 
	 */
	protected $_table;
	
	/**
	 * Name of the primary field 
	 * @var string 
	 */
	protected $_primary_key_id = 'id';
	
	/**
	 * Allow insert Primary key value while add Object
	 * @var string 
	 */
	protected $_allow_add_primary_key = false;
	
	/**
	 * List of Item fields
	 * @var array 
	 */
	protected $_fields = array();
	
	/**
	 * List of Item fields which is in customfields fields
	 * @var array 
	 */
	protected $_customfields = array();
	
	/**
	 * List of Item fields which couldn't be updated (for 'replace' method)
	 * @var array 
	 */
	protected $_non_edit_fields = array();
	
	/**
	 * Data of the Object
	 * @var array 
	 */
	protected $_data;
	
	/**
	 * Last inserted Id
	 * @var int 
	 */
	private $_last_inserted_id;
	
	
	
	
	
	public function __construct($criteria = array(), $db = null)
	{
		$this->_checkCorrectObject();
	
		$this->_setDB($db);
		$this->_create($criteria);
	}
	
	/**
	 * Init object data
	 * @param array|int $criteria
	 */
	protected function _create($criteria)
	{
		$this->_clearObject();
	
		if (empty($criteria)) {
			return true;
		}

		if (is_numeric($criteria)) {
			$criteria = array($this->getPrimaryKeyField() => $criteria);
		}
		
		$this->_beforeCreate();
		$data = $this->_getObjectDataFromDB($criteria);
		$this->init($data);
	
		return $this->isExist();
	}
	
	public function init($data)
	{
		$this->_data = $data;
		
		if ($this->isExist()) {
			$this->_unserializeCustomfields()
				->_convertFields();
			$this->_afterCreate(true);
		} else {
			$this->_clearObject();
		}
		
		return $this->isExist();
	}
	
	public function add($data)
	{
		$this->_data = $data;
		if (!$this->_allow_add_primary_key) {
			$this->_clearPrimaryKeyDataField();
		}
	
		$success = $this->_beforeAdd();
		if ($success !== false) {
			$this->_serializeCustomfields()
				->_filterData();

			$success = $this->_insert();
			$this->_addConnectedResources($success);
			
			if ($success) {
				$this->_create($this->getInsertedId());
			}
			
			$this->_afterAdd($success);
		} else {
			$success = false;
		}
		
		return $success;
	}
	
	public function replace($data)
	{
		$fields_empty_data = array_flip($this->_fields);
		foreach ($this->_customfields as $customfield_name => $fields) {
			unset($fields_empty_data[$customfield_name]);
			$fields_empty_data = array_merge($fields_empty_data, array_flip($fields));
		}
		foreach ($fields_empty_data as $field => $value) {
			if (!in_array($field, $this->_non_edit_fields)) {
				$fields_empty_data[$field] = '';
			} else {
				unset($fields_empty_data);
			}
		}
		unset($fields_empty_data[$this->getPrimaryKeyField()]);
		$data = array_merge($fields_empty_data, $data);

		return $this->edit($data);
	}
	
	public function edit($data)
	{
		if (!$this->isExist()) {
			return false;
		}
		
		// object not changed but is actual
		if (empty($data)) {
			return true;
		}

		unset($data[$this->getPrimaryKeyField()]);

		$this->_data = array_merge($this->_data, $data);

		$this->_beforeEdit();
	
		$this->_serializeCustomfields()
			->_filterData();

		$success = $this->save();
		$this->_afterEdit($success);

		return $success;
	}
	
	
	
	public function save()
	{
		if (!$this->isExist()) {
			return false;
		}
	
		$criteria = array($this->getPrimaryKeyField() => $this->getID());
		$success = $this->_update($criteria);
		if ($success === 0) {
			$success = true;
		}
		
		$this->_create($this->getID());

		return $success;
	}
	
	
	public function delete()
	{
		if (!$this->isExist()) {
			return false;
		}

		$this->_beforeDelete();
		$this->_deleteConnectedResources();
		
		$success = $this->_delete();
		if ($success) {
			$this->_clearObject();
		}
		$this->_afterDelete($success);

		return $success;
	}
	
	private function _unserializeCustomfields()
	{
		foreach ($this->_customfields as $field_name => $subfields) {
			if (!empty($this->_data[$field_name])) {
				$customfields = unserialize($this->_data[$field_name]);
				if ($customfields !== false) {
					$this->_data = array_merge($this->_data, $customfields);
				} else {
					// @TODO высылать письмо на почту с указанием id объявы и некорректным сериалайзом
				}
				break;
			}
		}
		
		return $this;
	}
	
	private function _serializeCustomfields()
	{
		if (!empty($this->_customfields) && !empty($this->_data)) {
			foreach ($this->_customfields as $group => $group_fields) {
				$data = array();
				foreach ($group_fields as $field) {
					if (isset($this->_data[$field])) {
						$data[$field] = $this->_data[$field] === 'on' ? '1' : $this->_data[$field];
					}
				}
				$this->_data[$group] = serialize(Common_Helper::prepareForSerialize($data));
			}
		}

		return $this;
	}
	
	private function _filterData()
	{
		foreach ($this->_data as $field => $value) {
			if (!in_array($field, $this->_fields)) {
				unset($this->_data[$field]);
			}
		}
		
		return $this;
	}
	
	

	
	public function getId()
	{
		return $this->getField($this->getPrimaryKeyField());
	}
	
	public function getField($field)
	{
		return isset($this->_data[$field]) ? $this->_data[$field] : null;
	}
	
	public function setField($field, $value)
	{
		if ($field != $this->getPrimaryKeyField()) {
			$this->_data[$field] = $value;
		}
	}
	
	
	
	/* DB actions block */
	private function _delete()
	{
		$sql = 'delete from ' . $this->_table . ' where id = ' . $this->getID() . ' limit 1';
	
		return $this->_getDB()->exec($sql);
	}
	
	private function _insert()
	{
		if (empty($this->_data) || empty($this->_table)) {
			return false;
		}
	
		foreach ($this->_data as $field => $value) {
			$fields[] = '`' . $field . '`';
			$values[] = $this->_getDB()->quote($value);
		}
		
		$sql = 'insert into ' . $this->_table . ' (' . implode(', ', $fields) . ')';
		$sql .= ' values (' . implode(', ', $values) . ')';

		$success = $this->_getDB()->exec($sql);
		$this->_last_inserted_id = $this->_getDB()->lastInsertId();
		
		return $success;
	}
	
	private function _update($criteria)
	{
		if (empty($this->_data) || empty($criteria) || empty($this->_table)) {
			return false;
		}
		
		foreach ($criteria as $field => $value) {
			if (!in_array($field, $this->_fields)) {
				return false;
			}
		}

		foreach ($this->_data as $field => $value) {
			if ($field != $this->getPrimaryKeyField()) {
				$set[] = '`' . $field . '` = ' . $this->_getDB()->quote($value);
			}
		}
		
		$sql_where = $this->_getSqlWhereString($criteria);
		if (empty($sql_where)) {
			return false;
		}
		
		$sql = 'update ' . $this->_table . ' set ' . implode(', ', $set) . ' where ' . $sql_where;

		return $this->_getDB()->exec($sql);
	}
	
	private function _getObjectDataFromDB($criteria)
	{
		$sql_where = $this->_getSqlWhereString($criteria);

		if (!empty($sql_where)) {
			$sql = 'select * from ' . $this->_table . ' where ' . $sql_where . ' limit 1';
			$data = $this->_getDB()->query($sql)->fetch();
		}
		
		return !empty($data) ? $data : array();
	}
	
	private function _getSqlWhereString($criteria)
	{
		$where = array();
	
		if (is_array($criteria)) {
			foreach ($criteria as $field => $value) {
				if (in_array($field, $this->_fields)) {
					$where[] = '`' . $field . '` = ' . $this->_getDB()->quote($value);
				}
			}
		}
		
		return implode(' and ', $where);
	}
	
	public function isExist()
	{
		return isset($this->_data[$this->getPrimaryKeyField()]);
	}
	
	public function getInsertedId()
	{
		return !empty($this->_last_inserted_id) ? $this->_last_inserted_id : 0;
	}
	
	public function getPrimaryKeyField()
	{
		return $this->_primary_key_id;
	}
	
	private function _clearPrimaryKeyDataField()
	{
		unset($this->_data[$this->getPrimaryKeyField()]);
	}
	
	protected function _getDB()
	{
		if (is_null($this->_db)) {
			$this->_db = Environment::getDB();
		}
		
		return $this->_db;
	}
	
	private function _setDB($db)
	{
		if (!empty($db)) {
			$this->_db = $db;
		}
	}
	
	private function _clearObject()
	{
		$this->_data = array();
	}
	
	private function _checkCorrectObject()
	{
		if (empty($this->_table)) {
			throw new Exception('Unknown table of the object ' . get_class());
		} elseif (empty($this->_fields)) {
			throw new Exception('Unknown fields of the object ' . get_class());
		}
	}
	
	
	protected abstract function _convertFields();
	
	protected function _beforeCreate() { return true; }
	protected function _afterCreate($success) { return true; }
	protected function _beforeAdd() { return true; }
	protected function _afterAdd($success) { return true; }
	protected function _addConnectedResources($success) { return true; }
	protected function _beforeEdit() { return true; }
	protected function _afterEdit($success) { return true; }
	protected function _beforeDelete() { return true; }
	protected function _afterDelete($success) { return true; }
	protected function _deleteConnectedResources() { return true; }
	
}