<?php

class PageHeader 
{
	private $_title = '';
	private $_meta_description = '';
	private $_meta_keywords = '';
	private $_seo_top_text = '';
	private $_seo_bottom_text = '';

	private $_item_title = '';
	private $_page_type;
	private $_category;
	private $_seo_tag;
	
	private $_url;
	
	public function __construct() 
	{
		$this->_url = Environment::getParseUrl();
		$this->_page_type = Environment::getPageType();

		$this->_createDefaultTitle();
	}

	
	public function getTitle() 
	{
		return $this->_title;
	}

	
	public function getPageType() 
	{
		return $this->_page_type;
	}
	
	public function getMetaDescription() 
	{
		return $this->_meta_description;
	}
	
	public function getMetaKeywords() 
	{
		return $this->_meta_keywords;
	}
	
	public function getSeoTopText() 
	{
		return $this->_seo_top_text;
	}
	
	public function getSeoBottomText()
	{
		return $this->_seo_bottom_text;
	}
	
	
	private function _createDefaultTitle() {
	
	}

}