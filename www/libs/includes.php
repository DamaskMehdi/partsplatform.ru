<?php
	spl_autoload_register('autoload');
	set_error_handler("myErrorHandler", E_ALL | E_STRICT);
	date_default_timezone_set('Europe/Minsk');

	require('Config.php');
	
	Config::setParams();
	Environment::setParams();
	
	
	function myErrorHandler($errno, $msg, $file, $line) {
		if (error_reporting() != 0) {
			echo '<div>';
			echo "Произошла ошибка:<b>$errno</b>!<br>"; 
			echo "Файл: <tt>$file</tt>, строка $line.<br>";  
			echo "Текст ошибки: <i>$msg</i>";    
			echo "</div>";
		}
	}

	function autoload($class_name)
	{
		if (file_exists(Config::getParam('CLASSDIR') . str_replace('_', Config::getParam('DELIMITER'), $class_name) . '.php')) {
			include(Config::getParam('CLASSDIR') . str_replace('_', Config::getParam('DELIMITER'), $class_name) . '.php');
		}
	}
	
	function rdate($param, $time = 0) 
	{
		if (intval($time) == 0) {
			$time = time();
		}
		
		$rus_months = array(
			'Января', 'Февраля', 'Марта', 'Апреля',
			'Мая', 'Июня', 'Июля', 'Августа',
			'Сентября', 'Октября', 'Ноября', 'Декабря'
		);
		
		$rus_months_lower = array(
			'января', 'февраля', 'марта', 'апреля',
			'мая', 'июня', 'июля', 'августа',
			'сентября', 'октября', 'ноября', 'декабря'
		);
		
		if (mb_strpos($param, 'M') !== false) {
			return date(str_replace('M', $rus_months[date('n', $time) - 1], $param), $time);
		} elseif (mb_strpos($param, 'm') !== false) {
			return date(str_replace('m', $rus_months_lower[date('n', $time) - 1], $param), $time);
		} else {
			return date($param, $time);			
		}
	}
	
	function hd($var)
	{
		echo '<!--';
		d($var);
		echo '-->';
	}
	
	function d($var) 
	{
		if (Environment::isConsole()) {
			print_r($var);
			echo PHP_EOL;
		} else {
			echo '<pre>';
			print_r($var);
			echo '</pre>';
		}

		return $var;
	}
	
	function dump($var) 
	{
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
		
		return $var;
	}
	
	function cp($text) {return iconv('utf-8', 'windows-1251', $text);}
	function utf($text) {return iconv('windows-1251', 'utf-8', $text);}
	function clear_spec($text) {return preg_replace("/&#?[a-z0-9]{2,8};/i", "", $text);}