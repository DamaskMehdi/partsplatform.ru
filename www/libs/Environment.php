<?php

class Environment 
{
	private static $post_vars = array();
	private static $get_vars = array();
	private static $session_vars = array();
	private static $cookie_vars = array();
	private static $server_vars = array();
	private static $request_vars = array();
	
	private static $_db = null;
	
	private static $user = null;
	private static $_models = array();
	
	private static $_tpl = null;
	private static $page_content = null;
	private static $_parse_url = null;
	private static $_page_type = null;
	private static $_region = null;
	private static $_rates = null;
	private static $_page;
	
	private static $_console = false;
	private static $_ajax = false;

/* @TODO добавить очистку параметров get и post от вредоносного кода
	убрать двоение кода в setSessionVar
*/
	public static function setParams() 
	{
		self::$post_vars = &$_POST;
		self::$get_vars = &$_GET;
		self::$session_vars = &$_SESSION;
		self::$cookie_vars = $_COOKIE;
		self::$server_vars = &$_SERVER;
		self::$request_vars = &$_REQUEST;
		
		$sapi = php_sapi_name();
		if ($sapi == 'cli') {
			self::$_console = true;
		}

		if (self::getServerVar('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest') {
			self::$_ajax = true;
		}
	}
	
	public static function isConsole() 
	{
		return self::$_console;
	}
	
	public static function isAjax() 
	{
		return self::$_ajax;
	}
	
	public static function getModel($name) 
	{
		if (!isset(self::$_models[$name])) {
			$class_name = 'Model_' . $name;
			if (class_exists($class_name)) {
				self::$_models[$name] = new $class_name();
			} else {
				self::$_models[$name] = false;
			}
		}
	
		return self::$_models[$name];
	}
	
	public static function getPageType() 
	{
		return self::$_page_type;
	}
	
	public static function setPageType($page_type) 
	{
		self::$_page_type = $page_type;
	}
	
	public static function getRegion() 
	{
		return self::$_region;
	}
	
	public static function setRegion($region) 
	{
		self::$_region = $region;
	}
	
	public static function getPage() 
	{
		return self::$_page;
	}
	
	public static function setPage($page) 
	{
		self::$_page = $page;
	}
	
	public static function getParseUrl() 
	{
		if (self::$_parse_url == null && self::getDB()) {
			self::$_parse_url = new ParseUrl();
		}
		return self::$_parse_url;
	}
	
	public static function getPageContent() 
	{
		if (self::$page_content == null && self::getDB()) {
			self::$page_content = new PageContent();
		}
		return self::$page_content;
	}
	
	public static function getUser() 
	{
		if (self::$user == null) {
			self::$user = new User();
		}
		return self::$user;
	}
	
	public static function getDB($db_label = 'local') 
	{
		if (!isset(self::$_db[$db_label])) {
			$db_config = Config::getParam('DB');
			$db_params = isset($db_config[$db_label]) ? $db_config[$db_label] : $db_config['local'];
			self::$_db[$db_label] = DB::getDB($db_params['host'], $db_params['user'], $db_params['pwd'], $db_params['db_name'], $db_label);	
		}
		return self::$_db[$db_label];
	}
	
	public static function closeDB() 
	{
		foreach (self::$_db as $db) {
			$db->close();
		}
	}
		
	public static function getTPL() 
	{
		if (is_null(self::$_tpl)) {
			require(Config::getParam('LIBDIR') . 'smarty/Smarty.class.php');
			self::$_tpl = new Smarty();
			self::$_tpl->template_dir = Config::getParam('TEMPLATEDIR');
			self::$_tpl->compile_dir = Config::getParam('COMPILEDIR');
			self::$_tpl->force_compile = Config::getParam('SMARTY_FORCE_COMPILE');
			self::$_tpl->compile_check = Config::getParam('SMARTY_COMPILE_CHECK');
			//self::$_tpl->caching = false;
			self::$_tpl->registerPlugin("modifier","ss", "stripslashes"); //Stripslashes Plugin
			//self::$_tpl->loadFilter('output', 'trimwhitespace');
		}
	
		return self::$_tpl;
	}
	
	
	public static function getRates() 
	{
		if (is_null(self::$_rates)) {
			self::$_rates = array();
			foreach (self::getDB()->query("select code, rate from atrru.currency")->fetchAll() as $rate) {
				self::$_rates[$rate['code']] = $rate['rate'];
			}
		}
		
		return self::$_rates;
	}
	
	
	
	
	
	
	public static function getPostVar($name) 
	{
		if (isset(self::$post_vars[$name])) {
			return self::$post_vars[$name];
		} else {
			return '';
		}
	}
	
	public static function getPost() 
	{
		return self::$post_vars;
	}
	
	public static function setPostVar($name, $value) 
	{
		self::$post_vars[$name] = $value;
	}
	
	public static function setPost($vars) 
	{
		self::$post_vars = $vars;
	}
	
	
	public static function getGetVar($name) 
	{
		if(isset(self::$get_vars[$name])) {
			return self::$get_vars[$name];
		} else {
			return '';
		}
	}
	
	public static function getGet() 
	{
		return self::$get_vars;
	}
	
	public static function setGetVar($name, $value) 
	{
		self::$get_vars[$name] = $value;
	}
	
	public static function setGet($vars) 
	{
		self::$get_vars = $vars;
	}
	
	
	public static function getRequestVar($name) 
	{
		if(isset(self::$request_vars[$name])) {
			return self::$request_vars[$name];
		} else {
			return '';
		}
	}
	
	public static function getRequest() 
	{
		return self::$request_vars;
	}
	
	public static function setSessionVar($name, $value) 
	{
		self::$session_vars[$name] = $value;
		$_SESSION[$name] = $value;
	}
	
	public static function getSessionVar($name) 
	{
		if(isset(self::$session_vars[$name])) {
			return self::$session_vars[$name];
		} else {
			return '';
		}
	}
		
	public static function getSession() 
	{
		return self::$session_vars;
	}
	
	public static function getCookieVar($name) 
	{
		if(isset(self::$cookie_vars[$name])) {
			return self::$cookie_vars[$name];
		} else {
			return '';
		}
	}
	
	public static function getCookie() 
	{
		return self::$cookie_vars;
	}
	
	public static function setServerVar($name, $value) 
	{
		self::$server_vars[$name] = $value;
	}
	
	public static function getServerVar($name) 
	{
		if(isset(self::$server_vars[$name])) {
			return self::$server_vars[$name];
		} else {
			return '';
		}
	}
		
	public static function getServer() 
	{
		return self::$server_vars;
	}
	
}

