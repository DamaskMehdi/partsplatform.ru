<?php
Class Config
{
	private static $_delimiter = '/';

	private static $lib_dir = 'libs';
	private static $class_dir = 'libs';
	private static $module_dir = 'modules';
	private static $template_dir = 'templates';
	private static $_compile_dir = 'templates_c';
	private static $js_dir = 'js';
	private static $css_dir = 'css';
	private static $images_dir = 'images';
	private static $upload_dir = 'upload';
	private static $log_dir = 'logs';
	private static $adodb_cache_dir = 'cache/_sqlcache';

	private static $_memcache_host = 'localhost';
	private static $_memcache_port = 11211;

	private static $config_params = array(
		'DOMAIN' => 'partsplatform.ru',
		'DELIMITER' => '/',
		'LOCALPATH' => '/home/partsplatform.ru/www/',
		'MODULEPATH' => '',
		'SITEURL' => '',
		'LIBDIR' => '',
		'CLASSDIR' => '',
		'TEMPLATEDIR' => '',
		'COMPILEDIR' => '',
		'LOGDIR' => '',
		'JSURL' => '',
		'CSSURL' => '',
		'IMAGEURL' => '',
		'MAIL_ROBOT' => '',
		'DB' => array(
			'remote' => array(
				'host' => '212.71.248.80',
				'db_name' => 'atr',
				'user' => 'atr',
				'pwd' => 'Ghy69Kls32M',
			),
			'local' => array(
				'host' => '212.71.248.80',
				'db_name' => 'atrru',
				'user' => 'atr',
				'pwd' => 'Ghy69Kls32M',
			),
		),
		'API_CONFIG' => '',
		'PMA' => '',
		'CONF_ONE_STR' => 0,
		'CONF_STATISTIC' => 0,
		'ADODB_LANG' => 'ru1251',
		'ADODB_CACHE_DIR' => '',
		'PUBLICKEY' => '6Lfo7rsSAAAAAHHtl0XeEnGFQp3UYSVRR_bZPDNw',
		'PRIVATEKEY' => '6Lfo7rsSAAAAANMRqP2Q9U2v7JU3pl3PsG7poxT8',
		'SMARTY_FORCE_COMPILE' => true,
		'SMARTY_COMPILE_CHECK' => true,
		'CONF_DB_QUERY_CACHE' => 1,
		'SHOW_COUNTERS' => false,

		//'API_SERVER' => 'http://atr-catalog.by/json/',
		'API_SERVER' => 'http://api.atr.ru/json/',
		//'API_SERVER2' => 'http://atr-catalog.by/api2/',
		'API_SERVER2' => 'http://api.atr.ru/api2/',
		'API_KEY' => '599d766d32b0cd97c86367b0408b6bf1',

	);

	public static function setParams()
	{
		self::$config_params['DELIMITER'] = self::$_delimiter;

		self::$config_params['SITEURL'] =  'http://' . $_SERVER['SERVER_NAME'] . '/';

		self::$config_params['ADODB_CACHE_DIR'] = self::$config_params['LOCALPATH'] . self::$adodb_cache_dir;
		self::$config_params['CONF_SMARTY_FORCE_COMPILE'] = (isset($_COOKIE['compile']) && $_COOKIE['compile'] == 1) ? 1 : 0;
		self::$config_params['LIBDIR'] = self::$config_params['LOCALPATH'] . self::$lib_dir . self::$_delimiter;
		self::$config_params['CLASSDIR'] = self::$config_params['LOCALPATH'] . self::$class_dir . self::$_delimiter;
		self::$config_params['MODULEDIR'] = self::$config_params['LOCALPATH'] . self::$module_dir . self::$_delimiter;
		self::$config_params['TEMPLATEDIR'] = self::$config_params['LOCALPATH'] . self::$template_dir . self::$_delimiter;
		self::$config_params['COMPILEDIR'] = self::$config_params['LOCALPATH'] . self::$_compile_dir . self::$_delimiter;
		self::$config_params['LOGDIR'] = self::$config_params['LOCALPATH'] . self::$log_dir . self::$_delimiter;
		self::$config_params['UPLOADDIR'] = self::$config_params['LOCALPATH'] . self::$upload_dir . self::$_delimiter;
		self::$config_params['UPLOADURL'] = self::$config_params['SITEURL'] . self::$upload_dir . '/';
		self::$config_params['JSURL'] = self::$config_params['SITEURL'] . self::$js_dir . '/';
		self::$config_params['CSSURL'] = self::$config_params['SITEURL'] . self::$css_dir . '/';
		self::$config_params['IMAGESURL'] = 'https://atr.ru/' . self::$images_dir . '/';
		//self::$config_params['IMAGESURL'] = self::$config_params['SITEURL'] . self::$images_dir . '/';
	}

	public static function getParam($name)
	{
		if(isset(self::$config_params[$name])) {
			return self::$config_params[$name];
		} else {
			return '';
		}
	}

	public static function getParams()
	{
		return self::$config_params;
	}

	public static function setParam($name, $value)
	{
		self::$config_params[$name] = $value;
	}

}
?>