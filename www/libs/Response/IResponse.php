<?php

interface Response_IResponse 
{
	/** Set message
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message);
	
	/** Set messages array
	 * @param array $messages
	 * @return void
	 */
	public function setMessages($messages);
	
	/** Set data array
	 * @param array $data
	 * @return void
	 */
	public function setData($data);
	
	/*
	 * Send responce
	 */
	public function send();
	public function stop();
}