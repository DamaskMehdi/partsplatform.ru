<?php

class Response_Json implements Response_IResponse
{
	/**
	 * @var string $_message
	 */
	private $_message;
	
	/**
	 * @var bool $_success
	 */
	private $_success;
	
	/**
	 * @var array $_result
	 */
	private $_result = array(
		'messages' => array(),
		'message' => '',
		'success' => true,
	);
	
	/*
	 * Constructor of object
	 * @return void
	 */
	public function __construct()
	{
		
	}
	
	/*
	 * Set message
	 * @param string $message
	 * @return void
	 */
	public function setSuccessMessage($message)
	{
		$this->_result['message'] = $message;
	}
	
	/*
	 * Set message
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message)
	{
		$this->_result['message'] = $message;
		if (!empty($this->_result['message']) || !empty($this->_result['messages'])) {
			$this->_result['success'] = false;
		} else {
			$this->_result['success'] = true;
		}
	}
	
	/*
	 * Set messages
	 * @param array $messages
	 * @return void
	 */
	public function setMessages($messages)
	{
		$this->_result['messages'] = $messages;
		if (!empty($this->_result['message']) || !empty($this->_result['messages'])) {
			$this->_result['success'] = false;
		} else {
			$this->_result['success'] = true;
		}
	}
	
	/*
	 * Set data
	 * @param array $data
	 * @return void
	 */
	public function setData($data)
	{
		if (!empty($data)) {
			$this->_result['data'] = $data;
		}
	}
	
	/*
	 * Send response
	 * @return Response_Json
	 */
	public function send()
	{
		echo json_encode($this->_result);
		return $this;
	}
	
	/*
	 * Stop execution
	 * @return void
	 */
	public function stop()
	{
		Environment::getDB()->close();
	
		die();
	}
}