<?php

class Response_Text implements Response_IResponse
{
	/**
	 * @var string $_message
	 */
	private $_message = '';

	
	/*
	 * Constructor of object
	 * @return void
	 */
	public function __construct()
	{
		
	}
	
	/*
	 * Set message
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message)
	{
		$this->_message = $message;
	}
	
	public function setMessages($messages) { }
	public function setData($data) { }
	
	/*
	 * Send response
	 * @return Response_Json
	 */
	public function send()
	{
		echo $this->_message;
		
		return $this;
	}
	
	/*
	 * Stop execution
	 * @return void
	 */
	public function stop()
	{
		Environment::getDB()->close();
	
		die();
	}
}