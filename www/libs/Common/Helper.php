<?php

class Common_Helper
{
	const USD_BYR = 21700;
	const EUR_BYR = 24100;
	const RUR_BYR = 284;
	
	const CC_CODE_BELARUS = '375';
	const CC_CODE_RUSSIA = '7';
	
	const SORT_LABEL = 'order';
	const SORT_DELIMITER = '_';
	const SORT_FIELD_DELIMITER = ':';
	const SORT_FIELD_ASC = 'ASC';
	const SORT_FIELD_DESC = 'DESC';
	const SORT_ARROW_UP = '&uarr;';
	const SORT_ARROW_DOWN = '&darr;';
	
	private static $_week_days = array(
		1 => array(
			'short' => 'пн',
			'full' => 'понедельник',
		),
		2 => array(
			'short' => 'вт',
			'full' => 'вторник',
		),
		3 => array(
			'short' => 'ср',
			'full' => 'среда',
		),
		4 => array(
			'short' => 'чт',
			'full' => 'четверг',
		),
		5 => array(
			'short' => 'пт',
			'full' => 'пятница',
		),
		6 => array(
			'short' => 'сб',
			'full' => 'суббота',
		),
		7 => array(
			'short' => 'вс',
			'full' => 'воскресенье',
		),
	);
	
	private static $_time_of_day = array(
		0 => 'утро',
		1 => 'день',
		2 => 'вечер',
		3 => 'ночь',
	);
	
	
	
	/**
	 * Redirect to new url
	 *
	 * @param string $url
	 *
	 * @return void
	 */
	public static function createRedirect($url) 
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $url);
		exit();
	}
	
	/**
	 * Get week info by number
	 * @param int $day_number
	 * @return array
	 */
	public static function getWeekDay($day_number)
	{
		if (isset(self::$_week_days[$day_number])) {
			return self::$_week_days[$day_number];
		}
		
		return array();
	}
	
	/**
	 * Get time of day info by number
	 * @param int $time_number
	 * @return string
	 */
	public static function getTimeOfDay($time_number)
	{
		if (isset(self::$_time_of_day[$time_number])) {
			return self::$_time_of_day[$time_number];
		}
		
		return '';
	}
	
	/**
	 * Convert date (18.12.14) to format date string (2014.12.18)
	 *
	 * @param string $date
	 *
	 * @return string
	 */
	public static function formatDate($date)
	{
		if (preg_match('~^([0-9]{2,2})\.([0-9]{2,2})\.([0-9]{2,2})$~', $date, $matches)) {
			$date = '20' . $matches[3] . '-' . $matches[2] . '-' . $matches[1];
		}

		return $date;
	}
	
	/**
	 * Функция для получения правильных окончаний слов
	 *
	 * @param int    $iNumber       Число, к которому привязываемся
	 * @param array  $aTitles       Массив слов для склонения
	 * @return string
	 **/
	public static function pluralTuning($iNumber, $aTitles) 
	{
		$cases = array(2, 0, 1, 1, 1, 2);
		return sprintf($aTitles[ ($iNumber%100>4 && $iNumber%100<20)? 2 : $cases[min($iNumber%10, 5)] ], $iNumber);
	}

	
	/**
	 * Parse range of numbers
	 *
	 * @param string $range
	 *
	 * @return array
	 */
	public static function parseRangeInt($range)
	{
		$range_array = array();
		
		$range = str_replace(' ', '', $range);
		if (!empty($range) && !preg_match('~[^0-9,\-]~', $range, $matches)) {
			foreach (explode(',', $range) as $item) {
				$item = trim($item);
				if (strpos($item, '-') !== false) {
					$values = explode('-', $item);
					if ($values[0] < $values[1]) {
						for ($i = $values[0]; $i <= $values[1]; $i++) {
							$range_array[] = $i;
						}
					}
				} else {
					$range_array[] = $item;
				}
			}
			
			sort($range_array);
		}
	
		return $range_array;
	}
	

	 
	/**
	 * Get current sort fields for search
	 *
	 * @return array
	 */
	public static function getSort()
	{
		$result = array();

		$sort = Environment::getParseUrl()->getSort();
		if (!empty($sort)) {
			$allow_fields = array('date_start', 'price');
		
			foreach ($sort as $field => $dir) {
				if (in_array($field, $allow_fields)) {
					$result[] = array(
						'field' => $field,
						'dir' => $dir,
					);
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Get links for order buttons
	 * @param string $default_order_field
	 * @param string $default_order_dir
	 *
	 * @return array
	 */
	public static function getOrderLinks($default_order_field = '', $default_order_dir = '')
	{
		$uri = Environment::getParseUrl()->getPageFullUri();
		$sort = array_slice(Environment::getParseUrl()->getSort(), 0, 1);
		$order_fields = array(
			'price',
			'date_start',
		);
		$order_string = array();

		$links = array();
		foreach ($order_fields as $field) {
			if (isset($sort[$field])) {
				if (strtolower($sort[$field]) == strtolower(self::SORT_FIELD_ASC)) {
					$label = strtolower($field . self::SORT_DELIMITER . self::SORT_FIELD_DESC);
					$arrow = self::SORT_ARROW_DOWN;
					$order_string[] = strtolower($field . self::SORT_DELIMITER . self::SORT_FIELD_ASC); 
				} else {
					$label = strtolower($field . self::SORT_DELIMITER . self::SORT_FIELD_ASC);
					$arrow = self::SORT_ARROW_UP;
					$order_string[] = strtolower($field . self::SORT_DELIMITER . self::SORT_FIELD_DESC); 
				}
				$links[$field] = array(
					'active' => true,
					'arrow' => $arrow,
					'url' => self::addParameterToUri($uri, self::SORT_LABEL, $label),
					'label' => $label,
				);
			} else {
				$label = strtolower($field . self::SORT_DELIMITER . self::SORT_FIELD_DESC);
				$links[$field] = array(
					'active' => false,
					'arrow' => '',
					'url' => self::addParameterToUri($uri, self::SORT_LABEL, $label),
					'label' => $label,
				);
			}
		}
		
		Environment::getPageContent()->setParam('order_string', implode(self::SORT_FIELD_DELIMITER, $order_string));
		
		return $links;
	}
	
	/**
	 * Add GET parameter to url string
	 * @param string $uri
	 * @param string $name
	 * @param string $value
	 *
	 * @return string
	 */
	public static function addParameterToUri($uri, $name, $value)
	{
		if (mb_strpos($uri, $name . '=') !== false) {
			$uri = preg_replace("/(" . $name . "=[a-zA-Z0-9\.\/\-\_\#–\?=%:]+)/", $name . '=' . $value, $uri);
		} elseif (strpos($uri, '?') !== false) {
			$uri .= '&' . $name . '=' . $value;
		} else {
			$uri .= '?' . $name . '=' . $value;
		}
		
		return $uri;
	}
	
	/**
	 * Return price in 4 rur
	 *
	 * @param int $price
	 * @param string $currency
	 * @return array
	 */
	public static function getPriceRur($price, $currency = 'USD')
	{
		if (!empty($price)) {
			$rates = Environment::getRates();
			
			if ($currency == 'USD' or $currency == '$') {
				$price_rur = !empty($rates['USD']) ? ceil($price * $rates['USD']) : ceil($price * self::USD_BYR / self::RUR_BYR);
			} elseif (in_array($currency, array('EUR', '&euro;', '€'))) {
				$price_rur = !empty($rates['EUR']) ? ceil($price * $rates['EUR']) : ceil($price * self::EUR_BYR / self::RUR_BYR);
			} elseif (in_array($currency, array('RUR', 'руб.', 'рос. руб.'))) {
				$price_rur = ceil($price);
			} elseif (in_array($currency, array('BYR', 'бел. руб.', 'RUB'))) {
				$price_rur = !empty($rates['BYR']) ? ceil($price * $rates['BYR']) : ceil($price / self::RUR_BYR);
			}
		}
		
		if (empty($price_rur)) {
			$price_rur = 0;
		}

		return $price_rur;
	}
	 
	/**
	 * Convert price in 4 currencies
	 *
	 * @param int $price
	 * @param string $currency
	 * @return array
	 */
	public static function convertPrice($price, $currency = 'USD')
	{
		if (!empty($price)) {
			if ($currency == 'USD' or $currency == '$') {
				$result['price_usd'] = ceil($price);
				$result['price_eur'] = ceil($price / self::EUR_BYR * self::USD_BYR);
				$result['price_rur'] = ceil($price * self::USD_BYR / self::RUR_BYR);
				$result['price_byr'] = ceil($price * self::USD_BYR);
			} elseif (in_array($currency, array('EUR', '&euro;', '€'))) {
				$result['price_usd'] = ceil($price * self::EUR_BYR / self::USD_BYR);
				$result['price_eur'] = ceil($price);
				$result['price_rur'] = ceil($price * self::EUR_BYR / self::RUR_BYR);
				$result['price_byr'] = ceil($price * self::EUR_BYR);
			} elseif (in_array($currency, array('RUR', 'RUB', 'руб.', 'рос. руб.'))) {
				$result['price_usd'] = ceil($price / self::USD_BYR * self::RUR_BYR);
				$result['price_eur'] = ceil($price / self::EUR_BYR * self::RUR_BYR);
				$result['price_rur'] = ceil($price);
				$result['price_byr'] = ceil($price * self::RUR_BYR);
			} elseif ($currency == 'BYR' or $currency == 'бел. руб.') {
				$result['price_usd'] = ceil($price / self::USD_BYR);
				$result['price_eur'] = ceil($price / self::EUR_BYR);
				$result['price_rur'] = ceil($price / self::RUR_BYR);
				$result['price_byr'] = ceil($price);
			}
		}
		
		if (empty($result)) {
			$result['price_usd'] = $result['price_eur'] = $result['price_rur'] = $result['price_byr'] = 0;
		}

		return $result;
	}
	
	/**
	 * Convert quotes with many slashes	
	 *
	 * @param string $string
	 * @return string
	 */
	public static function convertSlashQuotes($string)
	{
		return preg_replace('/([\\\]+")/', '"', $string);
	}
	
	/**
	 * Prepare data for serializing
	 *
	 * @param array|string $data
	 * @return array|string
	 */
	public static function prepareForSerialize($data)
	{
		$replace_array = array(
			'"' => '&Prime;',
			"\t" => '',
			"\n" => '',
			"\r" => '',
			'\\' => '',
		);
	
		if (is_array($data)) {
			$from = array_keys($replace_array);
			$to = array_values($replace_array);
			foreach ($data as $key => $value) {
				$data[$key] = str_replace($from, $to, $value);
			}
		} else {
			$data = str_replace($from, $to, $data);
		}
		
		return $data;
	}
	
	/**
	 * @param array $items
	 * @param int $columns
	 * @return array
	 */
	public static function getByColumnsWithLetters($items, $columns)
	{
		$result = array();

		foreach ($items as $item) {
			$letters[mb_substr($item['name'], 0, 1)][] = $item;
		}

		$c = 0;
		$columns_count = 0;
		$items_per_column = ceil(count($items) / $columns);
		foreach ($letters as $letter => $data) {
			$columns_count += count($data);
			$result[$c][$letter] = $data;
			if ($columns_count > $items_per_column) {
				$columns_count = 0;
				$c++;
			}
		}

		return $result;
	}
	
	/**
	 * @param array $items
	 * @param int $columns
	 * @return array
	 */
	public static function getByColumns($items, $columns)
	{
		$result = array();
		
		$items_per_column = ceil(count($items) / $columns);
		$c = 0;
		foreach ($items as $item) {
			$result[floor($c / $items_per_column)][$c % $items_per_column] = $item;
			$c++;
		}
		return $result;
	}
	
	
	/**
	 * Get either a Gravatar URL or complete image tag for a specified email address.
	 *
	 * @param string 	$email	The email address
	 * @param int 		$s 		Size in pixels, defaults to 80px [ 1 - 2048 ]
	 * @param string 	$d		Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
	 * @param string 	$r		Maximum rating (inclusive) [ g | pg | r | x ]
	 * @param bool 		$img	True to return a complete IMG tag False for just the URL
	 * @param array 	$atts	Optional, additional key/value attributes to include in the IMG tag
	 *
	 * @return string			String containing either just a URL or a complete image tag
	 */
	public static function gravatar($email, $s = 50, $d = 'mm', $r = 'g', $img = false, $atts = array() ) 
	{
		$url = 'http://www.gravatar.com/avatar/';
		$url .= md5(strtolower(trim($email)));
		$url .= "?s=$s&d=$d&r=$r";

		if ($img) {
			$url = '<img src="' . $url . '"';
			foreach ($atts as $key => $val) {
				$url .= ' ' . $key . '="' . $val . '"';
			}
			$url .= ' />';
		}

		return $url;
	}
	
	/**
	 * @param string $text
	 * 
	 * @return string
	 */
	public static function toUpperLower($text)
	{
		if (!empty($text)) {
			$words = explode(' ', $text);
			
			foreach ($words as $key => $word) {
				$words[$key] = mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_strtolower(substr($word, 1), 'UTF-8');
			}
			
			$text = implode(' ', $words);
		}
	
		return $text;
	}
	
	/**
	 * @param string $text
	 * 
	 * @return string
	 */
	public static function cp($text)
	{
		return iconv('utf-8', 'windows-1251//IGNORE', $text);
	}
	
	/**
	 * @param string $text
	 * 
	 * @return string
	 */
	public static function utf($text)
	{
		return iconv('windows-1251', 'utf-8//IGNORE', $text);
	}
	
	/**
	 * @param string $text
	 * 
	 * @return string
	 */
	public static function clear_spec($text)
	{
		return preg_replace("/&#?[a-z0-9]{2,8};/i", "", $text);
	}
	
	/**
	 *
	 *
	 * @return string
	 */
	public static function str_replace_once($search, $replace, $text) 
	{ 
	   $pos = strpos($text, $search);
	   
	   return $pos !== false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
	} 
	
	/**
	 * @param string $str
	 * @param string $begin
	 * @param string $end
	 * @param string $center
	 * 
	 * @return string
	 */
	public static function sub_str($str, $begin, $end = '', $center = '')
	{
		if (!empty($center)) {
			$str = substr($str, strpos($str, $begin, strpos($str, $center)) + strlen($begin), 
				strlen($str) - strpos($str, $begin, strpos($str, $center)) - strlen($begin));
		} else {
			$str = substr($str, strpos($str, $begin) + strlen($begin), 
				strlen($str) - strpos($str, $begin) - strlen($begin));
		}
		if (!empty($end)) {
			$str = substr($str, 0, strpos($str, $end));
		}
		
		return $str;
	}

	
	/**
	 * @param int $bytes
	 * @param int $precision
	 * 
	 * @return string
	 */
	public static function convertsBytesToString($bytes, $precision = 0)
	{
		if (!empty($bytes)) {
			if ($bytes > 1099511627776) { // Tbyte
				$string = round($bytes / 1099511627776, $precision) . ' Tbytes';
			} elseif ($bytes > 1073741824) { // Gbyte
				$string = round($bytes / 1073741824, $precision) . ' Gbytes';
			} elseif ($bytes > 1048576) { // Mbyte
				$string = round($bytes / 1048576, $precision) . ' Mbytes';
			} elseif ($bytes > 1024) { // Kbyte
				$string = round($bytes / 1024, $precision) . ' Kbytes';
			} else {
				$string = $bytes . ' bytes';
			}
		} else {
			$string = '0 bytes';
		}
		
		return $string;
	}
}