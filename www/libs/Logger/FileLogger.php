<?php
class Logger_FileLogger 
{
	private $_fp = null;
	private $_file_name;

	public function __construct($file_name) 
	{
		$this->_file_name = Config::getParam('LOGDIR') . $file_name;
		$this->_fp = fopen($this->_file_name, 'a');
	}
	
	public function write($text) 
	{
		if ($this->_fp && !empty($text)) {
			$text = date('d M Y H:i:s') . ' : message = ' . $text;
			fwrite($this->_fp, $text . "\n");
		}
	}
	
	public function save() 
	{
		if ($this->_fp) {
			fclose($this->_fp);
			$this->_fp = fopen($this->_file_name, 'a');
		}
	}
	
	public function close() 
	{
		if ($this->_fp) {
			fclose($this->_fp);
		}
	}
}