<?php
class Builder 
{
	private $_tpl;
	private $_page_content;
	private $_user;
	private $_url;

	public function __construct() 
	{
		$this->_tpl = Environment::getTPL();
	}
	
	public function createStructure()
	{
		$this->_parseUrl();
		$this->_createUser();
		$this->_createPageContent();
	}
	
	private function _parseUrl() 
	{
		$this->_url = Environment::getParseUrl();
	}
	
	private function _createUser() 
	{
		$this->_user = Environment::getUser();
		/*if ($this->_user->isAdmin()) {
			$this->_adminActions();
			$this->_tpl->assign('user_type', 'admin');
		} elseif ($this->_user->isSuperAdmin()) {
			$this->_authorizedActions();
			$this->_tpl->assign('user_type', 'superadmin');
		} else {
			$this->_tpl->assign('user_type', 'unknown');
			if ($this->_url->getPageUri() != '') {
				$this->_createRedirect(Config::getParam('SITEURL')); 
			}
		}*/
	}
	
	private function _createPageContent() 
	{
		$this->_page_content = Environment::getPageContent();

		$this->_page_content->setUser($this->_user);

		$this->_tpl->assign('CONFIG', Config::getParams());

		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'bootstrap.min.css');
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'bootstrap-theme.min.css');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'bootstrap.min.js');
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'libs.css');
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'atr.css');
		//$this->_page_content->addStyles(Config::getParam('CSSURL') . 'font-awesome.css');
		
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'libs/angular.min.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'libs/angular-route.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'atr.min.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.relatedselects.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery-ui.min.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.ui.autocomplete.min.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'common.js');
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.scroll-up.js');
		
		/*
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'main.css');
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'logo.css');
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'style.css');
		$this->_page_content->addStyles(Config::getParam('CSSURL') . 'icons.css');
		*/
		
		$this->_tpl->assign('page', $this->_page_content);
	}
	
	public function buildTemplate() 
	{
		$view = View_Factory::create();
		$view->getPage();
		
		$this->_tpl->assign('is_ajax', Environment::isAjax());
		//if (Environment::getParseUrl()->getModuleName() == '')
		$this->displayTemplate('index.tpl');
	}
	
	public function displayTemplate($template_name) 
	{
		$this->_tpl->display($template_name);
	}
	
	public function buildTemplate404($template_name = '404.tpl') 
	{
		header("HTTP/1.1 404 Not Found", true, "404");
		$this->_tpl->display($template_name);
	}
	
	public function closeAll() 
	{
		Environment::closeDB();
	}
	
	private function _createRedirect($url) 
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $url);
		exit();
	}
	
	private function _authorizedActions() 
	{
		
	}
	
	private function _adminActions() 
	{
		
	}
}