<?php
class Constants 
{

	// current modules
	const PAGE_MAIN = 'main';
	
	const PAGE_OUTER_SYSTEMS = 'outer_systems';
	const PAGE_OUTER_SYSTEMS_ADD = 'outer_systems_add';
	const PAGE_OUTER_SYSTEMS_EDIT = 'outer_systems_edit';
	const PAGE_USERS = 'users';
	const PAGE_USER_ADD = 'user_add';
	const PAGE_USER_EDIT = 'user_edit';
	const PAGE_PRODUCT_ADD = 'product_add';
	const PAGE_PRODUCT_EDIT = 'product_edit';
	const PAGE_PRODUCT_SERVICE_ADD = 'product_service_add';
	const PAGE_PRODUCT_SERVICE_EDIT = 'product_service_edit';
	const PAGE_SERVICES = 'services';
	const PAGE_SERVICE_ADD = 'service_add';
	const PAGE_SERVICE_EDIT = 'service_edit';
	const PAGE_PAY = 'pay';
	const PAGE_STATISTICS = 'statistics';

	
	const PAGE_SERVICES_AND_PRODUCTS = 'services_and_products';


	
	
	const PAGE_MAIN_ID = 1;
	const PAGE_ADVERT_ID = 2;
	const PAGE_ARTICLES_ID = 4;
	const PAGE_CATALOG_ID = 5;
	const PAGE_REVIEW_ID = 6;
	const PAGE_INFO_ID = 7;
	const PAGE_NEWS_ID = 8;
	const PAGE_TRANSPORT_ID = 10;
	const PAGE_ACCOUNTS_ID = 11;
	

	const ITEMS_PER_PAGE_DEFAULT = 20;
	
	const DATE_NO_PERIOD = '2079-01-01 00:00:00';
	const DATE_LASTED_PERIOD = '0000-00-00 00:00:00';
	
}