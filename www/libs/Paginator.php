<?php

class Paginator implements Iterator
{
	const PAGE_LABEL = 'page';
	const PREV_PAGE_TEXT = '<< пред';
	const NEXT_PAGE_TEXT = 'след >>';
	const PAGE_SPACER = '...';
	
	const PAGE_NUMBER_MAX = 500;

	private $_pages = array();
	private $_page_uri = '';
	private $_current_page = '';
	private $_current_id = 0;
	private $_search_string = '';
	private $_total_items = 0;

	
	public function __construct()
	{
		$this->setPageUri(trim(Environment::getParseUrl()->getPageUri(), '/'));
		$this->_current_page = Environment::getParseUrl()->getPage();
	}
	
	public function setPageUri($page_uri)
	{
		if (mb_strpos($page_uri, '?') !== false) {
			$this->_page_uri = mb_substr($page_uri, 0, mb_strpos($page_uri, '?')) //. '%s' 
				. mb_substr($page_uri, mb_strpos($page_uri, '?'));
		} else {
			$this->_page_uri = $page_uri; //. '%s';
		}
	}
	
	public function havePages() 
	{
		return !empty($this->_pages);
	}
	
	public function getPagesAsArray()
	{
		return $this->_pages;
	}

	public function getCurrentPage()
	{
		return !empty($this->_current_page) ? $this->_current_page : 1;
	}
	
	public function setItemsCount($total_items)
	{
		if (is_numeric($total_items)) {
			$this->_total_items = $total_items;
		}
	}
	
	public function getItemsCount()
	{
		return $this->_total_items;
	}
	
	public function getPages()
	{
		return $this->_pages;
	}
	
	public function setSearchString($search_string)
	{
		$this->_search_string = $search_string;
	}
	
	public function setPagesFromValues($total_count, $per_page = 0)
	{
		$this->setItemsCount($total_count);

		if (empty($per_page)) {
			$per_page = Environment::getParseUrl()->getPerPage();
		}
		
		$pages = ceil($total_count / $per_page);
		$this->setPages((int)$pages);
	}
	
	public function setPages($total_pages) 
	{
		if ($total_pages > self::PAGE_NUMBER_MAX) {
			$total_pages = self::PAGE_NUMBER_MAX;
		}
	
		if (is_int($total_pages) && $total_pages > 1 && $this->_current_page <= $total_pages) {
			if($this->_current_page > 1) {
				$this->_addPrevPage($this->_current_page - 1);
			}
			if($total_pages < 12) {
				for($i = 1; $i <= $total_pages; $i++) {
					$this->_addPage($i);
				}
			} elseif($this->_current_page < 7) {
				for($i = 1; $i <= $this->_current_page + 2; $i++) {
					$this->_addPage($i);
				}
				$this->_addSpacerPage();
				for($i = $total_pages - 2; $i <= $total_pages; $i++) {
					$this->_addPage($i);
				}
			} elseif($this->_current_page > $total_pages - 6) {
				for($i = 1; $i <= 3; $i++) {
					$this->_addPage($i);
				}
				$this->_addSpacerPage();
				for($i = $this->_current_page - 2; $i <= $total_pages; $i++) {
					$this->_addPage($i);
				}
			} else {
				for($i = 1; $i <= 3; $i++) {
					$this->_addPage($i);
				}
				$this->_addSpacerPage();
				for($i = $this->_current_page - 2; $i <= $this->_current_page + 2; $i++) {
					$this->_addPage($i);
				}
				$this->_addSpacerPage();
				for($i = $total_pages - 2; $i <= $total_pages; $i++) {
					$this->_addPage($i);
				}
			}
			if($this->_current_page < $total_pages) {
				$this->_addNextPage($this->_current_page + 1);
			}
		} else {
			$this->_pages = array();
		}
	}
	
	private function _addPage($number_page)
	{
		if ($number_page == $this->_current_page) {
			$uri = '';
		} elseif ($number_page == 1) {
			//$uri = sprintf($this->_page_uri, '') . $this->_search_string;
			$uri = Common_Helper::addParameterToUri($this->_page_uri . $this->_search_string, self::PAGE_LABEL, $number_page);
		} else {
			//$uri = sprintf($this->_page_uri, self::PAGE_LABEL . $number_page) . $this->_search_string;
			$uri = Common_Helper::addParameterToUri($this->_page_uri . $this->_search_string, self::PAGE_LABEL, $number_page);
		}
		$this->_pages[] = array(
			'url' => $uri,
			'label' => $number_page,
			'current' => $number_page == $this->_current_page,
			'number' => $number_page,
		);
	}
	
	private function _addSpacerPage()
	{
		$this->_pages[] = array(
			'url' => '',
			'label' => self::PAGE_SPACER,
			'current' => false,
			'number' => 0,
		);
	}
	
	private function _addPrevPage($number_page)
	{
		$this->_pages[] = array(
			'url' => sprintf($this->_page_uri, ($number_page == 1 ? '' : (self::PAGE_LABEL . $number_page)) . $this->_search_string),
			'label' => self::PREV_PAGE_TEXT,
			'current' => false,
			'number' => $number_page,
		);
	}
	
	private function _addNextPage($number_page)
	{
		$this->_pages[] = array(
			//'url' => sprintf($this->_page_uri, self::PAGE_LABEL . $number_page . $this->_search_string),
			'url' => Common_Helper::addParameterToUri($this->_page_uri . $this->_search_string, self::PAGE_LABEL, $number_page),
			'label' => self::NEXT_PAGE_TEXT,
			'current' => false,
			'number' => $number_page,
		);
	}
	
	// for Iterator interface
	
	public function rewind()
    {
        $this->_current_id = 0;
    }
  
    public function current()
    {
        return $this->_pages[$this->_current_id];
    }
  
    public function key() 
    {
        return $this->_current_id;
    }
  
    public function next() 
    {
		if (isset($this->_pages[$this->_current_id])) {
			$next_item = $this->_pages[$this->_current_id];
			$this->_current_id++;
		} else {
			$next_item = false;
		}

		return $next_item;
    }
	
	public function prev() 
    {
		return isset($this->_pages[$this->_current_id - 1]) ? $this->_pages[--$this->_current_id] : false;
    }
  
    public function valid()
    {
        return isset($this->_pages[$this->_current_id]);
    }

}