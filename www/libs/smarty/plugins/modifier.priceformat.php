<?php
function smarty_modifier_priceformat($string, $ex = ' ')
{
	if (preg_match('~( \$| &euro;|\$|&euro;)~', $string, $strs)) {
		$string = str_replace($strs[1], '',$string);
	}
	
	return strrev(chunk_split(strrev($string), 3, $ex)) . (!empty($strs[1]) ? $strs[1] : ''); 
}

