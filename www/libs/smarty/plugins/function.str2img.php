<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {str2img} function plugin
 *
 * Type:     function<br>
 * Name:     str2img<br>
 * Date:     Sept 5, 2008<br>
 * Purpose:  display a string as an image<br>
 * Input:
 *         - string = string to display in the image
 *         - font = font to use (optional)
 *         - fontsize = font size in pixels (optional)
 *         - fontangle = font angle in degrees (optional)
 *         - textcolor = text color in HEX (optional)
 *         - bgcolor = background color in HEX (optional)
 *
 * Examples:<br>
 * <pre>
 * {str2img text="myemail@domain.com"}
 * </pre>
 * @author Monte Ohrt <monte at ohrt dot com>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return null
 */

require($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'libs'.DIRECTORY_SEPARATOR.'str2img.lib.php');

function smarty_function_str2img($params, &$smarty)
{
  
// set text to render
$text = !empty($params['text']) ? $params['text'] : 'no input'; 

$str2img = new str2img();

if(!empty($params['bgcolor']))
  $str2img->bgcolor = $params['bgcolor'];
if(!empty($params['textcolor']))
  $str2img->textcolor = $params['textcolor'];
if(!empty($params['font']))
  $str2img->font = $params['font'];
if(!empty($params['fontsize']))
  $str2img->fontsize = $params['fontsize'];
if(!empty($params['fontangle']))
  $str2img->fontangle = $params['fontangle'];
  
return $str2img->imgsrc($text);

}

/* vim: set expandtab: */

?>
