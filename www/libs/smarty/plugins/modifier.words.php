<?php
//Вывод определенного кол-ва слов из строки

function smarty_modifier_words($string, $words_count)
{
    return preg_match("/((\S+[\s-]+){".$words_count."})/s", $string, $m) ? rtrim($m[1]) . '...' : $string;
}