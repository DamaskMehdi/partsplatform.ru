<?php
function smarty_modifier_phones($string)
{
	if(strstr($string,'375(')) {
		if(strstr($string,'+')) {
			$arr = array("МТС"=>array("+375(29)2","+375(29)5","+375(29)7","+375(29)8","+375(33)"),
					"Velcom"=>array("+375(29)1","+375(29)3","+375(29)6","+375(29)9","+375(44)"),
					"Life:)"=>array("+375(25)"),
					"Dialog"=>array("+375(29)4"));
					
			$phone1 = substr($string,0,8);	
			$phone2 = substr($string,0,9);
		} else {
			$arr = array("МТС"=>array("375(29)2","375(29)5","375(29)7","375(29)8","375(33)"),
					"Velcom"=>array("375(29)1","375(29)3","375(29)6","375(29)9","375(44)"),
					"Life:)"=>array("375(25)"),
					"Dialog"=>array("375(29)4"));
					
			$phone1 = substr($string,0,7);	
			$phone2 = substr($string,0,8);
			$string = '+'.$string;
		}
		
		$icon = "Город.";

		foreach($arr as $key=>$val)
		{
			foreach($val as $vl)
			{
				if($vl==$phone1) $icon = $key;
				if($vl==$phone2) $icon = $key;
			}	
		}
		return $string."<span>".$icon."</span>";
	} 
	else return $string;
}
?>
