<?php 

function smarty_modifier_youtube($videoURL) { 
    if ($videoURL) {            
		if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $videoURL, $match)) {
			 $video_id = $match[1];
			 return $video_id;
		}
		else return $videoURL;
	} 
	else return false;
}