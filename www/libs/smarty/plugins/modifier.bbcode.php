<?php
/*
 * Smarty plugin
 * ------------------------------------------------------------
 * Type:     modifier
 * Name:     bbcode
 * Purpose:  Converts BBCode tags to HTML tags.
 * Version:  1.4.5
 * Date:     31.08.2006
 *
 * Install:  Drop into the plugins directory.
 * Author:   Sam
 * Homepage: http://rmcreative.ru/
 *
 * Based on Andre Rabold's bbcode modifier.
 * Added:    some new tags
 *           php code highlighting
 *           new line to <br> converter
 *              antispam mail protection
 *              rss compatibility
 *
 * Changes
 * 1.3:
 * [+] [h2],[h3],[h4],[h5],[h6] added
 * [*] [strike] tag changed to [s]
 * [*] More tags added & removed
 *
 * 1.4
 * [+] "@" replacement to ".sobaka." in email tag
 * [*] HTML entities displaying in [php] tags fixed
 * [-] HTML stripping removed
 *
 * 1.4.1
 * [*] <img/> tag changed to <img>
 *
 * 1.4.2
 * [*] All external CSS built in
 * [+] RSS Feeds Compatible
 *
 * 1.4.3
 * [*] HTML tags displaying fixed
 *
 * 1.4.4
 * [-] Some unncessary code cleaned ;)
 * [*] HTML displaying fixed once again
 * 
 * 1.4.5
 * [+] Valid e-mail indexing prevention  
 *
 * ------------------------------------------------------------
 */

function smarty_modifier_bbcode($message) {

	$message = preg_replace('/\[(\/?)(b|i|u|s)\s*\]/', "<$1$2>", $message);
	
	$message = preg_replace('/\[code\]/', '<pre><code>', $message);
	$message = preg_replace('/\[\/code\]/', '</code></pre>', $message);
	
	//$message = preg_replace('#\[color=([^\]]+)\](.+?)\[/color\]#is','<span style=\'color:\\1\'>\\2</span>',$message);
	$message = preg_replace('#\[color=([^\]]+)\](.+?)\[/color\]#is','<span style=\'color:#FF0000\'>\\2</span>',$message);
	
	$message = preg_replace('/\[(\/?)quote\]/', "<$1blockquote>", $message);
	$message = preg_replace('/\[(\/?)quote(\s*=\s*([\'"]?)([^\'"]+)\3\s*)?\]/', "<$1blockquote><cite>$4</cite>", $message);
	
	$message = preg_replace('/\[url\](?:http:\/\/)?([a-z0-9-.]+\.\w{2,4})\[\/url\]/', "<a href=\"http://$1\">$1</a>", $message);
	$message = preg_replace('/\[url\s?=\s?([\'"]?)(?:http:\/\/)?([a-z0-9-.]+\.\w{2,4})\1\](.*?)\[\/url\]/', "<a href=\"\" data-link=\"http://$2\">$3</a>", $message);
	
	
	$message = preg_replace('/\[img\s*\]([^\]\[]+)\[\/img\]/', "<img src='$1'/>", $message);
	$message = preg_replace('/\[img\s*=\s*([\'"]?)([^\'"\]]+)\1\]/', "<img src='$2'/>", $message);
	
	return $message;
/*   
   $preg = array(
          '~<([^<]+)>~' => '&lt;$1&gt;',
          // Macromedia flash object (commented for security reasons)
          //'~(?<!\\\\)\[flash(?::\w+)?=(.*?)x(.*?)\](.*?)\[/flash(?::\w+)?\]~si' => '<object width="\1" height="\2"><param name="movie" value="\3" /><embed src="\3" width="\1" height="\2"></embed></object>',
          // Text arrtibutes
          '~(?<!\\\\)\[s(?::\w+)?\](.*?)\[/s(?::\w+)?\]~si'        				=> '<del>\1</del>',
          '/(?<!\\\\)\[b(?::\w+)?\](.*?)\[\/b(?::\w+)?\]/si'                 	=> "<b>\\1</b>",
          '/(?<!\\\\)\[i(?::\w+)?\](.*?)\[\/i(?::\w+)?\]/si'                 	=> "<i>\\1</i>",
          '/(?<!\\\\)\[u(?::\w+)?\](.*?)\[\/u(?::\w+)?\]/si'                 	=> "<u>\\1</u>",
          '/(?<!\\\\)\[color(?::\w+)?=(.*?)\](.*?)\[\/color(?::\w+)?\]/si'   	=> "<span style=\"color:\\1\">\\2</span>",
          //align
          '/(?<!\\\\)\[leftfloat(?::\w+)?\](.*?)\[\/leftfloat(?::\w+)?\]/si' 	=> "<div style=\"float: left; margin-right: 10px; margin-left: 10px; clear: none;\">\\1</div>",
          '/(?<!\\\\)\[rightfloat(?::\w+)?\](.*?)\[\/rightfloat(?::\w+)?\]/si' 	=> "<div style=\"float: right; margin-right: 10px; margin-left: 10px; clear: none\">\\1</div>",
          '/(?<!\\\\)\[center(?::\w+)?\](.*?)\[\/center(?::\w+)?\]/si'       	=> "<div style=\"text-align: center\">\\1</div>",
          '/(?<!\\\\)\[left(?::\w+)?\](.*?)\[\/left(?::\w+)?\]/si'           	=> "<div style=\"text-align: left\">\\1</div>",
          '/(?<!\\\\)\[right(?::\w+)?\](.*?)\[\/right(?::\w+)?\]/si'        	=> "<div style=\"text-align: right\">\\1</div>",
          //headers
          '/(?<!\\\\)\[h1(?::\w+)?\](.*?)\[\/h1(?::\w+)?\]/si'               	=> "<h1>\\1</h1>",
          '/(?<!\\\\)\[h2(?::\w+)?\](.*?)\[\/h2(?::\w+)?\]/si'               	=> "<h2>\\1</h2>",
          '/(?<!\\\\)\[h3(?::\w+)?\](.*?)\[\/h3(?::\w+)?\]/si'               	=> "<h3>\\1</h3>",
          '/(?<!\\\\)\[h4(?::\w+)?\](.*?)\[\/h1(?::\w+)?\]/si'               	=> "<h4>\\1</h4>",
          '/(?<!\\\\)\[h5(?::\w+)?\](.*?)\[\/h2(?::\w+)?\]/si'               	=> "<h5>\\1</h5>",
          '/(?<!\\\\)\[h6(?::\w+)?\](.*?)\[\/h3(?::\w+)?\]/si'               	=> "<h6>\\1</h6>",

          // Code & PHP frames. PHP is highlighted ;)
          '/(?<!\\\\)\[code(?::\w+)?\](.*?)\[\/code(?::\w+)?\]/si'           	=> "<div style=\"text-align: left; border: 1px solid #cccccc; background-color: #e8e8e8; padding-left: 10px; padding-right: 10px; font-family: Courier-new, monospace; font-size:13\">\\1</div>",
          '/(?<!\\\\)\[php(?::\w+)?\](.*?)\[\/php(?::\w+)?\]/sei'            	=> "'<div style=\"text-align: left; border: 1px solid #cccccc; background-color: #e8e8e8; padding-left: 10px; padding-right: 10px; font-family: Courier-new, monospace; font-size:13\">'.highlight_string('<?php\n'.'$1'.'\n?>', true).'</div>'",

          // email with indexing prevention & @ replacement
          '/(?<!\\\\)\[email(?::\w+)?\](.*?)\[\/email(?::\w+)?\]/sei'         	=> "'<a rel=\"noindex\" href=\"mailto:'.str_replace('@', '.at.','$1').'\">'.str_replace('@', '.at.','$1').'</a>'",
          '/(?<!\\\\)\[email(?::\w+)?=(.*?)\](.*?)\[\/email(?::\w+)?\]/sei'   	=> "'<a rel=\"noindex\" href=\"mailto:'.str_replace('@', '.at.','$1').'\">$2</a>'",
          //"'\\1'.strtoupper('\\2').'\\3'"
          // links
          '/(?<!\\\\)\[url(?::\w+)?\]www\.(.*?)\[\/url(?::\w+)?\]/si'        	=> "<a href=\"http://www.\\1\">\\1</a>",
          '/(?<!\\\\)\[url(?::\w+)?\](.*?)\[\/url(?::\w+)?\]/si'             	=> "<a href=\"\\1\">\\1</a>",
          '/(?<!\\\\)\[url(?::\w+)?=(.*?)?\](.*?)\[\/url(?::\w+)?\]/si'      	=> "<a href=\"\\1\">\\2</a>",
          // images
          '/(?<!\\\\)\[img(?::\w+)?\](.*?)\[\/img(?::\w+)?\]/si'             	=> "<img src=\"$1\" alt=\"$1\">",
          '/(?<!\\\\)\[img(?::\w+)?=(.*?)x(.*?)\](.*?)\[\/img(?::\w+)?\]/si' 	=> "<img width=\"$1\" height=\"$2\" src=\"$3\" alt=\"$3\">",
          // alt. images
          '/(?<!\\\\)\[nbimg(?::\w+)?\](.*?)\[\/nbimg(?::\w+)?\]/si'             => "<img src=\"$1\" alt=\"$1\">",
          '/(?<!\\\\)\[nbimg(?::\w+)?=(.*?)x(.*?)\](.*?)\[\/nbimg(?::\w+)?\]/si' => "<img width=\"$1\" height=\"$2\" src=\"$3\" alt=\"$3\">",
          // quoting
          '/(?<!\\\\)\[quote(?::\w+)?\](.*?)\[\/quote(?::\w+)?\]/si'         	=> "<blockquote>\\1</blockquote>",
          '/(?<!\\\\)\[quote(?::\w+)?=(?:&quot;|"|\')?(.*?)["\']?(?:&quot;|"|\')?\](.*?)\[\/quote\]/si'   => "<blockquote><cite>\\1:</cite>\\2</blockquote>",

          // lists
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\*(?::\w+)?\](.*?)(?=(?:\s*<br\s*\/?>\s*)?\[\*|(?:\s*<br\s*\/?>\s*)?\[\/?list)/si' => "\n<li>\\1</li>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\/list(:(?!u|o)\w+)?\](?:<br\s*\/?>)?/si'    => "\n</ul>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\/list:u(:\w+)?\](?:<br\s*\/?>)?/si'         => "\n</ul>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\/list:o(:\w+)?\](?:<br\s*\/?>)?/si'         => "\n</ol>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(:(?!u|o)\w+)?\]\s*(?:<br\s*\/?>)?/si'   => "\n<ul>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list:u(:\w+)?\]\s*(?:<br\s*\/?>)?/si'        => "\n<ul>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list:o(:\w+)?\]\s*(?:<br\s*\/?>)?/si'        => "\n<ol>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=1\]\s*(?:<br\s*\/?>)?/si' => "\n<ol style=\"list-style-type:decimal\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=i\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol style=\"list-style-type:lower-roman\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=I\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol style=\"list-style-type:upper-roman\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=a\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol style=\"list-style-type:lower-alpha\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=A\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol style=\"list-style-type:upper-alpha\">",

          // escaped tags like \[b], \[color], \[url], e.t.c.
          '/\\\\(\[\/?\w+(?::\w+)*\])/'                                      => "\\1",

          //new line to <br>
          '~\n~' => '<br>'
          
          
  );
  return preg_replace(array_keys($preg), array_values($preg), $message);
 */
}