<?php
function smarty_modifier_in_array($string, $array, $sep=null)
{
	if(is_array($string)) return in_array($array,$string);
	else
	{
		if(is_array($array)) return in_array($string, $array);
		elseif($sep!=null){
			$arr = split($sep,$array);
			return in_array($string, $arr);
		}
		else return strstr($string,$array);
	}
}