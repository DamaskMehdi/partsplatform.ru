<?php
	function smarty_function_resize($params, &$smarty) {
		header('Content-Type: image/jpeg');
		include('../../classes/resize.class.php');

		$image = new SimpleImage();
		$image->load($params['img_url']);
		
		#$width = $image->getWidth();
		#$height = $image->getHeight();
		
		if($params['height']>$params['width']) $image->resizeToHeight($params['height']);
		else $image->resizeToWidth($params['width']);
		
		$image->output();
	}