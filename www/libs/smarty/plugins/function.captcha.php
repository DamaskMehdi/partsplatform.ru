<?php
/**
* Smarty plugin
* @package Smarty
* @subpackage plugins
*/


/**
* Smarty {captcha} function plugin
*
* Type: function<br>
* Name: captcha<br>
* Purpose: Display captcha-images in webpages
* @author PHPeter.eu
* @link http://www.phpeter.eu/
* @param array
* @return string
* {:captcha width="250" height="120" font="luttx43.ttf" fontsize="25" font_x="5" font_y="115" length="7" color="FF0000" shadow="00FF00" background="EEEEEE" session_name="captcha_in_md5":} 
*/

function smarty_function_captcha_setcaptcha($length, $sessname){
    $chars = '23456789ABCDEFGHJKMNPQRSTVWXYZ';
    $code = '';
    $i = 0;
    while ($i < $length) { 
     $code .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
     $i++;
    }
    $_SESSION[$sessname] = md5($code);
    return $code;
}

function smarty_function_captcha_verify($plugin='', $param_model, $params) {
    foreach(array('requires', 'allowed', 'deprecated') as $category) {
        if (!array_key_exists($category, $param_model)) {
            $param_model[$category] = array();
        }
    }
    $_missing = array_diff(array_keys($param_model['requires']), array_keys($params));
    if (!empty($_missing)) {
        throw new SmartyException("[$plugin] missing required parameter(s) for ".join(', ', $_missing), E_USER_NOTICE);
    }
    
    foreach ($param_model['requires'] as $field=>$type) {
        if (array_key_exists($field, $params) && !empty($params[$field])) {
            $_params[$field] = $params[$field];
        }
        else{
            throw new SmartyException("[$plugin] supplied parameter unknown for ".$field." = ".$params[$field], E_USER_NOTICE);
        }
    }
    foreach ($param_model['allowed'] as $field=>$type) {
        if (array_key_exists($field, $params) && isset($params[$field])) {
            $_params[$field] = $params[$field];
        }
    }
    foreach ($param_model['deprecated'] as $field=>$type) {
        if (array_key_exists($field, $params)) {
            throw new SmartyException("[$plugin] deprecated parameter '$field'", E_USER_NOTICE);
            $_params[$field] = $params[$field];
        }
    }
    return $_params;
}

function smarty_function_captcha($params){
    $_params = smarty_function_captcha_verify(
    'captcha'
    , array(
    'requires' => array(
    'width' => 'int',
    'height' => 'int',
    'font' => 'string',
    'fontsize' => 'int',
    'font_x' => 'int',
    'font_y' => 'int',
    'length' => 'int'
    )
    , 'allowed' => array(
    'color' => 'hex',
    'background' => 'hex',
    'shadow' => 'hex',
    'random_angle' => 'bool',
    'cssclass' => 'string',
    'alttext' => 'string',
    'font_dir' => 'string',
    'session_name' => 'string'
    )
    , 'deprecated' => array(
    )
    )
    , $params
    );
    $atags = array();
    foreach ($_params as $field=>$value) {
        switch($field){
            case "font":
                $font = $value;
            break;
            case "font_dir":
                define('SMARTY_FONT_DIR', $value . DS);
            break;
            case "fontsize":
                if(is_int((int)$value)){
                    $size = (int)$value;
                }
            break;
            case "font_x":
                if(is_int((int)$value)){
                    $fontx = (int)$value;
                }
            break;
            case "font_y":
                if(is_int((int)$value)){
                    $fonty = (int)$value;
                }
            break;
            case "width":
                if(is_int((int)$value)){
                    $width = (int)$value;
                }
                $atags[] = 'width="'.$value.'"';
            break;
            case "height":
                if(is_int((int)$value)){
                    $height = (int)$value;
                }
                $atags[] = 'height="'.$value.'"';
            break;
            case "length":
                if(is_int((int)$value) && $value >= 1){
                    $length = (int)$value;
                }
                else{
                    $length = 6;
                }
            break;
            case "color":
                if(strlen($value) != 6){
                    throw new SmartyException("[$plugin] wrong value for '$field'", E_USER_NOTICE);
                }
                else{
                    $color_red = hexdec(substr($value, 0, 2));
                    $color_green = hexdec(substr($value, 2, 2));
                    $color_blue = hexdec(substr($value, 4, 2));
                }
            break;
            case "shadow":
                if(strlen($value) != 6){
                    throw new SmartyException("[$plugin] wrong value for '$field'", E_USER_NOTICE);
                }
                else{
                    $shadow_red = hexdec(substr($value, 0, 2));
                    $shadow_green = hexdec(substr($value, 2, 2));
                    $shadow_blue = hexdec(substr($value, 4, 2));
                }
            break;
            case "background":
                if(strlen($value) != 6){
                    throw new SmartyException("[$plugin] wrong value for '$field'", E_USER_NOTICE);
                }
                else{
                    $back_red = hexdec(substr($value, 0, 2));
                    $back_green = hexdec(substr($value, 2, 2));
                    $back_blue = hexdec(substr($value, 4, 2));
                }
            break;
            case "random_angle":
                if(is_bool($value)){
                    $rand_angle = $value;
                }
                else{
                    $rand_angle = false;
                }
                if($rand_angle == true){
                    $angle = (rand(0,50) - 25);
                }
                else{
                    $angle = 0;
                }
            break;
            case "cssclass":
                $atags[] = 'class="'.$value.'"';
            break;
            case "alttext":
                $atags[] = 'alt="'.$value.'"';
            break;
            case "session_name":
                $sessionname = $value;
            break;
            default:
                throw new SmartyException("[$plugin] unknown parameter '$field'", E_USER_NOTICE);
            break;
        }
    }
    if(!isset($sessionname) || strip_tags($sessionname) == ""){
        $sessionname = "captcha";
    }
    $captcha = smarty_function_captcha_setcaptcha($length, $sessionname);
    if(!isset($angle) || !is_numeric($angle) || $angle > 25 || $angle < -25){
        $angle = 0;
    }
    $stags = implode(" ", $atags);
    if(!isset($color_red)){
        $color_red = 255;
        $color_green = 255;
        $color_blue = 255;
    }
    if(!isset($shadow_red)){
        $shadow_red = 128;
        $shadow_green = 128;
        $shadow_blue = 128;
    }
    if(!isset($back_red)){
        $back_red = 0;
        $back_green = 0;
        $back_blue = 0;
    }
    if (!defined('SMARTY_FONT_DIR')) {
        define('SMARTY_FONT_DIR', SMARTY_DIR . 'font' . DS);
    }
    $im = imagecreatetruecolor($width, $height);
    $font_captcha_col = imagecolorallocate($im, $color_red, $color_green, $color_blue);
    $shadow_captcha = imagecolorallocate($im, $shadow_red, $shadow_green, $shadow_blue);
    $background_captcha = imagecolorallocate($im, $back_red, $back_green, $back_blue);
    imagefilledrectangle($im, 0, 0, $width, $height, $background_captcha);
    $font = SMARTY_FONT_DIR . $font;
    imagettftext($im, $size, $angle, $fontx, $fonty, $shadow_captcha, $font, $captcha);
    imagettftext($im, $size, $angle, ($fontx + 5), ($fonty + 8), $font_captcha_col, $font, $captcha);
    ob_start();
    imagegif($im);
    imagedestroy($im);
    $cap_img_str = ob_get_clean();
    $cap_img_crypt = base64_encode($cap_img_str);
    return "<img src=\"data:image/gif;base64,".$cap_img_crypt."\" ".$stags." />";

}

?>