<?php
class DB
{
	private static $_instance = null;

	private $_persistant_connection = false;
	private $_default_charset = 'utf8';

	private $_db = null;
	
	private $_debug = array();

	private function __construct($host, $user, $pwd, $dbname)
	{
		$this->open($host, $user, $pwd, $dbname);
	}
	
	public static function getDB($host, $user, $pwd, $dbname, $label = '')
	{
		if (!isset(self::$_instance[$label])) {
			self::$_instance[$label] = new self($host, $user, $pwd, $dbname);
		}
		
		return self::$_instance[$label];
	}
	
	public function open($host, $user, $pwd, $dbname)
	{
		if ($this->_db === null) {
			$db_config = Config::getParam('DB');
			$coonection_string = 'mysql:host=' . $host . ';dbname=' . $dbname;
			
			$params = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
			/*if ($this->_persistant_connection) {
				$params[PDO::ATTR_PERSISTENT] = true;
			}*/
			
			try {
				$this->_db = new PDO($coonection_string, $user, $pwd, $params);
				$this->_db->query('set names ' . $this->_default_charset);
			} catch(PDOException $e) {
				/** @TODO add handler for errors */
				//die("Error: ".$e->getMessage());
			}
		}
	}
	
	/**
	 * @param string $query
	 * @return PDOStatement
	 */
	public function prepare($query)
	{
		return $this->_db->prepare($query);
	}
	
	/**
	 * @param string $query
	 * @param array $params
	 * @return PDOStatement
	 */
	public function prepareWithExec($query, $params = array())
	{
		$start_time = microtime(true);

		$st = $this->_db->prepare($query);
		if (!empty($params)) {
			foreach ($params as $name => $param) {
				$st->bindParam($name, $param['value'], $param['type']);
				$query = str_replace($name, $param['value'], $query);
			}
		}

		$st->execute();
		
		$end_time = microtime(true);
		$this->_debug[] = array(
			'time' => $end_time - $start_time,
			'query' => $query,
		);
	
		return $st;
	}
	
	/**
	 * @param mixed $value
	 * @return string
	 */
	public function quote($value)
	{
		return $this->_db->quote($value);
	}
	
	/**
	 * @return int
	 */
	public function lastInsertId()
	{
		return $this->_db->lastInsertId();
	}
	
	/**
	 * @return PDOStatement
	 */
	public function query($sql)
	{
		$start_time = microtime(true);
		$res = $this->_db->query($sql);
		$end_time = microtime(true);
		$this->_debug[] = array(
			'time' => $end_time - $start_time,
			'query' => $sql,
		);
		if ($this->_db->errorCode() != 0000) {
			$res = false;
		}
	
		return $res;
	}
	
	/**
	 * @param string $query
	 * @return int|bool
	 */
	public function exec($query)
	{
		$start_time = microtime(true);
		$res = $this->_db->exec($query);
		$end_time = microtime(true);
		$this->_debug[] = array(
			'time' => $end_time - $start_time,
			'query' => $query,
		);
		
		if ($this->_db->errorCode() != 0000) {
			$rows = false;
		}
 		/** @TODO add logging for errors */
	
		return $res;
	}
	

	/**
	 * For compatibility with old class.db.php syntax
	 * @param string $query
	 * @return mixed
	 */
	public function getVal($query)
	{
		$res = $this->query($query);
		if ($res) {
			$row = $res->fetch();
			$value = array_shift($row);
		} else {
			$value = false;
		}
		/** @TODO add handler for errors */
		
		return $value;
	}
	
	/**
	 * Return debug info about queries
	 * 
	 * @return array
	 */
	public function getDebug()
	{
		if (Config::getParam('DOMAIN') == 'new.autoban.by') {
			$total_time = 0;
			foreach ($this->_debug as $item) {
				$total_time += $item['time'];
			}
			
			$debug = array(
				'total_time' => round($total_time, 3),
				'count_queries' => count($this->_debug),
				'items' => $this->_debug,
			);
		} else {
			$debug = array();
		}
	
		return $debug;
	}

	
	public function close()
	{
		$this->_db = null;
	}
} 