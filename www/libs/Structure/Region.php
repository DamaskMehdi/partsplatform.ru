<?php

class Structure_Region extends Item_Abstract
{
	protected $_table = 'atrru.domains';

	protected $_fields = array(
		'id',
		'domain',
		'city',
		'city_to',
		'title',
		'meta_description',
		'meta_keywords',
		'main_text',
		'visible',
		'tires_text',
	);
	
	
	protected function _convertFields()
	{
		
	}
}