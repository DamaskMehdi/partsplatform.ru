<?php

class Structure_Page extends Item_Abstract
{
	const MAIN = 'main';
	const BLANK = 'blank';


	protected $_table = 'yc_pages';

	protected $_fields = array(
		'id',
		'name',
		'title',
		'meta_description',
		'meta_keywords',
		'uri',
		'template_name',
		'visible',
	);
	
	protected $_parents;

	
	
	public function getParents()
	{
		$this->_setParents();

		return $this->_parents;
	}
	
	public function hasParents()
	{
		$this->_setParents();
		
		return !empty($this->_parents);
	}
	
	protected function _setParents()
	{
		if (is_null($this->_parents)) {
			$parents = array();

			if ($this->getField('parent_id')) {
				$sql = "select p2.id p2_id, p2.name p2_name, p2.name_rus p2_name_rus, p2.title p2_title, p2.uri p2_uri,
						p3.id p3_id, p3.name p3_name, p3.name_rus p3_name_rus, p3.title p3_title, p3.uri p3_uri,
						p4.id p4_id, p4.name p4_name, p4.name_rus p4_name_rus, p4.title p4_title, p4.uri p4_uri,
						p5.id p5_id, p5.name p5_name, p5.name_rus p5_name_rus, p5.title p5_title, p5.uri p4_uri
					from " . $this->_table . " p
					left join yc_pages p2 on p.parent_id = p2.id
					left join yc_pages p3 on p2.parent_id = p3.id
					left join yc_pages p4 on p3.parent_id = p4.id
					left join yc_pages p5 on p4.parent_id = p5.id
					where p.id = " . $this->getField('id');
				
				$res = Environment::getDB()->query($sql)->fetch();
				
				if (!empty($res['p5_id'])) {
					$parents[] = array(
						'id' => $res['p5_id'],
						'name' => $res['p5_name'],
						'name_rus' => $res['p5_name_rus'],
						'title' => $res['p5_title'],
						'uri' => $res['p5_uri'],
					);
				}
				if (!empty($res['p4_id'])) {
					$parents[] = array(
						'id' => $res['p4_id'],
						'name' => $res['p4_name'],
						'name_rus' => $res['p4_name_rus'],
						'title' => $res['p4_title'],
						'uri' => $res['p4_uri'],
					);
				}
				if (!empty($res['p3_id'])) {
					$parents[] = array(
						'id' => $res['p3_id'],
						'name' => $res['p3_name'],
						'name_rus' => $res['p3_name_rus'],
						'title' => $res['p3_title'],
						'uri' => $res['p3_uri'],
					);
				}
				if (!empty($res['p2_id'])) {
					$parents[] = array(
						'id' => $res['p2_id'],
						'name' => $res['p2_name'],
						'name_rus' => $res['p2_name_rus'],
						'title' => $res['p2_title'],
						'uri' => $res['p2_uri'],
					);
				}
			}
			
			$this->_parents = $parents;
		}
	}
}