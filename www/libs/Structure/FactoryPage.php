<?php

class Structure_FactoryPage extends Item_Abstract
{

	
	public static function create($page_uri)
	{
		$page = new Structure_Page(array('uri' => $page_uri));
		
		return $page;
	}

}