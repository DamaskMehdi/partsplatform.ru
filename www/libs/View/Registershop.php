<?php
class View_Registershop extends View_AbstractView
{

	public function __construct() 
	{
		parent::__construct();
		
	}
	
	public function getPage() 
	{
		$this->_tpl->assign('page_template', 'registershop.tpl');
		
		if (Environment::getGet()) {
			if (Environment::getModel('Account')->addShop(Environment::getGet())) {
				Environment::getUser()->login(Environment::getGetVar('email'), Environment::getGetVar('pwd'));
				$this->redirect('/account');
			} else {
				$this->_tpl->assign('error_message', 'Не удалось зарегистрировать Ваш магазин');
			}
		}
	}
	
}