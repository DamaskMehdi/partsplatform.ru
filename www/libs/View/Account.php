<?php

class View_Account extends View_AbstractView
{
	private $_links = array(


	);


	public function __construct()
	{
		parent::__construct();

	}

	public function getPage()
	{
		$this->_tpl->assign('page_template', 'account.tpl');

		$this->_uri = Environment::getParseUrl()->getUriArray();

		$this->_redirect();

		if (!empty($this->_uri[1])) {
			if ($this->_uri[1] == 'logout') {
				$this->getLogoutPage();
			} elseif ($this->_uri[1] == 'enter') {
				$this->getEnterPage();
			} elseif ($this->_uri[1] == 'transactions') {
				$this->getTransactionsPage();
			} elseif ($this->_user->getField('seller')) {
				switch ($this->_uri[1]) {
					case 'click_stats':
						$this->getClickStatsPage();
						break;
					default:
						$this->getMainPage();
						break;
				}
			} else {
				switch ($this->_uri[1]) {
					case 'orders':
						$this->getBuyerOrdersPage();
						break;
					default:
						$this->getMainBuyerPage();
						break;
				}
			}
		} else {
			$this->getMainPage();
		}
	}

	public function getMainPage()
	{
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.form.js');
		$this->_tpl->assign('page_tpl', 'main.tpl');
		
		if (Environment::getGet()) {
			Environment::getModel('Account')->editAccount(Environment::getUser()->getId(), Environment::getUser()->getField('type'), Environment::getGet());
			
			Common_Helper::createRedirect('/account');
		}

		$this->_tpl->assign('account_data', Environment::getModel('Account')->getAccountAddFields(Environment::getUser()->getId(), Environment::getUser()->getField('type'))); 
	}
	
	
	public function getTransactionsPage()
	{
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.form.js');
		$this->_tpl->assign('page_tpl', 'transactions.tpl');
		
		$transactions_list = Environment::getModel('Account')->getTransactionList(Environment::getUser()->getId(), Environment::getUser()->getField('type'));
		$this->_tpl->assign('transactions_list', $transactions_list);
	}
	
	
	public function getMainBuyerPage()
	{
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.form.js');
		$this->_tpl->assign('page_tpl', 'buyer_main.tpl');
		
		$mdcompany = new Model_Company(Environment::getDB('local'));
		$this->_tpl->assign('category_values', $mdcompany->getCategoryValues());
	}
	
	public function getBuyerOrdersPage()
	{
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.form.js');
		$this->_tpl->assign('page_tpl', 'buyer_orders.tpl');
		
		
	}
	

	
	
	public function getClickStatsPage()
	{
		$this->_tpl->assign('page_tpl', 'click_stats.tpl');

		if ($this->_user->getField('seller')) {
			$company = new Item_Company(array('account_id' => $this->_user->getId()), Environment::getDB('local'));
			$this->_tpl->assign('company', $company);
			
			$clicks_stat = Environment::getModel('ShopStatistics')->getIpStatistics($company->getId());
			$this->_tpl->assign('clicks_stat', $clicks_stat);
			$this->_tpl->assign('click_stats_pages', Environment::getModel('ShopStatistics')->getPages());
		}
	}
	
	public function getDocumentsPage()
	{
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'jquery.form.js');
		$this->_tpl->assign('page_tpl', 'documents.tpl');

		$mdcompany = new Model_Company(Environment::getDB('local'));
		$this->_tpl->assign('category_values', $mdcompany->getCategoryValues());

		if ($this->_user->getField('seller')) {
			$company = new Item_Company(array('account_id' => $this->_user->getId()), Environment::getDB('local'));
			$this->_tpl->assign('company', $company);
		}
	}

	
	
	

	public function getLogoutPage()
	{
		Environment::getUser()->logout();
		
		Common_Helper::createRedirect(Config::getParam('SITEURL'));
	}
	
	

	public function getEnterPage()
	{
		$this->_tpl->assign('page_tpl', 'enter.tpl');

	}


	
	private function _getPageNumber()
	{
		$url = parse_url($_SERVER['REQUEST_URI']);
		if (!empty($url['query'])) {
			foreach (explode('&', $url['query']) as $param) {
				$p = explode('=', $param);
				if (count($p) == 2) {
					$get[$p[0]] = $p[1];
				}
			}
		}
		
		return !empty($get['page']) && $get['page'] > 1 ? $get['page'] : 1;
	}


	public function _redirect()
	{
		if (!empty($this->_uri[1]) && in_array($this->_uri[1], array('register', 'enter'))) {

		} elseif (!$this->_user->isAuthorized()) {
			Common_Helper::createRedirect(Config::getParam('SITEURL'));
		}

	}
}