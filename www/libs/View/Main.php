<?php

class View_Main extends View_AbstractView
{
	private $_links = array(


	);


	public function __construct()
	{
		parent::__construct();

	}

	public function getPage()
	{
		$this->_page_content->addScripts(Config::getParam('JSURL') . 'common.js');

		$this->_tpl->assign('page_template', 'main.tpl');

	}

}