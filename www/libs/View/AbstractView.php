<?php

abstract class View_AbstractView
{
	protected $_tpl;
	protected $_page_content;
	
	protected $_curpage;
	protected $_db;
	protected $_user;

	public function __construct()
	{
		$this->_tpl = Environment::getTPL();
		$this->_page_content = Environment::getPageContent();
		$this->_user = Environment::getUser();
		$this->_db = Environment::getDB();

		$this->_createPageVariables();
	}

	private function _createPageVariables()
	{
		$this->_setMap();
		$this->_setRegions();
	}
	

	private function _setMap()
	{
		$cities_coordinates = array(
			'Москва' => '55.75399400,37.62209300',
			'Санкт-Петербург' => '59.91815364,30.30557800',
			'Екатеринбург' => '56.87837967,60.58276043',
			'Владивосток' => '43.11639473,131.91537722',
			'Калининград' => '54.70891314,20.51911010',
			'Волгоград' => '48.70693136,44.48704091',
			'Казань' => '55.78873767,49.11081375',
			'Краснодар' => '45.02740857,38.97595291',
			'Красноярск' => '56.01817343,92.90429096',
			'Нижний Новгород' => '56.31439875,44.03066417',
			'Новосибирск' => '55.01344628,82.94254409',
			'Омск' => '54.96347539,73.37019025',
			'Пермь' => '57.98765947,56.22039075',
			'Ростов-на-Дону' => '47.21947620,39.71419637',
			'Самара' => '53.17914551,50.13224741',
			'Крым' => '45.14925905,34.20758298',
		);

		$this->_tpl->assign('current_city_coordinates', isset($cities_coordinates[Environment::getRegion()->getField('city')]) 
			? $cities_coordinates[Environment::getRegion()->getField('city')] : $cities_coordinates['Москва']);
		$this->_tpl->assign('cities_coordinates', $cities_coordinates);
		
	}
	
	
	private function _setRegions()
	{
		$region_model = new Model_Region();
		$this->_tpl->assign('regions', $region_model->getRegions());
	}
	
	
	public function redirect($url)
	{
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . $url);
		exit;
	}
	
	
	abstract public function getPage();
}