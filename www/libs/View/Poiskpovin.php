<?php

class View_Poiskpovin extends View_AbstractView
{

	private $_module_path = 'poiskpovin';
	private $_breadcrumbs = array();


	public function __construct()
	{
		parent::__construct();

		$this->_tecdoc = new Model_Tecdoc();
	}


	public function getPage()
	{
		$this->_tpl->assign('page_template', 'poiskpovin.tpl');

		if (Environment::getGetVar('search_text')) {
			$this->getSearchTextPage();
		} else {
			switch (Environment::getGetVar('ft')) {
				case 'searchText':
					$this->getSearchTextPage();
					break;

				case 'findByVIN':
				case 'findByFrame':
					$this->getTreeByVinPage();
					break;

				case 'findByGroup':
					$this->getGroupArticlesPage();
					break;

				case 'findByArticle':
					$this->getArticlesListByArticlePage();
					break;

				case 'listByCategory':
					$this->getCategoryGroupsPage();
					break;

				case 'groupDetails':
					$this->getGroupDetailsPage();
					break;

				case 'findByBrand':
					$this->getBrandFiltersPage();
					break;

				case 'detailInfo':
					$this->getDetailInfoPage();
					break;
				
				case 'findByWizard':
					$this->getModelsByWizardPage();
					break;

				default:
					$this->getMainPage();
					break;
			}
		}
		
		$this->_tpl->assign('breadcrumbs', $this->_breadcrumbs);
	}
	
	
	public function getMainPage()
	{
		$this->_tpl->assign('page_tpl', 'main.tpl');

		$this->_tpl->assign('message', Environment::getGetVar('message'));

		$data = Environment::getModel('Laximo_Laximo')->getCatalogs();
		if (!empty($data)) {
			foreach ($data->row as $key => $brand) {
				$brand->addAttribute('link', $this->_module_path . '?ft=findByBrand&c=' . $brand['code'] . '&ssd=');
			}

			$hide_brands = array();//'ABARTH', 'BMW Motorrad', 'Fiat Professional', 'DAF', 'MAN', 'Ram', 'Rolls-Royce', 'Subaru Japan', 'Subaru USA',
				//'Alfa Romeo', 'Chevrolet', 'Citroen', 'Fiat', 'Ford Europe', 'Honda', 'Lancia', 'Opel/Vauxhall', 'Peugeot', 'Subaru Europe', );
			$brands = Common_Helper::getByColumnsWithLetters($data->row, 9, $hide_brands);
			$this->_tpl->assign('brands', $brands);
			
			$this->_tpl->assign('h1_title', 'Поиск запчастей в ' . Environment::getRegion()->getField('city_to'));
			Environment::getPageContent()->setTitle('Поиск запчастей в ' . Environment::getRegion()->getField('city_to'));
			$this->_addBreadcrumb('Поиск запчастей в ' . Environment::getRegion()->getField('city_to'), '');
		}
	}
	
	
	public function getSearchTextPage()
	{
		$this->_tpl->assign('page_tpl', 'articles_list_demo.tpl');
		
		$search_text = trim(Environment::getGetVar('search_text'));

		$this->_tpl->assign('header_search_text', $search_text);
		
		

		$details_with_prices = Environment::getModel('Poiskpovin')->getArticlesListByArticle(Environment::getGetVar('search_text'));
		$this->_tpl->assign('details_with_prices', $details_with_prices);
		
		$this->_tpl->assign('h1_title', 'Поиск запчастей по артикулу ' . Environment::getGetVar('oem') . ' в ' . Environment::getRegion()->getField('city_to'));
		$this->_addBreadcrumb('Поиск по артикулу ' . Environment::getGetVar('oem'), '');
		
		
		/*if (mb_strlen($search_text, 'utf8') == 17) {
			Environment::setGetVar('vin', $search_text);
			$this->getTreeByVinPage();
		} elseif (preg_match('~^([A-z0-9]{3,7})-([0-9]{3,7})$~', $search_text, $matches)) {
			Environment::setGetVar('frame', $matches[1]);
			Environment::setGetVar('frameNo', $matches[2]);
			$this->getTreeByVinPage();
		} else {
			Environment::setGetVar('oem', $search_text);
			$this->getArticlesListByArticlePage();
		}*/
	}
	
	public function getDetailInfoPage()
	{
		$this->_tpl->assign('page_tpl', 'detail_info.tpl');
		
		Environment::getPageContent()->addStyles(Config::getParam('CSSURL') . 'fancybox.css');
		Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'jquery.fancybox.js');
		Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'jquery-ui.min.js');
		Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'rating.js');

		$detail = Environment::getModel('Laximo_Helper')->getDetailData(Environment::getGetVar('detailid'));

		$this->_tpl->assign('tecdoc_criterias', $detail['tecdoc_criterias']);
		$this->_tpl->assign('detail', $detail['detail']);
		$this->_tpl->assign('shops', $detail['prices']);

		if (!empty($detail['detail'])) {
			$h1_title = 'Деталь ' . $detail['detail']->FindDetails->detail['manufacturer'] . ' ' . $detail['detail']->FindDetails->detail['oem'] . ' в ' . Environment::getRegion()->getField('city_to');
		} else {
			$h1_title = 'Не найдена деталь';
		}
		
		$this->_tpl->assign('h1_title', $h1_title);
		$this->_addBreadcrumb($h1_title, $this->_module_path);
	}
	
	
	public function getArticlesListByArticlePage()
	{
		$this->_tpl->assign('page_tpl', 'articles_list.tpl');

		$this->_tpl->assign('brand', Environment::getGetVar('brand'));
		$this->_tpl->assign('oem', Environment::getGetVar('oem'));

		$details_with_prices = Environment::getModel('Laximo_Helper')->getArticlesListByArticle(Environment::getGetVar('brand'), Environment::getGetVar('oem'), $this->_module_path);
		$this->_tpl->assign('details_with_prices', $details_with_prices);
		$this->_tpl->assign('h1_title', 'Поиск запчастей по артикулу ' . Environment::getGetVar('oem') . ' в ' . Environment::getRegion()->getField('city_to'));
		$this->_addBreadcrumb('Поиск по артикулу ' . Environment::getGetVar('oem'), '');
	}
	
	
	public function getGroupDetailsPage()
	{
		$this->_tpl->assign('page_tpl', 'group_details.tpl');
		
		Environment::getPageContent()->addStyles(Config::getParam('CSSURL') . 'guayaquil.css');
		Environment::getPageContent()->addStyles(Config::getParam('CSSURL') . 'unit.css');
		Environment::getPageContent()->addStyles(Config::getParam('CSSURL') . 'colorbox.css');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'dragscrollable.js');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'jquery.mousewheel.js');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'jquery.colorbox.js');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'unit.js');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'jquery.tooltip.js');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'detailslist.js');

		$details = Environment::getModel('Laximo_Helper')->getGroupDetails(Environment::getGetVar('c'), Environment::getGetVar('ssd'), Environment::getGetVar('uid'), Environment::getGetVar('vid'), $this->_module_path);

		$this->_tpl->assign('details', $details);
		
		if (!empty($details['unit_info'])) {
			$this->_addBreadcrumb($details['car_info']->row['brand'] . ' ' . $details['car_info']->row['name'], $this->_module_path . '?ft=findByVIN&vid=' . Environment::getGetVar('vid') . '&ssd=' . Environment::getGetVar('ssd'));
			$this->_addBreadcrumb($details['unit_info']->row['name'], '');
		} else {
			$this->_addBreadcrumb('Результат поиска', '');
		}
	}
	
	
	public function getCategoryGroupsPage()
	{
		$this->_tpl->assign('page_tpl', 'category_groups.tpl');

		$groups = Environment::getModel('Laximo_Helper')->getCategoryGroups(Environment::getGetVar('c'), Environment::getGetVar('ssd'), Environment::getGetVar('vid'), Environment::getGetVar('cid'), $this->_module_path);

		$this->_tpl->assign('groups', $groups);
		if (!empty($groups['categories'])) {
			$category_title = 'Категория';
			foreach ($groups['categories']->row as $category) {
				if ($category['categoryid'] == Environment::getGetVar('cid')) {
					$category_title = $category['name'];
				}
			}
			
			$this->_addBreadcrumb($groups['car_info']->row['brand'] . ' ' . $groups['car_info']->row['name'], $this->_module_path . '?ft=findByVIN&vid=' . Environment::getGetVar('vid') . '&ssd=' . Environment::getGetVar('ssd'));
			$this->_addBreadcrumb($category_title, '');
		}
	}
	
	
	public function getGroupArticlesPage($is_ajax = false)
	{
		$this->_tpl->assign('page_tpl', 'group_articles.tpl');

		$details = Environment::getModel('Laximo_Laximo')->getGroupData(Environment::getGetVar('c'), Environment::getGetVar('ssd'), Environment::getGetVar('vid'), Environment::getGetVar('gid'));

		if (!empty($details)) {
			$size = 175;
			$zoom_image = Config::getParam('IMAGEURL') . 'zoom.png';

			$parts = array(
				'categories' => array(),
			);

			$articles = array();
			$category_uri = 'c=' . Environment::getGetVar('c') . '&vid=' . Environment::getGetVar('vid') . '&ssd=' . Environment::getGetVar('ssd');

			if (!empty($details[0])) {
				foreach ($details[0]->Category as $category) {
					$cc = array(
						'link' => $this->_module_path . '?ft=listByCategory&cid=' . $category['categoryid'] . '&' . $category_uri,
						'name' => (string)$category['name'],
						'units' => array(),
					);

					foreach ($category->Unit as $unit_item) {
						$unit = array(
							'link' => $this->_module_path . '?ft=groupDetails&c=' . Environment::getGetVar('c') . '&vid=' . Environment::getGetVar('vid') . '&uid=' . $unit_item['unitid'] . '&cid=' . Environment::getGetVar('cid') . '&ssd=' . $unit_item['ssd'],
							'img' => !empty($unit_item['imageurl']) ? ('<img class="img_group" src="' . str_replace('http://img.laximo.net/', '/ppv/', str_replace('%size%', $size, $unit_item['imageurl'])) . '">') : '',
							'width' => $size + 4,
							'imageurl' => str_replace('http://img.laximo.net/', '/ppv/', str_replace('%size%', 'source', (string)$unit_item['imageurl'])),
							'code' => (string)$unit_item['code'],
							'name' => (string)$unit_item['name'],
							'zoom_image' => $zoom_image,
							'gdImage' => empty($img) ? ' gdNoImage' : '',
							'size' => $size,
							'details' => array(),
						);

						foreach ($unit_item->Detail as $detail_item) {
							$detail = array(
								'codeonimage' => (string)$detail_item['codeonimage'],
								'filter' => (string)$detail_item['filter'],
								'name' => (string)$detail_item['name'],
								'oem' => (string)$detail_item['oem'],
								'ssd' => (string)$detail_item['ssd'],
								'link' => $this->_module_path . '?ft=findByArticle&brand=' . $details[1]->row['brand'] . '&oem=' . str_replace(' ', '', (string)$detail_item['oem']),
								'attributes' => array(),
							);

							$articles[] = $this->_db->quote((string)$detail_item['oem']);

							foreach ($detail_item->attribute as $attr) {
								$detail['attributes'][] = array(
									'key' => (string)$attr['key'],
									'name' => (string)$attr['name'],
									'value' => (string)$attr['value'],
								);
							}

							$unit['details'][] = $detail;
						}

						$cc['units'][] = $unit;
					}

					$parts['categories'][] = $cc;
				}
			}
			$this->_tpl->assign('parts', $parts);
		} else {
			$this->_tpl->assign('unfound_link', $this->_module_path . '?ft=listByCategory&c=' . Environment::getGetVar('c') . '&vid=' . Environment::getGetVar('vid') . '&cid=' . Environment::getGetVar('cid') . '&ssd=' . Environment::getGetVar('ssd'));
		}

		if ($is_ajax) {
			$this->_tpl->display('pages/poiskpovin.tpl');
		}
	}

	
	
	public function getTreeByVinPage()
	{
		$this->_tpl->assign('page_tpl', 'tree_by_vin.tpl');

		Environment::getPageContent()->addStyles(Config::getParam('CSSURL') . 'groups.css');
        Environment::getPageContent()->addScripts(Config::getParam('JSURL') . 'groups.js');
		
		$data = Environment::getModel('Laximo_Helper')->getTreeFullData(Environment::getGetVar('vin'), Environment::getGetVar('frame'), Environment::getGetVar('frameNo'), Environment::getGetVar('vid'));

		if (!empty($data['car_info']['attributes'])) {
			$this->_tpl->assign('car_info', $data['car_info']);
			$this->_tpl->assign('tree_html', !empty($data['tree_html']) ? $data['tree_html'] : '');
		} else {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->_module_path . '?message=no_result&action=' . Environment::getGetVar('ft'));
			exit;
		}
		
		$this->_tpl->assign('h1_title', 'Автозапчасти для ' . $data['car_info']['attributes']->row['brand'] . ' ' . $data['car_info']['attributes']->row['name'] . ' в ' . Environment::getRegion()->getField('city_to'));
		$this->_addBreadcrumb('Автозапчасти для ' . $data['car_info']['attributes']->row['brand'] . ' ' . $data['car_info']['attributes']->row['name'] . ' в ' . Environment::getRegion()->getField('city_to'), '');
	}
	
	
	public function getModelsByWizardPage()
	{
		$this->_tpl->assign('page_tpl', 'models_list.tpl');
	
		$vehicles = Environment::getModel('Laximo_Helper')->getVehicleByWizard(Environment::getGetVar('c'), Environment::getGetVar('ssd'), $this->_module_path);
		$this->_tpl->assign('vehicles', $vehicles);
		
		$this->_tpl->assign('h1_title', 'Список модификаций'); 
		$this->_addBreadcrumb('Список модификаций', ''); 
	}
	
	
	public function getBrandFiltersPage()
	{
		$this->_tpl->assign('page_tpl', 'brand_filters.tpl');

		$brand_filters = Environment::getModel('Laximo_Helper')->getCatalogBrandFilters(Environment::getGetVar('c'), Environment::getGetVar('ssd'), $this->_module_path);
		$this->_tpl->assign('filters', $brand_filters['filters']);
		$this->_tpl->assign('html_filters', $brand_filters['html_filters']);
		
		$this->_tpl->assign('h1_title', 'Поиск по марке'); 
		$this->_addBreadcrumb('Поиск по марке', ''); 
	}

	
	
	private function _addBreadcrumb($title, $url)
	{
		$this->_breadcrumbs[] = array(
			'title' => $title,
			'url' => $url,
		);
	}
	
}