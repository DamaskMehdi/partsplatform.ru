<?php

class View_Factory
{
	public static function create()
	{
		$module_name = Environment::getParseUrl()->getModuleName();
		if ($module_name == '') {
			$module_name = 'main';
		}

		$class_name = 'View_' . str_replace(' ', '', ucwords(str_replace(array('_', '/', '-'), ' ', $module_name)));
		if (class_exists($class_name)) {
			$view = new $class_name();
		} else {
			$view = new View_Main();
			//throw new Exception('Page type ' . $page_type . ' unknown from View_factory');
		}

		return $view;
	}
	

}
