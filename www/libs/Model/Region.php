<?php
class Model_Region extends Model_AbstractObject 
{
	
	
	public function getRegions()
	{
		$select = "select * from atrru.domains d order by d.sorting, d.city";
		
		return $this->_getDB()->query($select)->fetchAll();
	}
	
	
	private function _getDB()
	{
		if (is_null($this->_db)) {
			$this->_db = Environment::getDB();
		}
		
		return $this->_db;
	}
}