<?php
class Model_Account extends Model_AbstractObject 
{
	const TYPE_PLATFORM = 1;
	const TYPE_SHOP = 2;
	
	const PLATFORM_PROCENT = 0.6;
	
	
	
	
	
	public function editAccount($account_id, $type, $data)
	{
		if ($type == self::TYPE_PLATFORM) {
			$update = "update atrru.parts_platforms
				set platform_url = " .  $this->getDB()->quote($data['platform_url']) . "
				where account_id = " . $account_id;
			$this->getDB()->query($update);
			
		} elseif ($type == self::TYPE_SHOP) {	
			$update = "update atrru.parts_shops
				set name = " .  $this->getDB()->quote($data['name']) . ",
					click_price = " .  $this->getDB()->quote($data['click_price']) . ",
					phones = " .  $this->getDB()->quote($data['phones']) . "
				where account_id = " . $account_id;
			$this->getDB()->query($update);
		}
	}
	
	
	public function getTransactionList($account_id, $type)
	{
		if ($type == self::TYPE_PLATFORM) {
			$select = "select * from atrru.parts_platforms p where account_id = " . $account_id;
			$platform = $this->getDB()->query($select)->fetch();
			
			$select = "select *, round(amount * " . self::PLATFORM_PROCENT . ", 2) as amount_platform 
				from atrru.parts_transactions t 
				where platform_id_to = " . $this->getDB()->quote($platform['id']) . "
				order by date_create desc";
			$list = $this->getDB()->query($select)->fetchAll();
		} elseif ($type == self::TYPE_SHOP) {
			$select = "select * from atrru.parts_shops p where account_id = " . $account_id;
			$shop = $this->getDB()->query($select)->fetch();
			
			$select = "select * 
				from atrru.parts_transactions 
				where shop_id_from = " . $this->getDB()->quote($shop['id']) . "
				order by date_create desc";
			$list = $this->getDB()->query($select)->fetchAll();
		} else {
			$list = array();
		}
		
		return $list;
	}
	
	
	public function getShopContacts($shop_id)
	{
		$contacts = '';
		
		$select = "select * from atrru.parts_shops where id = " . $this->getDB()->quote($shop_id);
		$shop = $this->getDB()->query($select)->fetch();
		
		if (!empty($shop)) {
			// записать транзакцию
			$transaction_bc_id = '';
			$platform_id_to = 1;
			$description = "Списание за клик";
			$amount = $shop['click_price'];
			$ip_client = $_SERVER['REMOTE_ADDR'];
			
			$insert = "insert into atrru.parts_transactions (transaction_bc_id, shop_id_from, platform_id_to, description, amount, ip_client, date_create) values (" . $this->getDB()->quote($transaction_bc_id) . ", " . $this->getDB()->quote($shop_id) . ", " . $this->getDB()->quote($platform_id_to) . ", " . $this->getDB()->quote($description) . ", " . $this->getDB()->quote($amount) . ", " . $this->getDB()->quote($ip_client) . ", now())";
			$this->getDB()->query($insert);
			
			
			// списать баланс у магазина
			$update = "update atrru.parts_accounts
				set balance = balance - " . $amount . "
				where id = " . $shop['account_id'];
			$this->getDB()->query($update);
			
			
			// добавить баланс площадке
			$update = "update atrru.parts_accounts a, atrru.parts_platforms p
				set a.balance = a.balance + " . ($amount * self::PLATFORM_PROCENT) . "
				where a.id = p.account_id and p.id = " . $platform_id_to;
			$this->getDB()->query($update);
			
			
			// вернуть контакты
			$contacts = $shop['phones'];
		}
		
		return $contacts;
	}
	
	
	
	public function addShop($data)
	{
		$success = false;
		
		$balance_start_value = 200;
		$wallet_address = $this->_createWallet(self::TYPE_SHOP);
		
		$insert = "insert into atrru.parts_accounts (email, pwd, type, wallet_address, balance) values (" . $this->getDB()->quote($data['email']) . ", " . $this->getDB()->quote(md5($data['pwd'])) . ", " . self::TYPE_SHOP . ", " . $this->getDB()->quote($wallet_address) . ", " . $balance_start_value . ")";
		if ($this->getDB()->query($insert)) {
			$account_id = $this->getDB()->lastInsertId();
			
			$insert = "insert into atrru.parts_shops (account_id, name) values (" . $account_id . ", " . $this->getDB()->quote($data['shop_name']) . ")";
			if ($this->getDB()->query($insert)) {
				$success = true;
			}
		}
		
		return $success;
	}
	
	
	
	public function addPlatform($data)
	{
		$success = false;
		
		$balance_start_value = 0;
		$wallet_address = $this->_createWallet(self::TYPE_PLATFORM);
		
		$insert = "insert into atrru.parts_accounts (email, pwd, type, wallet_address, balance) values (" . $this->getDB()->quote($data['email']) . ", " . $this->getDB()->quote(md5($data['pwd'])) . ", " . self::TYPE_PLATFORM . ", " . $this->getDB()->quote($wallet_address) . ", " . $balance_start_value . ")";
		if ($this->getDB()->query($insert)) {
			$account_id = $this->getDB()->lastInsertId();
			
			$insert = "insert into atrru.parts_platforms (account_id, platform_url) values (" . $account_id . ", " . $this->getDB()->quote($data['platform_url']) . ")";
			if ($this->getDB()->query($insert)) {
				$success = true;
			}
		}
		
		return $success;
	}
	
	
	
	public function getAccountAddFields($account_id, $type)
	{
		if ($type == self::TYPE_PLATFORM) {
			$select = "select * from atrru.parts_platforms where account_id = " . $this->getDB()->quote($account_id);
			$fields = $this->getDB()->query($select)->fetch();
		} elseif ($type == self::TYPE_SHOP) {
			$select = "select * from atrru.parts_shops where account_id = " . $this->getDB()->quote($account_id);
			$fields = $this->getDB()->query($select)->fetch();
		} else {
			$fields = array();
		}
		
		return $fields;
	}
	

	
	
	private function _createWallet($type)
	{
		$wallet_address = '';
		
		return $wallet_address;
	}
	
}