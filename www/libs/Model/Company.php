<?php
class Model_Company extends Model_AbstractObject
{

	public function getImportLogsByCompany($company_id, $source_id)
	{
		$select = "select * from " . ImportMod_Logger_DataObject::TABLE . "
			where company_id = " . $this->_db->quote($company_id) . " and source_id = " . $this->_db->quote($source_id) . "
			order by date_start desc";

		return $this->_db->query($select)->fetchAll();
	}


	public function getTiresDataStatus($company_id)
	{
		$select = "select st1.cnt as `found`, st2.cnt as unfound
			from (
				select count(*) cnt
				from " . ImportMod_Destination_DataObject_Tires::TABLE . "
				where supplier_id = " . $this->_db->quote($company_id) . " and model_id <> 0
			) st1, (
				select count(*) cnt
				from " . ImportMod_Destination_DataObject_Tires::TABLE . "
				where supplier_id = " . $this->_db->quote($company_id) . " and model_id = 0
			) st2";

		return $this->_db->query($select)->fetch();
	}
	
	public function getPartsDataStatus($company_id)
	{
		$select = "select st1.cnt as `found`, st2.cnt as unfound
			from (
				select count(*) cnt
				from " . ImportMod_Destination_DataObject_Tires::TABLE . "
				where supplier_id = " . $this->_db->quote($company_id) . " and model_id <> 0
			) st1, (
				select count(*) cnt
				from " . ImportMod_Destination_DataObject_Tires::TABLE . "
				where supplier_id = " . $this->_db->quote($company_id) . " and model_id = 0
			) st2";

		return $this->_db->query($select)->fetch();
	}


	public function getShopsByCompany($company_id)
	{
		$select = "select * from " . Item_Shop::TABLE . " where company_id = " . $this->_db->quote($company_id);

		return $this->_db->query($select)->fetchAll();
	}

	public function getCategoryValues()
	{
		$select = "select id, category_id, name, ifnull(value, id) as value from ru_register_values order by category_id, sort, name";

		$values = array();
		foreach ($this->_db->query($select)->fetchAll() as $value) {
			if (!isset($values[$value['category_id']])) {
				$values[$value['category_id']] = array(
					'category_id' => $value['category_id'],
					'values' => array(),
				);
			}

			$values[$value['category_id']]['values'][] = $value;
		}

		return $values;
	}

	public function getCompanies()
	{
		$select = "select c.*, a.*
			from atrru.companies c
			left join atrru.address a on a.id = c.address_id
			order by date_create desc";

		return $this->_db->query($select)->fetchAll();
	}

	public function getCompanyInfoHtml($company_id, $article_id, $price_id, $type)
	{
		$info_html = array();
		$company = new Item_Company($company_id, Environment::getDB('local'));
		$user = Environment::getUser();

		if ($company->isExist()) {
			$shops = $company->getShops();

			$info = '<div><h4>' . $company->getField('name') . '</h4> <p><span class="label label-warning">Опт</span>  <span class="label label-success">Розница</span></p></div>';
			if ($company->getField('phone0')) {
				$info .= '<i class="fa fa-phone"></i> <span>' . $company->getField('phone0') . '</span>';
				if ($company->getField('phone1')) {
					$info .= '<br><i class="fa fa-phone"></i> <span>' . $company->getField('phone1') . '</span>';
				}
				$info .= '<br><span>Пожалуйста, сообщите продавцу, что вы нашли информацию на сайте Atr.ru</span></div>';
			}
			if ($company->getField('web')) {
				$info .= '<div><i class="fa fa-link"></i> Адрес сайта: <a target="__blank" href="' . $company->getField('web') . '">' . $company->getField('web') . '</a></div>';
			}
			if ($company->getField('skype')) {
				$info .= '<div><i class="fa fa-skype"></i> Скайп: <span>' . $company->getField('skype') . '</span></div>';
			}
			if ($company->getField('wholesale')) {
				$info .= '<div><i class="fa fa-money"></i> <span>Торговля оптом</span></div>';
			}
			if ($company->getField('retail')) {
				$info .= '<div><i class="fa fa-money"></i> <span>Торговля в розницу</span></div>';
			}
			if ($company->getField('has_internet_shop')) {
				$info .= '<div><i class="fa fa-tags"></i> <span>Есть интернет-магазин</span></div>';
			}
			if ($company->getField('delivery')) {
				$info .= '<div><i class="fa fa-truck"></i> <span>Есть доставка курьером</span></div>';
			}
			if (!empty($shops) && $shops[0]->getField('work_time')) {
				$info .= '<div><i class="fa fa-clock-o"></i> Время работы: <span>' . $shops[0]->getField('work_time') . '</span></div>';
			}
			if ($company->getField('date_create')) {
				$info .= '<div><small>На сайте с: ' . date('d.m.Y', strtotime($company->getField('date_create'))) . '</small></div>';
			}
			if ($user->isAuthorized()) {
				$info .= '<div><button>Написать сообщение</button></div>';
				$info .= '<input type="hidden" id="shop_msg_email" value="' . $company->getField('request_email') . '">';
			} else {
				$info .= '<div>Чтобы написать сообщение, авторизуйтесь</div>';
			}

			$info_html['info'] = $info;
			$info_html['location'] = $company->getField('location');
			$address = $company->getAddress();

			$info_html['address'] = ($address->getField('country') ? $address->getField('country') : 'Россия') .
				($address->getField('city') ? ', ' . $address->getField('city') : '') .
				($address->getField('street_type') && $address->getField('street_type') != '(выбрать)' ? ', ' . $address->getField('street_type') : '') .
				($address->getField('street') ? ' ' . $address->getField('street') : '') .
				($address->getField('house') ? ', ' . $address->getField('house') : '');

			if (!empty($article_id)) {
				if ($type == 'part') {
					$price_info = $this->_getArticlePriceInfo($article_id, $company_id, $price_id);
				} elseif ($type == 'tire') {
					$price_info = $this->_getTirePriceInfo($article_id, $company_id, $price_id);
				}
			}
				
			if (!empty($price_info)) {
				$mdcompany = new Model_Company(Environment::getDB('local'));
				$category_values = $mdcompany->getCategoryValues();

				$order = '<div class="order-info"><div><h4>Заказ</h4></div>';
				$order .= '<div class="gray"><table>
					<tr><th>Наименование</th><th>Стоимость</th><th>Количество</th></tr>
					<tr><td>' . $price_info['name'] . ' </td><td>' . $price_info['price'] . '</td><td><input type="text" maxlength="4" size="4" id="order_count" name="order_count" value="1"></td></tr>
					</table></div>
					<input type="hidden" id="order_price_id" value="' . $price_id . '">
					<input type="hidden" id="order_shop_id" value="' . $company_id . '">';
				$order .= '<div><span>Имя:</span> <input type="text" name="order_fio" id="order_fio" value="' . $user->getField('first_name') . '"></div>';
				$order .= '<div><span>Регион:</span> <select id="order_region_id" name="order_region_id">';
				foreach ($category_values[1]['values'] as $region) {
					$order .= '<option value="' . $region['value'] . '" ' . ($region['value'] == $user->getField('region_id') ? 'selected' : '') . '">' . $region['name'] . '</option>';
				}
				$order .= '</select></div>';
				$order .= '<div><span>Ваш контактный email:</span> <input type="text" name="order_email" id="order_email" value="' . $user->getField('email') . '"></div>';
				$order .= '<div><span>Ваш контактный телефон:</span> <input type="text" name="order_phone" id="order_phone" value="' . $user->getField('contact_phone') . '"></div>';
				$order .= '<div><span>Оповещать по sms</span> <input type="checkbox" name="order_notify_by_sms" id="order_notify_by_sms" value="1"></div>';
				$order .= '<div><span>Пожелания:</span> <textarea name="order_text" id="order_text"></textarea></div>';
				//if ($user->isAuthorized()) {
					//$order .= '<div><button id="order_send_btn">Послать заказ продавцу</button></div>';
					$order .= '<div><button id="order_add_to_cart_btn">Положить в корзину</button></div>';
				/*} else {
					$order .= '<div>Для отправки заказа продавцу авторизуйтесь</div>';
				}*/
				if ($company->getField('phone1')) {
					$order .= '<div>Заказать или купить можно по телефону ' . $company->getField('phone1') . '</div>';
				}
				$order .= '</div>';
			} else {
				$order = '';
			}
			$info_html['order'] = $order;

			$review_list = $this->getCompanyReviews($company->getId());
			$review = '<div class="order-info"><div><h4>Отзывы</h4></div>';
			if (!empty($review_list)) {
				$review .= '<div>';
				foreach ($review_list as $rw) {
					$mark = '';
					$rmark = round($rw['mark']);
					for ($i = 1; $i <= 5; $i++) {
						$mark .= $i <= $rmark ? '<img src="/images/star_blue_20.png" /> ' : '<img src="/images/star_gray_20.png" /> ';
					}
					$review .= '<div><div class="comment-acc">' . htmlspecialchars($rw['account_name']) . ' - ' . date('d.m.Y', strtotime($rw['date_create'])) . '</div><div class="comment-mark">' . $mark . '</div><div class="comment-review">' . htmlspecialchars($rw['review']) . '</div></div>';
				}
				$review .= '</div>';
			} else {
				$review .= '<div>Нет отзывов</div>';
			}
			if ($user->isAuthorized()) {
				$review .= '<h3>Добавить отзыв</h3>';
				$review .= '<form action="" method="GET" id="review_form">
					<input type="hidden" name="action" value="add_company_review">
					<input type="hidden" name="company_id" value="' . $company->getId() . '">
					<div>
					<table><tr><td>
						<div class="company_mark_wrap">
							<div>Оценка:</div>
							<div class="company_mark">
								<input type="radio" name="mark" class="rating" value="1" />
                                <input type="radio" name="mark" class="rating" value="2" />
                                <input type="radio" name="mark" class="rating" value="3" />
                                <input type="radio" name="mark" class="rating" value="4" />
                                <input type="radio" name="mark" class="rating" value="5" />
							</div>
							<script type="text/javascript">$(\'.company_mark\').rating();</script>
						</div>
						Замечания к продавцу:<br>
						<input type="checkbox" name="no_notifications" value="1"> Замечаний нет<br>
						<input type="checkbox" name="no_correct_price" value="1"> Цена не соответствует заявленной<br>
						<input type="checkbox" name="no_existing" value="1"> Наличие не соответствует заявленному<br>
						<input type="checkbox" name="error_description" value="1"> Ошибочное описание<br>
						<input type="checkbox" name="no_original" value="1"> Неоригинал под видом оригинала<br>
						<input type="checkbox" name="no_correct_phone" value="1"> Телефон неверный или не отвечает<br>
						<input type="checkbox" name="no_correct_address" value="1"> Адрес не соответствует действительности<br>
						<input type="checkbox" name="no_partners" value="1"> Не знают что такое Atr.ru<br>
						<input type="checkbox" name="other_problems" value="1"> Другие проблемы<br>
					</td><td valign="top" style="">
						Комментарии:<br>
						<textarea name="text"></textarea><br>
						Имя:<br>
						<input type="text" name="name" value="' . $user->getField('first_name') . '"><br>
						<input type="hidden" name="account_id" value="' . $user->getField('id') . '"><br>
						Ваш контактный телефон:<br>
						<input type="text" name="phone" value="' . $user->getField('contact_phone') . '"><br>
						Имя и телефон нужны для того, что бы сотрудники atr.ru могли проверить информацию. Телефон будет виден только струдникам atr.ru и продавцу.
					</td></tr></table>
					</div>
					<div><input type="button" value="Сохранить" id="add_company_comment"></div>
					</form>';
			} else {
				$review .= '<div>для того, чтобы оставить отзыв авторизуйтесь</div>';
			}
			$review .= '</div>';
			$info_html['review'] = $review;


			$check = '<div>Нет данных</div>';
			$info_html['check'] = $check;


			$requisits = '<div class="order-info"><div><h4>' . $company->getField('full_name') . '</h4></div>';
			if ($company->getField('fio_director')) {
				$requisits .= '<div><span>Директор:</span> <span>' . $company->getField('fio_director') . '</span></div>';
			}
			if ($company->getField('inn')) {
				$requisits .= '<div><span>ИНН:</span> <span>' . $company->getField('inn') . '</span></div>';
			}
			if ($company->getField('kpp')) {
				$requisits .= '<div><span>КПП:</span> <span>' . $company->getField('kpp') . '</span></div>';
			}
			if ($company->getField('bank')) {
				$requisits .= '<div><span>Банк:</span> <span>' . $company->getField('bank') . '</span></div>';
			}
			if ($company->getField('bill')) {
				$requisits .= '<div><span>Счет:</span> <span>' . $company->getField('bill') . '</span></div>';
			}
			if ($company->getField('correspondent_bill')) {
				$requisits .= '<div><span>Корр. счет:</span> <span>' . $company->getField('correspondent_bill') . '</span></div>';
			}
			if ($company->getField('bik')) {
				$requisits .= '<div><span>БИК:</span> <span>' . $company->getField('bik') . '</span></div>';
			}

			$requisits .= '</div>';
			$info_html['requisits'] = $requisits;
		}

		return $info_html;
	}
	
	
	private function _getArticlePriceInfo($article_id, $company_id, $price_id)
	{
		$price_info = array(
			'price' => 'не указана',
			'name' => '',
		);
		
		$json = new Model_Json('fullinfo:' . $article_id);
		$content = $json->getDataJSON();

		if (!empty($content['part'])) {
			foreach ($content['part']['shops'] as $shop) {
				if ($shop['shop_id'] == $company_id) {
					if (!empty($price_id)) {
						if ($shop['price_id'] == $price_id) {
							$price_info['price'] = $shop['price_rub'];
						}
					} else {
						$price_info['price'] = $shop['price_rub'];
					}
					break;
				}
			}
			
			$price_info['name'] = $content['part']['title'] . ' ' . $content['part']['brand'] . ' ' . $content['part']['article'];
		}
		
		return $price_info;
	}
	
	private function _getTirePriceInfo($article_id, $company_id, $price_id)
	{
		$price_info = array(
			'price' => 'не указана',
			'name' => '',
		);
		
		$select = "select concat(tb.name, ' ', tm.name, ' ', ts.name) as tire_name, tp.price, tp.currency
			from tires_prices tp, tires_sizes ts, tires_models tm, tires_brands tb
			where tp.id = 35066492 and tp.size_id = ts.id and ts.model_id = tm.id and tm.brand_id = tb.id";
		$price = $this->_db->query($select)->fetch();
		if (!empty($price)) {
			$price_info['price'] = Common_Helper::getPriceRur($price['price'], $price['currency']);
			$price_info['name'] = 'Шины ' . $price['tire_name'];
		}
		
		return $price_info;
	}


	public function getCompanyReviews($company_id)
	{
		$select = "select c.*
			from atrru.company_comments c
			where company_id = " . $company_id . " and visible = '1'
			order by date_create desc
			limit 30";

		return $this->_db->query($select)->fetchAll();
	}

}