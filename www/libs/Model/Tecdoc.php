<?php
class Model_Tecdoc extends Model_AbstractObject
{
	const DB_PRICES = 'pricesru'; //pricesru
	const DB_ATRRU = 'atrru'; //pricesru
	const DB_TECDOC = 'tecdoc2';

	const TECDOC_LNG_ID = 16;
	const TECDOC_FILES_PREFIX_LOCAL = 'http://pix.atr-catalog.by/';
	const TECDOC_GETPARTS_LIMIT = 80;




	public function __construct()
	{
		parent::__construct();


	}


	public function getParts($engine, $tree)
	{
		$parts = $this->GetPartsBySectionID($engine, $tree, self::TECDOC_LNG_ID, self::TECDOC_GETPARTS_LIMIT);
		if (!empty($parts)) {
			$arResult = $this->PrepareParts($parts);

			/*$CrossL = $this->LookupCrossNew($arResult['ART_IDS'],3);
			#Добавление запчастей из таблицы кроссов
			if(!empty($CrossL)) {
				foreach($CrossL as $val) {
					$number = $this->artToNumber($val['CROSS_NUMS'],'FULL');
					$number_short = $this->artToNumber($val['CROSS_NUMS'],'SHORT');
					if(!in_array($number,$arResult['ANRS'])) {
						$arResult['PARTS'][] =  array(
										'ART_ID' => $val['ARL_ART_ID'],
										'CROSS_ID' => $val['ID'],
										'ART_ARTICLE_NR'=>$val['CROSS_NUMS'],
										'SUP_BRAND' => $val['CROSS_BRAND'],
										'COU_ISO2' => $val['COU_ISO2'],
										'COU_DES_TEXT' => $val['COU_DES_TEXT'],
										'IMG' => $this->GetImage($val['CROSS_NUMS'],$val['CROSS_BRAND'],self::TECDOC_LNG_ID),
										'PART_NAME' => '',
										'SUP_BRAND_F' => '',
										'NUMBER' => $number,
										'NUMBER_SHORT' => $number_short);
						$arResult['ANRS'][] = $number;
					}
					if(!in_array($val['CROSS_BRAND'],$arResult['BRANDS'])) $arResult['BRANDS'][] = $val['CROSS_BRAND'];
				}
			}*/

			if (!empty($arResult['PARTS'])) {
				$rsPrices = $this->GetPriceListForOneShopNew(
					array( "c.name" => ""),
					array( "a.ART_NUM" => $arResult['ANRS']),
					''
				);

				if (!empty($rsPrices)) {
					foreach($rsPrices as $val){
						$arASP[StrToUp($val['SUP_BRAND'])][StrToUp($val['ART_NUM'])][] = $val;
					}

					#Сортировать по цене
					$arWith_Price = array();
					$arNo_Price = array();
					foreach($arResult['PARTS'] as $arNoPart){
						if(!empty($arASP[StrToUp($arNoPart['SUP_BRAND'])][StrToUp($arNoPart['NUMBER_SHORT'])])) {
							$arWith_Price[] = $arNoPart;
						} else {
							$arNo_Price[] = $arNoPart;
						}
					}

					$arResult['PARTS'] = Array();
					foreach($arWith_Price as $arPart1) {
						$arResult['PARTS'][] = $arPart1;
					}
					foreach($arNo_Price as $arPart2) {
						$arResult['PARTS'][] = $arPart2;
					}

					$this->createPriceTable($arResult['PARTS'], $arASP, 'parts');
					$this->carInfo();

				} else {
					$this->createPriceTable($arResult['PARTS'], array(), 'parts'); //$this->noResult('html');
					$this->carInfo();
				}
			}
		}

		return $parts;
	}

	#Запчасти (по ID раздела)
	public function GetPartsBySectionID($TYP_ID, $Section, $LNG_ID, $LIMIT)
	{
        $SQL="SELECT DISTINCT
				ART_ID, ART_ARTICLE_NR, SUP_BRAND, DES_TEXTS.TEX_TEXT AS STR_DES_TEXT, DES_TEXTS2.TEX_TEXT AS ART_COMPLETE_DES_TEXT,
				COU_ISO2, DES_TEXTS3.TEX_TEXT AS COU_DES_TEXT
			FROM ".self::DB_TECDOC.".LINK_GA_STR
				INNER JOIN ".self::DB_TECDOC.".LINK_LA_TYP ON LAT_TYP_ID = ".$TYP_ID." AND
				LAT_GA_ID = LGS_GA_ID
				INNER JOIN ".self::DB_TECDOC.".LINK_ART ON LA_ID = LAT_LA_ID
				INNER JOIN ".self::DB_TECDOC.".SEARCH_TREE ON STR_ID = LGS_STR_ID
				INNER JOIN ".self::DB_TECDOC.".DESIGNATIONS ON DESIGNATIONS.DES_ID = STR_DES_ID AND DESIGNATIONS.DES_LNG_ID = ".$LNG_ID."
				INNER JOIN ".self::DB_TECDOC.".DES_TEXTS ON DES_TEXTS.TEX_ID = DESIGNATIONS.DES_TEX_ID
				INNER JOIN ".self::DB_TECDOC.".ARTICLES ON ART_ID = LA_ART_ID
				INNER JOIN ".self::DB_TECDOC.".SUPPLIERS ON SUP_ID = ART_SUP_ID
				INNER JOIN ".self::DB_TECDOC.".DESIGNATIONS AS DESIGNATIONS2 ON DESIGNATIONS2.DES_ID = ART_COMPLETE_DES_ID
				INNER JOIN ".self::DB_TECDOC.".DES_TEXTS AS DES_TEXTS2 ON DES_TEXTS2.TEX_ID = DESIGNATIONS2.DES_TEX_ID AND DESIGNATIONS2.DES_LNG_ID = ".$LNG_ID."
				LEFT JOIN ".self::DB_TECDOC.".SUPPLIER_ADDRESSES ON SAD_SUP_ID = SUP_ID
					AND SAD_COU_ID = 0
				LEFT JOIN ".self::DB_TECDOC.".COUNTRIES ON COU_ID = SAD_COU_ID_POSTAL
				LEFT JOIN ".self::DB_TECDOC.".DESIGNATIONS AS DESIGNATIONS3 ON DESIGNATIONS3.DES_ID = COU_DES_ID
					AND DESIGNATIONS3.DES_LNG_ID = ".$LNG_ID."
				LEFT JOIN ".self::DB_TECDOC.".DES_TEXTS AS DES_TEXTS3 ON DES_TEXTS3.TEX_ID = DESIGNATIONS3.DES_TEX_ID
			WHERE
				STR_ID = ".$Section." LIMIT ".$LIMIT;
		d($SQL);
		return $this->getDB()->query($SQL)->fetchAll();
    }

	#Формирование массива запчстей
	public function PrepareParts($rsParts)
	{
		$arUnique = array();
		foreach($rsParts as $val) {
			$PSTAMP = $val['ART_ARTICLE_NR'].$val['SUP_BRAND'];
			if(!in_array($PSTAMP, $arUnique)) {
				$arUnique[] = $PSTAMP;
				$arPart = $this->PreparePart($val);
				$arResult['ANRS'][] = $arPart['NUMBER_SHORT'];
				$arResult['ART_IDS'][] = $arPart['ART_ID'];
				$arResult['BRANDS'][] = $arPart['SUP_BRAND'];
				$arResult['PARTS'][] = $arPart;
			}
		}
		$arResult['BRANDS'] = array_unique($arResult['BRANDS']);
		return $arResult;
	}

	#Поиск аналогов (ARL_KIND - Тип аналога: 2 - Торговый, 3 - Оригинальный, 4 - Неоригинальный)
	public function LookupCrossNew($ART_ID,$TYPE)
	{
		if(is_array($ART_ID)) $WHERE = " IN ('".implode("','",$ART_ID)."')";
		else $WHERE = " = '".$ART_ID."'";
		$SQL="SELECT l.ID, l.CROSS_NUMS, l.CROSS_BRAND, l.COU_ISO2, l.COU_DES_TEXT, ARL_ART_ID
			FROM ".self::DB_TECDOC.".ART_LOOKUP
				LEFT JOIN ".self::DB_TECDOC.".BRANDS ON BRA_ID = ARL_BRA_ID
				INNER JOIN ".self::DB_TECDOC.".ARTICLES ON ARTICLES.ART_ID = ART_LOOKUP.ARL_ART_ID
				INNER JOIN ".self::DB_TECDOC.".SUPPLIERS ON SUPPLIERS.SUP_ID = ARTICLES.ART_SUP_ID
				INNER join ".self::DB_PRICES.".LINKS l on l.ORIGINAL_BRAND = IF (ART_LOOKUP.ARL_KIND = 2, SUPPLIERS.SUP_BRAND, BRANDS.BRA_BRAND) and l.ORIGINAL_NUMS = ARL_DISPLAY_NR
			WHERE
				ARL_ART_ID ".$WHERE." AND ARL_KIND = ".$TYPE."
			GROUP BY l.CROSS_NUMS, l.CROSS_BRAND, l.COU_ISO2, l.COU_DES_TEXT
			ORDER BY l.CROSS_BRAND LIMIT 100;";

		return $this->getDB()->query($SQL)->fetchAll();
    }

	#Фото по бренду и артикулу запчасти
	public function GetImage($ART_NUM,$SUP_BRAND,$LNG_ID)
	{
		$SQL = "SELECT CONCAT(
						'images/',
						GRA_TAB_NR, '/',
						GRA_GRD_ID, '.',
						IF(LOWER(DOC_EXTENSION)='jp2', 'jpg', LOWER(DOC_EXTENSION))
					) AS PATH
				FROM
					".self::DB_TECDOC.".LINK_GRA_ART
					INNER JOIN ".self::DB_TECDOC.".GRAPHICS ON GRA_ID = LGA_GRA_ID
					INNER JOIN ".self::DB_TECDOC.".DOC_TYPES ON DOC_TYPE = GRA_DOC_TYPE
					INNER JOIN ".self::DB_TECDOC.".ARTICLES ON LGA_ART_ID = ART_ID
					INNER JOIN ".self::DB_TECDOC.".SUPPLIERS ON ART_SUP_ID = SUP_ID
				WHERE
					ART_ARTICLE_NR = '".$ART_NUM."' AND SUP_BRAND = '".$SUP_BRAND."' AND
					(GRA_LNG_ID = ".$LNG_ID." OR GRA_LNG_ID = 255) AND
					GRA_DOC_TYPE <> 2
				ORDER BY GRA_GRD_ID;";

		$image = $this->getDB()->query($SQL)->fetch();

		return !empty($image) ? $image['PATH'] : '';
	}

	#Картинки и PDF файлы
	public function PreparePart($arPart)
	{
		$arPart = $this->FirstPic($arPart);
		$arPart = $this->PartTexts($arPart);
		return $arPart;
	}

	private function FirstPic($arPart)
	{
		#Изображения
		if($arPart['ART_ID']>0){
			$rsImg = $this->GetImages($arPart['ART_ID'],self::TECDOC_LNG_ID,1);
			$arPart['IMG'] = (!empty($rsImg))?$rsImg['PATH']:'';
		}
		return $arPart;
	}

	public function PartTexts($arPart)
	{
		if(!empty($arPart['PART_NAME'])) $arPart['PART_NAME']=trim($arPart['PART_NAME']);
		else $arPart['PART_NAME']='';

		if(!empty($arPart['ART_COMPLETE_DES_TEXT']) && $arPart['PART_NAME']=='') {
			$arPart['PART_NAME']=trim($arPart['ART_COMPLETE_DES_TEXT']);
			unset($arPart['ART_COMPLETE_DES_TEXT']);
		}
		if(!empty($arPart['STR_DES_TEXT']) && $arPart['PART_NAME']=='') {
			$arPart['PART_NAME']=trim($arPart['STR_DES_TEXT']);
			unset($arPart['STR_DES_TEXT']);
		}

		if($arPart['SUP_BRAND']!=''){
			$arPart['SUP_BRAND']=StrToUp($arPart['SUP_BRAND']);
			$arPart['SUP_BRAND_F']=$this->BrandNameEncode($arPart['SUP_BRAND']);
		}

		if(!empty($arPart['ART_ARTICLE_NR'])) $ToNumber=$arPart['ART_ARTICLE_NR'];
		elseif($arPart['ART_NUM']!='') $ToNumber=$arPart['ART_NUM'];

		$arPart['NUMBER'] = StrToUp($this->artToNumber($ToNumber,'FULL'));
		$arPart['NUMBER_SHORT'] = StrToUp($this->artToNumber($ToNumber,'SHORT'));
		return $arPart;
	}

	public function BrandNameEncode($Bname)
	{
		$Bname=str_replace("'",'_a_',$Bname);
		$Bname=str_replace(' & ','_and_',$Bname);
		$Bname=str_replace('&','and',$Bname);
		$Bname=str_replace(' + ','_plus_',$Bname);
		$Bname=str_replace('+','_cplus_',$Bname);
		$Bname=str_replace(' ','_',$Bname);
		$Bname=mb_strtolower($Bname);
		return $Bname;
	}

	# Построение JSON дерева запчастей с ценами магазинов
	private function createPriceTable($arrParts, $arrPrices, $tag)
	{
		$parts_with_prices = array();

		$i = 0;
		foreach($arrParts as $arPart) {
			#Запчасть
			$parts_with_prices[$i] = array(
				'article_id' => $arPart['ART_ID'],
				'cross_id' => !empty($arPart['CROSS_ID']) ? $arPart['CROSS_ID'] : '',
				'article' => $arPart['ART_ARTICLE_NR'],
				'brand' => $arPart['SUP_BRAND'],
				'country' => $arPart['COU_DES_TEXT'],
				'country_logo' => self::TECDOC_FILES_PREFIX_LOCAL.'countries/'.((!empty($arPart['COU_ISO2']))?$arPart['COU_ISO2']:'Unknown').'.png',
				'title' => $arPart['PART_NAME'],
				'number' => $arPart['NUMBER'],
				'images' => (isset($arPart['IMG'])&&$arPart['IMG']!='')? array('small' => self::TECDOC_FILES_PREFIX_LOCAL.str_replace('images/','small/',$arPart['IMG']),
					'full' => self::TECDOC_FILES_PREFIX_LOCAL.str_replace('images/','',$arPart['IMG'])):''
			);

			/*if (!empty($arrPrices)) {
				foreach($arrPrices as $key=>$val)
				{
					foreach($val as $key2=>$val2)
					{
						if($key2 == $arPart['NUMBER_SHORT'])
						{
							foreach($val2 as $key3=>$val3)
							{
								# Магазины и цены
								$parts_with_prices[$i]['shops'][] = array('shop_id' => $val3['USER_ID'],
									'price_id' => $val3['ID'],
									'shop_name' => $val3['NAME'],
									'supplier_id' => $val3['SUPPLIER_ID'],
									'price_rub' => $val3['PRICE_RUB'],
									'quantity' => $val3['AVAILABLE'],
									'percent' => $val3['PERCENT'],
									'email' => $val3['EMAIL'],
									'phones' => $val3['PHONES']
								);
							}
						}
					}
				}
			}*/
			++$i;
		}

		return $parts_with_prices;
	}

	# Путь по сайту
	private function carInfo()
	{
		#Инфо о марке
		/*if(!empty($this->url['car'])) {
			$rsManuf = $this->GetManufByName($this->url['car']['brand']);
			$this->json['carInfo'][0] = array('id' => $rsManuf['MFA_ID'],
												'title' => StrToUp($rsManuf['MFA_BRAND']),
												'url' => MarkNameEncode($rsManuf['MFA_BRAND']),
												//'uri' => $rsManuf['MFA_CAT_VIN']
											);
		}
		#Инфо о модели
		if(!empty($this->url['model'])) {
			$rsModel = $this->GetModelByID($this->url['model'],TECDOC_LNG_ID);
			$this->json['carInfo'][1] = array('id' => $rsModel['MOD_ID'],
												'title' => StrToUp($rsModel['MOD_CDS_TEXT']),
												'start' => TDDateFormat($rsModel['MOD_PCON_START'],'н.в.'),
												'end' => TDDateFormat($rsModel['MOD_PCON_END'],'н.в.'),
												'url' => $rsModel['MOD_ID']
											);
		}
		#Инфо о модификации
		if(!empty($this->url['engine'])) {
			$rsType = $this->GetTypeByID($this->url['model'],$this->url['engine'],TECDOC_LNG_ID);
			$this->json['carInfo'][2] = array('id' => $rsType['TYP_ID'],
												'title' => StrToUp($rsType['TYP_CDS_TEXT']." ".mb_ucwords($rsType['TYP_FUEL_DES_TEXT'])." ".mb_ucwords($rsType['TYP_BODY_DES_TEXT'])),
												'kw' => $rsType['TYP_KW_FROM'],
												'hp' => $rsType['TYP_HP_FROM'],
												'url' => $rsType['TYP_ID']
											);
		}
		#Инфо о разделе запчастей
		if(!empty($this->url['tree'])) {
			$rsNode = $this->GetSection($this->url['tree']);
			$this->json['carInfo'][3] = array('id' => $rsNode['STR_ID'],
												'title' => StrToUp($rsNode['STR_DES_TEXT']),
												'url' => $rsNode['STR_ID']
											);
		}*/
	}



	public function getPart($art_number)
	{
		$part = $this->GetPartInfo($art_number, self::TECDOC_LNG_ID);

		if(!empty($part)) {
			$part['shops'] = array();
			// если кросс
			if (!empty($this->url['cross'])) {
				$cross_prices = $this->GetLinksWithPriceByIdNew($this->url['cross']);

				if (!empty($cross_prices)) {
					foreach ($cross_prices as $key => $val) {
						if ($key == 0) {
							$cross = $val;

							$part['article_number'] = $cross['CROSS_NUMS'];
							$part['brand'] = $cross['SUP_BRAND'];
							$part['country'] = $cross['COU_DES_TEXT'];

						}

						$part['shops'][] = array(
							'shop_id' => $val['SUPPLIER_ID'],
							'price_id' => $val['ID'],
							'shop_name' => $val['NAME'],
							'supplier_id' => $val['SUPPLIER_ID'],
							'price_rub' => $val['PRICE_RUB'],
							'quantity' => $val['AVAILABLE'],
							'email' => $val['EMAIL'],
							'phones' => $val['PHONES'],
							'phones_array' => !empty($val['PHONES']) ? unserialize($cross['PHONES']) : array(),
						);
					}
				}
			}

			$part['images'] = $this->GetImages($art_number, self::TECDOC_LNG_ID, !empty($cross));

			if (empty($part['shops'])) {
				$part['shops'] = $this->GetPriceListForOneShopNew(
					array( "c.name" => "" ),
					array( "a.ART_NUM" => array($this->artToNumber($part['ART_ARTICLE_NR'], 'SHORT')) ),
					''
				);
			}

			$country_iso = !empty($cross) ? $cross['COU_ISO2'] : $part['COU_ISO2'];
			$part['country_logo'] = self::TECDOC_FILES_PREFIX_LOCAL . 'countries/' . (!empty($country_iso) ? $country_iso : 'Unknown') . '.png';
			#Характеристики запчасти
			$part['properties'] = $this->_partInfo($art_number);
			#Оригинальные номера запчасти
			$part['analogs'] = $this->_originalNumbersInfo($art_number);
			#Применяемость к автомобилям
			$part['autostack'] = $this->_autoStack($art_number);

			#Отзывы о запчасти
			$part['comments'] = $this->GetCommentsByArtNum($part['ART_ID'],$part['ART_ARTICLE_NR'],$part['SUP_BRAND']);

		}

		return $part;
	}


	public function GetPartInfo($ART_ID, $LNG_ID)
	{
		$SQL = "SELECT a.ART_ID, a.ART_ARTICLE_NR, s.SUP_BRAND, s.SUP_ID, dt.TEX_TEXT as title, c.COU_ISO2, dt2.TEX_TEXT AS country,
				a.ART_ARTICLE_NR as article, s.SUP_BRAND as brand,
				case when sl.SLO_ID is null then '' else CONCAT('" . self::TECDOC_FILES_PREFIX_LOCAL . "logos/', sl.SLO_ID, '.png') end AS BRAND_LOGO_PATH
			FROM " . self::DB_TECDOC . ".ARTICLES a
				INNER JOIN " . self::DB_TECDOC . ".DESIGNATIONS d ON d.DES_ID = a.ART_COMPLETE_DES_ID AND d.DES_LNG_ID = " . $LNG_ID . "
				INNER JOIN " . self::DB_TECDOC . ".DES_TEXTS dt ON dt.TEX_ID = d.DES_TEX_ID
				INNER JOIN " . self::DB_TECDOC . ".SUPPLIERS s ON s.SUP_ID = a.ART_SUP_ID
				LEFT JOIN " . self::DB_TECDOC . ".SUPPLIER_ADDRESSES ad ON ad.SAD_SUP_ID = s.SUP_ID AND ad.SAD_COU_ID = 0
				LEFT JOIN " . self::DB_TECDOC . ".COUNTRIES c ON c.COU_ID = ad.SAD_COU_ID_POSTAL
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS d2 ON d2.DES_ID = c.COU_DES_ID AND d2.DES_LNG_ID = " . $LNG_ID . "
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS dt2 ON dt2.TEX_ID = d2.DES_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".SUPPLIER_LOGOS sl ON sl.SLO_SUP_ID = a.ART_SUP_ID
			WHERE a.ART_ID = " . $this->getDB()->quote($ART_ID) . ";";

		return $this->getDB()->query($SQL)->fetch();
    }


	public function GetImages($ART_ID, $LNG_ID, $find_cross)
	{
        $SQL = "SELECT CONCAT(
					GRA_TAB_NR, '/',
					GRA_GRD_ID, '.',
					IF(LOWER(DOC_EXTENSION)='jp2', 'jpg', LOWER(DOC_EXTENSION))
				) AS PATH
			FROM
				" . self::DB_TECDOC . ".LINK_GRA_ART
				INNER JOIN " . self::DB_TECDOC . ".GRAPHICS ON GRA_ID = LGA_GRA_ID
				INNER JOIN " . self::DB_TECDOC . ".DOC_TYPES ON DOC_TYPE = GRA_DOC_TYPE
			WHERE
				LGA_ART_ID = ".$ART_ID." AND
				(GRA_LNG_ID = " . $LNG_ID . " OR GRA_LNG_ID = 255) AND
				GRA_DOC_TYPE <> 2
			ORDER BY GRA_GRD_ID
			limit 1;";

		$images	= $this->getDB()->query($SQL)->fetchAll();

		if (!empty($images[0]['PATH']) && !$find_cross) {
			$part_images = array(
				'small' => self::TECDOC_FILES_PREFIX_LOCAL . 'small/' . $images[0]['PATH'],
				'full' => self::TECDOC_FILES_PREFIX_LOCAL . '' . $images[0]['PATH'],
			);
		} else {
			$part_images = '';
		}

		return $part_images;
    }

	public function GetPriceListForOneShopNew($arOrder,$arFilter,$arParams=Array())
	{
		$where = $this->checkSQLWhere($arFilter);
		$SQL = "SELECT *
			FROM (
				SELECT a.ID as price_id, a.ART_NUM, a.SUP_BRAND, c.name as NAME, c.name as shop_name, c.id as shop_id, a.PRICE, a.CURRENCY,
					ROUND(a.PRICE*d.rate,0) as PRICE_RUB, a.AVAILABLE, a.IMPORT_DATE, c.request_email as EMAIL, c.phones as PHONES, c.click_price,
					c.location shop_location, COUNT(cc.id) shop_comments_num, ROUND(AVG(cc.mark), 1) shop_avg_mark
				FROM " . ImportMod_Destination_DataObject_Tecdoc::TABLE . " a
				INNER JOIN ".self::DB_ATRRU.".companies c ON a.SUPPLIER=c.id
				LEFT JOIN ".self::DB_ATRRU.".currency d ON REPLACE(a.CURRENCY, 'RUB', 'RUR')=d.code
				LEFT JOIN ".self::DB_ATRRU.".company_comments cc ON c.id = cc.company_id
				".$where['column']."
				GROUP BY c.id
				".$this->checkSQLOrder($arOrder)."
				".((!empty($arParams['LIMIT']))?$this->_dbc->checkSQLLimit($arParams['LIMIT']):'') .
			") t GROUP BY ART_NUM, shop_id
			order by click_price desc";

		return $this->_convertPricesToRur($this->getDB()->query($SQL)->fetchAll());
	}

	#Технические характеристики детали
	private function _partInfo($art_id)
	{
		$rsInfo = $this->GetPropertys($art_id, self::TECDOC_LNG_ID);

		$properties = array();
		foreach($rsInfo as $value) {
			$properties[] = array(
				'title' => $value['CRITERIA_DES_TEXT'],
				'value' => $value['CRITERIA_VALUE_TEXT']
			);
		}

		return $properties;
	}

	#Характеристики
	public function GetPropertys($ART_ID,$LNG_ID)
	{
	   $SQL = "SELECT DES_TEXTS.TEX_TEXT AS CRITERIA_DES_TEXT,
				IFNULL(DES_TEXTS2.TEX_TEXT, ACR_VALUE) AS CRITERIA_VALUE_TEXT
			FROM " . self::DB_TECDOC . ".ARTICLE_CRITERIA
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS AS DESIGNATIONS2 ON DESIGNATIONS2.DES_ID = ACR_KV_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS2 ON DES_TEXTS2.TEX_ID = DESIGNATIONS2.DES_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".CRITERIA ON CRI_ID = ACR_CRI_ID
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS ON DESIGNATIONS.DES_ID = CRI_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS ON DES_TEXTS.TEX_ID = DESIGNATIONS.DES_TEX_ID
			WHERE ACR_ART_ID = ".$ART_ID." AND
				(DESIGNATIONS.DES_LNG_ID IS NULL OR DESIGNATIONS.DES_LNG_ID = " . $LNG_ID . ") AND
				(DESIGNATIONS2.DES_LNG_ID IS NULL OR DESIGNATIONS2.DES_LNG_ID = " . $LNG_ID . ");";
		return $this->getDB()->query($SQL)->fetchAll();
    }

	#Оригинальные номера для выбранной детали
	private function _originalNumbersInfo($art_id)
	{
		$originals = $this->LookupAnalog($art_id, 3);

		if (!empty($originals)) {
			foreach($originals as $key => $orig) {
				$originals[$key]['number'] = $this->artToNumber($orig['article'], 'SHORT');
			}
		}

		return $originals;
	}

	#Поиск аналогов (ARL_KIND - Тип аналога: 2 - Торговый, 3 - Оригинальный, 4 - Неоригинальный)
	public function LookupAnalog($ART_ID,$TYPE)
	{
		if (is_array($ART_ID)) {
			$WHERE = " IN ('".implode("','",$ART_ID)."')";
		} else {
			$WHERE = " = '".$ART_ID."'";
		}
		$SQL = "SELECT ARL_ART_ID, ARL_KIND,
				IF (ART_LOOKUP.ARL_KIND = 2, SUPPLIERS.SUP_BRAND, BRANDS.BRA_BRAND) AS brand,
				ARL_DISPLAY_NR as article
			FROM " . self::DB_TECDOC . ".ART_LOOKUP
				LEFT JOIN " . self::DB_TECDOC . ".BRANDS ON BRA_ID = ARL_BRA_ID
				INNER JOIN " . self::DB_TECDOC . ".ARTICLES ON ARTICLES.ART_ID = ART_LOOKUP.ARL_ART_ID
				INNER JOIN " . self::DB_TECDOC . ".SUPPLIERS ON SUPPLIERS.SUP_ID = ARTICLES.ART_SUP_ID
			WHERE
				ARL_ART_ID ".$WHERE." AND ARL_KIND = ".$TYPE."
			GROUP BY ARL_ART_ID, ARL_KIND, BRA_BRAND, ARL_DISPLAY_NR
			ORDER BY ARL_KIND, BRA_BRAND, ARL_DISPLAY_NR LIMIT 100;";

		return $this->getDB()->query($SQL)->fetchAll();
    }

	#Применяемость к автомобилям
	private function _autoStack($art_id)
	{
		$rsATypes = $this->GetAutoStack($art_id, self::TECDOC_LNG_ID);

		$autostack = array();
		if(count($rsATypes) > 0){
			$un = array();
			foreach($rsATypes as $val)
			{
				$arMd = explode(' ',$val['MOD_CDS_TEXT']);
				$arMds = $val['MFA_BRAND'].' '.$arMd[0];

				if(!in_array($arMds, $un)) {
					$un[] = $arMds;
					$list = array();

					foreach($rsATypes as $vl) {
						$arMd2 = explode(' ',$vl['MOD_CDS_TEXT']);
						$arMds2 = $vl['MFA_BRAND'].' '.$arMd2[0];
						if($arMds == $arMds2) {
							$list[] = array('brand'=> $this->MarkNameEncode($vl['MFA_BRAND']),
								'model_id' => $vl['MOD_ID'],
								'type_id'=>$vl['TYP_ID'],
								'title' => $vl['MOD_CDS_TEXT'].' / '.$vl['TYP_CDS_TEXT'],
								'year_start' => $this->TDDateFormat($vl['TYP_PCON_START'],'н.в.'),
								'year_end' => $this->TDDateFormat($vl['TYP_PCON_END'],'н.в.'),
								'body' => $vl['TYP_BODY_DES_TEXT'],
								'engine' => $vl['TYP_FUEL_DES_TEXT'],
								'engine_type' => $vl['ENG_CODE'],
								'volume' => $vl['TYP_CCM'],
								'kw' => $vl['TYP_KW_FROM'].' кВ',
								'hp' => $vl['TYP_HP_FROM']. ' л.с.'
							);
						}
					}
					$autostack[] = array('title' => $arMds, 'list' => $list);
				}
			}
		}

		return $autostack;
	}

	#Применимость к автомобилям
	public function GetAutoStack($ART_ID,$LNG_ID)
	{
	   $SQL="SELECT TYP_ID, MOD_ID, MFA_BRAND,
				DES_TEXTS7.TEX_TEXT AS MOD_CDS_TEXT,
				DES_TEXTS.TEX_TEXT AS TYP_CDS_TEXT,
				TYP_PCON_START, TYP_PCON_END, TYP_CCM, TYP_KW_FROM, TYP_KW_UPTO, TYP_HP_FROM, TYP_HP_UPTO, TYP_CYLINDERS, ENGINES.ENG_CODE,
				DES_TEXTS2.TEX_TEXT AS TYP_ENGINE_DES_TEXT,
				DES_TEXTS3.TEX_TEXT AS TYP_FUEL_DES_TEXT,
				IFNULL(DES_TEXTS4.TEX_TEXT, DES_TEXTS5.TEX_TEXT) AS TYP_BODY_DES_TEXT,
				DES_TEXTS6.TEX_TEXT AS TYP_AXLE_DES_TEXT,
				TYP_MAX_WEIGHT
			FROM " . self::DB_TECDOC . ".LINK_ART
				INNER JOIN " . self::DB_TECDOC . ".LINK_LA_TYP ON LAT_LA_ID = LA_ID
				INNER JOIN " . self::DB_TECDOC . ".TYPES ON TYP_ID = LAT_TYP_ID
				INNER JOIN " . self::DB_TECDOC . ".COUNTRY_DESIGNATIONS ON COUNTRY_DESIGNATIONS.CDS_ID = TYP_CDS_ID
				INNER JOIN " . self::DB_TECDOC . ".DES_TEXTS ON DES_TEXTS.TEX_ID = COUNTRY_DESIGNATIONS.CDS_TEX_ID
				INNER JOIN " . self::DB_TECDOC . ".MODELS ON MOD_ID = TYP_MOD_ID
				INNER JOIN " . self::DB_TECDOC . ".MANUFACTURERS ON MFA_ID = MOD_MFA_ID
				INNER JOIN " . self::DB_TECDOC . ".COUNTRY_DESIGNATIONS AS COUNTRY_DESIGNATIONS2 ON COUNTRY_DESIGNATIONS2.CDS_ID = MOD_CDS_ID
				INNER JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS7 ON DES_TEXTS7.TEX_ID = COUNTRY_DESIGNATIONS2.CDS_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS ON DESIGNATIONS.DES_ID = TYP_KV_ENGINE_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS2 ON DES_TEXTS2.TEX_ID = DESIGNATIONS.DES_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS AS DESIGNATIONS2 ON DESIGNATIONS2.DES_ID = TYP_KV_FUEL_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS3 ON DES_TEXTS3.TEX_ID = DESIGNATIONS2.DES_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".LINK_TYP_ENG ON LTE_TYP_ID = TYP_ID
				LEFT JOIN " . self::DB_TECDOC . ".ENGINES ON ENG_ID = LTE_ENG_ID
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS AS DESIGNATIONS3 ON DESIGNATIONS3.DES_ID = TYP_KV_BODY_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS4 ON DES_TEXTS4.TEX_ID = DESIGNATIONS3.DES_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS AS DESIGNATIONS4 ON DESIGNATIONS4.DES_ID = TYP_KV_MODEL_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS5 ON DES_TEXTS5.TEX_ID = DESIGNATIONS4.DES_TEX_ID
				LEFT JOIN " . self::DB_TECDOC . ".DESIGNATIONS AS DESIGNATIONS5 ON DESIGNATIONS5.DES_ID = TYP_KV_AXLE_DES_ID
				LEFT JOIN " . self::DB_TECDOC . ".DES_TEXTS AS DES_TEXTS6 ON DES_TEXTS6.TEX_ID = DESIGNATIONS5.DES_TEX_ID
			WHERE
				LA_ART_ID = ".$ART_ID." AND
				COUNTRY_DESIGNATIONS.CDS_LNG_ID = " . $LNG_ID . " AND
				COUNTRY_DESIGNATIONS2.CDS_LNG_ID = " . $LNG_ID . " AND
				(DESIGNATIONS.DES_LNG_ID IS NULL OR DESIGNATIONS.DES_LNG_ID = " . $LNG_ID . ") AND
				(DESIGNATIONS2.DES_LNG_ID IS NULL OR DESIGNATIONS2.DES_LNG_ID = " . $LNG_ID . ") AND
				(DESIGNATIONS3.DES_LNG_ID IS NULL OR DESIGNATIONS3.DES_LNG_ID = " . $LNG_ID . ") AND
				(DESIGNATIONS4.DES_LNG_ID IS NULL OR DESIGNATIONS4.DES_LNG_ID = " . $LNG_ID . ") AND
				(DESIGNATIONS5.DES_LNG_ID IS NULL OR DESIGNATIONS5.DES_LNG_ID = " . $LNG_ID . ")
			ORDER BY MFA_BRAND, MOD_CDS_TEXT, TYP_CDS_TEXT, TYP_PCON_START, TYP_CCM
			LIMIT 500;";

		return $this->getDB()->query($SQL)->fetchAll();
    }


	public function GetCommentsByArtNum ($artNum, $artName, $supBrand)
	{
		$SQL = "SELECT * FROM " . self::DB_ATRRU . ".comments
			where (ART_NUM = '" . $artName . "' and SUP_BRAND = '" . $supBrand . "' or ART_ID = '" . $artNum . "')
			and `VISIBLE` = '1'";

		return $this->getDB()->query($SQL)->fetchAll();
	}




	private function _convertPricesToRur($prices)
	{
		if (!empty($prices)) {
			foreach ($prices as $key => $price) {
				$prices[$key]['PRICE_RUR'] = Common_Helper::getPriceRur($price['PRICE'], $price['CURRENCY']);
			}
		}

		return $prices;
	}



	#Где ищем
	public function checkSQLWhere($where)
    {
 	    if (!is_null($where) && is_array($where))
        {
            $params = array();
			foreach ($where as $k=>$v)
            {
                $part=$k;  // сначала соединяем часть запроса 'поле' и 'знак'
				if (!is_array($v)) //если не массив, то готовим prepare statement
                {
					$part.= (!is_numeric($v))?"='".$v."'":"=";
                    $params[]=$v; // добавляем параметры в массив
                }
                else { // если массив, то вероятно мы используем IN (array)
                    $part.=" in ('".implode("','",$v)."')";
                }
                $res[]=$part;

            }
            $result['column']="WHERE ".implode(" AND ",$res);
            $result['params']=$params;
        }
        return $result;
    }

	#Сортировка
	public function checkSQLOrder($order)
    {
        if (is_array($order) && count($order)>0)
        {
            foreach ($order as $row=>$dir)
            {
                $res[]=$row." ".$dir;
            }
            return "ORDER BY ".implode(",",$res);
        }
        else return false;
    }


	#Преобразование названия марки авто
	public function MarkNameEncode($Mark)
	{
		if($Mark == 'MERCEDES-BENZ') $Mark=str_replace('-','_',$Mark);
		else $Mark=str_replace(' ','_',$Mark);
		if($Mark == 'CITROËN' || strstr($Mark,'citro')) $Mark='CITROEN';
		$Mark = mb_strtolower(trim($Mark));
		return $Mark;
	}

	#Преобразование даты
	public function TDDateFormat($date_ym,$DoNV=''){
	   if($date_ym!=0){
		  $year = substr($date_ym, 0,4);
		  $mount = substr($date_ym, 4,2);
		  $dat_my= "$mount.$year";
		 }else{$dat_my=$DoNV;}
	   return  $dat_my;
	}


	#Преобразование артикула в номер
	public function artToNumber($Article, $Type = 'FULL')
	{
		if ($Type == 'FULL') {
			$Article = preg_replace("/[^a-zA-Z0-9_.-]/u", "", trim($Article));
		} elseif ($Type == 'SHORT') {
			$Article = preg_replace("/[^a-zA-Z0-9_]/u", "", trim($Article));
		}

		return $Article;
	}

}


function StrToUp($str){
	$str = mb_strtoupper($str); //strtoupper($str);
	return trim($str);
}