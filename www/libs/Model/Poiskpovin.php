<?php

class Model_Poiskpovin extends Model_AbstractObject 
{
	
	
	public function getArticlesListByArticle($oem)
	{
		$select = "select p.ID, p.ART_NUM as oem, p.SUP_BRAND as manufacturer, round(p.PRICE, 2) as PRICE, p.CURRENCY, p.AVAILABLE
			from pricesru.PRICES p 
			where p.ART_NUM like " . $this->getDB()->quote($oem . '%') . "
			limit 1";
		
		$part = $this->getDB()->query($select)->fetch();
		
		if (!empty($part)) {
			if ($part['CURRENCY'] == 'BYR') {
				$part['PRICE'] = round($part['PRICE'] / 10000, 1);
			}
			$part['prices'] = array();
			
			$select = "select *
				from atrru.parts_shops";
			$shops = $this->getDB()->query($select)->fetchAll();
			foreach ($shops as $shop) {
				$part['prices'][] = array(
					'shop_id' => $shop['id'],
					'shop_name' => $shop['name'],
					'price' => round($part['PRICE'] * (rand(100, 150) / 100), 2),
				);
			}
			
			$part_with_prices = array($part);
		} else {
			$part_with_prices = array();
		}

		return $part_with_prices;
	}


}	