<?php
class Model_AbstractObject {
	
	protected $_fields;
	
	protected $_db;
	
	public function __construct($db = null) 
	{
		$this->setDB($db);
	}
	
	
	
	public function getDB()
	{
		if (is_null($this->_db)) {
			$this->_db = Environment::getDB();
		}
		
		return $this->_db;
	}
	
	public function setDB($db)
	{
		if (!empty($db)) {
			$this->_db = $db;
		}
	}

}