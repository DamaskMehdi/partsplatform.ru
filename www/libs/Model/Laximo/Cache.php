<?php
class Model_Laximo_Cache
{

	
	public function __construct()
	{
	}
	
	/**
	 * 
	 * @param string $vin
	 *
	 * @return array | false
	 */
    public function getCarInfoByVIN($vin)
	{
		$data = false;
		
		if (empty($vin)) {
			return $data;
		}
		
		$select = "select c.* from laximo_vin v, laximo_car c where v.id = c.vin_id and v.vin = " . Environment::getDB('remote')->quote($vin);
		$data = Environment::getDB('remote')->query($select)->fetch();
		
		return $data;
	}
	
	/**
	 * 
	 * @param string $vid
	 *
	 * @return array | false
	 */
    public function getCarInfoByVid($vid)
	{
		$data = false;
		
		if (empty($vid)) {
			return $data;
		}
		
		$select = "select c.* from laximo_car c where c.vehicleid = " . Environment::getDB('remote')->quote($vid);
		$data = Environment::getDB('remote')->query($select)->fetch();
		
		return $data;
	}
	
	
	/**
	 * 
	 * @param string $car_id
	 *
	 * @return array | false
	 */
    public function getCarInfoByCarId($car_id)
	{
		$data = false;
		
		if (empty($car_id)) {
			return $data;
		}
		
		$select = "select c.* from laximo_car c where c.id = " . Environment::getDB('remote')->quote($car_id);
		$data = Environment::getDB('remote')->query($select)->fetch();
		
		return $data;
	}
	
	
	public function setCarInfoData($car_data, $vehicleid, $vin)
	{
		$success = false;
		
		if (!empty($car_data)) {
			// insert vin to base
			$insert = "insert into laximo_vin (vin) values (" . Environment::getDB('remote')->quote($vin) . ")";
			if (Environment::getDB('remote')->exec($insert)) {
				$vin_id = Environment::getDB('remote')->lastInsertId();
				// insert car data to vin
				$insert = "insert into laximo_car (`vin_id`, `vehicleid`, `attributes`) values (" . $vin_id . "," . $vehicleid . "," . Environment::getDB('remote')->quote($car_data->asXML()) . ")";
				//$insert = "insert into laximo_car (`vin_id`, `attributes`) " . implode('`,`', array_keys($data)) . "`) values ('" . implode("','", array_values($data)) . "')";

				$success = Environment::getDB('remote')->exec($insert);
				if ($success) {
					$success = Environment::getDB('remote')->lastInsertId();
				}
			}
		}
		
		return $success;
	}
	
	
	public function getTreeByCarInfo($car_id)
	{
		$data = false;
		
		if (!empty($car_id)) {
			$select = "select t.* from laximo_tree t where t.car_id = " . Environment::getDB('remote')->quote($car_id);
			$data = Environment::getDB('remote')->query($select)->fetch();
		}
		
		return $data;
	}
	
	
	public function setTreeData($car_id, $tree)
	{
		$success = false;
		
		if (!empty($tree)) {
			// insert vin to base
			$insert = "insert into laximo_tree (car_id, tree) values (" . Environment::getDB('remote')->quote($car_id) . ", " . Environment::getDB('remote')->quote($tree->asXML()) . ")";

			$success = Environment::getDB('remote')->exec($insert);
			if ($success) {
				$success = Environment::getDB('remote')->lastInsertId();
			}
		}
		
		return $success;
	}
	
	
	
	private function _getDB()
	{
		
		return $this->_db;
	}
}
