<?php

class Model_Laximo_Laximo
{
	private $_db;
	
	private $_request;
	private $_cache;
	
	
	public function __construct($db = null)
	{
		if (!empty($db)) {
			$this->_db = $db;
		}
	}
	
	
	
	public function getCatalogs()
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestOEM('', '', Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		$this->_request->appendListCatalogs();

		$data = $this->_request->query();

		return $data[0];
	}
	
	
	public function getBrandFilters($catalog, $ssd)
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestOEM($catalog, $ssd, Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		// Append commands to request
		$this->_request->appendGetCatalogInfo();
		$this->_request->appendGetWizard2($ssd);

		// Execute request
		$data = $this->_request->query();
		
		return $data;
	}
	
	
	public function getArticlesListByArticle($oem, $options, $brand, $replacementtypes)
	{
		$data = array();

		$this->_request = new Model_Laximo_RequestAM('ru_RU');
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}
		$this->_request->appendFindOEM($oem, $options, $brand, $replacementtypes);
		
		$data = $this->_request->query();
		if (!empty($data)) {
			$data = new SimpleXmlElement($data);
		}

		return $data;
	}
	
	
	/**
	 * 
	 * @param string $vin
	 *
	 * @return array
	 */
    public function getAllByVIN($vin)
	{
		$data = array();
		
		if (empty($vin)) {
			return $data;
		}
		
		$vin = trim(trim($vin, '+'));

		$data['car_info'] = $this->getCarInfoByVIN($vin);
		$car_id = !empty($data['car_info']['id']) ? $data['car_info']['id'] : '';
		$data['parts_tree'] = $this->getTreeByCarInfo($car_id);
				
		return $data;
	}
	
	
	
	/**
	 * 
	 * @param string $vid
	 *
	 * @return array
	 */
    public function getAllByVid($vid)
	{
		$data = array();
		
		if (empty($vid)) {
			return $data;
		}
		
		$data['car_info'] = $this->getCarInfoByVid($vid);
		$car_id = !empty($data['car_info']['id']) ? $data['car_info']['id'] : '';
		$data['parts_tree'] = $this->getTreeByCarInfo($car_id);
				
		return $data;
	}
	
	
	/**
	 * 
	 * @param string $vid
	 *
	 * @return array
	 */
    public function getAllByFrame($frame, $frameNo)
	{
		$data = array();

		if (empty($frame)) {
			return $data;
		}
		
		$data['car_info'] = $this->getCarInfoByFrame($frame, $frameNo);
		
		if (!empty($data['car_info'])) {
			$catalog_code = self::getXmlAttribute($data['car_info']['attributes']->row, 'catalog');
			$ssd = self::getXmlAttribute($data['car_info']['attributes']->row, 'ssd');
			$vehicleid = self::getXmlAttribute($data['car_info']['attributes']->row, 'vehicleid');
			$data['parts_tree']['tree'] = $this->_getTree($catalog_code, $ssd, $vehicleid);
		}
				
		return $data;
	}
	
	
	public function getCarInfoByFrame($frame, $frameNo)
	{
		$this->_request = new Model_Laximo_RequestOEM('', '', Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}
		$this->_request->appendFindVehicleByFrame($frame, $frameNo);
		
		$result = $this->_request->query();
		
		$data = array(
			'attributes' => $result[0],
		);
		
		return $data;
	}
	
	
	public function getCarInfoByVIN($vin)
	{
		$data = array();
		
		if (empty($vin)) {
			return $data;
		}
		
		$data = $this->_getCache()->getCarInfoByVIN($vin);
		if (empty($data)) {
			//echo 'Car info to_cache<br>';
			$catalog_code = isset($_GET['c']) ? $_GET['c'] : false;
			$ssd = isset($_GET['ssd']) ? $_GET['ssd'] : '';
			
			$this->_request = new Model_Laximo_RequestOEM($catalog_code, $ssd, Model_Laximo_Config::$catalog_data);
			if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
				$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
			}
			
			$this->_request->appendFindVehicleByVIN($vin);
			$result = $this->_request->query();
			
			if (empty($result[0])) {
				return $data;
			} else {
				$data = array(
					'attributes' => $result[0],
				);
				$success = $this->_getCache()->setCarInfoData($result[0], self::getXmlAttribute($result[0]->row, 'vehicleid'), $vin);
				if ($success && is_numeric($success)) {
					$data['id'] = $success;
				}
			}
		} else {
			//echo 'Car Info from_cache<br>';

			$data['attributes'] = new SimpleXmlElement($data['attributes']);
		}
		
		return $data;
	}
	
	
	public function getCarInfoByVid($vid)
	{
		$data = array();
		
		if (empty($vid)) {
			return $data;
		}
		
		$data = $this->_getCache()->getCarInfoByVid($vid);
		if (empty($data)) {
			//echo 'Car info to_cache<br>';
			$catalog_code = isset($_GET['c']) ? $_GET['c'] : false;
			$ssd = isset($_GET['ssd']) ? $_GET['ssd'] : '';
			
			$this->_request = new Model_Laximo_RequestOEM($catalog_code, $ssd, Model_Laximo_Config::$catalog_data);
			if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
				$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
			}
			
			$this->_request->appendGetVehicleInfo($vid);
			$result = $this->_request->query();
			
			if (empty($result[0])) {
				return $data;
			} else {
				$data = array(
					'attributes' => $result[0],
				);
				$success = $this->_getCache()->setCarInfoData($result[0], self::getXmlAttribute($result[0]->row, 'vehicleid'), '');
				if ($success && is_numeric($success)) {
					$data['id'] = $success;
				}
			}
		} else {
			//echo 'Car Info from_cache<br>';

			$data['attributes'] = new SimpleXmlElement($data['attributes']);
		}
		
		return $data;
	}
	
	
	public function getCategoryGroups($catalog, $ssd, $vid, $cid)
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestOEM($catalog, $ssd, Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		$this->_request->appendGetVehicleInfo($vid);
		$this->_request->appendListCategories($vid, !empty($cid) ? $cid : -1);
		$this->_request->appendListUnits($vid, !empty($cid) ? $cid : -1);

		// Execute request
		$result = $this->_request->query();
		
		if (!$this->getError()) {
			$data = array(
				'car_info' => $result[0],
				'categories' => $result[1],
				'groups' => $result[2],
			);
		}
		
		return $data;
	}
	
	public function getGroupDetails($catalog, $ssd, $uid, $vid)
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestOEM($catalog, $ssd, Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		// Append commands to request
		$this->_request->appendGetUnitInfo($uid);
		$this->_request->appendListDetailByUnit($uid);
		$this->_request->appendListImageMapByUnit($uid);
		$this->_request->appendGetVehicleInfo($vid);

		// Execute request
		$result = $this->_request->query();

		// Check errors
		if ($this->_request->error != '') {
			echo $this->_request->error;
		} else {
			$data = array(
				'unit_info' => $result[0],
				'details' => $result[1],
				'image_map' => $result[2],
				'car_info' => $result[3],
			);
		}
		
		return $data;
	}

	
	public function getTreeByCarInfo($car_id)
	{
		$data = array();
		
		if (empty($car_id)) {
			return $data;
		}

		$data = $this->_getCache()->getTreeByCarInfo($car_id);
		if (empty($data)) {
			//echo 'Tree to_cache<br>';
			
			$car_info = $this->_getCache()->getCarInfoByCarId($car_id);
			if (!empty($car_info)) {
				$xml = new SimpleXmlElement($car_info['attributes']);
				$catalog_code = self::getXmlAttribute($xml->row, 'catalog');
				$ssd = self::getXmlAttribute($xml->row, 'ssd');
				
				$result = $this->_getTree($catalog_code, $ssd, self::getXmlAttribute($xml->row, 'vehicleid'));
				
				if (!empty($result)) {
					$data = array(
						'car_id' => $car_id,
						'tree' => $result,
					); //= $this->_convertToTreeData($result[0]);
					$success = $this->_getCache()->setTreeData($car_id, $data['tree']);
					if ($success && is_numeric($success)) {
						$data['id'] = $success;
					}
				}
			}
		} else {
			//echo 'Tree from_cache<br>';
			
			$data['tree'] = new SimpleXmlElement($data['tree']);
		}
		
		return $data;
	}
	
	
	private function _getTree($catalog_code, $ssd, $vehicleid) 
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestOEM($catalog_code, $ssd, Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		$this->_request->appendListQuickGroup($vehicleid);
		$result = $this->_request->query();
		
		if (!empty($result[0])) {
			$data = $result[0];
		}
		
		return $data;
	}

	public function getGroupData($catalog, $ssd, $vid, $gid)
	{
		$this->_request = new Model_Laximo_RequestOEM($catalog, $ssd, Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}
		$this->_request->appendListQuickDetail($vid, $gid, 1);
		$this->_request->appendGetVehicleInfo($vid);
		
		$data = $this->_request->query();

		return $data;
	}
	
	
	public function getDetailData($detail_id, $options)
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestAM('ru_RU');
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		$this->_request->appendFindDetail($detail_id, $options);

		$data = $this->_request->query();
		if ($this->getError()) {
			$data = array();
		} else {
			$data = new SimpleXmlElement($data);
		}
		
		return $data;
	}
	
	public function getVehicleByWizard($catalog, $ssd)
	{
		$data = array();
		
		$this->_request = new Model_Laximo_RequestOEM($catalog, $ssd, Model_Laximo_Config::$catalog_data);
		if (Model_Laximo_Config::$useLoginAuthorizationMethod) {
			$this->_request->setUserAuthorizationMethod(Model_Laximo_Config::$userLogin, Model_Laximo_Config::$userKey);
		}

		$this->_request->appendFindVehicleByWizard2($ssd);

		$data = $this->_request->query();
		
		return !empty($data[0]) ? $data[0] : array();
	}
	
	
	
	
	
	
	
	
	
	public function getError()
	{
		return is_null($this->_request) ? '' : $this->_request->error;
	}
	
	
	private function _getCache()
	{
		if (is_null($this->_cache)) {
			$this->_cache = new Model_Laximo_Cache($this->_db);
		}
		
		return $this->_cache;
	}
	
	
	public static function getXmlAttribute($xml, $attribute)
	{
		return isset($xml[$attribute]) ? (string)$xml[$attribute] : '';
	}
}
