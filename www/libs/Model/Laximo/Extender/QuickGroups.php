<?php

class Model_Laximo_Extender_QuickGroups extends Model_Laximo_Extender_Common
{
	function FormatLink($type, $dataItem, $catalog, $renderer)
	{
		if ($type == 'vehicle')
			$link = '/?ft=findByGroup&c='.$catalog.'&vid='.$renderer->vehicleid. '&ssd=' . $renderer->ssd;
		else
			$link = '/?ft=findByGroup&c='.$catalog.'&gid='.$dataItem['quickgroupid']. '&vid=' . $renderer->vehicleid. '&ssd=' . $renderer->ssd;

		return $link;
	}
}