<?php

class Model_Laximo_AAPTecdoc
{

	public function __construct()
	{

	}
	
	
	public function getPricesGroupByArticles($articles_list)
	{
		$select = "select p.ART_NUM, ifnull(llb.laximo_brand, p.SUP_BRAND) as brand, min(ROUND(p.PRICE * cur.rate, 0)) as minp, max(ROUND(p.PRICE * cur.rate, 0)) as maxp, count(p.SUPPLIER) as cnt
				from " . ImportMod_Destination_DataObject_Tecdoc::TABLE . " p
				LEFT JOIN atrru.currency cur ON REPLACE(p.CURRENCY, 'RUB', 'RUR') = cur.code
				left join prices.LINKS_LAXIMO_BRANDS llb on p.SUP_BRAND = llb.tecdoc_brand
				where REPLACE(p.ART_NUM, ' ', '') in ('" . implode("','", $articles_list) . "')
				group by p.ART_NUM, ifnull(llb.laximo_brand, p.SUP_BRAND)";
		return Environment::getDB()->query($select)->fetchAll();
	}
	
	public function getDetailByArticle($article, $brand)
	{
		$article = str_replace(array(' ', '-',), '', $article);

		$result = $this->getPricesByArticles(array($article), array($brand));

		if (!empty($result)) {
			$detail = array(
				'ART_ID' => $result[0]['ART_ID'],
				'ART_ARTICLE_NR' => $result[0]['ART_ARTICLE_NR'],
				'SUP_BRAND' => $result[0]['SUP_BRAND'],
				'IMAGE_PATH' => $result[0]['IMAGE_PATH'],
				'shops' => array(),
				'criterias' => array(),
				'prices_range' => '',
			);
			
			$price_min = $price_max = 0;
			foreach ($result as $val) {
				if (!empty($val['PRICE_ID'])) {
					if (!empty($val['PRICE_RUB'])) {
						if ($price_min == 0 || $price_min > $val['PRICE_RUB']) {
							$price_min = $val['PRICE_RUB'];
						}
						if ($price_max < $val['PRICE_RUB']) {
							$price_max = $val['PRICE_RUB'];
						}
					}
					
					$detail['shops'][] = array(
						'shop_id' => $val['shop_id'],
						'price_id' => $val['PRICE_ID'],
						'shop_name' => $val['shop_name'],
						'supplier_id' => $val['shop_id'],
						'PRICE_RUB' => $val['PRICE_RUB'],
						'quantity' => $val['AVAILABLE'],
						'IMPORT_DATE' => $val['IMPORT_DATE'],
						'email' => $val['EMAIL'],
						'phones' => $val['PHONES'],
						//'phones_array' => !empty($val['PHONES']) ? unserialize($val['PHONES']) : array(),
					);
				}
			}
			if (!empty($price_min)) {
				if ($price_min == $price_max) {
					$detail['prices_range'] = $price_min;
				} else {
					$detail['prices_range'] = $price_min . ' - ' . $price_max;
				}
			}
			
			if (!empty($detail['ART_ID'])) {
				$sql = "select dt3.TEX_TEXT AS title, IFNULL(dt2.TEX_TEXT, ac.ACR_VALUE) AS `value`
					from tecdoc2.ARTICLE_CRITERIA ac
					LEFT JOIN tecdoc2.DESIGNATIONS AS d2 ON d2.DES_ID = ac.ACR_KV_DES_ID
					LEFT JOIN tecdoc2.DES_TEXTS AS dt2 ON dt2.TEX_ID = d2.DES_TEX_ID
					LEFT JOIN tecdoc2.CRITERIA c ON c.CRI_ID = ac.ACR_CRI_ID
					LEFT JOIN tecdoc2.DESIGNATIONS d3 ON d3.DES_ID = c.CRI_DES_ID
					LEFT JOIN tecdoc2.DES_TEXTS dt3 ON dt3.TEX_ID = d3.DES_TEX_ID
					where ac.ACR_ART_ID = " . $detail['ART_ID'];
				$criterias = Environment::getDB()->query($sql)->fetchAll();
				if (!empty($criterias)) {
					foreach ($criterias as $val) {
						$detail['criterias'][] = array(
							'title' => $val['title'],
							'value' => $val['value'],
						);
					}
				}
			}

		}
		
		if (empty($detail['IMAGE_PATH'])) {
			$sql = "select *
				from prices.ARTICLES_LAXIMO_IMAGES ali
				where ali.name_trim = " . Environment::getDB()->quote(str_replace(array(' ', '-', '/', '"', "'",), '', $brand . $article));
			$image = Environment::getDB()->query($sql)->fetch();
			if (!empty($image)) {
				$detail['IMAGE_PATH'] = $image['image'];
			}
		}

		return !empty($detail) ? $detail : array();
	}
	
	
	public function getPricesByArticles($articles, $brands = array())
	{
		$sql = "
			select a.ART_ID, ai.name_trim as ART_ARTICLE_NR, s.SUP_BRAND, ai.image AS IMAGE_PATH, p.ID as PRICE_ID, 
				ROUND(p.PRICE * cur.rate, 0) as PRICE_RUB, p.AVAILABLE, p.IMPORT_DATE, 
				comp.id as shop_id, comp.name as shop_name, comp.request_email as EMAIL, comp.phones as PHONES, comp.location shop_location
			from " . ImportMod_Destination_DataObject_Tecdoc::TABLE . " p
			inner join atrru.companies comp on p.SUPPLIER = comp.id
			LEFT JOIN atrru.currency cur ON REPLACE(p.CURRENCY, 'RUB', 'RUR') = cur.code
			left join prices.LINKS_LAXIMO_BRANDS llb on p.SUP_BRAND = llb.tecdoc_brand
			left join tecdoc2.SUPPLIERS s on p.SUP_BRAND = s.SUP_BRAND
			left join prices.ARTICLES_IMAGES ai on ai.name_trim = p.ART_NUM
			left join tecdoc2.ARTICLES a on a.ART_SUP_ID = s.SUP_ID and ai.article_id = a.ART_ID
			where REPLACE(p.ART_NUM, ' ', '') in ('" . implode("','", $articles) . "') " . (!empty($brands) ? " and ifnull(llb.laximo_brand, p.SUP_BRAND) in ('" . implode("','", $brands) . "')" : "") . "
			order by comp.click_price desc"; hd($sql);
		
		return Environment::getDB()->query($sql)->fetchAll();
	}
	
	
	public function getDetailsByArticles($articles)
	{
		if (!empty($articles)) {
			$arts = array();
			foreach ($articles as $art) {
				$arts[] = Environment::getDB()->quote(str_replace(array(' ', '-',), '', $art));
			}
			
			$sql = "
				select a.ART_ID, ai.name_trim as ART_ARTICLE_NR, s.SUP_BRAND, ai.image AS IMAGE_PATH 
				from tecdoc2.ARTICLES a
				inner join tecdoc2.SUPPLIERS s on a.ART_SUP_ID = s.SUP_ID
				left join prices.LINKS_LAXIMO_BRANDS llb on llb.tecdoc_brand = s.SUP_BRAND
				inner join prices.ARTICLES_IMAGES ai on ai.article_id = a.ART_ID
				where ai.name_trim in (" . implode(',', $arts) . ")
				group by a.ART_ID, ai.name_trim, s.SUP_BRAND
			";

			$details = Environment::getDB()->query($sql)->fetchAll();
		}
		
		return !empty($details) ? $details : array();
	}
	
	public function getGImages($image_labels)
	{
		if (!empty($image_labels)) {
			$imgs = array();
			foreach ($image_labels as $img) {
				$imgs[] = Environment::getDB()->quote($img);
			}
			
			$sql = "select *
				from prices.ARTICLES_LAXIMO_IMAGES ali
				where ali.name_trim in (" . implode(',', $imgs) . ")";
			$images = Environment::getDB()->query($sql)->fetchAll();
		}
		
		return !empty($images) ? $images : array();
	}
	
}