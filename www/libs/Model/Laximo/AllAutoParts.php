<?php

class Model_Laximo_AllAutoParts
{
	private $_client;
	private $_errors = array();
	
	private $_map_brands = array(
		'MANN' => 'MANN-FILTER',
		'HENGST' => 'HENGST FILTER',
		'KNECHT/MAHLE' => 'MAHLE ORIGINAL',
	);
	
	public function __construct()
	{
		
		$this->_loadClient();
	}
	
	
	private function _loadClient()
	{
		$ext_soap = extension_loaded('soap');
		$ext_openssl = extension_loaded('openssl');
		$ext_SimpleXML = extension_loaded('SimpleXML');
		if ($ext_soap && $ext_openssl && $ext_SimpleXML) {
			$this->_client = new Model_Laximo_AAPSoapTransport();
		}
	}
	
	
	/**
    * Поиск запчастей
	* пример массива array(
				'session_id' => '',
				'session_login' => '',
				'session_password' => '',
				'search_code' => array(
					'oc47',
					'gdb454',
				)
    *
    * @param array $data ассоц. массив с данными
    * @return array возвращает массив запчастей с ценами
    */
	public function searchOffer($data)
	{
		$result = array();
		
		if (!is_null($this->_client)) {
			$this->_errors = array();
			
			$requestXMLstring = $this->_createSearchRequestXML($data);
			$responceXML = $this->_client->query('SearchOffer', array('SearchParametersXml' => $requestXMLstring), $errors);

			if ($responceXML) {
				//Разбор данных ответа
				$result = $this->_parseSearchResponseXML($responceXML);
			}
		}
		
		return $result;
	}
	
	/**
    * createSearchRequestXML
    *
    * Генерация строки запроса на поиск
    *
    * @param &array $data ссылка на ассоц. массив с данными
    * @return string возвращает строку с XML
    */
	private function _createSearchRequestXML($data) 
	{
		$xml = array();
		$data = $this->_validateData($data);
		
		if (!empty($data)) {
			$session_info = !empty($data['session_guid']) ?
				'SessionGUID="' . $data['session_guid'] . '"' :
				'UserLogin="' . base64_encode($data['session_login']) . '" UserPass="' . base64_encode($data['session_password']) . '"';

			if (is_array($data['search_code'])) {
				$search_code = '';
				
			} else {
				
			}
				
			$xml = '<root>
					  <SessionInfo ParentID="' . $data['session_id'] . '" ' . $session_info . '/>
					  <search>
						 <skeys>
							' . (is_array($data['search_code']) ? '<skey>' . implode('</skey><skey>', $data['search_code']) . '</skey>' : '<skey>' . $data['search_code'] . '</skey>') . '
						 </skeys>
						 <instock>' . $data['instock'] . '</instock>
						 <showcross>' . $data['showcross'] . '</showcross>
						 <periodmin>' . $data['periodmin'] . '</periodmin>
						 <periodmax>' . $data['periodmax'] . '</periodmax>
					  </search>
					</root>';
		}
		
		return $xml;
	}
	
	
	/**
    * parseSearchResponseXML
    *
    * Разбор ответа сервиса поиска.
    *
	 * Собственно просто преобразует данные из SimpleXMLObject в массив,
    * также добавляет к каждой записи уникальный ReferenceID. В данном примере
    * в этом качестве будет выступать случайным образом сгенерированная строка.
    * В реальном использовании Reference обозначает ID конкретной записи в контексте
    * системы, в которой используются сервисы (например, id из таблицы БД, с которой
    * сопоставлено предложение)
	 *
    * @param SimpleXMLObject XML-объект
    * @return array возвращает массив данных
    */
	private function _parseSearchResponseXML($xml) 
	{
		$data = array();
		
		foreach($xml->rows->row as $row) {
			$_row = array();
			foreach($row as $key => $field) {
				$_row[(string)$key] = (string)$field;
			}
			$_row['Reference'] = $this->_generateRandom(9);
			
			$detail_name = mb_strtolower(str_replace(array(' ', '-'), '', $this->_getMapBrand((string)$row->ManufacturerName) . '_' . (string)$row->CodeAsIs), 'utf8');
			$data[$detail_name][] = $_row;
		}
		
		foreach ($data as $key => $parts) {
			usort($data[$key], function($a, $b){
				return ((float)$a['Price'] > (float)$b['Price']);
			});
		}

		return $data;
	}
	
	
	
	
	private function _getMapBrand($brand)
	{
		return isset($this->_map_brands[$brand]) ? $this->_map_brands[$brand] : $brand;
	}
	

	/**
    * validateData
    *
    * Фунцкия производит проверку и подготовку данных для отправки в запрос
    *
    * @param &array $data ссылка на ассоц. массив с данными
    * @param &array $errors ссылка на массив ошибок
    * @return true в случае, если данные корректны, false при ошибке
    */
	private function _validateData($data) 
	{
		if (!empty($data['search_code']) && ((!empty($data['session_login']) && !empty($data['session_id']) && !empty($data['session_password'])) || !empty($data['session_guid']))) {
			$data['instock'] = !empty($data['instock']) ? 1 : 0;
			$data['showcross'] = !empty($data['showcross']) ? 1 : 0;
			$data['periodmin'] = !empty($data['periodmin']) ? (int)$data['periodmin'] : -1;
			$data['periodmax'] = !empty($data['periodmax']) ? (int)$data['periodmax'] : -1;
		} else {
			$data = array();
		}

		return $data;
	}
	
	
	/**
    * generateRandom
    *
    * Генерирует случайную строку из чисел заданой длины
    *
    * @param int $maxlen длина строки
    * @return string
    */
	private function _generateRandom($maxlen = 32) 
	{
		$code = '';
		while (strlen($code) < $maxlen) {
		 $code .= mt_rand(0, 9);
		}
		return $code;
   }
}