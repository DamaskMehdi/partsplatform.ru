<?php

class Model_Laximo_Render_Template
{
	var $extender;

	public function __construct(Model_Laximo_Extender_IExtender $extender)
	{
		$this->extender = $extender;

		static $guayaquil_templateinitialized;
		
		if (!isset($guayaquil_templateinitialized))
		{
			$this->AppendCSS(str_replace('public_html/', '', dirname(__FILE__).'/guayaquil.css'));
			//$this->AppendJavaScript(str_replace('public_html/', '', dirname(__FILE__).'/jquery.js'));
			
			$guayaquil_templateinitialized = 1;
		}
	}

	public function GetLocalizedString($name, $params = false)
	{
		if ($this->extender == NULL)
			return $name;

		return $this->extender->GetLocalizedString($name, $params, $this);
	}

	public function FormatLink($type, $dataItem, $catalog)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method FormatLink');

		return $this->extender->FormatLink($type, $dataItem, $catalog, $this);
	}

	public function AppendJavaScript($filename)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method AppendJavaScript');

		return $this->extender->AppendJavaScript($filename, $this);
	}

	public function AppendCSS($filename)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method AppendCSS');

		return $this->extender->AppendCSS($filename, $this);
	}

	public function Convert2uri($filename)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method Convert2uri');

		return $this->extender->Convert2uri($filename, $this);
	}
}
