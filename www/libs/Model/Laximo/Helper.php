<?php

class Model_Laximo_Helper
{
	const DEFAULT_PRICE = 999999999;
	
	const SEARCH_TYPE_VIN = 1;
	const SEARCH_TYPE_ARTICLE = 2;
	const SEARCH_TYPE_FRAME = 3;
	
	const AAP_PRICE_MARKUP = 1.3;
	
	private $_simple_image;
	
	private $_map_brand_to_aap = array(
		'SEAT' => 'VAG',
		'VOLKSWAGEN' => 'VAG',
		'AUDI' => 'VAG',
		'SKODA' => 'VAG',
		'KIA' => 'KIA/HYUNDAI/MOBIS',
		'HYUNDAI' => 'KIA/HYUNDAI/MOBIS',
		'ALCO FILTERS' => 'ALCO',
		'PEUGEOT' => 'CITROEN/PEUGEOT',
		'CITROEN' => 'CITROEN/PEUGEOT',
	);
	
	
	public function __construct()
	{
	}
	
	
	
	public function getArticleImage($simple_image)
	{
		$this->_simple_image = $simple_image;
		
		$article = str_replace(array(' ', '-',), '', $_REQUEST['gifg']);
		
		$select = "select ai.id, ai.image
			from prices.ARTICLES_IMAGES ai
			inner join tecdoc2.ARTICLES a on ai.article_id = a.ART_ID
			inner join tecdoc2.SUPPLIERS s on a.ART_SUP_ID = s.SUP_ID
			left join prices.LINKS_LAXIMO_BRANDS llb on llb.tecdoc_brand = s.SUP_BRAND
			where ai.name_trim = " . Environment::getDB()->quote($article) . " and ifnull(llb.laximo_brand, s.SUP_BRAND) = " . Environment::getDB()->quote($_REQUEST['brand']) . "
				";
				
		$detail = Environment::getDB()->query($select)->fetch();
		if (!empty($detail['id'])) {
			if (!empty($detail['image'])) {
				$image_name = '/pix/' . $detail['image'];
			} else {
				$image_name = $this->_uploadImageFromGoogle($_REQUEST['brand'], $_REQUEST['gifg'], $detail['id']);
				if (!empty($image_name)) {
					$update = "update prices.ARTICLES_IMAGES set image = " . Environment::getDB()->quote($image_name) . " where id = " . $detail['id'];
					Environment::getDB()->exec($update);
				}
			}
		} else {
			$image_label = $this->_getImageLabelFromGoogle($_REQUEST['brand'], $_REQUEST['gifg']);
			
			$select = "select ali.id, ali.image
				from prices.ARTICLES_LAXIMO_IMAGES ali
				where ali.name_trim = " . Environment::getDB()->quote($image_label);
			$image_row = Environment::getDB()->query($select)->fetch();
			
			if (!empty($image_row)) {
				$image_name = $image_row['image'];
			} else {
				$image_name = $this->_uploadImageFromGoogle($_REQUEST['brand'], $_REQUEST['gifg']);
				if (!empty($image_name)) {
					$insert = "insert into prices.ARTICLES_LAXIMO_IMAGES (name_trim, image) values (" . Environment::getDB()->quote($image_label) . ", " . Environment::getDB()->quote($image_name) . ")";
					Environment::getDB()->exec($insert);
				}
			}
		}
		
		return !empty($image_name) ? $image_name : '';
	}
	
	private function _getImageLabelFromGoogle($brand, $article)
	{
		return str_replace(array(' ', '-', '/', '"', "'",), '', $brand . $article);
	}
	
	private function _uploadImageFromGoogle($brand, $article, $detail_id = 0)
	{
		$google_url = 'https://www.google.by/search?q=' . str_replace(' ', '+', $article . ' ' . $brand) . '&espv=2&biw=1920&bih=950&site=webhp&source=lnms&tbm=isch&sa=X&ved=0ahUKEwj5kb-oyuTQAhVC7xQKHYMFCmAQ_AUIBigB';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $google_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36');
		$page = curl_exec($ch);//echo $page;die();
		if (!empty($page)) {
			$image = substr($page, strpos($page, 'id="rg"'));
			$image = substr($image, strpos($image, 'ou":"') + 5);//echo $image; die();
			$image = substr($image, 0, strpos($image, '"'));
			
			if (!empty($detail_id)) {
				$filename = 'gimages/' . $detail_id . '.jpg';
				$filename_small = 'gimages/small/' . $detail_id . '.jpg';
			} else {
				$filename = 'gimages/' . $this->_uploadImageFromGoogle($brand, $article) . '.jpg';
				$filename_small = 'gimages/small/' . $this->_uploadImageFromGoogle($brand, $article) . '.jpg';
			}

			$local_path = $_SERVER['DOCUMENT_ROOT'] . '/images/' . $filename;
			$local_path_small = $_SERVER['DOCUMENT_ROOT'] . '/images/' . $filename_small;
			if (copy($image, $local_path)) {
				$this->_simple_image->load($local_path);
				$this->_simple_image->resizeToHeight(50);
				$this->_simple_image->save($local_path_small);
				
				$image_path = '/images/' . $filename_small;
			}
		}
		
		return !empty($image_path) ? $image_path : '';
	}
	
	
	public function getArticlesWithPrices($oem, $options, $brand, $replacementtypes, $details = null, $add_allautoparts = false, $module_path = POISKPOVIN_URL)
	{
		$articles = array();
		
		if (is_null($details)) {
			$details = Environment::getModel('Laximo_Laximo')->getArticlesListByArticle($oem, $options, $brand, $replacementtypes);
		}
		
		//hhd($details);
		if (!empty($details)) {
			$oem_list = array();
			
			foreach ($details->FindOEM->detail as $detail) {
				$oem_list[] = str_replace(' ', '', $detail['oem']);

				$articles[(string)$detail['detailid']] = array(
					'detailid' => (string)$detail['detailid'],
					'manufacturer' => (string)$detail['manufacturer'],
					'oem' => (string)$detail['oem'],
					'formattedoem' => (string)$detail['formattedoem'],
					'name' => (string)$detail['name'],
					'link' => $module_path . '?ft=detailInfo&detailid=' . (string)$detail['detailid'],
					'replacement' => array(),
				);
				
				foreach ($detail->replacements->replacement as $repl) {
					$oem_list[] = str_replace(' ', '', $repl->detail['oem']);
					
					$articles[(string)$detail['detailid']]['replacement'][(string)$repl->detail['detailid']] = array(
						'detailid' => (string)$repl->detail['detailid'],
						'manufacturer' => (string)$repl->detail['manufacturer'],
						'oem' => (string)$repl->detail['oem'],
						'formattedoem' => (string)$repl->detail['formattedoem'],
						'name' => (string)$repl->detail['name'],
						'link' => $module_path . '?ft=detailInfo&detailid=' . (string)$repl->detail['detailid'],
					);
				}
			}
			
			$group_prices = Environment::getModel('Laximo_AAPTecdoc')->getPricesGroupByArticles($oem_list);

			if (!empty($group_prices)) {
				$prices = array();
				foreach ($group_prices as $price) {
					$prices[strtolower($price['brand']) . strtolower(str_replace(' ', '', $price['ART_NUM']))] = $price;
				}
				
				foreach ($articles as $key => $article) {
					$article_price_min = self::DEFAULT_PRICE;
					$article_price_max = self::DEFAULT_PRICE;
					$sellers = 0;

					$pkey = strtolower($article['manufacturer']) . strtolower(str_replace(' ', '', $article['oem']));
					if (isset($prices[$pkey])) {
						$article_price_min = $prices[$pkey]['minp'];
						$article_price_max = $prices[$pkey]['maxp'];
						$sellers = $prices[$pkey]['cnt'];
						
						unset($prices[$pkey]);
					}
					
					$articles[$key]['price_min'] = $article_price_min;
					$articles[$key]['price_max'] = $article_price_max;
					$articles[$key]['sellers'] = $sellers;
					
					foreach ($article['replacement'] as $rkey => $rarticle) {
						$rarticle_price_min = self::DEFAULT_PRICE;
						$rarticle_price_max = self::DEFAULT_PRICE;
						$rsellers = 0;
						
						$pkey = strtolower($rarticle['manufacturer']) . strtolower(str_replace(' ', '', $rarticle['oem']));
						if (isset($prices[$pkey])) {
							$rarticle_price_min = $prices[$pkey]['minp'];
							$rarticle_price_max = $prices[$pkey]['maxp'];
							$rsellers = $prices[$pkey]['cnt'];
							
							unset($prices[$pkey]);
						}
						
						$articles[$key]['replacement'][$rkey]['price_min'] = $rarticle_price_min;
						$articles[$key]['replacement'][$rkey]['price_max'] = $rarticle_price_max;
						$articles[$key]['replacement'][$rkey]['sellers'] = $rsellers;
					}
				}
				
				foreach ($articles as $key => $article) {
					usort($articles[$key]['replacement'], function($a, $b){
						return ((float)$a['price_min'] > (float)$b['price_min']);
					});
				}
			}

			if ($add_allautoparts) {
				$articles = $this->_addAllAutoPartsPrices($articles);
			}
			
			//d($prices);
			//hhd($articles);
		}

		return $articles;
	}
	
	
	private function _addAllAutoPartsPrices($articles)
	{
		$parts_oem = array();
		foreach ($articles as $id => $art) {
			if (empty($art['price']) || $art['price'] == self::DEFAULT_PRICE) {
				$parts_oem[] = $art['oem'];
			}
			if (!empty($art['replacement'])) {
				foreach ($art['replacement'] as $key => $art_repl) {
					if (empty($art_repl['price']) || $art_repl['price'] == self::DEFAULT_PRICE) {
						$parts_oem[] = $art_repl['oem'];
					}
				}
			}
		}
		$parts_oem = array_unique($parts_oem);

		if (!empty($parts_oem)) {
			$aap = new Model_Laximo_AllAutoParts();
			$data = array(
				'session_id' => '8418',
				'session_login' => '6666029',
				'session_password' => '6666029',
				'search_code' => $parts_oem,
			);
			$result = $aap->searchOffer($data);

			if (!empty($result)) {
				foreach ($articles as $id => $art) {
					if ((empty($art['price']) || $art['price'] == self::DEFAULT_PRICE)) {
						$article_name = $this->_getAAPKey($art);
						if (!empty($result[$article_name])) {
							$articles[$id]['price'] = round($result[$article_name][0]['Price'] * self::AAP_PRICE_MARKUP, 2);
							$articles[$id]['available'] = $result[$article_name][0]['Quantity'];
							$articles[$id]['delivery_time'] = $result[$article_name][0]['PeriodMax'];
							$articles[$id]['price_id'] = 0;
						}
						
					}
					if (!empty($art['replacement'])) {
						foreach ($art['replacement'] as $key => $art_repl) {
							if (empty($art_repl['price']) || $art_repl['price'] == self::DEFAULT_PRICE) {
								$article_name = $this->_getAAPKey($art_repl);
								if (!empty($result[$article_name])) {
									$articles[$id]['replacement'][$key]['price'] = round($result[$article_name][0]['Price'] * self::AAP_PRICE_MARKUP, 2);
									$articles[$id]['replacement'][$key]['available'] = $result[$article_name][0]['Quantity'];
									$articles[$id]['replacement'][$key]['delivery_time'] = $result[$article_name][0]['PeriodMax'];
									$articles[$id]['replacement'][$key]['price_id'] = 0;
								}
							}
						}
					}
				}
			}
		}
		
		
		//d($aap->searchOffer($data));
			
		return $articles;
	}
	
	
	private function _getAAPKey($art)
	{
		$key = isset($this->_map_brand_to_aap[$art['manufacturer']]) ? $this->_map_brand_to_aap[$art['manufacturer']] : $art['manufacturer'];
		$key .= '_' . $art['oem'];
		
		return mb_strtolower(str_replace(array(' ', '-'), '', $key), 'utf8');
	}
	
	
	public function addSearchHistory($type, $brand, $search_text, $link = '')
	{
		$insert = "insert into atr.search_parts_history (`type`, brand, `text`, link, date_add) values (" . Environment::getDB()->quote($type) . ", " . Environment::getDB()->quote($brand) . ", " . Environment::getDB()->quote($search_text) . ", " . Environment::getDB()->quote($link) . ", now())";
		Environment::getDB()->exec($insert);
	}
	
	
	public function getTreeFullData($vin, $frame, $frameNo, $vid)
	{
		if (!empty($vin)) {
			$data = Environment::getModel('Laximo_Laximo')->getAllByVIN($vin);
			if (!empty($data['car_info']['attributes'])) {
				// купить запчасти к марка модель
				$text = 'купить запчасти к ' . $data['car_info']['attributes']->row['brand'] . ' ' . $data['car_info']['attributes']->row['name'] . ' ' . $vin;
				$this->addSearchHistory(self::SEARCH_TYPE_VIN, $data['car_info']['attributes']->row['brand'], $text, $_SERVER['REQUEST_URI']);
			}
		} elseif ($frame && $frameNo) {
			$data = Environment::getModel('Laximo_Laximo')->getAllByFrame($frame, $frameNo);
			if (!empty($data['car_info']['attributes'])) {
				$text = 'купить запчасти к ' . $data['car_info']['attributes']->row['brand'] . ' ' . $data['car_info']['attributes']->row['name'] . ' ' . $frame . '-' . $frameNo;
				$this->addSearchHistory(Model_Laximo_Helper::SEARCH_TYPE_FRAME, $data['car_info']['attributes']->row['brand'], $text, $_SERVER['REQUEST_URI']);
			}
		} else {
			$data = Environment::getModel('Laximo_Laximo')->getAllByVid($vid);
		}

		if (!empty($data['car_info']['attributes'])) {
			$renderer = new Model_Laximo_Render_QuickGroupsList(new Model_Laximo_Extender_QuickGroups());
			$car_attributes = $data['car_info']['attributes']->row;
			$catalog = Model_Laximo_Laximo::getXmlAttribute($car_attributes, 'catalog');
			$vid = Model_Laximo_Laximo::getXmlAttribute($car_attributes, 'vehicleid');
			$ssd = Model_Laximo_Laximo::getXmlAttribute($car_attributes, 'ssd');
			
			if (!empty($data['parts_tree']['tree'])) {
				$data['tree_html'] = $renderer->Draw($data['parts_tree']['tree'], $catalog, $vid, $ssd);
			} else {
				$data['tree_html'] = '';
			}
		}
		
		return $data;
	}
	
	
	public function getCategoryGroups($catalog, $ssd, $vid, $cid, $module_path)
	{
		$groups = Environment::getModel('Laximo_Laximo')->getCategoryGroups($catalog, $ssd, $vid, $cid);

		if (!empty($groups['categories'])) {
			$category_uri = '&c=' . $catalog . '&vid=' . $vid . '&ssd=' . $ssd;
			$category_title = 'Категория';
			foreach ($groups['categories']->row as $category) {
				$category->addAttribute('active', $category['categoryid'] == $cid ? true : false);
				$category->addAttribute('link', $module_path . '?ft=listByCategory&cid=' . $category['categoryid'] . $category_uri);
				if ($category['categoryid'] == $cid) {
					$category_title = $category['name'];
				}
			}
		}

		$small_size = 175;
		if (!empty($groups['groups'])) {
			$category_uri = '&c=' . $catalog . '&vid=' . $vid . '&ssd=' . $ssd;
			foreach ($groups['groups']->row as $group) {
				//$category->addAttribute('link', '/poiskpovin?ft=listByCategory&cid=' . $category['categoryid'] . $category_uri);
				$group['imageurl'] = str_replace('http://img.laximo.net/', '/ppv/', str_replace('%size%', $small_size, $group['imageurl']));
				$group['largeimageurl'] = str_replace('http://img.laximo.net/', '/ppv/', str_replace('%size%', 'source', $group['largeimageurl']));
				$group['link'] = $module_path . '?ft=groupDetails&c=' . $catalog . '&vid=' . $vid . '&uid=' . $group['unitid'] . '&cid=' . $cid . '&ssd=' . $group['ssd'];
			}
		}

		return $groups;
	}
	
	
	public function getGroupDetails($catalog, $ssd, $uid, $vid, $module_path)
	{
		$details = Environment::getModel('Laximo_Laximo')->getGroupDetails($catalog, $ssd, $uid, $vid);
		if (!empty($details)) {
			$small_size = 175;
			$details['unit_info']->row['imageurl'] = str_replace('http://img.laximo.net/', '/ppv/', str_replace('%size%', $small_size, $details['unit_info']->row['imageurl']));
			$details['unit_info']->row['largeimageurl'] = str_replace('http://img.laximo.net/', '/ppv/', str_replace('%size%', 'source', $details['unit_info']->row['largeimageurl']));

			foreach ($details['details']->row as $detail) {
				$detail->addAttribute('link', $module_path . '?ft=findByArticle&brand=' . $details['car_info']->row['brand'] . '&oem=' . str_replace(' ', '', (string)$detail['oem']));
			}
		} else {
			$details = array();
		}

		return $details;
	}
	
	
	public function getArticlesListByArticle($brand, $oem, $module_path)
	{
		$options = 'crosses';
		$replacementtypes = 'synonym,Replacement,Duplicate,Bidirectional';

		$details = Environment::getModel('Laximo_Laximo')->getArticlesListByArticle($oem, $options, $brand, $replacementtypes);
		if (!empty($details->FindOEM)) {
			$text = 'купить запчасть ' . (string)$details->FindOEM->detail[0]['manufacturer'] . ' ' . $oem . ' ' . (string)$details->FindOEM->detail[0]['name'];
			$this->addSearchHistory(Model_Laximo_Helper::SEARCH_TYPE_ARTICLE, (string)$details->FindOEM->detail[0]['manufacturer'], $text, $_SERVER['REQUEST_URI']);
		}

		$details_with_prices = $this->getArticlesWithPrices($oem, $options, $brand, $replacementtypes, $details, false, $module_path);
		
		if (!empty($details_with_prices)) {
			$oems = array();
			$labels = array();
			foreach ($details_with_prices as $detail) {
				$oems[] = $detail['oem'];
				$labels[] = $this->_getImageLabelFromGoogle($detail['manufacturer'], $detail['oem']);
				
				foreach ($detail['replacement'] as $repl) {
					$oems[] = $repl['oem'];
					$labels[] = $this->_getImageLabelFromGoogle($repl['manufacturer'], $repl['oem']);
				}
			}

			$tecdoc_details = Environment::getModel('Laximo_AAPTecdoc')->getDetailsByArticles($oems);
			$gimages = Environment::getModel('Laximo_AAPTecdoc')->getGImages($labels);

			if (!empty($tecdoc_details)) {
				$tds = array();
				$gis = array();
				foreach ($tecdoc_details as $td) {
					$key = mb_strtolower(str_replace(' ', '', $td['SUP_BRAND'] . '_' . $td['ART_ARTICLE_NR']), 'utf8');
					$tds[$key] = $td;
				}
				foreach ($gimages as $gi) {
					$gis[$gi['name_trim']] = $gi;
				}
				
				foreach ($details_with_prices as $kd => $detail) {
					$key = mb_strtolower(str_replace(' ', '', $detail['manufacturer'] . '_' . $detail['oem']), 'utf8');
					$ikey = $this->_getImageLabelFromGoogle($detail['manufacturer'], $detail['oem']);
					if (!empty($tds[$key])) {
						$details_with_prices[$kd]['image'] = $tds[$key]['IMAGE_PATH'];
						$details_with_prices[$kd]['image_full'] = str_replace('small/', '', $tds[$key]['IMAGE_PATH']);
					} elseif (!empty($gis[$ikey])) {
						$details_with_prices[$kd]['image'] = $gis[$ikey]['image'];
						$details_with_prices[$kd]['image_full'] = str_replace('small/', '', $gis[$ikey]['image']);
					}

					foreach ($detail['replacement'] as $rd => $repl) {
						$key = mb_strtolower(str_replace(' ', '', $repl['manufacturer'] . '_' . $repl['oem']), 'utf8');
						$ikey = $this->_getImageLabelFromGoogle($repl['manufacturer'], $repl['oem']);
						if (!empty($tds[$key])) {
							$details_with_prices[$kd]['replacement'][$rd]['image'] = $tds[$key]['IMAGE_PATH'];
							$details_with_prices[$kd]['replacement'][$rd]['image_full'] = str_replace('small/', '', $tds[$key]['IMAGE_PATH']);
						} elseif (!empty($gis[$ikey])) {
							$details_with_prices[$kd]['image'] = $gis[$ikey]['image'];
							$details_with_prices[$kd]['image_full'] = str_replace('small/', '', $gis[$ikey]['image']);
						}
					}
				}
			}
		}
		
		return $details_with_prices;
	}
	
	
	public function getDetailData($detailid)
	{
		$options = 'crosses';
		$detail = Environment::getModel('Laximo_Laximo')->getDetailData($detailid, $options);
		
		if (!empty($detail)) {
			$tecdoc_article = Environment::getModel('Laximo_AAPTecdoc')->getDetailByArticle($detail->FindDetails->detail['oem'], $detail->FindDetails->detail['manufacturer']);
			
			if (!empty($tecdoc_article['ART_ID'])) {
				$detail->FindDetails->detail->addAttribute('tecdoc_art_id', $tecdoc_article['ART_ID']);
			}
			if (!empty($tecdoc_article['IMAGE_PATH'])) {
				$detail->FindDetails->detail->addAttribute('tecdoc_image', $tecdoc_article['IMAGE_PATH']);
				$detail->FindDetails->detail->addAttribute('tecdoc_image_big', str_replace('small/', '', $tecdoc_article['IMAGE_PATH']));
			}
			if (!empty($tecdoc_article['prices_range'])) {
				$detail->FindDetails->detail->addAttribute('prices_range', $tecdoc_article['prices_range']);
			}
			/*if (!empty($tecdoc_article['shops'])) {
				$detail->FindDetails->detail->addAttribute('shops', $tecdoc_article['shops']);
			} else {
				$aap = new Model_Laximo_AllAutoParts();
				$data = array(
					'session_id' => '8418',
					'session_login' => '6666029',
					'session_password' => '6666029',
					'search_code' => array($detail->FindDetails->detail['oem']),
				);
				$result = $aap->searchOffer($data);
				if (!empty($result)) {
					$article_name = mb_strtolower(str_replace(' ', '', $detail->FindDetails->detail['manufacturer'] . '_' . $detail->FindDetails->detail['oem']), 'utf8');
					if (!empty($result[$article_name])) {
						$detail->FindDetails->detail->addAttribute('price', str_replace(',', '.', round($result[$article_name][0]['Price'] * Model_Laximo_Helper::AAP_PRICE_MARKUP, 2)));
						$detail->FindDetails->detail->addAttribute('available', $result[$article_name][0]['Quantity']);
						$detail->FindDetails->detail->addAttribute('delivery_time', $result[$article_name][0]['PeriodMax']);
						$detail->FindDetails->detail->addAttribute('price_id', 0);
					}
				}
			}*/
		}
		
		$result = array(
			'detail' => $detail,
			'tecdoc_criterias' => !empty($tecdoc_article['criterias']) ? $tecdoc_article['criterias'] : array(),
			'prices' => !empty($tecdoc_article['shops']) ? $tecdoc_article['shops'] : array(),
		);
		
		return $result;
	}

	
	public function getCatalogBrands($module_path, $columns = 9)
	{
		$data = Environment::getModel('Laximo_Laximo')->getCatalogs();
		if (!empty($data)) {
			foreach ($data->row as $key => $brand) {
				$brand->addAttribute('link', $module_path . '?ft=findByBrand&c=' . $brand['code'] . '&ssd=');
			}

			$hide_brands = array('ABARTH', 'Fiat Professional', );//'BMW Motorrad', 'DAF', 'MAN', 'Ram', 'Rolls-Royce', 'Subaru Japan', 'Subaru USA',
				//'Alfa Romeo', 'Chevrolet', 'Citroen', 'Fiat', 'Ford Europe', 'Honda', 'Lancia', 'Opel/Vauxhall', 'Peugeot', 'Subaru Europe', );
			$brands = $this->_getByColumnsWithLetters($data->row, $columns, $hide_brands);
		}
		
		return !empty($brands) ? $brands : array();
	}
	
	
	public function getCatalogBrandFilters($catalog, $ssd, $module_path)
	{
		$brand_filters = array(
			'filters' => array(),
			'html_filters' => '',
		);
		
		$brand_filters['filters'] = Environment::getModel('Laximo_Laximo')->getBrandFilters($catalog, $ssd);
		if (!empty($brand_filters['filters'][1])) {
			$renderer = new Model_Laximo_Render_Wizard(new Model_Laximo_Extender_Wizard());
			$brand_filters['html_filters'] = $renderer->Draw($catalog, $brand_filters['filters'][1]);
		}
		
		return $brand_filters;
	}
	
	
	public function getVehicleByWizard($catalog, $ssd, $module_path)
	{
		$vehicles_xml = Environment::getModel('Laximo_Laximo')->getVehicleByWizard($catalog, $ssd);

		if (!empty($vehicles_xml)) {
			$vehicles = array();
			foreach ($vehicles_xml->row as $vehicle) {
				$veh = array(
					'brand' => (string)$vehicle['brand'], 
					'name' => (string)$vehicle['name'], 
					'link' => $module_path . '?ft=findByVIN&c=' . (string)$vehicle['catalog'] . '&ssd=' . (string)$vehicle['ssd'] . '&vid=' . (string)$vehicle['vehicleid'],
				);
				
				foreach ($vehicle->attribute as $attr) {
					$veh[(string)$attr['key']] = (string)$attr['value'];
				}
		
				
				$vehicles[] = $veh;
			}
		}
		
		return !empty($vehicles) ? $vehicles : array();
	}
	
	
	
	private function _getByColumnsWithLetters($items, $columns, $hide_brands = array())
	{
		$result = array();

		foreach ($items as $item) {
			if (!in_array($item['name'], $hide_brands)) {
				$letters[mb_substr($item['name'], 0, 1)][] = $item;
			}
		}

		$c = 0;
		$columns_count = 0;
		$items_per_column = ceil(count($items) / $columns);
		foreach ($letters as $letter => $data) {
			$columns_count += count($data);
			$result[$c][$letter] = $data;
			if ($columns_count > $items_per_column) {
			$columns_count = 0;
				$c++;
			}
		}

		return $result;
	}
}