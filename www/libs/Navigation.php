<?php
class Navigation
{
	private $_navigation = array();
	
	private $_page_type;
	private $_category;
	private $_auto_item;
	private $_item;
	private $_parse_url;

	public function __construct($item_title = '')
	{
		$this->_page_type = Environment::getPageType();
		$this->_parse_url = Environment::getParseUrl();
		
		/*if (!empty($this->_module) && $this->_module->getField('title') != Constants::MODULE_MAIN) {
			$this->_createElement(Constants::MODULE_MAIN_LABEL, '/');
			
			$this->_createModuleElement();
			$this->_createCategoryElement();
			$this->_createAutoItemElement();
			$this->_createItemElement($item_title);
		}*/
	}

	
	public function getNavigation() 
	{
		return $this->_navigation;
	}
	
	private function _createLeaf($label)
	{
		$this->_createElement($label, '');
	}
	
	private function _createElement($label, $url)
	{
		$this->_navigation[] = array(
			'label' => $label,
			'url' => $url,
		);
	}
}