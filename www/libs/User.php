<?php
class User
{
	const USER_TABLE = 'atrru.parts_accounts';

	
	private static $_unathorized_fields = array(
		'id' => 0,
		'email' => '',
		'password' => '',
		'type' => 0,
	);

	// @TODO проверить авторизацию, аутентификацию и корректное заполнение полей
	public $auth_message = '';

	private $_db;
	private $_login_status;
	private $_logged = false;
	private $session_id = '';
	private $user_logo = '';
	private $additional_fields = array();
	private $_data = array();
	
	private $_account;
	
	
	
	// проверить правильность получения session_id
	// доработать получение информации о пользователе, который уже авторизован
	public function __construct()
	{
		$this->_db = Environment::getDB('local');
		$this->session_id = Environment::getCookieVar('PHPSESSID');
		$this->_account = new Item_Account();
		
		$_logged = Environment::getSessionVar('logged');
		$email = Environment::getSessionVar('user_email');
		if (!empty($_logged) && !empty($email)) {
			$this->_authorize($email);
		}
	}
	
	public function login($email, $password) 
	{
		$this->_account = new Item_Account(array('email' => $email, 'password' => md5($password)), Environment::getDB('local'));

		if ($this->_account->isExist()) {
			$this->_fillAccountFields();
			$success = true;
		} else {
			$this->_fillAccountFields();
			$this->auth_message = "Unknown email";
			$success = false;
		}
		
		return $success;
	}
	
	public function logout() 
	{
		$this->_account = new Item_Account(array(), Environment::getDB('local'));
		$this->_fillAccountFields();
	}
	
	/* 
	 * check if user with this email exist then initialize him, in other case - create new user
	 * @param string $email
	 * @param string $password
	 * @return void
	 */
	public function createIfNotExist($email, $password)
	{
		// @TODO переделать если понадобиться
		/*$user = $this->_db->getUserByEmailPassword($email, $password);
		if (!empty($user)) {
			$this->_login_status = self::LOGIN_STATUS_SUCCESS;
		} else {
			$user = $this->_db->getUserByEmail($email);
			if (!empty($user)) {
				$this->_login_status = self::LOGIN_STATUS_USER_EXIST;
			} else {
				$new_account = new Item_Account();
				$data = array(
					'email' => $email,
					'new_pwd' => $password,
				);
				if ($new_account->add($data)) {
					$this->_login_status = self::LOGIN_STATUS_NEW_USER;
					$user = $new_account->getFields();
				}
			}
		}
		
		$this->_setFields($user);*/
	}
	
	private function _authorize($email)
	{
		$this->_account = new Item_Account(array('email' => $email), Environment::getDB('local'));
		$this->_fillAccountFields();
	}
	
	private function _fillAccountFields() 
	{
		if(!empty($this->_account) && $this->_account->isExist()) {
			$this->_setToSession(1, $this->_account->getId(), $this->_account->getField('email'), $this->_account->getField('role'));
		} else {
			$this->_setToSession('', '', '', '', '');
		}
	}
	
	private function _setToSession($logged, $user_id, $user_login, $role)
	{
		Environment::setSessionVar('logged', $logged);
		Environment::setSessionVar('user_id', $user_id);
		Environment::setSessionVar('user_email', $user_login);
		Environment::setSessionVar('role', $role);
	}
	
	public function getId() 
	{
		return !empty($this->_account) ? $this->_account->getId() : null;
	}
	
	public function getEmail() 
	{
		return !empty($this->_account) ? $this->_account->getField('email') : null;
	}
	
	public function getName() 
	{
		return !empty($this->_account) ? $this->_account->getField('email') : null;
	}
	
	public function getField($name) 
	{
		return !empty($this->_account) ? $this->_account->getField($name) : null;
	}
	
	public function getSessionId() 
	{
		return $this->session_id;
	}

	public function isAuthorized() 
	{
		return !empty($this->_account) && $this->_account->isExist();
	}
	
	public function isAdmin() 
	{
		return !empty($this->_account) ? $this->_account->getField('role') == Item_Account::ROLE_ADMIN : false;
	}
	
	public function getLoginStatus()
	{
		return !empty($this->_login_status) ? $this->_login_status : 0;
	}
	
	public function getFields()
	{
		return !empty($this->_account) ? $this->_account->getFields() : array();
	}
	
	public function edit($input) 
	{
		return $this->_account->edit($input);
	}
	
	public function changePassword($input) 
	{
		$success = false;

		if (!empty($input['new_password'])) {
			$input = array('password' => md5($input['new_password']));
			
			$success = $this->_account->edit($input, array('id' => $this->getId()));
		} else {
			$input = array();
		}
		
		 
		return $success;
	}
	
	//Дописать реализацию
	public function add($data) 
	{
		$success = $this->_account->add($data);
		
		return $success;
	}
	
	public function remindPassword($email) 
	{
		$this->_account = new Item_Account(array('email' => $email), Environment::getDB('local'));
		if (!$this->_account->isExist()) {
			return false;
		}

		return $this->_account->remindPassword();
	}

}