<?php
class PageContent
{//доработать
	public $title = 'Поиск запчастей он-лайн';
	public $meta_description = '';
	public $meta_keywords = '';
	public $navigation = array();
	public $seo_top_text = '';
	public $seo_bottom_text = '';
	public $page_type = '';
	private $_region;
	public $user;

	private $scripts = array();
	private $styles = array();
	private $_page_variables = array();
	private $_search_string;
	
	private $_parse_url;
	
	private $_paginator;
	private $_comments_paginator;
	
	public function __construct()
	{
		$this->_search_string = Environment::getParseUrl()->getSearchString();
		if (!empty($this->_search_string)) {
			$this->_search_string = '?' . $this->_search_string;
		}
		
		$this->_paginator = new Paginator();
		$this->_paginator->setSearchString($this->_search_string);
		
		$this->_parse_url = Environment::getParseUrl();
		
		$this->setContent(new PageHeader());
		$this->setNavigation(new Navigation(''));
	}
	
	public function setPagesFromValues($total_count, $per_page = 0)
	{
		$this->_paginator->setPagesFromValues($total_count, $per_page);
	}
	
	public function setCommentsPagesFromValues($total_count, $per_page = 0)
	{
		$this->_comments_paginator->setPagesFromValues($total_count, $per_page);
	}
	
	public function getCurrentPage()
	{
		return $this->_paginator->getCurrentPage();
	}
	
	public function getPaginator()
	{
		return $this->_paginator;
	}
	
	public function getParseUrl()
	{
		return $this->_parse_url;
	}
	
	public function getCommentsPaginator()
	{
		return $this->_comments_paginator;
	}
	
	public function haveNavigation() 
	{
		return !empty($this->navigation);
	}
	
	public function setNavigation(Navigation $navigation) 
	{
		$this->navigation = $navigation->getNavigation();
	}
	
	public function setContent(PageHeader $header) 
	{
		$this->setTitle($header->getTitle());
		$this->setMetaDescription($header->getMetaDescription());
		$this->setMetaKeywords($header->getMetaKeywords());
		$this->setPageType($header->getPageType());
		$this->setSeoTopText($header->getSeoTopText());
		$this->setSeoBottomText($header->getSeoBottomText());
	}
	
	public function setPageType($page_type) 
	{
		if (!empty($page_type)) {
			$this->page_type = $page_type;
		}
	}
	
	public function setUser($user) 
	{
		if (!empty($user)) {
			$this->user = $user;
		}
	}
	
	public function getRegion() 
	{
		return Environment::getRegion();
	}
	
	public function getConfigParam($param) 
	{
		return Config::getParam($param);
	}
	
	public function setItemTitle($title) 
	{
		if (!empty($title)) {
			$this->item_title = $title;
		}
	}
	
	public function getItemTitle()
	{
		return preg_replace('|([\\\\]+)|s', '', stripslashes($this->item_title));
	}
	
	public function setTitle($title)
	{
		if (!empty($title)) {
			$this->title = $title;
		}
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function setMetaKeywords($meta_keywords)
	{
		if (!empty($meta_keywords)) {
			$this->meta_keywords = $meta_keywords;
		}
	}
	
	public function getMetaKeywords()
	{
		return $this->meta_keywords;
	}
		
	public function setMetaDescription($meta_description)
	{
		if (!empty($meta_description)) {
			$this->meta_description = $meta_description;
		}
	}
	
	public function getMetaDescription()
	{
		return $this->meta_description;
	}
	
	public function setSeoTopText($seo_top_text)
	{
		if (!empty($seo_top_text)) {
			$this->seo_top_text = $seo_top_text;
		}
	}
	
	public function getSeoTopText()
	{
		return $this->seo_top_text;
	}
	
	public function setSeoBottomText($seo_bottom_text)
	{
		if (!empty($seo_bottom_text)) {
			$this->seo_bottom_text = $seo_bottom_text;
		}
	}
	
	public function getSeoBottomText()
	{
		return $this->seo_bottom_text;
	}
	
	
	public function addScripts($url) 
	{
		if(!in_array($url, $this->scripts)) {
			$this->scripts[] = $url;
		}
	}
	
	public function getScripts() 
	{
		return $this->scripts;
	}
	
	public function addStyles($url) 
	{
		if(!in_array($url, $this->styles)) {
			$this->styles[] = $url;
		}
	}
	
	public function getStyles() {
		return $this->styles;
	}
	
	public function setParam($name, $value) 
	{
		$this->_page_variables[$name] = $value;
	}
	
	public function getParam($name) 
	{
		return isset($this->_page_variables[$name]) ? $this->_page_variables[$name] : null;
	}

}