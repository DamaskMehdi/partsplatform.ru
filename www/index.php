<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

session_start();

try {
	ob_start();

	require('libs/includes.php');

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
		include('ajax/index.php');
	} else {

		$builder = new Builder();
		$builder->createStructure();
		$builder->buildTemplate();
		$builder->closeAll();
	}
	
	
	ob_end_flush();
} catch (Exception $exc) {d($exc);
	$log_name = date('ymd') . '_errors.txt';
	$logger = new Logger_FileLogger($log_name);
	$message = $exc->getMessage() . ', error code = ' . $exc->getCode();
	$logger->write($message);
	$logger->close();
}