ymaps.ready(MapInit);
function MapInit() {
	var location = [55.75399400,37.62209300];
	if($('#location').val()!='') location = $('#location').val().split(",");
	
	var myMap = new ymaps.Map('map', {center: location,zoom: 9});
	var myPlacemark = new ymaps.Placemark(location,{},{draggable: true,hideIconOnBalloonOpen: true});
	
	myMap.geoObjects.add(myPlacemark);
	myMap.controls.add('zoomControl', { top: 10, right: 10 });
	
	myPlacemark.events.add('dragend', function(e) {
	   var thisPlacemark = e.get('target');
	   var coords = thisPlacemark.geometry.getCoordinates();
	   $('#location').val(coords);
	});

	$('#ya-search').click(function () {
		
		myMap.geoObjects.remove(myPlacemark);
		var search_query = $('#country').val()+' '+$('#city').val()+' '+$('#street_type option:selected').text()+' '+$('#street').val()+' '+$('#house').val();
		ymaps.geocode(search_query, {results: 1}).then(function (res) {
			
			var firstGeoObject = res.geoObjects.get(0),
				coords = firstGeoObject.geometry.getCoordinates(),
				bounds = firstGeoObject.properties.get('boundedBy');
			
			$('#location').val(coords);									
			myMap.setBounds(bounds, {checkZoomRange: true});
			
			var myPlacemark2 = new ymaps.Placemark(coords, {
				 iconContent: '',
				 balloonContent: firstGeoObject.properties.get('text')
				 }, {
					draggable: true,
					hideIconOnBalloonOpen: true
				 });

			myMap.geoObjects.add(myPlacemark2);
			
			myPlacemark2.events.add('dragend', function(e) {
			   var thisPlacemark = e.get('target');
			   var coords = thisPlacemark.geometry.getCoordinates();
			   $('#location').val(coords);
			});
		});
		return false;
	});
}