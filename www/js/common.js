$(function(){
	$('.header__login a[href=\\/account\\/register]').click(function () {
		var $menu = $('<div>').attr('id', 'register-menu').hide().append(
			$('<a>').attr('href', '/account/register?type=buyer').html('<i class="fa fa-user fa-lg"></i> <span>Я покупатель<span>')
		).append(
			$('<a>').attr('href', '/account/register?type=seller').html('<i class="fa fa-briefcase fa-lg"></i> <span>Я продавец</span>')
		).mouseleave(function () {
			$(this).slideUp(200, function () {
				$(this).remove();
			});
		});
		$('main').append($menu);
		$menu.slideDown(200);
		return false;
	});

	$("button.add-to-cart").click(function(){
		$('<div id="fancybox-loading"><div></div></div>').appendTo("body");
		var ct = $(this).parent().find('input');
		$.getJSON('/',{action:'add_to_cart_fast',price_id:ct.attr('data-price-id'),shop_id:ct.attr('data-shop-id'),count:ct.val()},function(data){
			if(data.status == 1) $("#in_cart").text(Number($("#in_cart").text())+Number(ct.val()));
			$("#fancybox-loading").remove();
			$.fancybox.open(data.message);
		});
		return false;
	});
	
	$('.open-contacts').click(function() {
		var el = $(this);
	
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "POST",
			data: {af: '1', action: 'show_shop_contacts', shop_id: $(this).data('shop-id')},
			success: function(data) {
				if (data.success) {
					el.parent().html(data.data);
				}
			}
		});
	});


	$('#select_region').click(function() {
		$(this).parent().hide().next().show();

		return false;
	});

	$('#select-region').click(function () {
		var $menu = $('<div>').attr('id', 'region-menu').hide().append(
			$('<a>').attr('href', 'http://atr.ru/').html('<span>Москва</span>')
		).append(
			$('<a>').attr('href', 'http://sankt-peterburg.atr.ru/').html('<span>Санкт-Петербург</span>')
		).append(
			$('<a>').attr('href', 'http://vladivostok.atr.ru/').html('<span>Владивосток</span>')
		).append(
			$('<a>').attr('href', 'http://volgograd.atr.ru/').html('<span>Волгоград</span>')
		).append(
			$('<a>').attr('href', 'http://ekaterinburg.atr.ru/').html('<span>Екатеринбург</span>')
		).append(
			$('<a>').attr('href', 'http://kazan.atr.ru/').html('<span>Казань</span>')
		).append(
			$('<a>').attr('href', 'http://kaliningrad.atr.ru/').html('<span>Калининград</span>')
		).append(
			$('<a>').attr('href', 'http://krasnodar.atr.ru/').html('<span>Краснодар</span>')
		).append(
			$('<a>').attr('href', 'http://krasnoyarsk.atr.ru/').html('<span>Красноярск</span>')
		).append(
			$('<a>').attr('href', 'http://krim.atr.ru/').html('<span>Крым</span>')
		).append(
			$('<a>').attr('href', 'http://nizny-novgorod.atr.ru/').html('<span>Нижний Новгород</span>')
		).append(
			$('<a>').attr('href', 'http://novosibirsk.atr.ru/').html('<span>Новосибирск</span>')
		).append(
			$('<a>').attr('href', 'http://omsk.atr.ru/').html('<span>Омск</span>')
		).append(
			$('<a>').attr('href', 'http://perm.atr.ru/').html('<span>Пермь</span>')
		).append(
			$('<a>').attr('href', 'http://rostov-na-dony.atr.ru/').html('<span>Ростов-на-Дону</span>')
		).append(
			$('<a>').attr('href', 'http://samara.atr.ru/').html('<span>Самара</span>')
		).mouseleave(function () {
			$(this).slideUp(200, function () {
				$(this).remove();
			});
		});
		$('main').append($menu);
		$menu.slideDown(200);
		return false;
	})

	$('select.domain_region').change(function() {
		window.location.href = 'http://' + $(this).val() + window.location.pathname;
	});

	$('#logout_btn').click(function() {
		$.ajax({
				url: '/',
				type: 'get',
				dataType: 'json',
				data: { af : 'a', action: 'logout' },
				success: function(data) {
					if (data.success) {
						window.location.href = "/";
					} else {
						if (typeof data.message != undefined) {
							alert(data.message);
						} else {
							alert('Неверный логин или пароль!');
						}
					}
				}
			});

		return false;
	});


	$('#podbor_brand').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "POST",
			data: {podbor_shin: 1, brand: $(this).val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выберите </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#podbor_model').html(text);
			}
		});
	});

	$('#podbor_model').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "POST",
			data: {podbor_shin: 1, model: $(this).val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выберите </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#podbor_year').html(text);
			}
		});
	});

	$('#podbor_year').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "POST",
			data: {podbor_shin: 1, year: $(this).val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выберите </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#podbor_modification').html(text);
			}
		});
	});


	$('#podbor_modification').change(function() {
		if ($(this).val() != 0 && $(this).data('type') == 'main_page') {
			window.location.href = '/tires/podbor_shin' + $(this).val();
		}
	});

	$('#podbor_shin_submit').click(function() {
		if ($('#podbor_modification').val() != 0) {
			window.location.href = '/tires/podbor_shin' + $('#podbor_modification').val();
		} else {
			alert('Выберите модификацию');
		}
	});



	$('#podbor_akb-type').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "GET",
			data: {podbor_akb: 1, type: $(this).val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выберите </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#podbor_akb-mark').html(text);
			}
		});

		$('#podbor_akb-model,#podbor_akb-modification').html('<option value="0"> выберите </option>');
	});

	$('#podbor_akb-mark').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "GET",
			data: {podbor_akb: 1, mark: $(this).val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выберите </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#podbor_akb-model').html(text);
			}
		});

		$('#podbor_akb-modification').html('<option value="0"> выберите </option>');
	});

	$('#podbor_akb-model').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "GET",
			data: {podbor_akb: 1, model: $(this).val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выберите </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#podbor_akb-modification').html(text);
			}
		});
	});

	//$('#podbor_akb-modification').change(function() {
	$('#podbor_akb_submit').click(function() {
		if ($('#podbor_akb-modification').val() != 0) {
			window.location.href = '/accumulator?podbor_akb=' + $('#podbor_akb-modification').val();
		} else {
			alert('Выберите модификацию');
		}
	});


	$('#battery_form').submit(function() {
		if ($('#battery_form input.capacity:checked').length > 0) {
			var capacity = $('#battery_form input.capacity:checked');

			$('#battery_form input[name=capacity_from]').val(capacity.data('from'));
			$('#battery_form input[name=capacity_to]').val(capacity.data('to'));
		}

		return true;
	});



	$('#starter_form [name=mark]').change(function() {
		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "POST",
			data: {starter_models: 1, mark: $(this).val(), category_id: $('#starter_form [name=category]').val()},
			success: function(data) {
				var text = '<option value="0" selected="selected"> выбрать </option>';
				if (data != null) {
					for (k in data) {
						text += '<option value="' + data[k].id + '">' + data[k].name + '</option>';
					}
				}

				$('#starter_form [name=model]').html(text);
			}
		});
	});

	$('#starter_form [name=model]').change(function() {
		window.location.href = "/startery-i-generatory/" + $('#starter_form [name=category]').val() + "?model=" + $(this).val();
	});





	jQuery.fn.highlight = function (pat) {
		function innerHighlight(node, pat) {
			var skip = 0;
			pat = pat.trim().replace(/\s{2,}/g,' ');

			if (node.nodeType == 3) {
				var regexp = new RegExp("(" + pat.replace(new RegExp(' ', 'g'), "|") + ")", (false ? "" : "i") + "g");
				var str = jQuery('<div></div>').text(node.data).html();
				jQuery(node).replaceWith(str.replace(regexp, '<span class="highlight">$1</span>'));

			} else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
				for (var i = 0; i < node.childNodes.length; i++) {
					innerHighlight(node.childNodes[i], pat);
				}
			}
			return skip;
		}
		return this.each(function () {
			innerHighlight(this, pat.toUpperCase());
		});
	};

	if (!String.prototype.trim) {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g,'');
		}
	}

	jQuery.fn.removeHighlight = function () {
		return this.find("span.highlight").each(function () {
			this.parentNode.firstChild.nodeName;
			with(this.parentNode) {
				replaceChild(this.firstChild, this);
				normalize();
			}
		}).end();
	};




	$(document).keyup(function (e) {
		if (e.keyCode == 27) {
			hide_search();
		}
	});
	$("#x").click(function() {$("#article").val("");$(this).hide();});

	/*** Search parts ***/

	function hide_search() {
		if ($('#search_result_bg').is(':visible')) {
			$('#article').val('');
			$('#search_result_bg').fadeOut();
			$('body').removeClass('no_scroll');
			$('.header__main,.header__search form').removeClass('search_result');
		}
	}
	function split(val ) {
		return val.split(/,\s*/);
    }
    function extractLast(term) {
		return split(term).pop();
    }

	$('#search-by-parts').on('click', '.ui-menu-item', function() {
		var href = $(this).find('a').prop('href');
		window.location.href = href;
	});


	timeout_id = 0;

	$("#article").bind("keydown", function(event) {
		$('body').addClass('no_scroll');

		if (timeout_id != 0) {
			clearTimeout(timeout_id);
		}

		timeout_id = setTimeout(executeSearch, 1000);
	});

	function executeSearch(search_string) {
		var search_string = $("#article").val().trim().replace(/\s{2,}/g,' ');
		if (search_string === '') {
			return;
		}

		$.ajax({
			url: "/",
			dataType: "JSON",
			type: "POST",
			data: {
				af: 'a',
				action: 'search_part_sphinx',
				text: search_string,
			},
			success: function(data) {
				var result_view = '';
				if (data.success) {
					result_view = '<table class="tbl_search_result">'
					var prices = '', sellers = '', image = '';
					for (key in data.data) {
						if (data.data[key].price_usd_min != null && data.data[key].price_usd_min != '') {
							prices = 'от ' + data.data[key].price_usd_min + ' Р.';
						} else {
							prices = 'нет в продаже';
						}

						if (data.data[key].sellers) {
							sellers = '<a href="' + data.data[key].url + '" target="_blank">' + data.data[key].sellers + ' предложений' + '</a>';
						}
						else {
							sellers = '';
						}

						if (data.data[key].part_image != '') {
							image = '<img src="' + data.data[key].part_image + '">';
						} else {
							image = '';
						}

						result_view += '<tr><td valign="top">' + data.data[key].bl_part_brand + '</td>'
						+ '<td valign="top">' + data.data[key].bl_part_article + '</td><td>' +
						'<div class="description">' + image + '<a href="' + data.data[key].url + '" target="_blank">'
						+ data.data[key].bl_car_brand + ' ' + data.data[key].bl_car_model
						+ ' ' + data.data[key].bl_part_brand + ' ' + data.data[key].bl_part_article + '</a><div style="font-size: 15px;">' + data.data[key].bl_part_description + '</div></div>'
						+ '</td><td><div class="price"><big>' + prices + '</big></div>'
						+ '<div class="sellers">' + sellers + '</div></td></tr>'
					}
					result_view += '</table>';
				} else {
					result_view = '<div id="search_not_found">Ничего не найдено по запросу</div>';
				}

				if (!$('#search_result_bg').length) {
					$('body').append(
						$('<div>').attr('id', 'search_result_bg').append(
							$('<div>').attr('id', 'search_result_wrap').append(
								$('<div>').attr('id', 'search_result_close').html('<i class="fa fa-times"></i>')
							).append(
								$('<div>').attr('id', 'search_result')
							)
						).click(function (e) {
							if (!$(e.target).parents('#search_result').length) {
								hide_search();
							}
						})
					);
				}
				$('#search_result_bg').fadeIn();
				$('.header__main,.header__search form').addClass('search_result');
				$('#search_result').html(result_view);
			}
		});
	}

if ($("#article_").length > 0) {
	$("#article_").bind("keydown", function(event) {
		var last = $(this).val().length - 1;
		if ( event.keyCode === $.ui.keyCode.SPACE && $(this).val().charAt(last) == ' ' ) {
			event.preventDefault();
		}
		if ( event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete( "instance" ).menu.active ) {
			event.preventDefault();
        }
	}).bind("focus", function(event)
	{
		$(".ui-autocomplete").show();
	}).autocomplete({
		delay: 1000,
		minLength: 3,
		source: function(request, response) {
			$.ajax({
				url: "/",
				dataType: "JSON",
				type: "POST",
				data: { af: 'a', text: request.term.trim().replace(/\s{2,}/g,' '), action: 'search_part_sphinx'},
				success: function(data) {
					last_data = data;
					if (data.data == null || data.data.length == 0) {
						data.data = [({
						  value:0,
						  label:"Запрос недостаточно четкий,<br> попробуйте уточнить или изменить запрос",
						})];
					}
					response(data.data);
				}
			});
		},
		search: function() {
			// custom minLength
			var term = extractLast( this.value );
			var last = term.length - 1;
			//if(term.charAt(last) == ' ') return false;
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		}
		//open: function(event,ui){
		//}
		//select: function(event,ui){
		//}
	}).data("ui-autocomplete")._renderItem = function(ul,item) {
		if(typeof item.value != undefined && item.value == 0) {
			return $("<li class='ui-menu-item' role='presentation'>").append('<div class="image"><img src="/images/alert.png"/></div><div class="description" style="padding-top:7px;">'+item.label+'</div>').appendTo(ul);
		}
		else {
			$('body').addClass('no_scroll');

			var re = /(?=\B(?:\d{3})+(?!\d))/g
			var image = item.image_path != null ? item.image_path : 'none';
			var prices = item.price != null ? item.price.replace(re,' ')+'р.' : '';

			var html = '<div class="image">' +
					 '<a href="/catalog1/details/'+item.art_id+'" class="nobr" target="_blank">' +
					(image != 'none'
						? ('<img height="60" src="http://pix.atr-catalog.by/' + image + '"/>')
						: ('<img src="http://atr.ru/images/no-image.png"/>')
					) +
					 '</a>' +
					 '</div>' +
					 '<div class="description">' +
					 '<p>' +
					 '<a href="/catalog1/details/'+item.art_id+'" target="_blank">'+item.brand+' '+item.model+' ('+item.sup_brand+' '+item.ART_ARTICLE_NR+')</a>' +
					 '</p>' +
					 '<small>'+item.article+' <a href="/catalog1/details/'+item.art_id+'" target="_blank">подробнее...</a></small>' +
					 '<div class="price"><big><a href="/catalog1/details/'+item.art_id+'" class="nobr" target="_blank">'+prices+'</a></big><a href="/catalog1/search/'+item.ART_ARTICLE_NR.replace(/[^A-Za-z0-9]/g, "")+'" class="btn-price nobr" target="_blank">Показать все аналоги</a></div>' +
					 '</div>';

			return $('<li>')
			.append(html)
			.appendTo(ul)
			.highlight($('#article').val());
		}
	};
}

});