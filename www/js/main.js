jQuery.fn.highlight = function (pat) {
    function innerHighlight(node, pat) {
        var skip = 0;
		pat = pat.trim().replace(/\s{2,}/g,' ');

        if (node.nodeType == 3) {
            var regexp = new RegExp("(" + pat.replace(new RegExp(' ', 'g'), "|") + ")", (false ? "" : "i") + "g");
            var str = jQuery('<div></div>').text(node.data).html();
            jQuery(node).replaceWith(str.replace(regexp, '<span class="highlight">$1</span>'));

        } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; i++) {
                innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.each(function () {
        innerHighlight(this, pat.toUpperCase());
    });
};

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,'');
	}
}

jQuery.fn.removeHighlight = function () {
    return this.find("span.highlight").each(function () {
        this.parentNode.firstChild.nodeName;
        with(this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};

$(function(){
	
	$("button.add-to-cart").click(function(){
		$('<div id="fancybox-loading"><div></div></div>').appendTo("body");
		var ct = $(this).parent().find('input');
		$.getJSON('/',{action:'add_to_cart_fast',price_id:ct.attr('data-price-id'),shop_id:ct.attr('data-shop-id'),count:ct.val()},function(data){
			if(data.status == 1) $("#in_cart").text(Number($("#in_cart").text())+Number(ct.val()));
			$("#fancybox-loading").remove();
			$.fancybox.open(data.message);
		});
		return false;
	});

	
	
	
	
	
	/*$('#sort_filter').change(function() {
		$('#filter_form #sort').val($(this).val());
		$('#filter_form').submit();
		
		return false;
	});*/

	/*
	$('#filter_form select').change(function () {
		$('#filter_form').submit();
	});
	*/
	$('.show_page_text').click(function() {
		if ($(this).text() == 'развернуть') {
			$('.page_text_short').hide();
			$('.page_text_full').show();
		} else {
			$('.page_text_full').hide();
			$('.page_text_short').show();
		}
		console.log($(this).text());
		return false;
	});
	
	
	

		
	$("#find").keyup(function(){if($(this).val()!='') $('#findbutton').removeClass('butsubmit').addClass('butsubmitact').attr('disabled',false); else $('#findbutton').removeClass('butsubmitact').addClass('butsubmit').attr('disabled',true);});
	$('.fancybox,.various').fancybox();
	$('.fancybox-print').fancybox({
		afterShow: function(){
			var win=null;
			var content = $('.fancybox-inner');
			var title = $('.fancybox-title .child')
			$('.fancybox-wrap')
			.append('<div id="fancy_print"></div>')
			.on("click", "#fancy_print", function(){
			  win = window.open("width=200,height=200");
			  self.focus();
			  win.document.open();
			  win.document.write('<html><head><style>');
			  win.document.write('body,td{font-family:Verdana;font-size:10pt;text-align:center;line-height:1.5em;} div{margin:20px 0;}');
			  win.document.write('</style></head><body>');
			  win.document.write('<h1>'+title.text()+'</h1>');
			  win.document.write('<div style="margin:20px 0;">'+content.html()+'</div>');
			  win.document.write('<div><img src="/images/phones_3.png"></div>');
			  win.document.write('</body></html>');
			  win.document.close();
			  win.print();
			  win.close();
			}); // on
		} //  afterShow 
	});

	/*var $menu = $("#second_menu"); 
	var limit = $menu.offset().top
	$(window).scroll(function(){
		if ( $(this).scrollTop()>limit) {
			$menu.css({"top":$(this).scrollTop(),"opacity":"0.9"});
		}
		else if($(this).scrollTop()<=limit){
			$menu.css({"top":limit,"opacity":"1"});
		}
	});
*/
	$('.ajaxForm').ajaxForm({ 
        target:        '#result',   
        beforeSubmit:  beforeSubmit, 
        success:       success 
	});
	
	/*** External Links ***/
	$("a[data-link]").click(function(){window.open($(this).attr("data-link"));return false;});
	$("tr[data-link]").click(function(){window.location.href=$(this).attr("data-link");});
	
	/*** Scroll To ***/
	$("a.scrollto,.scrollto").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });
	
	/*** Tabs ***/
	$('dl.tabs dt,dl.tabs2 dt').click(function(){
		$(this).siblings().removeClass('selected').end().next('dd').andSelf().addClass('selected');
	});

	function decimalAdjust(type, value, exp) {
		if (typeof exp === 'undefined' || +exp === 0) {
			return Math[type](value);
		}
		value = +value;
		exp = +exp;
		if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
			return NaN;
		}
		value = value.toString().split('e');
		value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
		value = value.toString().split('e');
		return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	}

	if (!Math.round10) {
		Math.round10 = function(value, exp) {
			return decimalAdjust('round', value, exp);
		};
	}
	if (!Math.floor10) {
		Math.floor10 = function(value, exp) {
			return decimalAdjust('floor', value, exp);
		};
	}
	if (!Math.ceil10) {
		Math.ceil10 = function(value, exp) {
			return decimalAdjust('ceil', value, exp);
		};
	}
	
	$("#article").keyup(function() {$("#x").fadeIn();if ($.trim($("#article").val()) == "") {$("#x").fadeOut();}});
	$("#x").click(function() {$("#article").val("");$(this).hide();});
	
	/*** Search parts ***/
	
	function split(val ) {
		return val.split(/,\s*/);
    }
    function extractLast(term) {
		return split(term).pop();
    }
	/*
	$("#article").bind("keydown", function(event) {
		var last = $(this).val().length - 1;
		if ( event.keyCode === $.ui.keyCode.SPACE && $(this).val().charAt(last) == ' ' ) {
			event.preventDefault();
		}
		if ( event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete( "instance" ).menu.active ) {
			event.preventDefault();
        }
	}).bind("focus", function(event)
	{
		$(".ui-autocomplete").show();
	}).autocomplete({
		delay: 1000,
		appendTo:"#search-by-parts",
		minLength: 3,
		source: function(request, response) {
			$.ajax({
				url: "/",
				dataType: "JSON",
				type: "POST",
				data: {text: request.term.trim().replace(/\s{2,}/g,' '),action: 'search_part'},
				success: function(data) {
					last_data = data;
					if (data.data == null || data.data.length == 0) {
						data.data = [({
						  value:0,
						  label:"Запрос недостаточно четкий,<br> попробуйте уточнить или изменить запрос",
						})];
					}
					response(data.data);
				}
			});
		},
		search: function() {
			// custom minLength
			var term = extractLast( this.value );
			var last = term.length - 1;
			//if(term.charAt(last) == ' ') return false;
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		}
		//open: function(event,ui){
		//}
		//select: function(event,ui){
		//}	
	}).data("ui-autocomplete")._renderItem = function(ul,item) {
		if(isset(item.value) && item.value == 0) {
			return $("<li class='ui-menu-item' role='presentation'>").append('<div class="image"><img src="/images/alert.png"/></div><div class="description" style="padding-top:7px;">'+item.label+'</div>').appendTo(ul);
		}
		else {
			var image = item.image_path != null ? item.image_path : 'none';
			var re = /(?=\B(?:\d{3})+(?!\d))/g
			if(item.price.length) item.price = item.price+'р.';
			var html = '<div class="image">' +
					 '<a href="/details/'+item.art_id+'" target="_blank">' +
						'<img src="http://autodbase.ru/325_2014_1q_intercrops56/' + image + '"/>' +
					 '</a>' +
					 '</div>' +
					 '<div class="description">' +
					 '<p>' +
					 '<a href="/details/'+item.art_id+'" target="_blank">'+item.brand+' '+item.model+' ('+item.sup_brand+' '+item.ART_ARTICLE_NR+')</a>' +
					 '</p>' +
					 '<small>'+item.article+' <a href="/details/'+item.art_id+'" target="_blank">подробнее...</a></small>' +
					 '<div class="price"><big><a href="/details/'+item.art_id+'" target="_blank">'+item.price.replace(re,' ')+'</a></big><a href="/search/'+item.ART_ARTICLE_NR.replace(/[^A-Za-z0-9]/g, "")+'" class="btn-price" target="_blank">Показать все аналоги</a></div>' +
					 '</div>';
			return $("<li>")
			.append(html)
			.appendTo(ul).highlight($('#article').val());
		}
	};*/
});

$(document).ready(function(){
	/*** Scroll Page ***/
	//$("#topvk").css('display','none');$(function(){$('#topvk').click(function(){$('html, body').stop().animate({scrollTop:$("body").offset().top},0)})});if(document.referrer==''){$("#topvk").hide()}else{$("#topvk").show();$('#topvk nobr').text('Назад').removeAttr('class').attr('class','back');$('#topvk').attr('onclick','window.location=\''+document.referrer+'\'')}
	
	/*** ToolTip ***/
	Tipped.create(".tip", {skin: 'yellow',shadow:false});
	Tipped.create(".tip-white", {skin: "light",hook:"bottommiddle"});
	Tipped.create(".search-vin","vin_search_form",{inline:true,skin:'white',hook:'bottommiddle',shadow:{blur:4,color:'#dddddd',offset:{x:0,y:0},opacity:.9}});
	
	$('.first-menu').mouseover(function(){Tipped.create(".first-menu",'sub_'+$(this).attr('data-id'),{inline:true,skin:'white',hook:'bottommiddle',shadow:{blur:4,color:'#dddddd',offset:{x:0,y:0},opacity:.9}});});
	
	/*** Cart ***/
	addToCart();
	
	/*** Comment ***/
	$("#add-comment").click(function(){
		if($("#comment_text").val() == '') alert('Поле отзыва не содержит текста!');
		else {
			$.ajax({url:'/',type:'POST',data:{action:'add_comment',article_id:$("#article_id").val(),article:$("#article_name").val(),brand:$("#brand_name").val(),comment_text:$("#comment_text").val(),mark:$(".rating:checked").val()},dataType:'json',success:function(data) {
				if(data.status == 1){
					if($("table.comments tr").is(".no-comment")) $(".no-comment").empty();
					$("table.comments").append(data.html);
				}
				$.fancybox.open(data.message);
			}});
		} return false;
	});
	
	/*** Tables ***/
	//$(".first-level").click(function(){$(".second-level").hide();$("."+$(this).attr("id")).show();});
	$(".second-level").click(function(){document.location=$(this).find("a").attr("href");});
	$("#findbutton").click(function(){if($(".search-form__text").val()==''){alert("Не указан номер запчасти для поиска!"); return false;}});
	
	var _timer = null;
	$(".mews__main_block li").hover(function(){	
		var block = $(this).children('div')
		if(block.is(":hidden")) {
			_timer = setTimeout(function(){
				$(".mews__main_block li div").slideUp(); 
				block.slideDown();
			}, 200);
		}
	}, function(){clearTimeout(_timer);});
});

//jQuery(window).scroll(function(){if(jQuery(window).scrollTop()>100){$("#topvk").show();$('#topvk nobr').text('Наверх').removeAttr('class');$('#topvk').removeAttr('onclick')}else{if(document.referrer==''){$("#topvk").hide()}else{$("#topvk").show();$('#topvk nobr').text('Назад').removeAttr('class').attr('class','back');$('#topvk').attr('onclick','window.location=\''+document.referrer+'\'')}}});document.write('<div id="topvk"><nobr id="stl_text">Наверх</nobr></div>');

function isset(variable)
{
	return(typeof(variable) != 'undefined');
}

function addToCart() {
	$("button.add-to-cart").click(function(){
		$('<div id="fancybox-loading"><div></div></div>').appendTo("body");
		var ct = $(this).parent().find('input');
		$.getJSON('/',{action:'add_to_cart_fast',price_id:ct.attr('data-price-id'),shop_id:ct.attr('data-shop-id'),count:ct.val()},function(data){
			if(data.status == 1) $("#in_cart").text(Number($("#in_cart").text())+Number(ct.val()));
			$("#fancybox-loading").remove();
			$.fancybox.open(data.message);
		});
		return false;
	});
	
	$("button.add-to-cart-other").click(function(){
		$('<div id="fancybox-loading"><div></div></div>').appendTo("body");
		var ct = $(this).parent().find('input');
		$.getJSON('/',{action:'add_to_cart_other',price_id:ct.attr('data-price-id'),shop_id:ct.attr('data-shop-id'),count:ct.val(),article:ct.attr('data-article'),brand:ct.attr('data-brand'),price:ct.attr('data-price')},function(data){
			if(data.status == 1) $("#in_cart").text(Number($("#in_cart").text())+Number(ct.val()));
			$("#fancybox-loading").remove();
			$.fancybox.open(data.message);
		});
		return false;
	});
	
	$("button.add-to-cart-zakaz").click(function(){
		var ct = $(this).parent().find('input');
		$.getJSON('/',{action:'add_to_cart_zakaz',price:ct.attr('data-price'),shop_id:ct.attr('data-shop-id'),supplier_id:ct.attr('data-supplier-id'),brand:ct.attr('data-brand'),article:ct.attr('data-article'),count:ct.val()},function(data){
			if(data.status == 1) $("#in_cart").text(Number($("#in_cart").text())+Number(ct.val()));
			$.fancybox.open(data.message);
		});
		return false;
	});
}

function beforeSubmit(){
	$(".required").each(function(){
		if($(this).val() == '') {
			alert('Проверьте правильность заполнения формы!');
			return false;
		}
	});
}

function success(){
	$("#result").fadeIn(300);
	setTimeout(function(){$("#result").fadeOut(300);},6000);
}