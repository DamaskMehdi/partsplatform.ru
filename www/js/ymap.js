$(function() {
	$('body').on('click', '.balloon-order-link', function () {
		$('.ymaps-2-1-34-button__icon_icon_collapse').click();
		$('tr[data-company_id=' + $(this).data('company-id') + '] .show_company_zakaz').click();
	});

	ymaps.ready(function () {
		var map = new ymaps.Map('details-map', {
			center:	[55.76, 37.64],
			zoom:	9
		});

		var om = new ymaps.ObjectManager({
			clusterize:	true,
			gridSize:	32
		});
		om.objects.options.set('preset', 'islands#blueDotIcon');
		om.clusters.options.set('preset', 'islands#blueClusterIcons');
		map.geoObjects.add(om);

		shops_location.forEach(function (shop) {
			om.add({
				id:			shop.id,
				type:		'Feature',
				geometry:	{
					type:			'Point',
					coordinates:	shop.location.split(',')
				},
				properties:	{
					hintContent:	shop.name,
					balloonContent:	'<div class="balloon">' +
						'<strong class="balloon-company-name">' + shop.name + '</strong>' +
						'<div class="balloon-cost red h2"><b>' + shop.cost + '</b> <small>р.</small></div>' +
						'<a href="javascript:;" class="balloon-order-link" data-company-id="' + shop.id + '">Заказать</a>' +
						'</div>'
				}
			});
		});
	});
});
