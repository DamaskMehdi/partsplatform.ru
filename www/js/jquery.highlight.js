jQuery.fn.highlight = function (pat) {
    function innerHighlight(node, pat) {
        var skip = 0;

        if (node.nodeType == 3) {
            var regexp = new RegExp("(" + pat.replace(new RegExp(' ', 'g'), "|") + ")", (false ? "" : "i") + "g");
            var str = jQuery('<div></div>').text(node.data).html();
            jQuery(node).replaceWith(str.replace(regexp, '<span class="highlight">$1</span>'));

        } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; i++) {
                innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.each(function () {
        innerHighlight(this, pat.toUpperCase());
    });
};

jQuery.fn.removeHighlight = function () {
    return this.find("span.highlight").each(function () {
        this.parentNode.firstChild.nodeName;
        with(this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};
