var ymap = {
	myMap: null,
	marker: null,
	key: '',
	lat: $('#lat'),
	lng: $('#lng'),
	zoom: $('#zoom'),
	undodesc: $('#undomapdesc'),
	isauto: true,
	init: function () {
		ymap.opts = {
			center: [ymap.lat.val(), ymap.lng.val()],
			zoom: ymap.zoomFromPercent(ymap.zoom.val()),
			controls: ['zoomControl'],
			updateCoordinates: function (aCoords) {
				ymap.lat.val(parseFloat(aCoords[0]).toFixed(7));
				ymap.lng.val(parseFloat(aCoords[1]).toFixed(7));
				$('#location').val(ymap.lat.val()+','+ymap.lng.val());
			},
			updateZoom: function (zoom) {
				ymap.zoom.val(ymap.zoomToPercent(zoom));
			}
		}
		ymap.myMap = new ymaps.Map('map', ymap.opts);
		ymap.marker = new ymaps.Placemark([ymap.lat.val(), ymap.lng.val()], {}, {draggable: true});
		ymap.myMap.geoObjects.add(ymap.marker);
		ymap.marker.events.add('dragend', function () {
			ymap.undo(true)
			var coords = ymap.marker.geometry.getCoordinates();
			ymap.opts.updateCoordinates(coords);
		});
		ymap.myMap.events.add('boundschange', function (e) {
			if (e.get('newZoom') != e.get('oldZoom')) {
				ymap.opts.updateZoom(e.get('newZoom'));
			}
		});
		ymap.myMap.events.add('click', function(e) {
			var pos = e.get('coords');
			ymap.marker.geometry.setCoordinates(pos);
			ymap.opts.updateCoordinates(pos);
		});
		ymap.undodesc.find('a').click(ymap.search);
		ymap.undo(false);
		$('#country, #street, #city, #house, #building').change(ymap.search);
		$('.the-map').show();
	},

	undo: function (flag) {
		if (flag) {
			ymap.undodesc.show();
			ymap.isauto = false;
		} else {
			ymap.undodesc.hide();
			ymap.isauto = true;
		}
	},
	zoomToPercent: function (zoomValue) {
		return parseInt(zoomValue / 17 * 100);
	},
	zoomFromPercent: function (zoomPerc) {
		return parseInt(zoomPerc / 100 * 17);
	},
	search: function () {
		var address =  [$('#country').val(), $('#city').val(), $('#street').val(), $('#house').val(), $('#building').val() ].join(', ');
		ymaps.geocode(address, {
			results: 1
		}).then(function (res) {
			var firstGeoObject = res.geoObjects.get(0),
					coords = firstGeoObject.geometry.getCoordinates();
			ymap.undo(false)
			ymap.marker.geometry.setCoordinates(coords);
			ymap.opts.updateCoordinates(coords);
			ymap.myMap.setCenter(coords, 16, {
				checkZoomRange: true,
				duration: 1000
			});
		});
		return false;
	}
}
$(function () {
	ymaps.ready(ymap.init);
})

