/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `parts_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '0',
  `pwd` varchar(50) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 - platform, 2 - shop',
  `wallet_address` varchar(50) NOT NULL DEFAULT '0',
  `balance` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `parts_accounts` DISABLE KEYS */;
INSERT INTO `parts_accounts` (`id`, `email`, `pwd`, `type`, `wallet_address`, `balance`) VALUES
	(1, 'shop@mail.ru', '698d51a19d8a121ce581499d7b701668', 2, '', 196),
	(2, 'platform@mail.ru', '698d51a19d8a121ce581499d7b701668', 1, '', 7.26),
	(3, 'shop2@mail.ru', '698d51a19d8a121ce581499d7b701668', 2, '', 191.9);
/*!40000 ALTER TABLE `parts_accounts` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `parts_platforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `platform_url` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `parts_platforms` DISABLE KEYS */;
INSERT INTO `parts_platforms` (`id`, `account_id`, `platform_url`) VALUES
	(1, 2, 'http://ito.by');
/*!40000 ALTER TABLE `parts_platforms` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `parts_shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '0',
  `click_price` float NOT NULL DEFAULT '0',
  `phones` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `parts_shops` DISABLE KEYS */;
INSERT INTO `parts_shops` (`id`, `account_id`, `name`, `click_price`, `phones`) VALUES
	(1, 1, 'Shop1', 2, '+7 (495) 333 4444 '),
	(2, 3, 'Shop2', 1.8, '+7 (495) 553 4408');
/*!40000 ALTER TABLE `parts_shops` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `parts_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_bc_id` varchar(100) NOT NULL,
  `shop_id_from` int(11) NOT NULL,
  `platform_id_to` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL,
  `date_create` datetime NOT NULL,
  `ip_client` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `parts_transactions` DISABLE KEYS */;
INSERT INTO `parts_transactions` (`id`, `transaction_bc_id`, `shop_id_from`, `platform_id_to`, `description`, `amount`, `date_create`, `ip_client`) VALUES
	(1, '', 1, 1, 'Списание за клик', 2, '2017-09-10 12:11:28', '134.17.24.127'),
	(2, '', 2, 1, 'Списание за клик', 1.5, '2017-09-10 12:19:52', '134.17.24.127'),
	(3, '', 2, 1, 'Списание за клик', 1.5, '2017-09-10 12:20:43', '134.17.24.127'),
	(4, '', 1, 1, 'Списание за клик', 2, '2017-09-10 12:23:08', '134.17.24.127'),
	(5, '', 2, 1, 'Списание за клик', 1.5, '2017-09-10 12:26:10', '134.17.24.127'),
	(6, '', 2, 1, 'Списание за клик', 1.8, '2017-09-10 13:32:53', '134.17.24.127'),
	(7, '', 2, 1, 'Списание за клик', 1.8, '2017-09-27 19:24:22', '93.84.16.70');
/*!40000 ALTER TABLE `parts_transactions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
